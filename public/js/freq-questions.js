
document.addEventListener('DOMContentLoaded', () => {
    
    Get(`get-preguntas-frecuentes/${ APP_LANG }`, response => {
        let preguntasHtml = '';
        let respuestasHtml = '';
        const preguntaContainer = document.querySelector('#preguntas-container');
        const respuestasContainer = document.querySelector('#respuestas-container');
        response.forEach( ( item, i )  => {
            const { id, pregunta, respuesta } = item;
            preguntasHtml += `<a class="collapse-link-item ${ i == 0 ? 'collapse-link-active' : '' }" 
            data-toggle="collapse" 
            href="#question${ id }" 
            data-id="question${ id }" 
            role="button" 
            aria-expanded="false" 
            aria-controls="question${ id }">
                ${ pregunta }
            </a>`;
            respuestasHtml += `<div class="collapse collapse-box" id="question${ id }" data-show="${ i == 0 ? 'true' : 'false' }">
            <strong>${ pregunta }</strong>
                <p>${ respuesta }</p>
            </div>`;
        });
        preguntaContainer.innerHTML = preguntasHtml;
        respuestasContainer.innerHTML = respuestasHtml;
        setCollapsAndBoxes();
    });

});
