$(document).ready(function () {

    
    $("#agregar").on( "click", function() {
          
          var producto_id = $("#producto").val();
          var cantidad = $("#cantidad").val();
          var total = $("#total").val();
          var precio_unitario = $("#precio_unitario").val();
          var nombre_producto = $("option:selected", "#producto").data("producto_nombre");
          var contador = parseFloat($("#numero_productos").val()) + 1;

          total = parseFloat(total).toFixed(2);
          precio_unitario = parseFloat(precio_unitario).toFixed(2);
          
     
          if(producto_id == null || cantidad == "" || total == "" || precio_unitario == ""){
            
            Swal.fire(
							'¡Datos invalidos!',
              'Completa los campos para agregar un registro.',
              'error'
            )

          }else{
            
            if(abonos){

              Swal.fire(
                '¡No permitido!',
                'No puedes agregar o eliminar productos a esta venta para evitar descuadre o negativos en los saldos porque ya contiene una cuenta por cobrar con abonos. Eliminar los abonos si deseas modificar el detalle de la venta',
                'error'
              )

            }else
            {

              var fila = `
                        <div class="row fila-`+contador+`">
                          <div class="col-sm-12">
                              <div class="row">
                                <div class="col-sm-4">
                                  <div class="form-group">
                                    <input type="text"  id="producto-nombre-`+contador+`" readonly value="`+nombre_producto+`"  maxlength="64"   class="form-control" placeholder="" >
                                    <input type="hidden" id="producto_`+contador+`" name="producto_`+contador+`" value="`+producto_id+`">
                                    </div>
                                </div>
                                <div class="col-sm-2">
                                  <div class="form-group">
                                    <input type="text" style="text-align: right" readonly name="cantidad_`+contador+`" id="cantidad_`+contador+`" value="`+cantidad+`" maxlength="64"   class="form-control" placeholder="Ingrese una cantidad" >
                                  </div>
                                </div>
                                <div class="col-sm-2">
                                  <div class="input-group">
                                    <div class="input-group-prepend">
                                      <div class="input-group-text">$</div>
                                    </div>
                                    <input type="text" style="text-align: right"  readonly maxlength="7" name="precio_unitario_`+contador+`" id="precion_unitario_`+contador+`" value="`+precio_unitario+`" maxlength="64"   class="form-control numerico" placeholder="Ingrese precio unitario $0.00" >
                                  </div>
                                </div>
                                <div class="col-sm-3">
                                  <div class="input-group">
                                    <div class="input-group-prepend">
                                      <div class="input-group-text">$</div>
                                    </div>
                                    <input type="text" style="text-align: right" readonly name="total_`+contador+`" id="total_`+contador+`" value="`+total+`" maxlength="64"   class="form-control" placeholder="Ingrese una total $0.00" >
                                  </div>
                                </div>
                                <div class="col-sm-1">
                                  <button style="margin-top: 0px" id="eliminar" type="button"  data-contador="`+contador+`" class="btn btn-lg btn-danger right eliminar-fila"><i class="fas fa-trash-alt"></i></button>
                                </div>
                              </div>
                          </div>
                        </div>`;

              $(".lista-productos").append(fila);
              $("#cantidad").val("");
              $("#total").val("");
              $("#precio_unitario").val("");
              $('#producto').val('ninguno').trigger('change'); // Select the option with a value of '1'
              $("#numero_productos").val(contador);
              //$("#producto option[value='"+producto_id+"']").remove();


              var productos_cantidad = parseFloat($("#productos_cantidad").val()) + parseFloat(cantidad);
              $("#productos_cantidad").val(productos_cantidad);

              var productos_total = parseFloat($("#productos_total").val()) + parseFloat(total);
              $("#productos_total").val(productos_total.toFixed(2));
              calcular_venta_neta();
            }
          }

    }); 


    $(document).on('click', '.eliminar-fila', function(event) {

        var contador = $(this).data("contador");
        var numero_productos = $("#numero_productos").val();
        var producto_id = $("#producto_"+contador).val();

        var data = {
            id: producto_id,
            text: $("#producto-nombre-"+contador).val(),
        };

        var cantidad = $("#productos_cantidad").val();
        var total = $("#productos_total").val();
        

        var productos_cantidad = parseFloat(cantidad) - parseFloat($("#cantidad_"+contador).val());
        var productos_total = parseFloat(total) - parseFloat($("#total_"+contador).val());


        var gastos =  $("#gastos_total").val();

            if(gastos > productos_total){

              Swal.fire(
							'¡Datos inválidos!',
              'El total de la venta no puede ser inferior al total de gastos.',
              'error'
            )

        }else{

           
          if(abonos){

            Swal.fire(
              '¡No permitido!',
              'No puedes agregar o eliminar productos a esta venta para evitar descuadre o negativos en los saldos porque ya contiene una cuenta por cobrar con abonos. Eliminar los abonos si deseas modificar el detalle de la venta',
              'error'
            )

          }else
          {

                $("#productos_total").val(productos_total.toFixed(2));
                $("#productos_cantidad").val(productos_cantidad);


                var newOption = new Option(data.text, data.id, false, false);
              /// $('#producto').append(newOption).trigger('change');
              // $("#producto option[value='"+producto_id+"']").attr("data-producto_nombre", $("#producto-nombre-"+contador).val());
                $(".fila-"+contador).remove();
                calcular_venta_neta();
          }

        }

        
       
    }); 









      
    $("#agregar-gasto").on( "click", function() {
          
          var gasto_id = $("#gasto").val();
          var total = $("#gasto-total-fila").val();
          var nombre_gasto = $("option:selected", "#gasto").data("gasto_nombre");
          var contador = parseFloat($("#numero_gastos").val()) + 1;

          total = parseFloat(total).toFixed(2);
          
          
     
          if(gasto_id == null ||  total == ""){
            
            
            Swal.fire(
							'¡Datos invalidos!',
              'Completa los campos para agregar un registro.',
              'error'
            )

          }else{

            var gastos =  $("#gastos_total").val();
            var productos_total = $("#productos_total").val();
            gastos = parseFloat(gastos) + parseFloat(total);

            if(gastos > parseFloat(productos_total)){

              Swal.fire(
							'¡Datos inválidos!',
              'El total de gastos no puede superar el total de la venta.',
              'error'
            )

            }else{



                var fila = `
                          <div class="row gasto-fila-`+contador+`">
                            <div class="col-sm-12">
                                <div class="row">
                                  <div class="col-sm-8">
                                    <div class="form-group">
                                      <input type="text"  id="gasto-nombre-`+contador+`" readonly value="`+nombre_gasto+`"  maxlength="64"   class="form-control" placeholder="" >
                                      <input type="hidden" id="gasto_`+contador+`" name="gasto_`+contador+`" value="`+gasto_id+`">
                                      </div>
                                  </div>
                                  <div class="col-sm-3">
                                    <div class="input-group">
                                      <div class="input-group-prepend">
                                        <div class="input-group-text">$</div>
                                      </div>
                                      <input type="text" style="text-align: right" readonly name="gasto_total_`+contador+`" id="gasto_total_`+contador+`" value="`+total+`" maxlength="64"   class="form-control" placeholder="Ingrese una total $0.00" >
                                    </div>
                                  </div>
                                  <div class="col-sm-1">
                                    <button style="margin-top: 0px" id="eliminar" type="button"  data-contador="`+contador+`" class="btn btn-lg btn-danger right eliminar-fila-gasto"><i class="fas fa-trash-alt"></i></button>
                                  </div>
                                </div>
                            </div>
                          </div>`;

                  $(".lista-gastos").append(fila);
            
                  $("#gasto-total-fila").val("");
                  $('#gasto').val('ninguno').trigger('change'); // Select the option with a value of '1'
                  $("#numero_gastos").val(contador);
                  $("#gasto option[value='"+gasto_id+"']").remove();

              

                  var gastos_total = parseFloat($("#gastos_total").val()) + parseFloat(total);
                  $("#gastos_total").val(gastos_total.toFixed(2));
                  calcular_venta_neta();
            }
          }

    }); 


   


    $(document).on('click', '.eliminar-fila-gasto', function(event) {

        var contador = $(this).data("contador");
        var numero_gastos = $("#numero_gastos").val();
        var gasto_id = $("#gasto_"+contador).val();

        var data = {
            id: gasto_id,
            text: $("#gasto-nombre-"+contador).val(),
        };

 
        var total = $("#gastos_total").val();
        

    

          var gastos_total = parseFloat(total) - parseFloat($("#gasto_total_"+contador).val());
          $("#gastos_total").val(gastos_total.toFixed(2));

        var newOption = new Option(data.text, data.id, false, false);
        $('#gasto').append(newOption).trigger('change');
        $("#gasto option[value='"+gasto_id+"']").attr("data-gasto_nombre", $("#gasto-nombre-"+contador).val());
        $(".gasto-fila-"+contador).remove();
        calcular_venta_neta();
       
    });

    $(document).on('click', '#credito', function(event) {

      if($("#credito").prop('checked') ){
         calcular_venta_neta();
      }
      if($(".fila-credito").hasClass("hide")){
        $(".fila-credito").removeClass("hide");
      }else{

        $(".fila-credito").addClass("hide");
      }

    });

    $(document).on('blur', '#monto_deuda', function(event) {

      if($("#credito").prop('checked') ){
         calcular_venta_neta();
      }


    }); 

    $(document).on('keyup', '#monto_deuda', function(event) {

      if($("#credito").prop('checked') ){
        
        calcular_venta_neta();
     }


   }); 

   $(document).on('keyup blur', '.precio_unitario', function(event) {
      
      precio_unitario_suma();
    
    }); 


   function precio_unitario_suma(){

      var precio_unitario = $("#precio_unitario").val();
      var cantidad = $("#cantidad").val();

      if(precio_unitario == ""){ precio_unitario = 0;}
      if(cantidad == ""){ cantidad = 0;}
      var subtotal = parseFloat(precio_unitario) * parseFloat(cantidad);

      $("#total").val(subtotal.toFixed(2));

   }

    function calcular_venta_neta() {

      var venta = $("#productos_total").val();
      var gastos = $("#gastos_total").val();

      var total = parseFloat(venta) - parseFloat(gastos);
      $("#venta_neta").val(total.toFixed(2));

      if($("#credito").prop('checked') ){

        var monto_deuda =  $("#monto_deuda").val();
        if(monto_deuda == ""){
          monto_deuda = 0;
        }
        if(monto_deuda > total){

          Swal.fire(
							'¡Datos inválidos!',
              'El monto adeudado no puede ser mayor la total de la venta. Modifica el monto adeudado.',
              'error'
            ).then((result) => {
              if (result.isConfirmed) {
                $("#monto_deuda").focus();

              }
            })

         

        }else{

          var monto_pagado = parseFloat(total) - parseFloat(monto_deuda);
          $("#monto_pagado").val(monto_pagado.toFixed(2));

        }

       
        

      }

    
    }


    $( "#guardar" ).on( "click", function( event) {
        
        if(jQuery('#venta').get(0).checkValidity()){
              event.preventDefault();

            if($("#productos_total").val() == 0){

              Swal.fire(
                  '¡Datos inválidos!',
                  'La venta no puede ser registrada a cero.',
                  'error'
                ).then((result) => {
                  if (result.isConfirmed) {
                    $("#cantidad").focus();

                  }
                })

            }else if($("#ruta_id").val() == null){

              Swal.fire(
                '¡UPS!',
                  'Parece se te olvido seleccionar un cliente.',
                  'error'
                ).then((result) => {
                  if (result.isConfirmed) {
                    $("#ruta_id").focus();

                  }
                })

            }else{

              if($("#credito").prop('checked') ){
               
                var total_deuda = parseFloat($("#monto_deuda").val()) + parseFloat($("#deudas_cliente").val());
                
                if(total_deuda > 300){

                        Swal.fire({
                          title: 'El cliente sobre pasa los limites crediticios. <br> Esta operación necesita autorización de un administrador.',
                          input: 'password',
                          inputAttributes: {
                            autocapitalize: 'off',
                            id:'clave',
                            value: '',
                            maxlenght:'4'
                          },
                          showCancelButton: true,
                          confirmButtonText: 'Autorizar',
                          showLoaderOnConfirm: true,
                          preConfirm: (login) => {
          
                            
                            //console.log(login);
                            $.ajax({
                              url:  APP_URL +"/autorizacion",
                              type: 'POST',
                              data: { 
                                
                                  clave: $("#clave").val(),
                                  '_token': $('meta[name="csrf-token"]').attr('content') 
                              },
                              })
                          .done(function(data) {
          
                            if(data == "1"){
                              Swal.close();
                              guardando();
                              $("#venta").submit();
                            
          
                            }else{
          
                              Swal.showValidationMessage(
                                `Clave inválida.`
                              )
          
                            }
                                
                            });
          
                            return false;
                          }
                        });                 
    
                 


                }else{

                  guardando();
                  $("#venta").submit();
  
  
                }

              }else{

                guardando();
                $("#venta").submit();


              }
              
              



            }

        }


      }); 


      $( "#ruta_id" ).on( "change", function( event) {

          $.ajax({
            url:  APP_URL +"/info_box_ruta",
            type: 'POST',
            data: { 
                ruta_id: $(this).val(),
                '_token': $('meta[name="csrf-token"]').attr('content') 
            },
            })
        .done(function(data) {

            data = jQuery.parseJSON(data);
            $(".box-cliente-nombre").empty().append(data.nombre);
            $(".box-fecha").empty().append(data.creado);
            $(".box-venta-mes-pasado").empty().append(parseFloat(data.ventas_mes_anterior).toFixed(2)); 
            $(".box-venta-mes-actual").empty().append(parseFloat(data.ventas_mes_actual).toFixed(2)); 
            $(".box-deudas").empty().append(parseFloat(data.deudas_totales).toFixed(2)); 
            $("#deudas_cliente").val(data.deudas_totales);
            $(".area-info-box").fadeIn(1000);;
            
            
        });

      }); 
  }); 
   