
document.addEventListener('DOMContentLoaded', ()=>{
    const tabLinks      = document.querySelectorAll('.tab-nav-link');
    const tabLinksBoxes = document.querySelectorAll('.tab-link-box');
    activeLinkListener(tabLinks, tabLinksBoxes, 'active-tab-link');
    setCollapsAndBoxes();
    launchTitulariceModal();
});
/**
 * Establece la funcionalidad de las cajas y los links del tipo "collapse"
 */
function setCollapsAndBoxes(){
    const collapseItems = document.querySelectorAll('.collapse-link-item');
    const collapseBoxes = document.querySelectorAll('.collapse-box');
    activeLinkListener(collapseItems, collapseBoxes, 'collapse-link-active');
}
function activeLinkListener(items, containers, activeClass){
    if(items.length == 0 && containers.lenght == 0) return;
    Array.prototype.map.call(items, (el) => {
        el.addEventListener('click', ({ target }) => {
            const questionID = target.getAttribute('data-id');
            items.forEach( item => item.classList.remove( activeClass ) );
            target.classList.add( activeClass );
            containers.forEach( box => {
                if ( box.id !== questionID){
                    box.setAttribute('data-show', false)
                } else {
                    box.setAttribute('data-show', true);
                }
            });
        });
    });
}
/**
 * Busca dentro de la vista si existe un modal con la clase "auto-load-tituarice-modal" para ser mostrado luego de la carga de la vista.
 */
function launchTitulariceModal(){
    const autoLoadModal = document.querySelector('.auto-load-tituarice-modal');
    if( !!autoLoadModal ){
        const titulariceModal = new bootstrap.Modal(autoLoadModal, {});
        titulariceModal.show();
    }
}
/**
 * Función para realizar peticiones GET asincronas.
 * @param {string} url Url de la petición
 * @param {function} succesCallBackFn Acción que sera aplicada a los datos obtenidos
 * @param {null|function} errorCallBackFn Acción que a realizar en caso de existir un error
 */
const Get = async (url='', succesCallBackFn=()=>{}, errorCallBackFn = null) => {
    try {
        await fetch(`${ APP_URL }/${ url }` )
        .then( response => response.json() )
        .then( response => {
            succesCallBackFn(response);
        });
    } catch ( error ) {
        console.error( error );
        !!errorCallBackFn && errorCallBackFn();
    }
}
/**
 * Función para realizar peticiones POST asincronas.
 * @param {string} url Url de la petición
 * @param {object} data datos que serán enviados al endpoint
 * @param {function} succesCallBackFn Acción que sera aplicada a los datos obtenidos
 * @param {null|function} errorCallBackFn Acción que a realizar en caso de existir un error
 */
const Post = async(url='', data={}, succesCallBackFn=()=>{}, errorCallBackFn = null) => {
    try {
        const _token = document.querySelector('meta[name="_token"]').getAttribute('content');
        await fetch(`${ APP_URL }/${ url }`, {
            method: "POST",
            body: JSON.stringify( data ),
            headers: {
                'Content-type': 'application/json; charset=UTF-8',
                'X-CSRF-TOKEN': _token
            }
        })
        .then( response => response.json() )
        .then( response => {
            succesCallBackFn(response);
        });
    } catch ( error ) {
        console.error( error );
        !!errorCallBackFn && errorCallBackFn();
    }
}
