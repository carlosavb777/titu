document.addEventListener('DOMContentLoaded', () => {
    document.querySelector('#anios-control').addEventListener('change', e => {
        const { value } = e.target;
        window.location = APP_URL + `/informacion-financiera/${ value }#documentos`; 
    });
});