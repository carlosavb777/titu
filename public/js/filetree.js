$(document).ready( function() {
    $('#filetree').fileTree(
        {
            root: ROOT_FOLDER,
            expandSpeed	: 50,
            expandSpeed	: 50,
            script: `${ APP_URL }/filetree-connector`,
        }, function(file) {
            const fileURL = encodeURI(file);
            window.open(`${ APP_URL }/${ fileURL }`, '_blank');
        }
    );
});
