-- phpMyAdmin SQL Dump
-- version 5.0.4
-- https://www.phpmyadmin.net/
--
-- Servidor: 127.0.0.1
-- Tiempo de generación: 07-06-2022 a las 03:35:57
-- Versión del servidor: 10.4.16-MariaDB
-- Versión de PHP: 7.4.12

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `titularice`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `accesos`
--

CREATE TABLE `accesos` (
  `id` int(11) NOT NULL,
  `usuario_id` int(11) NOT NULL,
  `ip` varchar(45) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `deleted_at` datetime DEFAULT NULL,
  `user_agent` varchar(512) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `accionistas`
--

CREATE TABLE `accionistas` (
  `id` int(11) NOT NULL,
  `nombre_ES` varchar(64) DEFAULT NULL,
  `porcentaje` varchar(45) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `deleted_at` datetime DEFAULT NULL,
  `nombre_EN` varchar(64) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Volcado de datos para la tabla `accionistas`
--

INSERT INTO `accionistas` (`id`, `nombre_ES`, `porcentaje`, `created_at`, `updated_at`, `deleted_at`, `nombre_EN`) VALUES
(1, 'Carlos Valles', '30%', '2022-06-06 02:51:24', '2022-06-06 03:01:23', '2022-06-06 03:01:23', 'Charlye Valles');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `bitacora`
--

CREATE TABLE `bitacora` (
  `id` int(11) NOT NULL,
  `entidad` varchar(128) NOT NULL,
  `accion` varchar(16) NOT NULL,
  `user_id` int(11) NOT NULL,
  `datos_salientes` longtext DEFAULT NULL,
  `datos_entrantes` longtext NOT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `deleted_at` datetime DEFAULT NULL,
  `tabla_padre_id` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `categorias`
--

CREATE TABLE `categorias` (
  `id` int(11) NOT NULL,
  `cartera` varchar(45) DEFAULT NULL,
  `categoria` varchar(255) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `deleted_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `configuraciones`
--

CREATE TABLE `configuraciones` (
  `id` int(11) NOT NULL,
  `key` varchar(45) DEFAULT NULL,
  `value` varchar(1024) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `deleted_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Volcado de datos para la tabla `configuraciones`
--

INSERT INTO `configuraciones` (`id`, `key`, `value`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 'TELEFONO', '0000000000', NULL, '2022-05-26 23:13:14', NULL);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `departamentos`
--

CREATE TABLE `departamentos` (
  `id` int(11) NOT NULL,
  `nombre` varchar(256) DEFAULT NULL,
  `codigo` varchar(64) DEFAULT NULL,
  `zona_pais_id` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `departamentos`
--

INSERT INTO `departamentos` (`id`, `nombre`, `codigo`, `zona_pais_id`) VALUES
(1, 'Ahuachapán', 'SV-AH', 1),
(2, 'Santa Ana', 'SV-SA', 1),
(3, 'Sonsonate', 'SV-SO', 1),
(4, 'La Libertad', 'SV-LI', 2),
(5, 'Chalatenango', 'SV-CH', 2),
(6, 'San Salvador', 'SV-SS', 2),
(7, 'Cuscatlán', 'SV-CU', 3),
(8, 'La Paz', 'SV-PA', 3),
(9, 'Cabañas', 'SV-CA', 3),
(10, 'San Vicente', 'SV-SV', 3),
(11, 'Usulután', 'SV-US', 4),
(12, 'Morazán', 'SV-MO', 4),
(13, 'San Miguel', 'SV-SM', 4),
(14, 'La Unión', 'SV-UN', 4);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `emisiones`
--

CREATE TABLE `emisiones` (
  `id` int(11) NOT NULL,
  `categoria_id` int(11) DEFAULT NULL,
  `nombre_documento` varchar(64) DEFAULT NULL,
  `documento_url` varchar(255) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `deleted_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `encabezados`
--

CREATE TABLE `encabezados` (
  `id` int(11) NOT NULL,
  `pagina` varchar(45) DEFAULT NULL,
  `titulo_ES` varchar(255) DEFAULT NULL,
  `subtitulo_ES` varchar(255) DEFAULT NULL,
  `descripcion_ES` varchar(255) DEFAULT NULL,
  `titulo_EN` varchar(255) DEFAULT NULL,
  `subtitulo_EN` varchar(255) DEFAULT NULL,
  `descripcion_EN` varchar(4255) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `uri` varchar(45) DEFAULT NULL,
  `imagen` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Volcado de datos para la tabla `encabezados`
--

INSERT INTO `encabezados` (`id`, `pagina`, `titulo_ES`, `subtitulo_ES`, `descripcion_ES`, `titulo_EN`, `subtitulo_EN`, `descripcion_EN`, `created_at`, `updated_at`, `uri`, `imagen`) VALUES
(1, 'EDUCACION FINANCIERA', '', '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(2, 'SOBRE TITULARICE', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(3, 'ACCIONISTAS', 'Lorem ipsum dolor sit amet', 'accionistas', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Nulla porttitor massa id neque aliquam vestibulum morbi blandit. Nibh sed pulvinar proin gravida hendrerit.', 'Lorem ipsum dolor sit amet', 'accionistas', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Nulla porttitor massa id neque aliquam vestibulum morbi blandit. Nibh sed pulvinar proin gravida hendrerit.', NULL, '2022-06-06 01:42:18', NULL, 'encabezados/q74BuduyxTrWEfo9Cp97VQXeWnt2ADPTMj7hW8Mw.png'),
(4, 'GOBIERNO CORPORATIVO', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(5, 'JUNTA DIRECTIVA', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(6, 'ORGANIZACION EMPRESARIAL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(7, 'NORMATIVIDAD', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(8, 'INFORMACION FINANCIERA', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(9, 'RIESGOS', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(10, 'RELACION CON INVERSIONISTAS', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(11, 'EMISIONES', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(12, 'CONTACTENOS', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `gastos`
--

CREATE TABLE `gastos` (
  `id` int(11) NOT NULL,
  `nombre` varchar(64) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `deleted_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `gastos`
--

INSERT INTO `gastos` (`id`, `nombre`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 'test', '2021-03-23 12:57:48', '2021-03-23 12:57:53', '2021-03-23 12:57:53'),
(2, 'Parqueo', '2021-03-24 09:58:02', '2021-03-24 09:58:02', NULL),
(3, 'Reparacion llanta', '2021-03-24 09:58:32', '2021-03-24 09:58:32', NULL),
(4, 'Compra de llantas', '2021-03-24 09:58:45', '2021-03-24 13:34:59', NULL),
(5, 'Gasolina', '2021-03-24 09:59:25', '2021-03-24 09:59:25', NULL),
(6, 'Renta', '2021-03-24 13:35:16', '2021-03-24 13:35:16', NULL),
(7, 'Viáticos', '2021-03-24 13:35:32', '2021-03-24 13:35:32', NULL),
(8, 'Pago ayudante', '2021-03-25 07:30:11', '2021-03-25 07:30:11', NULL),
(9, 'Pago Salvador Barillas', '2021-03-25 07:30:31', '2021-03-25 07:30:31', NULL),
(10, 'Otras reparaciones', '2021-05-04 07:50:53', '2021-05-04 07:50:53', NULL),
(11, 'Pago empleados (día domingo)', '2021-05-25 09:30:34', '2021-05-25 09:30:34', NULL),
(12, 'Pago Motorista', '2021-06-18 08:03:18', '2021-06-18 08:03:18', NULL),
(13, 'Gastos Varios', '2021-07-02 13:45:39', '2021-07-02 13:45:39', NULL);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `gobierno_corporativo`
--

CREATE TABLE `gobierno_corporativo` (
  `id` int(11) NOT NULL,
  `titulo_ES` varchar(255) DEFAULT NULL,
  `descripcion_ES` longtext DEFAULT NULL,
  `nombre_documento_ES` varchar(255) DEFAULT NULL,
  `titulo_EN` varchar(255) DEFAULT NULL,
  `descripcion_EN` longtext DEFAULT NULL,
  `nombre_documento_EN` varchar(255) DEFAULT NULL,
  `documento_url` varchar(255) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `deleted_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Volcado de datos para la tabla `gobierno_corporativo`
--

INSERT INTO `gobierno_corporativo` (`id`, `titulo_ES`, `descripcion_ES`, `nombre_documento_ES`, `titulo_EN`, `descripcion_EN`, `nombre_documento_EN`, `documento_url`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 'TEST', 'TEST', 'TEST', 'TEST', 'TEST', 'TEST', 'gobierno_corporativob', '2022-06-06 17:02:47', '2022-06-06 18:35:14', '2022-06-06 18:35:14'),
(2, 'TEST2', 'TEST2', 'TEST2', 'TEST2', 'TEST2', 'TEST2', 'gobierno_corporativo/TEST2 06062022.pdf', '2022-06-06 17:12:27', '2022-06-06 18:35:52', '2022-06-06 18:35:52'),
(3, 'TEST3', 'TEST3', 'TEST3', 'TEST3', 'TEST3', 'TEST3', 'gobierno_corporativo/TEST3 06062022.docx', '2022-06-06 17:56:06', '2022-06-06 18:38:29', '2022-06-06 18:38:29'),
(4, 'TEST4', 'TEST4', 'TEST4', 'TEST4', 'TEST4', 'TEST4', 'gobierno_corporativo/TEST4 06062022.pdf', '2022-06-06 18:13:44', '2022-06-06 18:34:33', '2022-06-06 18:34:33');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `modulos`
--

CREATE TABLE `modulos` (
  `id` int(11) NOT NULL,
  `nombre` varchar(256) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `deleted_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Volcado de datos para la tabla `modulos`
--

INSERT INTO `modulos` (`id`, `nombre`, `created_at`, `updated_at`, `deleted_at`) VALUES
(6, 'comparaciones', NULL, NULL, NULL),
(9, 'usuarios', NULL, NULL, NULL),
(10, 'roles', NULL, NULL, NULL),
(11, 'Accesos', NULL, NULL, NULL),
(12, 'Bitacoras', NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `municipios`
--

CREATE TABLE `municipios` (
  `id` int(11) NOT NULL,
  `nombre` varchar(64) DEFAULT NULL,
  `departamento_id` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `municipios`
--

INSERT INTO `municipios` (`id`, `nombre`, `departamento_id`) VALUES
(1, 'Ahuachapán', 1),
(2, 'Jujutla', 1),
(3, 'Atiquizaya', 1),
(4, 'Concepción de Ataco', 1),
(5, 'El Refugio', 1),
(6, 'Guaymango', 1),
(7, 'Apaneca', 1),
(8, 'San Francisco Menéndez', 1),
(9, 'San Lorenzo', 1),
(10, 'San Pedro Puxtla', 1),
(11, 'Tacuba', 1),
(12, 'Turín', 1),
(13, 'Candelaria de la Frontera', 2),
(14, 'Chalchuapa', 2),
(15, 'Coatepeque', 2),
(16, 'El Congo', 2),
(17, 'El Porvenir', 2),
(18, 'Masahuat', 2),
(19, 'Metapán', 2),
(20, 'San Antonio Pajonal', 2),
(21, 'San Sebastián Salitrillo', 2),
(22, 'Santa Ana', 2),
(23, 'Santa Rosa Guachipilín', 2),
(24, 'Santiago de la Frontera', 2),
(25, 'Texistepeque', 2),
(26, 'Acajutla', 3),
(27, 'Armenia', 3),
(28, 'Caluco', 3),
(29, 'Cuisnahuat', 3),
(30, 'Izalco', 3),
(31, 'Juayúa', 3),
(32, 'Nahuizalco', 3),
(33, 'Nahulingo', 3),
(34, 'Salcoatitán', 3),
(35, 'San Antonio del Monte', 3),
(36, 'San Julián', 3),
(37, 'Santa Catarina Masahuat', 3),
(38, 'Santa Isabel Ishuatán', 3),
(39, 'Santo Domingo de Guzmán', 3),
(40, 'Sonsonate', 3),
(41, 'Sonzacate', 3),
(42, 'Alegría', 11),
(43, 'Berlín', 11),
(44, 'California', 11),
(45, 'Concepción Batres', 11),
(46, 'El Triunfo', 11),
(47, 'Ereguayquín', 11),
(48, 'Estanzuelas', 11),
(49, 'Jiquilisco', 11),
(50, 'Jucuapa', 11),
(51, 'Jucuarán', 11),
(52, 'Mercedes Umaña', 11),
(53, 'Nueva Granada', 11),
(54, 'Ozatlán', 11),
(55, 'Puerto El Triunfo', 11),
(56, 'San Agustín', 11),
(57, 'San Buenaventura', 11),
(58, 'San Dionisio', 11),
(59, 'San Francisco Javier', 11),
(60, 'Santa Elena', 11),
(61, 'Santa María', 11),
(62, 'Santiago de María', 11),
(63, 'Tecapán', 11),
(64, 'Usulután', 11),
(65, 'Carolina', 13),
(66, 'Chapeltique', 13),
(67, 'Chinameca', 13),
(68, 'Chirilagua', 13),
(69, 'Ciudad Barrios', 13),
(70, 'Comacarán', 13),
(71, 'El Tránsito', 13),
(72, 'Lolotique', 13),
(73, 'Moncagua', 13),
(74, 'Nueva Guadalupe', 13),
(75, 'Nuevo Edén de San Juan', 13),
(76, 'Quelepa', 13),
(77, 'San Antonio del Mosco', 13),
(78, 'San Gerardo', 13),
(79, 'San Jorge', 13),
(80, 'San Luis de la Reina', 13),
(81, 'San Miguel', 13),
(82, 'San Rafael Oriente', 13),
(83, 'Sesori', 13),
(84, 'Uluazapa', 13),
(85, 'Arambala', 12),
(86, 'Cacaopera', 12),
(87, 'Chilanga', 12),
(88, 'Corinto', 12),
(89, 'Delicias de Concepción', 12),
(90, 'El Divisadero', 12),
(91, 'El Rosario (Morazán)', 12),
(92, 'Gualococti', 12),
(93, 'Guatajiagua', 12),
(94, 'Joateca', 12),
(95, 'Jocoaitique', 12),
(96, 'Jocoro', 12),
(97, 'Lolotiquillo', 12),
(98, 'Meanguera', 12),
(99, 'Osicala', 12),
(100, 'Perquín', 12),
(101, 'San Carlos', 12),
(102, 'San Fernando (Morazán)', 12),
(103, 'San Francisco Gotera', 12),
(104, 'San Isidro (Morazán)', 12),
(105, 'San Simón', 12),
(106, 'Sensembra', 12),
(107, 'Sociedad', 12),
(108, 'Torola', 12),
(109, 'Yamabal', 12),
(110, 'Yoloaiquín', 12),
(111, 'La Unión', 14),
(112, 'San Alejo', 14),
(113, 'Yucuaiquín', 14),
(114, 'Conchagua', 14),
(115, 'Intipucá', 14),
(116, 'San José', 14),
(117, 'El Carmen (La Unión)', 14),
(118, 'Yayantique', 14),
(119, 'Bolívar', 14),
(120, 'Meanguera del Golfo', 14),
(121, 'Santa Rosa de Lima', 14),
(122, 'Pasaquina', 14),
(123, 'Anamoros', 14),
(124, 'Nueva Esparta', 14),
(125, 'El Sauce', 14),
(126, 'Concepción de Oriente', 14),
(127, 'Polorós', 14),
(128, 'Lislique', 14),
(129, 'Antiguo Cuscatlán', 4),
(130, 'Chiltiupán', 4),
(131, 'Ciudad Arce', 4),
(132, 'Colón', 4),
(133, 'Comasagua', 4),
(134, 'Huizúcar', 4),
(135, 'Jayaque', 4),
(136, 'Jicalapa', 4),
(137, 'La Libertad', 4),
(138, 'Santa Tecla', 4),
(139, 'Nuevo Cuscatlán', 4),
(140, 'San Juan Opico', 4),
(141, 'Quezaltepeque', 4),
(142, 'Sacacoyo', 4),
(143, 'San José Villanueva', 4),
(144, 'San Matías', 4),
(145, 'San Pablo Tacachico', 4),
(146, 'Talnique', 4),
(147, 'Tamanique', 4),
(148, 'Teotepeque', 4),
(149, 'Tepecoyo', 4),
(150, 'Zaragoza', 4),
(151, 'Agua Caliente', 5),
(152, 'Arcatao', 5),
(153, 'Azacualpa', 5),
(154, 'Cancasque', 5),
(155, 'Chalatenango', 5),
(156, 'Citalá', 5),
(157, 'Comapala', 5),
(158, 'Concepción Quezaltepeque', 5),
(159, 'Dulce Nombre de María', 5),
(160, 'El Carrizal', 5),
(161, 'El Paraíso', 5),
(162, 'La Laguna', 5),
(163, 'La Palma', 5),
(164, 'La Reina', 5),
(165, 'Las Vueltas', 5),
(166, 'Nueva Concepción', 5),
(167, 'Nueva Trinidad', 5),
(168, 'Nombre de Jesús', 5),
(169, 'Ojos de Agua', 5),
(170, 'Potonico', 5),
(171, 'San Antonio de la Cruz', 5),
(172, 'San Antonio Los Ranchos', 5),
(173, 'San Fernando (Chalatenango)', 5),
(174, 'San Francisco Lempa', 5),
(175, 'San Francisco Morazán', 5),
(176, 'San Ignacio', 5),
(177, 'San Isidro Labrador', 5),
(178, 'Las Flores', 5),
(179, 'San Luis del Carmen', 5),
(180, 'San Miguel de Mercedes', 5),
(181, 'San Rafael', 5),
(182, 'Santa Rita', 5),
(183, 'Tejutla', 5),
(184, 'Cojutepeque', 7),
(185, 'Candelaria', 7),
(186, 'El Carmen (Cuscatlán)', 7),
(187, 'El Rosario (Cuscatlán)', 7),
(188, 'Monte San Juan', 7),
(189, 'Oratorio de Concepción', 7),
(190, 'San Bartolomé Perulapía', 7),
(191, 'San Cristóbal', 7),
(192, 'San José Guayabal', 7),
(193, 'San Pedro Perulapán', 7),
(194, 'San Rafael Cedros', 7),
(195, 'San Ramón', 7),
(196, 'Santa Cruz Analquito', 7),
(197, 'Santa Cruz Michapa', 7),
(198, 'Suchitoto', 7),
(199, 'Tenancingo', 7),
(200, 'Aguilares', 6),
(201, 'Apopa', 6),
(202, 'Ayutuxtepeque', 6),
(203, 'Cuscatancingo', 6),
(204, 'Ciudad Delgado', 6),
(205, 'El Paisnal', 6),
(206, 'Guazapa', 6),
(207, 'Ilopango', 6),
(208, 'Mejicanos', 6),
(209, 'Nejapa', 6),
(210, 'Panchimalco', 6),
(211, 'Rosario de Mora', 6),
(212, 'San Marcos', 6),
(213, 'San Martín', 6),
(214, 'San Salvador', 6),
(215, 'Santiago Texacuangos', 6),
(216, 'Santo Tomás', 6),
(217, 'Soyapango', 6),
(218, 'Tonacatepeque', 6),
(219, 'Zacatecoluca', 8),
(220, 'Cuyultitán', 8),
(221, 'El Rosario (La Paz)', 8),
(222, 'Jerusalén', 8),
(223, 'Mercedes La Ceiba', 8),
(224, 'Olocuilta', 8),
(225, 'Paraíso de Osorio', 8),
(226, 'San Antonio Masahuat', 8),
(227, 'San Emigdio', 8),
(228, 'San Francisco Chinameca', 8),
(229, 'San Pedro Masahuat', 8),
(230, 'San Juan Nonualco', 8),
(231, 'San Juan Talpa', 8),
(232, 'San Juan Tepezontes', 8),
(233, 'San Luis La Herradura', 8),
(234, 'San Luis Talpa', 8),
(235, 'San Miguel Tepezontes', 8),
(236, 'San Pedro Nonualco', 8),
(237, 'San Rafael Obrajuelo', 8),
(238, 'Santa María Ostuma', 8),
(239, 'Santiago Nonualco', 8),
(240, 'Tapalhuaca', 8),
(241, 'Cinquera', 9),
(242, 'Dolores', 9),
(243, 'Guacotecti', 9),
(244, 'Ilobasco', 9),
(245, 'Jutiapa', 9),
(246, 'San Isidro (Cabañas)', 9),
(247, 'Sensuntepeque', 9),
(248, 'Tejutepeque', 9),
(249, 'Victoria', 9),
(250, 'Apastepeque', 10),
(251, 'Guadalupe', 10),
(252, 'San Cayetano Istepeque', 10),
(253, 'San Esteban Catarina', 10),
(254, 'San Ildefonso', 10),
(255, 'San Lorenzo', 10),
(256, 'San Sebastián', 10),
(257, 'San Vicente', 10),
(258, 'Santa Clara', 10),
(259, 'Santo Domingo', 10),
(260, 'Tecoluca', 10),
(261, 'Tepetitán', 10),
(262, 'Verapaz', 10);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `permisos`
--

CREATE TABLE `permisos` (
  `id` int(11) NOT NULL,
  `rol_id` int(11) NOT NULL,
  `modulo_id` int(11) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `deleted_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `permisos`
--

INSERT INTO `permisos` (`id`, `rol_id`, `modulo_id`, `created_at`, `updated_at`, `deleted_at`) VALUES
(12, 2, 10, '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL),
(13, 2, 1, '2021-03-22 15:26:53', '2021-03-22 15:26:53', NULL),
(14, 2, 2, '2021-03-22 15:26:58', '2021-03-22 15:26:58', NULL),
(15, 2, 3, '2021-03-22 15:27:07', '2021-03-22 15:27:07', NULL),
(16, 2, 9, '2021-03-22 15:27:22', '2021-03-22 15:27:22', NULL),
(17, 2, 6, '2021-03-22 15:27:30', '2021-03-22 15:27:30', NULL),
(18, 2, 8, '2021-03-22 15:27:40', '2021-03-22 15:27:40', NULL),
(19, 2, 7, '2021-03-22 15:27:45', '2021-03-22 15:27:45', NULL),
(20, 6, 2, '2021-03-23 16:50:14', '2021-03-23 16:50:14', NULL),
(21, 6, 3, '2021-03-23 16:58:44', '2021-03-23 16:58:44', NULL);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `preguntas_frecuentes`
--

CREATE TABLE `preguntas_frecuentes` (
  `id` int(11) NOT NULL,
  `pregunta_ES` varchar(512) NOT NULL,
  `respuesta_ES` varchar(2048) DEFAULT NULL,
  `respuesta_EN` varchar(512) DEFAULT NULL,
  `pregunta_EN` varchar(2048) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `deleted_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Volcado de datos para la tabla `preguntas_frecuentes`
--

INSERT INTO `preguntas_frecuentes` (`id`, `pregunta_ES`, `respuesta_ES`, `respuesta_EN`, `pregunta_EN`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 'qhttp://127.0.0.1:8000/preguntas_frecuentes/create', NULL, 'qhttp://127.0.0.1:8000/preguntas_frecuentes/create', NULL, '2022-05-26 15:40:42', '2022-05-26 16:16:07', '2022-05-26 16:16:07'),
(2, 'qhttp://127.0.0.1:8000/preguntas_frecuentes/create', NULL, 'qhttp://127.0.0.1:8000/preguntas_frecuentes/create', NULL, '2022-05-26 15:47:56', '2022-05-26 19:23:14', '2022-05-26 19:23:14'),
(3, 'qhttp://127.0.0.1:8000/preguntas_frecuentes/create', 'Vivamus vulputate ex ante. Morbi non enim enim. Morbi arcu tellus, dictum a leo non, condimentum porta libero. In quis porttitor felis. Nunc maximus metus turpis, in accumsan justo malesuada nec.', 'qhttp://127.0.0.1:8000/preguntas_frecuentes/create', 'Vivamus vulputate ex ante. Morbi non enim enim. Morbi arcu tellus, dictum a leo non, condimentum porta libero. In quis porttitor felis. Nunc maximus', '2022-05-26 15:48:11', '2022-05-26 16:16:38', NULL),
(4, 'qhttp://127.0.0.1:8000/preguntas_frecuentes/create', NULL, 'qhttp://127.0.0.1:8000/preguntas_frecuentes/create', NULL, '2022-05-26 15:48:37', '2022-05-26 16:13:54', '2022-05-26 16:13:54'),
(5, 'http://127.0.0.1:8000/preguntas_frecuentes/5/edit', 'http://127.0.0.1:8000/preguntas_frecuentes/5/edit', 'http://127.0.0.1:8000/preguntas_frecuentes/5/edithttp://127.0.0.1:8000/preguntas_frecuentes/5/edit', 'http://127.0.0.1:8000/preguntas_frecuentes/5/edit', '2022-05-26 16:00:43', '2022-05-26 16:13:42', '2022-05-26 16:13:42'),
(6, 'Vivamus vulputate ex ante. Morbi non enim enim. Morbi arcu tellus, dictum a leo non, condimentum porta libero. In quis porttitor felis. Nunc maximus metus turpis, in accumsan justo malesuada nec.', 'Vivamus vulputate ex ante. Morbi non enim enim. Morbi arcu tellus, dictum a leo non, condimentum porta libero. In quis porttitor felis. Nunc maximus metus turpis, in accumsan justo malesuada nec.', 'Vivamus vulputate ex ante. Morbi non enim enim. Morbi arcu tellus, dictum a leo non, condimentum porta libero. In quis porttitor felis. Nunc maximus metus turpis, in accumsan justo malesuada nec.', 'Vivamus vulputate ex ante. Morbi non enim enim. Morbi arcu tellus, dictum a leo non, condimentum porta libero. In quis porttitor felis. Nunc maximus metus turpis, in accumsan justo malesuada nec.', '2022-05-26 16:34:14', '2022-05-26 19:47:52', '2022-05-26 19:47:52'),
(7, 'Vivamus vulputate ex ante. Morbi non enim enim. Morbi arcu tellu', 'Vivamus vulputate ex ante. Morbi non enim enim. Morbi arcu tellus, dictum a leo non, condimentum porta libero. In quis porttitor felis. Nunc maximus metus turpis, in accumsan justo malesuada nec.', 'Vivamus vulputate ex ante. Morbi non enim enim. Morbi arcu tellus, dictum a leo non, condimentum porta libero. In quis porttitor felis. Nunc maximus metus turpis, in accumsan justo malesuada nec.', 'Vivamus vulputate ex ante. Morbi non enim enim. Morbi arcu tellu', '2022-05-26 19:21:02', '2022-05-26 19:21:02', NULL),
(8, '1111111111111111111111111111111111111111111111111111111111111111111111111111111111111', '1111111111111111111111111111111111111111111111111111111111111111111111111111111111111', '1111111111111111111111111111111111111111111111111111111111111111111111111111111111111', '1111111111111111111111111111111111111111111111111111111111111111111111111111111111111', '2022-05-26 19:39:38', '2022-05-26 19:46:50', '2022-05-26 19:46:50');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `roles`
--

CREATE TABLE `roles` (
  `id` int(11) NOT NULL,
  `nombre` varchar(64) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `deleted_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `roles`
--

INSERT INTO `roles` (`id`, `nombre`, `created_at`, `updated_at`, `deleted_at`) VALUES
(2, 'Administrador', '2021-03-04 23:08:27', '2021-03-06 09:28:19', NULL),
(6, 'Asistente', '2021-03-23 16:32:54', '2021-03-23 16:40:58', NULL);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `usuarios`
--

CREATE TABLE `usuarios` (
  `id` int(11) NOT NULL,
  `nickname` varchar(45) NOT NULL,
  `email` varchar(64) NOT NULL,
  `password` varchar(64) NOT NULL,
  `nombres` varchar(128) NOT NULL,
  `apellidos` varchar(45) NOT NULL,
  `rol_id` varchar(45) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `deleted_at` datetime DEFAULT NULL,
  `clave_autorizacion` varchar(45) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `usuarios`
--

INSERT INTO `usuarios` (`id`, `nickname`, `email`, `password`, `nombres`, `apellidos`, `rol_id`, `created_at`, `updated_at`, `deleted_at`, `clave_autorizacion`) VALUES
(1, 'carlos', 'carlos@test.com', '$2y$10$s3sofRNOfwnKVCP1ppe44OtfroY7bhkfzGji0a89bhaLBcihcUF5W', 'Carlos', 'Valle', '2', '0000-00-00 00:00:00', '2021-03-23 13:03:26', NULL, '0794'),
(10, 'rocio', 'rocio.interiano@iprimaveral.com', '$2y$10$s3sofRNOfwnKVCP1ppe44OtfroY7bhkfzGji0a89bhaLBcihcUF5W', 'Rocio Carolina', 'Interiano Aguila', '2', '2021-03-06 08:02:14', '2022-03-14 21:49:38', '2022-03-14 21:49:38', NULL),
(11, 'marta.andrade', 'marta.andrade@iprimaveral.com', '$2y$10$q.Pb04mcDuoeUGl84jc9V.6bG8leDjVmWuH51j.44i1ZLaGO7RGx6', 'Marta Lisseth', 'Andrade García', '2', '2021-03-23 16:07:51', '2022-03-14 21:50:04', '2022-03-14 21:50:04', NULL),
(12, 'alejandra_erazo', 'alejandra_erazo@iprimaveral.com', '$2y$10$AE/1G7sP2xsHMeNueRCcGOhyLFJwsfn/q6WdlXfelUXv49NszKNXy', 'Alejandra Yamileth', 'Erazo', '6', '2021-03-23 16:44:52', '2022-03-14 21:51:02', '2022-03-14 21:51:02', NULL),
(13, 'marielos.paniagua', 'marielos.paniagua@iprimaveral.com', '$2y$10$Rp2JMqmwmV3DIKgx6TS0weFmdz2UwO/cHbIkmhvscZDbAVaISXptu', 'Marielos Alely', 'Paniagua Gallardo', '6', '2021-03-23 17:03:59', '2022-03-14 21:50:15', '2022-03-14 21:50:15', NULL),
(14, 'javier.interiano', 'javier.interiano@iprimaveral.com', '$2y$10$DOG1AhQddQugJFV25flYX.EjejihelKGBnBNDCeU7QHJxdw/d2.qS', 'Fernando Javier', 'Interiano Aguila', '2', '2021-03-23 17:13:19', '2022-03-14 21:50:27', '2022-03-14 21:50:27', '8822'),
(15, 'francisco.interiano', 'francisco.interiano@iprimaveral.com', '$2y$10$hSlc4LxAiBc5B1DQTycHZeuKweat9qANhBJvwmkxAuU9aS4axCAUm', 'Francisco Eduardo', 'Interiano Aguila', '6', '2021-03-23 17:45:08', '2022-03-14 21:50:37', '2022-03-14 21:50:37', NULL),
(16, 'monica_melara', 'monica_melara@iprimaveral.com', '$2y$10$v8MlHkzUB6aCl4kkAgIhVe1SEYXf0/0VKaAW8g08l0Fh7GZxEw5KS', 'Monica Mariela', 'Melara', '6', '2021-03-23 17:49:51', '2022-03-14 21:49:49', '2022-03-14 21:49:49', NULL),
(17, 'enrique.interiano', 'enrique.interiano@iprimaveral.com', '$2y$10$q8Gd0vknLnB4TuIrDIKrSeUZvp6Gmpc6QZ1ctEOywJCQ0lTgBREWu', 'Francisco Enrique', 'Interiano', '6', '2021-03-29 12:10:20', '2022-03-14 21:50:51', '2022-03-14 21:50:51', NULL),
(18, 'rventura', 'rventura@test.com', '$2y$10$RzDv7b8WX0/9C9nqWfuO7eTjyMVld7pG/m80yD9tc3oUpS.d1KAgi', 'Ronald', 'Ventura', '6', '2022-03-14 21:53:20', '2022-03-14 22:16:02', NULL, NULL),
(19, 'rventurad', 'rventura@test.comw', '$2y$10$AelY2vahhLAcSGxL1kInG.V7VP8Pm7b8wPNuOO0x.OeoFrTX5vcBq', 'test', 'test', '2', '2022-03-14 21:54:46', '2022-03-14 22:04:44', '2022-03-14 22:04:44', NULL),
(20, 'admin@admin.comx', 'admin@admin.comx', '$2y$10$b3jhqL768pZUCmXkqPIZsezoh3/MP.CD4BEztQe5Pg7M5iDj8FPGe', 'xsxs', 'xsxs', '2', '2022-03-14 22:01:52', '2022-03-14 22:05:02', '2022-03-14 22:05:02', NULL);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `zonas`
--

CREATE TABLE `zonas` (
  `id` int(11) NOT NULL,
  `nombre` varchar(64) NOT NULL,
  `municipio_id` int(11) NOT NULL,
  `created_at` datetime NOT NULL,
  `deleted_at` datetime DEFAULT NULL,
  `updated_at` datetime NOT NULL,
  `departamento_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `zonas`
--

INSERT INTO `zonas` (`id`, `nombre`, `municipio_id`, `created_at`, `deleted_at`, `updated_at`, `departamento_id`) VALUES
(1, 'test2', 13, '2021-03-24 11:37:37', '2021-03-24 11:38:11', '2021-03-24 11:38:11', 2);

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `accesos`
--
ALTER TABLE `accesos`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `accionistas`
--
ALTER TABLE `accionistas`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `bitacora`
--
ALTER TABLE `bitacora`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `categorias`
--
ALTER TABLE `categorias`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `configuraciones`
--
ALTER TABLE `configuraciones`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `departamentos`
--
ALTER TABLE `departamentos`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `emisiones`
--
ALTER TABLE `emisiones`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `encabezados`
--
ALTER TABLE `encabezados`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `gastos`
--
ALTER TABLE `gastos`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `gobierno_corporativo`
--
ALTER TABLE `gobierno_corporativo`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `modulos`
--
ALTER TABLE `modulos`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `municipios`
--
ALTER TABLE `municipios`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `permisos`
--
ALTER TABLE `permisos`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `preguntas_frecuentes`
--
ALTER TABLE `preguntas_frecuentes`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `roles`
--
ALTER TABLE `roles`
  ADD PRIMARY KEY (`id`,`nombre`);

--
-- Indices de la tabla `usuarios`
--
ALTER TABLE `usuarios`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `zonas`
--
ALTER TABLE `zonas`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `accesos`
--
ALTER TABLE `accesos`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `accionistas`
--
ALTER TABLE `accionistas`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT de la tabla `bitacora`
--
ALTER TABLE `bitacora`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `categorias`
--
ALTER TABLE `categorias`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `configuraciones`
--
ALTER TABLE `configuraciones`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT de la tabla `emisiones`
--
ALTER TABLE `emisiones`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `encabezados`
--
ALTER TABLE `encabezados`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;

--
-- AUTO_INCREMENT de la tabla `gastos`
--
ALTER TABLE `gastos`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;

--
-- AUTO_INCREMENT de la tabla `gobierno_corporativo`
--
ALTER TABLE `gobierno_corporativo`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT de la tabla `modulos`
--
ALTER TABLE `modulos`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;

--
-- AUTO_INCREMENT de la tabla `permisos`
--
ALTER TABLE `permisos`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=22;

--
-- AUTO_INCREMENT de la tabla `preguntas_frecuentes`
--
ALTER TABLE `preguntas_frecuentes`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT de la tabla `titulariceroles`
--
ALTER TABLE `roles`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT de la tabla `usuarios`
--
ALTER TABLE `usuarios`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=21;

--
-- AUTO_INCREMENT de la tabla `zonas`
--
ALTER TABLE `zonas`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
