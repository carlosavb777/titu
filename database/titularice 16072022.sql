-- phpMyAdmin SQL Dump
-- version 5.0.4
-- https://www.phpmyadmin.net/
--
-- Servidor: 127.0.0.1
-- Tiempo de generación: 16-07-2022 a las 23:36:50
-- Versión del servidor: 10.4.16-MariaDB
-- Versión de PHP: 7.4.12

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `titularice`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `accesos`
--

CREATE TABLE `accesos` (
  `id` int(11) NOT NULL,
  `usuario_id` int(11) NOT NULL,
  `ip` varchar(45) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `deleted_at` datetime DEFAULT NULL,
  `user_agent` varchar(512) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `accionistas`
--

CREATE TABLE `accionistas` (
  `id` int(11) NOT NULL,
  `nombre_ES` varchar(64) DEFAULT NULL,
  `porcentaje` varchar(45) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `deleted_at` datetime DEFAULT NULL,
  `nombre_EN` varchar(64) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Volcado de datos para la tabla `accionistas`
--

INSERT INTO `accionistas` (`id`, `nombre_ES`, `porcentaje`, `created_at`, `updated_at`, `deleted_at`, `nombre_EN`) VALUES
(1, 'HDL Titularice', '46.67%', '2022-06-21 14:44:38', '2022-06-21 14:44:38', NULL, 'Nombre Accionista'),
(2, 'Organización DeLima', '38.98%', '2022-06-07 15:50:33', '2022-06-21 14:43:23', NULL, 'Carlos Valle'),
(3, 'Eduardo Alfaro', '13.33%', '2022-06-21 14:44:31', '2022-06-21 14:44:31', NULL, 'Nombre Accionista'),
(4, 'Agrocolsa', '0.01%', '2022-06-21 14:44:06', '2022-06-21 14:44:06', NULL, 'Nombre Accionista'),
(6, 'Agrocuenca', '0.01%', '2022-06-21 14:43:47', '2022-06-21 14:43:47', NULL, 'Nombre Accionista');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `articulos`
--

CREATE TABLE `articulos` (
  `id` int(11) NOT NULL,
  `titulo_ES` longtext DEFAULT NULL,
  `titulo_EN` longtext DEFAULT NULL,
  `descripcion_corta_ES` longtext DEFAULT NULL,
  `descripcion_corta_EN` longtext DEFAULT NULL,
  `pilares` int(11) DEFAULT NULL,
  `pilar1_titulo_ES` longtext DEFAULT NULL,
  `pilar2_titulo_ES` mediumtext DEFAULT NULL,
  `pilar3_titulo_ES` mediumtext DEFAULT NULL,
  `pilar1_titulo_EN` mediumtext DEFAULT NULL,
  `pilar2_titulo_EN` mediumtext DEFAULT NULL,
  `pilar3_titulo_EN` mediumtext DEFAULT NULL,
  `pilar1_contenido_ES` longtext DEFAULT NULL,
  `pilar2_contenido_ES` longtext DEFAULT NULL,
  `pilar3_contenido_ES` longtext DEFAULT NULL,
  `pilar1_contenido_EN` longtext DEFAULT NULL,
  `pilar2_contenido_EN` longtext DEFAULT NULL,
  `pilar3_contenido_EN` longtext DEFAULT NULL,
  `imagen` longtext DEFAULT NULL,
  `contenido_ES` longtext DEFAULT NULL,
  `contenido_EN` longtext DEFAULT NULL,
  `createt_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `deleted_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `bitacora`
--

CREATE TABLE `bitacora` (
  `id` int(11) NOT NULL,
  `entidad` varchar(128) NOT NULL,
  `accion` varchar(16) NOT NULL,
  `user_id` int(11) NOT NULL,
  `datos_salientes` longtext DEFAULT NULL,
  `datos_entrantes` longtext NOT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `deleted_at` datetime DEFAULT NULL,
  `tabla_padre_id` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `categorias`
--

CREATE TABLE `categorias` (
  `id` int(11) NOT NULL,
  `cartera` varchar(255) DEFAULT NULL,
  `categoria_ES` varchar(255) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `deleted_at` datetime DEFAULT NULL,
  `categoria_EN` varchar(255) DEFAULT NULL,
  `url_ES` longtext DEFAULT NULL,
  `url_EN` longtext DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Volcado de datos para la tabla `categorias`
--

INSERT INTO `categorias` (`id`, `cartera`, `categoria_ES`, `created_at`, `updated_at`, `deleted_at`, `categoria_EN`, `url_ES`, `url_EN`) VALUES
(2, 'EMISIONES-CANCELADAS', 'Emision de prueba', '2022-07-14 15:50:54', '2022-07-14 15:50:54', NULL, 'Test Emission', 'emision_de_prueba/', 'test_emision/'),
(3, 'FLUJOS-FUTUROS', 'Emision de prueba', '2022-07-14 16:06:27', '2022-07-14 16:06:27', NULL, 'Test Emision', 'emision_de_prueba_flujos/', 'test_emision_flujos/');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `circulares`
--

CREATE TABLE `circulares` (
  `id` int(11) NOT NULL,
  `descripcion_corta_ES` longtext DEFAULT NULL,
  `descripcion_corta_EN` longtext DEFAULT NULL,
  `contenido_ES` longtext DEFAULT NULL,
  `contenido_EN` longtext DEFAULT NULL,
  `titulo_ES` varchar(1024) DEFAULT NULL,
  `titulo_EN` varchar(1024) DEFAULT NULL,
  `categoria_id` int(11) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `deleted_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `configuraciones`
--

CREATE TABLE `configuraciones` (
  `id` int(11) NOT NULL,
  `key` varchar(45) DEFAULT NULL,
  `value_ES` longtext DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `deleted_at` datetime DEFAULT NULL,
  `type` varchar(45) DEFAULT NULL,
  `value_EN` longtext DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Volcado de datos para la tabla `configuraciones`
--

INSERT INTO `configuraciones` (`id`, `key`, `value_ES`, `created_at`, `updated_at`, `deleted_at`, `type`, `value_EN`) VALUES
(1, 'TELEFONO1', '(315) 564 – 8052', NULL, '2022-07-14 23:43:46', NULL, 'TEXT', '(315) 564 – 8052'),
(2, 'TELEFONO2', '(315) 564 – 8052', NULL, '2022-07-14 23:42:52', NULL, 'TEXT', '(315) 564 – 8052'),
(3, 'CORREO', 'contacto@titularice.com', NULL, NULL, NULL, 'TEXT', 'contacto@titularice.com'),
(4, 'DIRECCION', 'Torre Empresarial FD 100, Oficina Piso 11 - Norte.', NULL, '2022-07-14 23:41:02', NULL, 'TEXTAREA', 'Torre Empresarial FD 100, Oficina Piso 11 - Norte.'),
(5, 'INSTAGRAM_URL', 'https://www.instagram.com/', NULL, NULL, NULL, 'TEXT', 'https://www.instagram.com/'),
(6, 'FACEBOOK_URL', 'https://www.facebook.com/', NULL, NULL, NULL, 'TEXT', 'https://www.facebook.com/'),
(7, 'YOUTUBE_URL', 'https://www.youtube.com/', NULL, NULL, NULL, 'TEXT', 'https://www.youtube.com/'),
(8, 'MISION', '<div align=\"justify\">NUESTRA MISIÓN ES SERVIR A TODOS LOS POTENCIALES ORIGINADORES\r\nCON ACTIVOS SUSCEPTIBLES DE TITULARIZACIÓN, Y A INVERSIONISTAS\r\nPROFESIONALES QUE DESEEN DIVERSIFICAR SUS PORTAFOLIOS, CON\r\nSOLUCIONES INNOVADORAS DE TITULARIZACIÓN EN COLOMBIA USANDO\r\nPARA EL EFECTO EL CONOCIMIENTO Y COMPROMISO SUPERIORES CON\r\nNUESTROS CLIENTES, INVERSIONISTAS Y OTROS INTEGRANTES DEL\r\nMERCADO, ASÍ COMO UN COMPROMISO TOTAL CON LA INNOVACIÓN QUE\r\nASEGURE QUE NOSOTROS Y NUESTROS CLIENTES, ESTEMOS SIEMPRE\r\nADELANTE.\r\n</div><div align=\"justify\"><br></div><div align=\"justify\">NUESTRO ÉXITO SE REFLEJARÁ EN NUESTRA CAPACIDAD DE\r\nDESARROLLAR SOLUCIONES ÓPTIMAS, PARA LA DIVERSIFICACIÓN DEL\r\nFONDEO DE NUESTROS CLIENTES, QUE SE AJUSTE A SUS NECESIDADES.\r\n</div><div align=\"justify\"><br></div><div align=\"justify\">NUESTRO TRABAJO TIENE COMO EJES FUNDAMENTALES DE SU\r\nACTUACIÓN LA INNOVACIÓN, PARA CONTRIBUIR CON EL COMPROMISO\r\nDEL DESARROLLO DE LOS MERCADOS DE CAPITALES DE LA REGIÓN, ASÍ\r\nCOMO LA RIGUROSIDAD Y EL COMPROMISO CON EL CLIENTE EN UN\r\nSECTOR TAN COMPLEJO Y SOFISTICADO COMO EL DE LA TITULARIZACIÓN;\r\nY ASI SER EL LÍDER EN EL MERCADO DE TITULARIZACIÓN EN COLOMBIA.</div>', NULL, '2022-07-15 18:46:18', NULL, 'EDITOR', '<div align=\"justify\">OUR MISSION IS TO SERVE ALL POTENTIAL ORIGINATORS WITH ASSETS SUBJECT TO SECURITIZATION, AND PROFESSIONAL INVESTORS WHO WISH TO DIVERSIFY THEIR PORTFOLIO, WITH INNOVATIVE SECURITIZATION SOLUTIONS IN COLOMBIA, USING TO THIS EFFECT SUPERIOR KNOWLEDGE AND COMMITMENT TO OUR CLIENTS, INVESTORS AND OTHER MARKET INTEGRATS , AS WELL AS A TOTAL COMMITMENT TO INNOVATION THAT ENSURE THAT WE AND OUR CUSTOMERS ARE ALWAYS AHEAD.<br><br>OUR SUCCESS WILL BE REFLECTED IN OUR CAPACITY TO DEVELOP OPTIMAL SOLUTIONS FOR THE DIVERSIFICATION OF OUR CLIENTS\' FUNDING, WHICH ADJUSTS TO THEIR NEEDS.<br><br>OUR WORK HAS INNOVATION AS THE FUNDAMENTAL AREAS OF ITS PERFORMANCE, TO CONTRIBUTE TO THE COMMITMENT TO THE DEVELOPMENT OF THE REGION\'S CAPITAL MARKETS, AS WELL AS RIGOROSITY AND COMMITMENT TO THE CUSTOMER IN A SECTOR AS COMPLEX AND SOPHISTICATED AS THE SECURITIZATION ; AND SO TO BE THE LEADER IN THE SECURITIZATION MARKET IN COLOMBIA.</div>'),
(9, 'VISION', 'Administrar 15 billones de pesos en el año 2025 de activos subyacentes en diferentes sectores económicos en Colombia y en el exterior ', NULL, NULL, NULL, 'EDITOR', 'Manage 15 billion pesos in the year 2025 of underlying assets in different economic sectors in Colombia and abroad'),
(10, 'FRASE_IMPORTANTE', '“Innovación en titularización”', NULL, '2022-07-15 18:36:21', NULL, 'TEXT', '“Innovation in securitization”'),
(11, 'JUNTA_DIRECTIVA_ADMINISTRADORES', 'documentos_generales/JUNTA_DIRECTIVA_ADMINISTRADORES 23062022 221506 ES.pdf', NULL, '2022-06-23 22:15:06', NULL, 'FILE', 'documentos_generales/JUNTA_DIRECTIVA_ADMINISTRADORES 23062022 221506 EN.pdf'),
(12, 'JUNTA_DIRECTIVA', 'documentos_generales/JUNTA_DIRECTIVA 23062022 221458 ES.pdf', NULL, '2022-06-23 22:14:58', NULL, 'FILE', 'documentos_generales/JUNTA_DIRECTIVA 23062022 221458 EN.pdf'),
(13, 'JUNTA_DIRECTIVA_AUDITOR_INTERNO', 'documentos_generales/JUNTA_DIRECTIVA_AUDITOR_INTERNO 23062022 221514 ES.pdf', NULL, '2022-06-23 22:15:14', NULL, 'FILE', 'documentos_generales/JUNTA_DIRECTIVA_AUDITOR_INTERNO 23062022 221514 EN.pdf'),
(14, 'RIESGOS_DOCUMENTO', NULL, NULL, NULL, NULL, 'FILE', NULL),
(15, 'INVERSIONISTAS_CALENDARIO_EVENTOS', 'documentos_generales/INVERSIONISTAS_CALENDARIO_EVENTOS 23062022 194239 ES.pdf', NULL, '2022-06-23 19:42:39', NULL, 'FILE', 'documentos_generales/INVERSIONISTAS_CALENDARIO_EVENTOS 23062022 194239 EN.pdf'),
(16, 'INVERSIONISTAS_ANALISTA_DE_INVERSIONES', 'documentos_generales/INVERSIONISTAS_ANALISTA_DE_INVERSIONES 23062022 191059 ES.pdf', NULL, '2022-06-23 19:10:59', NULL, 'FILE', 'documentos_generales/INVERSIONISTAS_ANALISTA_DE_INVERSIONES 23062022 191059 EN.pdf'),
(17, 'TERMINOS_CONDICIONES', '<p>\r\n        <span class=\"p-tittle-bold\">1. Ipsum dolor sit amet</span>\r\n        Leo vel orci porta non pulvinar neque laoreet suspendisse. Vel \r\npretium lectus quam id leo in vitae. Sed faucibus turpis in eu mi \r\nbibendum. Orci a scelerisque purus semper. Cras tincidunt lobortis \r\nfeugiat vivamus at augue eget. Dolor purus non enim praesent elementum \r\nfacilisis leo vel. Quis risus sed vulputate odio ut enim. Mattis \r\npellentesque id nibh tortor id. Ac turpis egestas sed tempus. \r\nUllamcorper morbi tincidunt ornare massa. Integer quis auctor elit sed \r\nvulputate. Lorem dolor sed viverra ipsum nunc.\r\n    </p>\r\n    <p>\r\n        <span class=\"p-tittle-bold\">2. Consectetur adipiscing elit</span>\r\n        Sed lectus vestibulum mattis ullamcorper velit sed. Quis risus \r\nsed vulputate odio ut enim blandit. Id nibh tortor id aliquet lectus \r\nproin nibh. Diam ut venenatis tellus in metus. Scelerisque eleifend \r\ndonec pretium vulputate. Libero justo laoreet sit amet cursus sit amet \r\ndictum. Quis blandit turpis cursus in hac habitasse platea dictumst \r\nquisque. Elementum nibh tellus molestie nunc non blandit massa enim. \r\nLorem ipsum dolor sit amet consectetur adipiscing elit pellentesque. \r\nDictum varius duis at consectetur lorem donec. Amet consectetur \r\nadipiscing elit ut aliquam purus sit amet. Habitant morbi tristique \r\nsenectus et netus et malesuada fames. Mauris in aliquam sem fringilla. \r\nLectus proin nibh nisl condimentum id venenatis. Sed viverra ipsum nunc \r\naliquet bibendum enim facilisis gravida. Dui accumsan sit amet nulla \r\nfacilisi morbi. Tortor condimentum lacinia quis vel eros. Dignissim enim\r\n sit amet venenatis urna. Amet mattis vulputate enim nulla aliquet \r\nporttitor lacus. Etiam sit amet nisl purus in.\r\n    </p>\r\n    <p>\r\n        At imperdiet dui accumsan sit amet nulla. Rhoncus mattis rhoncus\r\n urna neque viverra justo nec. Urna molestie at elementum eu facilisis \r\nsed. Libero id faucibus nisl tincidunt eget nullam non nisi est. Eu \r\nfacilisis sed odio morbi quis commodo odio aenean sed. Vulputate enim \r\nnulla aliquet porttitor lacus. Dictum varius duis at consectetur lorem \r\ndonec massa sapien faucibus. Nisi lacus sed viverra tellus. Porta non \r\npulvinar neque laoreet suspendisse interdum consectetur. Orci a \r\nscelerisque purus semper eget duis at. Morbi tristique senectus et \r\nnetus. Ipsum dolor sit amet consectetur.\r\n    </p>\r\n    <p>\r\n        <span class=\"p-tittle-bold\">3. Ipsum dolor sit amet, consectetur adipiscing elit</span>\r\n        Mauris augue neque gravida in fermentum et sollicitudin ac orci.\r\n Lacus laoreet non curabitur gravida arcu. Amet facilisis magna etiam \r\ntempor orci eu:\r\n        Amet mauris commodo quis imperdiet massa. Tristique et egestas \r\nquis ipsum suspendisse ultrices gravida dictum. Tempor orci eu lobortis \r\nelementum.\r\n        Vitae aliquet nec ullamcorper sit amet risus nullam eget.\r\n        </p><ul><li>Sed felis eget velit aliquet.</li><li>Tincidunt arcu non sodales neque sodales.</li><li>Etiam tempor orci eu lobortis elementum nibh tellus molestie nunc.</li><li>Etiam tempor orci eu lobortis elementum nibh tellus molestie nunc.</li><li>Donec et odio pellentesque diam volutpat commodo sed egestas egestas.</li><li>Molestie ac feugiat sed lectus vestibulum. Faucibus turpis in eu mi bibendum neque egestas.</li></ul>\r\n    <p></p>\r\n\r\n    <p>\r\n        <span class=\"p-tittle-bold\">Lorem Ipsum dolor</span>\r\n        Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do \r\neiusmod tempor incididunt ut labore et dolore magna aliqua. Eget lorem \r\ndolor sed viverra ipsum nunc aliquet bibendum enim. Montes nascetur \r\nridiculus mus mauris vitae ultricies. Tortor at risus viverra adipiscing\r\n at in tellus. A arcu cursus vitae congue mauris rhoncus. Volutpat odio \r\nfacilisis mauris sit amet. Suspendisse faucibus interdum posuere lorem \r\nipsum dolor sit amet consectetur. Pharetra pharetra massa massa \r\nultricies mi. Sed risus ultricies tristique nulla. Mauris augue neque \r\ngravida in fermentum et sollicitudin ac orci. Fermentum et sollicitudin \r\nac orci phasellus egestas tellus rutrum.\r\n    </p>\r\n    <p>\r\n        <span class=\"p-tittle-bold\">1. Ipsum dolor sit amet, consectetur adipiscing elit</span>\r\n        Leo vel orci porta non pulvinar neque laoreet suspendisse. Vel \r\npretium lectus quam id leo in vitae. Sed faucibus turpis in eu mi \r\nbibendum. Orci a scelerisque purus semper. Cras tincidunt lobortis \r\nfeugiat vivamus at augue eget. Dolor purus non enim praesent elementum \r\nfacilisis leo vel. Quis risus sed vulputate odio ut enim. Mattis \r\npellentesque id nibh tortor id. Ac turpis egestas sed tempus. \r\nUllamcorper morbi tincidunt ornare massa. Integer quis auctor elit sed \r\nvulputate. Lorem dolor sed viverra ipsum nunc.\r\n    </p>\r\n    <p>\r\n        <span class=\"p-tittle-bold\">2. Sit amet, consectetur adipiscing elit</span>\r\n        Sed lectus vestibulum mattis ullamcorper velit sed. Quis risus \r\nsed vulputate odio ut enim blandit. Id nibh tortor id aliquet lectus \r\nproin nibh. Diam ut venenatis tellus in metus. Scelerisque eleifend \r\ndonec pretium vulputate. Libero justo laoreet sit amet cursus sit amet \r\ndictum. Quis blandit turpis cursus in hac habitasse platea dictumst \r\nquisque. Elementum nibh tellus molestie nunc non blandit massa enim. \r\nLorem ipsum dolor sit amet consectetur adipiscing elit pellentesque. \r\n    </p>\r\n    <p>\r\n        <span class=\"p-tittle-bold\">3. Consectetur adipiscing elit</span>\r\n        Dictum varius duis at consectetur lorem donec. Amet consectetur \r\nadipiscing elit ut aliquam purus sit amet. Habitant morbi tristique \r\nsenectus et netus et malesuada fames. Mauris in aliquam sem fringilla. \r\nLectus proin nibh nisl condimentum id venenatis. Sed viverra ipsum nunc \r\naliquet bibendum enim facilisis gravida. Dui accumsan sit amet nulla \r\nfacilisi morbi. Tortor condimentum lacinia quis vel eros. Dignissim enim\r\n sit amet venenatis urna. Amet mattis vulputate enim nulla aliquet \r\nporttitor lacus. Etiam sit amet nisl purus in.\r\n    </p>\r\n    <p>\r\n        <span class=\"p-tittle-bold\">4. Ipsum dolor sit amet, consectetur adipiscing elit</span>\r\n        At imperdiet dui accumsan sit amet nulla. Rhoncus mattis rhoncus\r\n urna neque viverra justo nec. Urna molestie at elementum eu facilisis \r\nsed. Libero id faucibus nisl tincidunt eget nullam non nisi est. Eu \r\nfacilisis sed odio morbi quis.\r\n    </p>\r\n    <p>\r\n        <span class=\"p-tittle-bold\">5. Ipsum dolor sit amet, consectetur</span>\r\n        Mauris augue neque gravida in fermentum et sollicitudin ac orci.\r\n Lacus laoreet non curabitur gravida arcu. Amet facilisis magna etiam \r\ntempor orci eu. Amet mauris commodo quis imperdiet massa. Tristique et \r\negestas quis ipsum suspendisse ultrices gravida dictum. Tempor orci eu \r\nlobortis elementum. Vitae aliquet nec ullamcorper sit amet risus nullam.\r\n    </p>\r\n    <p>\r\n        <span class=\"p-tittle-bold\">6. Ipsum dolor sit amet</span>\r\n        Eget. Sed felis eget velit aliquet. Tincidunt arcu non sodales \r\nneque sodales. Etiam tempor orci eu lobortis elementum nibh tellus \r\nmolestie nunc. Donec et odio pellentesque diam volutpat commodo sed \r\negestas egestas. Molestie ac feugiat sed lectus vestibulum. Faucibus \r\nturpis in eu mi bibendum neque egestas.\r\n    </p>\r\n    <p>\r\n        <span class=\"p-tittle-bold\">7. Ipsum dolor sit amet, consectet</span>\r\n        Commodo odio aenean sed. Vulputate enim nulla aliquet porttitor \r\nlacus. Dictum varius duis at consectetur lorem donec massa sapien \r\nfaucibus. Nisi lacus sed viverra tellus. Porta non pulvinar neque \r\nlaoreet suspendisse interdum consectetur. Orci a scelerisque purus \r\nsemper eget duis at. Morbi tristique senectus et netus. Ipsum dolor sit \r\namet consectetur.\r\n    </p><p></p>', NULL, '2022-06-23 19:27:19', NULL, 'EDITOR', '<p>\r\n        <span class=\"p-tittle-bold\">1. Ipsum dolor sit amet</span>\r\n        Leo vel orci porta non pulvinar neque laoreet suspendisse. Vel \r\npretium lectus quam id leo in vitae. Sed faucibus turpis in eu mi \r\nbibendum. Orci a scelerisque purus semper. Cras tincidunt lobortis \r\nfeugiat vivamus at augue eget. Dolor purus non enim praesent elementum \r\nfacilisis leo vel. Quis risus sed vulputate odio ut enim. Mattis \r\npellentesque id nibh tortor id. Ac turpis egestas sed tempus. \r\nUllamcorper morbi tincidunt ornare massa. Integer quis auctor elit sed \r\nvulputate. Lorem dolor sed viverra ipsum nunc.\r\n    </p>\r\n    <p>\r\n        <span class=\"p-tittle-bold\">2. Consectetur adipiscing elit</span>\r\n        Sed lectus vestibulum mattis ullamcorper velit sed. Quis risus \r\nsed vulputate odio ut enim blandit. Id nibh tortor id aliquet lectus \r\nproin nibh. Diam ut venenatis tellus in metus. Scelerisque eleifend \r\ndonec pretium vulputate. Libero justo laoreet sit amet cursus sit amet \r\ndictum. Quis blandit turpis cursus in hac habitasse platea dictumst \r\nquisque. Elementum nibh tellus molestie nunc non blandit massa enim. \r\nLorem ipsum dolor sit amet consectetur adipiscing elit pellentesque. \r\nDictum varius duis at consectetur lorem donec. Amet consectetur \r\nadipiscing elit ut aliquam purus sit amet. Habitant morbi tristique \r\nsenectus et netus et malesuada fames. Mauris in aliquam sem fringilla. \r\nLectus proin nibh nisl condimentum id venenatis. Sed viverra ipsum nunc \r\naliquet bibendum enim facilisis gravida. Dui accumsan sit amet nulla \r\nfacilisi morbi. Tortor condimentum lacinia quis vel eros. Dignissim enim\r\n sit amet venenatis urna. Amet mattis vulputate enim nulla aliquet \r\nporttitor lacus. Etiam sit amet nisl purus in.\r\n    </p>\r\n    <p>\r\n        At imperdiet dui accumsan sit amet nulla. Rhoncus mattis rhoncus\r\n urna neque viverra justo nec. Urna molestie at elementum eu facilisis \r\nsed. Libero id faucibus nisl tincidunt eget nullam non nisi est. Eu \r\nfacilisis sed odio morbi quis commodo odio aenean sed. Vulputate enim \r\nnulla aliquet porttitor lacus. Dictum varius duis at consectetur lorem \r\ndonec massa sapien faucibus. Nisi lacus sed viverra tellus. Porta non \r\npulvinar neque laoreet suspendisse interdum consectetur. Orci a \r\nscelerisque purus semper eget duis at. Morbi tristique senectus et \r\nnetus. Ipsum dolor sit amet consectetur.\r\n    </p>\r\n    <p>\r\n        <span class=\"p-tittle-bold\">3. Ipsum dolor sit amet, consectetur adipiscing elit</span>\r\n        Mauris augue neque gravida in fermentum et sollicitudin ac orci.\r\n Lacus laoreet non curabitur gravida arcu. Amet facilisis magna etiam \r\ntempor orci eu:\r\n        Amet mauris commodo quis imperdiet massa. Tristique et egestas \r\nquis ipsum suspendisse ultrices gravida dictum. Tempor orci eu lobortis \r\nelementum.\r\n        Vitae aliquet nec ullamcorper sit amet risus nullam eget.\r\n        </p><ul><li>Sed felis eget velit aliquet.</li><li>Tincidunt arcu non sodales neque sodales.</li><li>Etiam tempor orci eu lobortis elementum nibh tellus molestie nunc.</li><li>Etiam tempor orci eu lobortis elementum nibh tellus molestie nunc.</li><li>Donec et odio pellentesque diam volutpat commodo sed egestas egestas.</li><li>Molestie ac feugiat sed lectus vestibulum. Faucibus turpis in eu mi bibendum neque egestas.</li></ul>\r\n    <p></p>\r\n\r\n    <p>\r\n        <span class=\"p-tittle-bold\">Lorem Ipsum dolor</span>\r\n        Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do \r\neiusmod tempor incididunt ut labore et dolore magna aliqua. Eget lorem \r\ndolor sed viverra ipsum nunc aliquet bibendum enim. Montes nascetur \r\nridiculus mus mauris vitae ultricies. Tortor at risus viverra adipiscing\r\n at in tellus. A arcu cursus vitae congue mauris rhoncus. Volutpat odio \r\nfacilisis mauris sit amet. Suspendisse faucibus interdum posuere lorem \r\nipsum dolor sit amet consectetur. Pharetra pharetra massa massa \r\nultricies mi. Sed risus ultricies tristique nulla. Mauris augue neque \r\ngravida in fermentum et sollicitudin ac orci. Fermentum et sollicitudin \r\nac orci phasellus egestas tellus rutrum.\r\n    </p>\r\n    <p>\r\n        <span class=\"p-tittle-bold\">1. Ipsum dolor sit amet, consectetur adipiscing elit</span>\r\n        Leo vel orci porta non pulvinar neque laoreet suspendisse. Vel \r\npretium lectus quam id leo in vitae. Sed faucibus turpis in eu mi \r\nbibendum. Orci a scelerisque purus semper. Cras tincidunt lobortis \r\nfeugiat vivamus at augue eget. Dolor purus non enim praesent elementum \r\nfacilisis leo vel. Quis risus sed vulputate odio ut enim. Mattis \r\npellentesque id nibh tortor id. Ac turpis egestas sed tempus. \r\nUllamcorper morbi tincidunt ornare massa. Integer quis auctor elit sed \r\nvulputate. Lorem dolor sed viverra ipsum nunc.\r\n    </p>\r\n    <p>\r\n        <span class=\"p-tittle-bold\">2. Sit amet, consectetur adipiscing elit</span>\r\n        Sed lectus vestibulum mattis ullamcorper velit sed. Quis risus \r\nsed vulputate odio ut enim blandit. Id nibh tortor id aliquet lectus \r\nproin nibh. Diam ut venenatis tellus in metus. Scelerisque eleifend \r\ndonec pretium vulputate. Libero justo laoreet sit amet cursus sit amet \r\ndictum. Quis blandit turpis cursus in hac habitasse platea dictumst \r\nquisque. Elementum nibh tellus molestie nunc non blandit massa enim. \r\nLorem ipsum dolor sit amet consectetur adipiscing elit pellentesque. \r\n    </p>\r\n    <p>\r\n        <span class=\"p-tittle-bold\">3. Consectetur adipiscing elit</span>\r\n        Dictum varius duis at consectetur lorem donec. Amet consectetur \r\nadipiscing elit ut aliquam purus sit amet. Habitant morbi tristique \r\nsenectus et netus et malesuada fames. Mauris in aliquam sem fringilla. \r\nLectus proin nibh nisl condimentum id venenatis. Sed viverra ipsum nunc \r\naliquet bibendum enim facilisis gravida. Dui accumsan sit amet nulla \r\nfacilisi morbi. Tortor condimentum lacinia quis vel eros. Dignissim enim\r\n sit amet venenatis urna. Amet mattis vulputate enim nulla aliquet \r\nporttitor lacus. Etiam sit amet nisl purus in.\r\n    </p>\r\n    <p>\r\n        <span class=\"p-tittle-bold\">4. Ipsum dolor sit amet, consectetur adipiscing elit</span>\r\n        At imperdiet dui accumsan sit amet nulla. Rhoncus mattis rhoncus\r\n urna neque viverra justo nec. Urna molestie at elementum eu facilisis \r\nsed. Libero id faucibus nisl tincidunt eget nullam non nisi est. Eu \r\nfacilisis sed odio morbi quis.\r\n    </p>\r\n    <p>\r\n        <span class=\"p-tittle-bold\">5. Ipsum dolor sit amet, consectetur</span>\r\n        Mauris augue neque gravida in fermentum et sollicitudin ac orci.\r\n Lacus laoreet non curabitur gravida arcu. Amet facilisis magna etiam \r\ntempor orci eu. Amet mauris commodo quis imperdiet massa. Tristique et \r\negestas quis ipsum suspendisse ultrices gravida dictum. Tempor orci eu \r\nlobortis elementum. Vitae aliquet nec ullamcorper sit amet risus nullam.\r\n    </p>\r\n    <p>\r\n        <span class=\"p-tittle-bold\">6. Ipsum dolor sit amet</span>\r\n        Eget. Sed felis eget velit aliquet. Tincidunt arcu non sodales \r\nneque sodales. Etiam tempor orci eu lobortis elementum nibh tellus \r\nmolestie nunc. Donec et odio pellentesque diam volutpat commodo sed \r\negestas egestas. Molestie ac feugiat sed lectus vestibulum. Faucibus \r\nturpis in eu mi bibendum neque egestas.\r\n    </p>\r\n    <p>\r\n        <span class=\"p-tittle-bold\">7. Ipsum dolor sit amet, consectet</span>\r\n        Commodo odio aenean sed. Vulputate enim nulla aliquet porttitor \r\nlacus. Dictum varius duis at consectetur lorem donec massa sapien \r\nfaucibus. Nisi lacus sed viverra tellus. Porta non pulvinar neque \r\nlaoreet suspendisse interdum consectetur. Orci a scelerisque purus \r\nsemper eget duis at. Morbi tristique senectus et netus. Ipsum dolor sit \r\namet consectetur.\r\n    </p><p></p>'),
(18, 'POLITICAS_DATOS_PERSONALES', NULL, NULL, NULL, NULL, 'EDITOR', NULL),
(19, 'OBJETIVO', 'Velit dignissim sodales ut eu sem integer. Et leo duis ut diam quam. Viverra suspendisse potenti nullam ac tortor vitae purus faucibus ornare. Facilisis volutpat est velit egestas dui id. Tellus id interdum velit laoreet id donec ultrices. Urna molestie at elementum eu facilisis sed odio. Tempor nec feugiat nisl pretium. Porttitor lacus luctus accumsan tortor posuere ac ut consequat. ', NULL, NULL, NULL, 'TEXTAREA', 'Velit dignissim sodales ut eu sem integer. Et leo duis ut diam quam. Viverra suspendisse potenti nullam ac tortor vitae purus faucibus ornare. Facilisis volutpat est velit egestas dui id. Tellus id interdum velit laoreet id donec ultrices. Urna molestie at elementum eu facilisis sed odio. Tempor nec feugiat nisl pretium. Porttitor lacus luctus accumsan tortor posuere ac ut consequat. ');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `departamentos`
--

CREATE TABLE `departamentos` (
  `id` int(11) NOT NULL,
  `nombre` varchar(256) DEFAULT NULL,
  `codigo` varchar(64) DEFAULT NULL,
  `zona_pais_id` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `departamentos`
--

INSERT INTO `departamentos` (`id`, `nombre`, `codigo`, `zona_pais_id`) VALUES
(1, 'Ahuachapán', 'SV-AH', 1),
(2, 'Santa Ana', 'SV-SA', 1),
(3, 'Sonsonate', 'SV-SO', 1),
(4, 'La Libertad', 'SV-LI', 2),
(5, 'Chalatenango', 'SV-CH', 2),
(6, 'San Salvador', 'SV-SS', 2),
(7, 'Cuscatlán', 'SV-CU', 3),
(8, 'La Paz', 'SV-PA', 3),
(9, 'Cabañas', 'SV-CA', 3),
(10, 'San Vicente', 'SV-SV', 3),
(11, 'Usulután', 'SV-US', 4),
(12, 'Morazán', 'SV-MO', 4),
(13, 'San Miguel', 'SV-SM', 4),
(14, 'La Unión', 'SV-UN', 4);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `documentos_emisiones`
--

CREATE TABLE `documentos_emisiones` (
  `id` int(11) NOT NULL,
  `nombre_ES` varchar(255) DEFAULT NULL,
  `nombre_EN` varchar(255) DEFAULT NULL,
  `url_ES` varchar(255) DEFAULT NULL,
  `url_EN` varchar(255) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `deleted_at` datetime DEFAULT NULL,
  `emision_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Volcado de datos para la tabla `documentos_emisiones`
--

INSERT INTO `documentos_emisiones` (`id`, `nombre_ES`, `nombre_EN`, `url_ES`, `url_EN`, `created_at`, `updated_at`, `deleted_at`, `emision_id`) VALUES
(7, 'Documento de prueba 1', 'Test Document 1', 'documentos_emision/ES/EmisionTest.pdf', 'documentos_emision/EN/EmisionTest.pdf', '2022-06-21 14:31:13', '2022-06-21 14:31:13', NULL, 9),
(8, 'Documento de prueba 1', 'Test Document 1', 'documentos_emision/ES/EmisionTest.pdf', 'documentos_emision/EN/EmisionTest.pdf', '2022-06-21 14:31:13', '2022-06-21 14:31:13', NULL, 10),
(9, 'Documento de prueba 1', 'Test Document 1', 'documentos_emision/ES/EmisionTest.pdf', 'documentos_emision/EN/EmisionTest.pdf', '2022-06-21 14:31:13', '2022-06-21 14:31:13', NULL, 11),
(10, 'Documento de prueba 1', 'Test Document 1', 'documentos_emision/ES/EmisionTest.pdf', 'documentos_emision/EN/EmisionTest.pdf', '2022-06-21 14:31:13', '2022-06-21 14:31:13', NULL, 12),
(11, 'Documento de prueba 1', 'Test Document 1', 'documentos_emision/ES/EmisionTest.pdf', 'documentos_emision/EN/EmisionTest.pdf', '2022-06-21 14:31:13', '2022-06-21 14:31:13', NULL, 13),
(12, 'Documento de prueba 1', 'Test Document 1', 'documentos_emision/ES/EmisionTest.pdf', 'documentos_emision/EN/EmisionTest.pdf', '2022-06-21 14:31:13', '2022-06-21 14:31:13', NULL, 14),
(13, 'Documento de prueba 1', 'Test Document 1', 'documentos_emision/ES/EmisionTest.pdf', 'documentos_emision/EN/EmisionTest.pdf', '2022-06-21 14:31:13', '2022-06-21 14:31:13', NULL, 15),
(14, 'Documento de prueba 1', 'Test Document 1', 'documentos_emision/ES/EmisionTest.pdf', 'documentos_emision/EN/EmisionTest.pdf', '2022-06-21 14:31:13', '2022-06-21 14:31:13', NULL, 16),
(15, 'Documento de prueba 1', 'Test Document 1', 'documentos_emision/ES/EmisionTest.pdf', 'documentos_emision/EN/EmisionTest.pdf', '2022-06-21 14:31:13', '2022-06-21 14:31:13', NULL, 174),
(16, 'Documento de prueba 1', 'Test Document 1', 'documentos_emision/ES/EmisionTest.pdf', 'documentos_emision/EN/EmisionTest.pdf', '2022-06-21 14:31:13', '2022-06-21 14:31:13', NULL, 19),
(17, 'Documento de prueba 1', 'Test Document 1', 'documentos_emision/ES/EmisionTest.pdf', 'documentos_emision/EN/EmisionTest.pdf', '2022-06-21 14:31:13', '2022-06-21 14:31:13', NULL, 20),
(18, 'Documento de prueba 1', 'Test Document 1', 'documentos_emision/ES/EmisionTest.pdf', 'documentos_emision/EN/EmisionTest.pdf', '2022-06-21 14:31:13', '2022-06-21 14:31:13', NULL, 21),
(19, 'Documento de prueba 1', 'Test Document 1', 'documentos_emision/ES/EmisionTest.pdf', 'documentos_emision/EN/EmisionTest.pdf', '2022-06-21 14:31:13', '2022-06-21 14:31:13', NULL, 22),
(20, 'Documento de prueba 1', 'Test Document 1', 'documentos_emision/ES/EmisionTest.pdf', 'documentos_emision/EN/EmisionTest.pdf', '2022-06-21 14:31:13', '2022-06-21 14:31:13', NULL, 23),
(21, 'Documento de prueba 1', 'Test Document 1', 'documentos_emision/ES/EmisionTest.pdf', 'documentos_emision/EN/EmisionTest.pdf', '2022-06-21 14:31:13', '2022-06-21 14:31:13', NULL, 24),
(22, 'Documento de prueba 1', 'Test Document 1', 'documentos_emision/ES/EmisionTest.pdf', 'documentos_emision/EN/EmisionTest.pdf', '2022-06-21 14:31:13', '2022-06-21 14:31:13', NULL, 25),
(23, 'Documento de prueba 1', 'Test Document 1', 'documentos_emision/ES/EmisionTest.pdf', 'documentos_emision/EN/EmisionTest.pdf', '2022-06-21 14:31:13', '2022-06-21 14:31:13', NULL, 26),
(24, 'Documento de prueba 1', 'Test Document 1', 'documentos_emision/ES/EmisionTest.pdf', 'documentos_emision/EN/EmisionTest.pdf', '2022-06-21 14:31:13', '2022-06-21 14:31:13', NULL, 27),
(25, 'Documento de prueba 1', 'Test Document 1', 'documentos_emision/ES/EmisionTest.pdf', 'documentos_emision/EN/EmisionTest.pdf', '2022-06-21 14:31:14', '2022-06-21 14:31:14', NULL, 28),
(26, 'Documento de prueba 1', 'Test Document 1', 'documentos_emision/ES/EmisionTest.pdf', 'documentos_emision/EN/EmisionTest.pdf', '2022-06-21 14:31:14', '2022-06-21 14:31:14', NULL, 29),
(27, 'Documento de prueba 1', 'Test Document 1', 'documentos_emision/ES/EmisionTest.pdf', 'documentos_emision/EN/EmisionTest.pdf', '2022-06-21 14:31:14', '2022-06-21 14:31:14', NULL, 30),
(28, 'Documento de prueba 1', 'Test Document 1', 'documentos_emision/ES/EmisionTest.pdf', 'documentos_emision/EN/EmisionTest.pdf', '2022-06-21 14:31:14', '2022-06-21 14:31:14', NULL, 31),
(29, 'Documento de prueba 1', 'Test Document 1', 'documentos_emision/ES/EmisionTest.pdf', 'documentos_emision/EN/EmisionTest.pdf', '2022-06-21 14:31:14', '2022-06-21 14:31:14', NULL, 32),
(30, 'Documento de prueba 1', 'Test Document 1', 'documentos_emision/ES/EmisionTest.pdf', 'documentos_emision/EN/EmisionTest.pdf', '2022-06-21 14:31:14', '2022-06-21 14:31:14', NULL, 33),
(31, 'Documento de prueba 1', 'Test Document 1', 'documentos_emision/ES/EmisionTest.pdf', 'documentos_emision/EN/EmisionTest.pdf', '2022-06-21 14:31:14', '2022-06-21 14:31:14', NULL, 34),
(32, 'Documento de prueba 1', 'Test Document 1', 'documentos_emision/ES/EmisionTest.pdf', 'documentos_emision/EN/EmisionTest.pdf', '2022-06-21 14:31:14', '2022-06-21 14:31:14', NULL, 35),
(33, 'Documento de prueba 1', 'Test Document 1', 'documentos_emision/ES/EmisionTest.pdf', 'documentos_emision/EN/EmisionTest.pdf', '2022-06-21 14:31:14', '2022-06-21 14:31:14', NULL, 36),
(34, 'Documento de prueba 1', 'Test Document 1', 'documentos_emision/ES/EmisionTest.pdf', 'documentos_emision/EN/EmisionTest.pdf', '2022-06-21 14:31:14', '2022-06-21 14:31:14', NULL, 37),
(35, 'Documento de prueba 1', 'Test Document 1', 'documentos_emision/ES/EmisionTest.pdf', 'documentos_emision/EN/EmisionTest.pdf', '2022-06-21 14:31:14', '2022-06-21 14:31:14', NULL, 38),
(36, 'Documento de prueba 1', 'Test Document 1', 'documentos_emision/ES/EmisionTest.pdf', 'documentos_emision/EN/EmisionTest.pdf', '2022-06-21 14:31:14', '2022-06-21 14:31:14', NULL, 39),
(37, 'Documento de prueba 1', 'Test Document 1', 'documentos_emision/ES/EmisionTest.pdf', 'documentos_emision/EN/EmisionTest.pdf', '2022-06-21 14:31:14', '2022-06-21 14:31:14', NULL, 40),
(38, 'Documento de prueba 1', 'Test Document 1', 'documentos_emision/ES/EmisionTest.pdf', 'documentos_emision/EN/EmisionTest.pdf', '2022-06-21 14:31:14', '2022-06-21 14:31:14', NULL, 49),
(39, 'Documento de prueba 1', 'Test Document 1', 'documentos_emision/ES/EmisionTest.pdf', 'documentos_emision/EN/EmisionTest.pdf', '2022-06-21 14:31:14', '2022-06-21 14:31:14', NULL, 50),
(40, 'Documento de prueba 1', 'Test Document 1', 'documentos_emision/ES/EmisionTest.pdf', 'documentos_emision/EN/EmisionTest.pdf', '2022-06-21 14:31:14', '2022-06-21 14:31:14', NULL, 51),
(41, 'Documento de prueba 1', 'Test Document 1', 'documentos_emision/ES/EmisionTest.pdf', 'documentos_emision/EN/EmisionTest.pdf', '2022-06-21 14:31:14', '2022-06-21 14:31:14', NULL, 52),
(42, 'Documento de prueba 1', 'Test Document 1', 'documentos_emision/ES/EmisionTest.pdf', 'documentos_emision/EN/EmisionTest.pdf', '2022-06-21 14:31:14', '2022-06-21 14:31:14', NULL, 53),
(43, 'Documento de prueba 1', 'Test Document 1', 'documentos_emision/ES/EmisionTest.pdf', 'documentos_emision/EN/EmisionTest.pdf', '2022-06-21 14:31:14', '2022-06-21 14:31:14', NULL, 54),
(44, 'Documento de prueba 1', 'Test Document 1', 'documentos_emision/ES/EmisionTest.pdf', 'documentos_emision/EN/EmisionTest.pdf', '2022-06-21 14:31:14', '2022-06-21 14:31:14', NULL, 55),
(45, 'Documento de prueba 1', 'Test Document 1', 'documentos_emision/ES/EmisionTest.pdf', 'documentos_emision/EN/EmisionTest.pdf', '2022-06-21 14:31:14', '2022-06-21 14:31:14', NULL, 56),
(46, 'Documento de prueba 1', 'Test Document 1', 'documentos_emision/ES/EmisionTest.pdf', 'documentos_emision/EN/EmisionTest.pdf', '2022-06-21 14:31:14', '2022-06-21 14:31:14', NULL, 161),
(47, 'Documento de prueba 1', 'Test Document 1', 'documentos_emision/ES/EmisionTest.pdf', 'documentos_emision/EN/EmisionTest.pdf', '2022-06-21 14:31:14', '2022-06-21 14:31:14', NULL, 162),
(48, 'Documento de prueba 1', 'Test Document 1', 'documentos_emision/ES/EmisionTest.pdf', 'documentos_emision/EN/EmisionTest.pdf', '2022-06-21 14:31:14', '2022-06-21 14:31:14', NULL, 163),
(49, 'Documento de prueba 1', 'Test Document 1', 'documentos_emision/ES/EmisionTest.pdf', 'documentos_emision/EN/EmisionTest.pdf', '2022-06-21 14:31:14', '2022-06-21 14:31:14', NULL, 164),
(50, 'Documento de prueba 1', 'Test Document 1', 'documentos_emision/ES/EmisionTest.pdf', 'documentos_emision/EN/EmisionTest.pdf', '2022-06-21 14:31:14', '2022-06-21 14:31:14', NULL, 165),
(51, 'Documento de prueba 1', 'Test Document 1', 'documentos_emision/ES/EmisionTest.pdf', 'documentos_emision/EN/EmisionTest.pdf', '2022-06-21 14:31:14', '2022-06-21 14:31:14', NULL, 166),
(52, 'Documento de prueba 1', 'Test Document 1', 'documentos_emision/ES/EmisionTest.pdf', 'documentos_emision/EN/EmisionTest.pdf', '2022-06-21 14:31:14', '2022-06-21 14:31:14', NULL, 167),
(53, 'Documento de prueba 1', 'Test Document 1', 'documentos_emision/ES/EmisionTest.pdf', 'documentos_emision/EN/EmisionTest.pdf', '2022-06-21 14:31:14', '2022-06-21 14:31:14', NULL, 168),
(54, 'Documento de prueba 1', 'Test Document 1', 'documentos_emision/ES/EmisionTest.pdf', 'documentos_emision/EN/EmisionTest.pdf', '2022-06-21 14:31:14', '2022-06-21 14:31:14', NULL, 170),
(55, 'Documento de prueba 1', 'Test Document 1', 'documentos_emision/ES/EmisionTest.pdf', 'documentos_emision/EN/EmisionTest.pdf', '2022-06-21 14:31:14', '2022-06-21 14:31:14', NULL, 171),
(56, 'Documento de prueba 1', 'Test Document 1', 'documentos_emision/ES/EmisionTest.pdf', 'documentos_emision/EN/EmisionTest.pdf', '2022-06-21 14:31:14', '2022-06-21 14:31:14', NULL, 172),
(57, 'Documento de prueba 1', 'Test Document 1', 'documentos_emision/ES/EmisionTest.pdf', 'documentos_emision/EN/EmisionTest.pdf', '2022-06-21 14:31:15', '2022-06-21 14:31:15', NULL, 1),
(58, 'Documento de prueba 1', 'Test Document 1', 'documentos_emision/ES/EmisionTest.pdf', 'documentos_emision/EN/EmisionTest.pdf', '2022-06-21 14:31:15', '2022-06-21 14:31:15', NULL, 2),
(59, 'Documento de prueba 1', 'Test Document 1', 'documentos_emision/ES/EmisionTest.pdf', 'documentos_emision/EN/EmisionTest.pdf', '2022-06-21 14:31:15', '2022-06-21 14:31:15', NULL, 3),
(60, 'Documento de prueba 1', 'Test Document 1', 'documentos_emision/ES/EmisionTest.pdf', 'documentos_emision/EN/EmisionTest.pdf', '2022-06-21 14:31:15', '2022-06-21 14:31:15', NULL, 4),
(61, 'Documento de prueba 1', 'Test Document 1', 'documentos_emision/ES/EmisionTest.pdf', 'documentos_emision/EN/EmisionTest.pdf', '2022-06-21 14:31:15', '2022-06-21 14:31:15', NULL, 5),
(62, 'Documento de prueba 1', 'Test Document 1', 'documentos_emision/ES/EmisionTest.pdf', 'documentos_emision/EN/EmisionTest.pdf', '2022-06-21 14:31:15', '2022-06-21 14:31:15', NULL, 6),
(63, 'Documento de prueba 1', 'Test Document 1', 'documentos_emision/ES/EmisionTest.pdf', 'documentos_emision/EN/EmisionTest.pdf', '2022-06-21 14:31:15', '2022-06-21 14:31:15', NULL, 7),
(64, 'Documento de prueba 1', 'Test Document 1', 'documentos_emision/ES/EmisionTest.pdf', 'documentos_emision/EN/EmisionTest.pdf', '2022-06-21 14:31:15', '2022-06-21 14:31:15', NULL, 8),
(65, 'Documento de prueba 1', 'Test Document 1', 'documentos_emision/ES/EmisionTest.pdf', 'documentos_emision/EN/EmisionTest.pdf', '2022-06-21 14:31:15', '2022-06-21 14:31:15', NULL, 41),
(66, 'Documento de prueba 1', 'Test Document 1', 'documentos_emision/ES/EmisionTest.pdf', 'documentos_emision/EN/EmisionTest.pdf', '2022-06-21 14:31:15', '2022-06-21 14:31:15', NULL, 42),
(67, 'Documento de prueba 1', 'Test Document 1', 'documentos_emision/ES/EmisionTest.pdf', 'documentos_emision/EN/EmisionTest.pdf', '2022-06-21 14:31:15', '2022-06-21 14:31:15', NULL, 43),
(68, 'Documento de prueba 1', 'Test Document 1', 'documentos_emision/ES/EmisionTest.pdf', 'documentos_emision/EN/EmisionTest.pdf', '2022-06-21 14:31:15', '2022-06-21 14:31:15', NULL, 44),
(69, 'Documento de prueba 1', 'Test Document 1', 'documentos_emision/ES/EmisionTest.pdf', 'documentos_emision/EN/EmisionTest.pdf', '2022-06-21 14:31:15', '2022-06-21 14:31:15', NULL, 45),
(70, 'Documento de prueba 1', 'Test Document 1', 'documentos_emision/ES/EmisionTest.pdf', 'documentos_emision/EN/EmisionTest.pdf', '2022-06-21 14:31:15', '2022-06-21 14:31:15', NULL, 46),
(71, 'Documento de prueba 1', 'Test Document 1', 'documentos_emision/ES/EmisionTest.pdf', 'documentos_emision/EN/EmisionTest.pdf', '2022-06-21 14:31:15', '2022-06-21 14:31:15', NULL, 47),
(72, 'Documento de prueba 1', 'Test Document 1', 'documentos_emision/ES/EmisionTest.pdf', 'documentos_emision/EN/EmisionTest.pdf', '2022-06-21 14:31:15', '2022-06-21 14:31:15', NULL, 48),
(73, 'Documento de prueba 1', 'Test Document 1', 'documentos_emision/ES/EmisionTest.pdf', 'documentos_emision/EN/EmisionTest.pdf', '2022-06-21 14:31:15', '2022-06-21 14:31:15', NULL, 57),
(74, 'Documento de prueba 1', 'Test Document 1', 'documentos_emision/ES/EmisionTest.pdf', 'documentos_emision/EN/EmisionTest.pdf', '2022-06-21 14:31:15', '2022-06-21 14:31:15', NULL, 58),
(75, 'Documento de prueba 1', 'Test Document 1', 'documentos_emision/ES/EmisionTest.pdf', 'documentos_emision/EN/EmisionTest.pdf', '2022-06-21 14:31:15', '2022-06-21 14:31:15', NULL, 59),
(76, 'Documento de prueba 1', 'Test Document 1', 'documentos_emision/ES/EmisionTest.pdf', 'documentos_emision/EN/EmisionTest.pdf', '2022-06-21 14:31:15', '2022-06-21 14:31:15', NULL, 60),
(77, 'Documento de prueba 1', 'Test Document 1', 'documentos_emision/ES/EmisionTest.pdf', 'documentos_emision/EN/EmisionTest.pdf', '2022-06-21 14:31:15', '2022-06-21 14:31:15', NULL, 61),
(78, 'Documento de prueba 1', 'Test Document 1', 'documentos_emision/ES/EmisionTest.pdf', 'documentos_emision/EN/EmisionTest.pdf', '2022-06-21 14:31:15', '2022-06-21 14:31:15', NULL, 62),
(79, 'Documento de prueba 1', 'Test Document 1', 'documentos_emision/ES/EmisionTest.pdf', 'documentos_emision/EN/EmisionTest.pdf', '2022-06-21 14:31:15', '2022-06-21 14:31:15', NULL, 63),
(80, 'Documento de prueba 1', 'Test Document 1', 'documentos_emision/ES/EmisionTest.pdf', 'documentos_emision/EN/EmisionTest.pdf', '2022-06-21 14:31:15', '2022-06-21 14:31:15', NULL, 64),
(81, 'Documento de prueba 1', 'Test Document 1', 'documentos_emision/ES/EmisionTest.pdf', 'documentos_emision/EN/EmisionTest.pdf', '2022-06-21 14:31:16', '2022-06-21 14:31:16', NULL, 65),
(82, 'Documento de prueba 1', 'Test Document 1', 'documentos_emision/ES/EmisionTest.pdf', 'documentos_emision/EN/EmisionTest.pdf', '2022-06-21 14:31:16', '2022-06-21 14:31:16', NULL, 66),
(83, 'Documento de prueba 1', 'Test Document 1', 'documentos_emision/ES/EmisionTest.pdf', 'documentos_emision/EN/EmisionTest.pdf', '2022-06-21 14:31:16', '2022-06-21 14:31:16', NULL, 67),
(84, 'Documento de prueba 1', 'Test Document 1', 'documentos_emision/ES/EmisionTest.pdf', 'documentos_emision/EN/EmisionTest.pdf', '2022-06-21 14:31:16', '2022-06-21 14:31:16', NULL, 68),
(85, 'Documento de prueba 1', 'Test Document 1', 'documentos_emision/ES/EmisionTest.pdf', 'documentos_emision/EN/EmisionTest.pdf', '2022-06-21 14:31:16', '2022-06-21 14:31:16', NULL, 69),
(86, 'Documento de prueba 1', 'Test Document 1', 'documentos_emision/ES/EmisionTest.pdf', 'documentos_emision/EN/EmisionTest.pdf', '2022-06-21 14:31:16', '2022-06-21 14:31:16', NULL, 70),
(87, 'Documento de prueba 1', 'Test Document 1', 'documentos_emision/ES/EmisionTest.pdf', 'documentos_emision/EN/EmisionTest.pdf', '2022-06-21 14:31:16', '2022-06-21 14:31:16', NULL, 71),
(88, 'Documento de prueba 1', 'Test Document 1', 'documentos_emision/ES/EmisionTest.pdf', 'documentos_emision/EN/EmisionTest.pdf', '2022-06-21 14:31:16', '2022-06-21 14:31:16', NULL, 72),
(89, 'Documento de prueba 1', 'Test Document 1', 'documentos_emision/ES/EmisionTest.pdf', 'documentos_emision/EN/EmisionTest.pdf', '2022-06-21 14:31:16', '2022-06-21 14:31:16', NULL, 73),
(90, 'Documento de prueba 1', 'Test Document 1', 'documentos_emision/ES/EmisionTest.pdf', 'documentos_emision/EN/EmisionTest.pdf', '2022-06-21 14:31:16', '2022-06-21 14:31:16', NULL, 74),
(91, 'Documento de prueba 1', 'Test Document 1', 'documentos_emision/ES/EmisionTest.pdf', 'documentos_emision/EN/EmisionTest.pdf', '2022-06-21 14:31:16', '2022-06-21 14:31:16', NULL, 75),
(92, 'Documento de prueba 1', 'Test Document 1', 'documentos_emision/ES/EmisionTest.pdf', 'documentos_emision/EN/EmisionTest.pdf', '2022-06-21 14:31:16', '2022-06-21 14:31:16', NULL, 76),
(93, 'Documento de prueba 1', 'Test Document 1', 'documentos_emision/ES/EmisionTest.pdf', 'documentos_emision/EN/EmisionTest.pdf', '2022-06-21 14:31:16', '2022-06-21 14:31:16', NULL, 77),
(94, 'Documento de prueba 1', 'Test Document 1', 'documentos_emision/ES/EmisionTest.pdf', 'documentos_emision/EN/EmisionTest.pdf', '2022-06-21 14:31:16', '2022-06-21 14:31:16', NULL, 78),
(95, 'Documento de prueba 1', 'Test Document 1', 'documentos_emision/ES/EmisionTest.pdf', 'documentos_emision/EN/EmisionTest.pdf', '2022-06-21 14:31:16', '2022-06-21 14:31:16', NULL, 79),
(96, 'Documento de prueba 1', 'Test Document 1', 'documentos_emision/ES/EmisionTest.pdf', 'documentos_emision/EN/EmisionTest.pdf', '2022-06-21 14:31:16', '2022-06-21 14:31:16', NULL, 80),
(97, 'Documento de prueba 1', 'Test Document 1', 'documentos_emision/ES/EmisionTest.pdf', 'documentos_emision/EN/EmisionTest.pdf', '2022-06-21 14:31:16', '2022-06-21 14:31:16', NULL, 81),
(98, 'Documento de prueba 1', 'Test Document 1', 'documentos_emision/ES/EmisionTest.pdf', 'documentos_emision/EN/EmisionTest.pdf', '2022-06-21 14:31:16', '2022-06-21 14:31:16', NULL, 82),
(99, 'Documento de prueba 1', 'Test Document 1', 'documentos_emision/ES/EmisionTest.pdf', 'documentos_emision/EN/EmisionTest.pdf', '2022-06-21 14:31:16', '2022-06-21 14:31:16', NULL, 83),
(100, 'Documento de prueba 1', 'Test Document 1', 'documentos_emision/ES/EmisionTest.pdf', 'documentos_emision/EN/EmisionTest.pdf', '2022-06-21 14:31:16', '2022-06-21 14:31:16', NULL, 84),
(101, 'Documento de prueba 1', 'Test Document 1', 'documentos_emision/ES/EmisionTest.pdf', 'documentos_emision/EN/EmisionTest.pdf', '2022-06-21 14:31:16', '2022-06-21 14:31:16', NULL, 85),
(102, 'Documento de prueba 1', 'Test Document 1', 'documentos_emision/ES/EmisionTest.pdf', 'documentos_emision/EN/EmisionTest.pdf', '2022-06-21 14:31:16', '2022-06-21 14:31:16', NULL, 86),
(103, 'Documento de prueba 1', 'Test Document 1', 'documentos_emision/ES/EmisionTest.pdf', 'documentos_emision/EN/EmisionTest.pdf', '2022-06-21 14:31:16', '2022-06-21 14:31:16', NULL, 87),
(104, 'Documento de prueba 1', 'Test Document 1', 'documentos_emision/ES/EmisionTest.pdf', 'documentos_emision/EN/EmisionTest.pdf', '2022-06-21 14:31:16', '2022-06-21 14:31:16', NULL, 88),
(105, 'Documento de prueba 1', 'Test Document 1', 'documentos_emision/ES/EmisionTest.pdf', 'documentos_emision/EN/EmisionTest.pdf', '2022-06-21 14:31:17', '2022-06-21 14:31:17', NULL, 89),
(106, 'Documento de prueba 1', 'Test Document 1', 'documentos_emision/ES/EmisionTest.pdf', 'documentos_emision/EN/EmisionTest.pdf', '2022-06-21 14:31:17', '2022-06-21 14:31:17', NULL, 90),
(107, 'Documento de prueba 1', 'Test Document 1', 'documentos_emision/ES/EmisionTest.pdf', 'documentos_emision/EN/EmisionTest.pdf', '2022-06-21 14:31:17', '2022-06-21 14:31:17', NULL, 91),
(108, 'Documento de prueba 1', 'Test Document 1', 'documentos_emision/ES/EmisionTest.pdf', 'documentos_emision/EN/EmisionTest.pdf', '2022-06-21 14:31:17', '2022-06-21 14:31:17', NULL, 92),
(109, 'Documento de prueba 1', 'Test Document 1', 'documentos_emision/ES/EmisionTest.pdf', 'documentos_emision/EN/EmisionTest.pdf', '2022-06-21 14:31:17', '2022-06-21 14:31:17', NULL, 93),
(110, 'Documento de prueba 1', 'Test Document 1', 'documentos_emision/ES/EmisionTest.pdf', 'documentos_emision/EN/EmisionTest.pdf', '2022-06-21 14:31:17', '2022-06-21 14:31:17', NULL, 94),
(111, 'Documento de prueba 1', 'Test Document 1', 'documentos_emision/ES/EmisionTest.pdf', 'documentos_emision/EN/EmisionTest.pdf', '2022-06-21 14:31:17', '2022-06-21 14:31:17', NULL, 95),
(112, 'Documento de prueba 1', 'Test Document 1', 'documentos_emision/ES/EmisionTest.pdf', 'documentos_emision/EN/EmisionTest.pdf', '2022-06-21 14:31:17', '2022-06-21 14:31:17', NULL, 96),
(113, 'Documento de prueba 1', 'Test Document 1', 'documentos_emision/ES/EmisionTest.pdf', 'documentos_emision/EN/EmisionTest.pdf', '2022-06-21 14:31:17', '2022-06-21 14:31:17', NULL, 97),
(114, 'Documento de prueba 1', 'Test Document 1', 'documentos_emision/ES/EmisionTest.pdf', 'documentos_emision/EN/EmisionTest.pdf', '2022-06-21 14:31:17', '2022-06-21 14:31:17', NULL, 98),
(115, 'Documento de prueba 1', 'Test Document 1', 'documentos_emision/ES/EmisionTest.pdf', 'documentos_emision/EN/EmisionTest.pdf', '2022-06-21 14:31:17', '2022-06-21 14:31:17', NULL, 99),
(116, 'Documento de prueba 1', 'Test Document 1', 'documentos_emision/ES/EmisionTest.pdf', 'documentos_emision/EN/EmisionTest.pdf', '2022-06-21 14:31:17', '2022-06-21 14:31:17', NULL, 100),
(117, 'Documento de prueba 1', 'Test Document 1', 'documentos_emision/ES/EmisionTest.pdf', 'documentos_emision/EN/EmisionTest.pdf', '2022-06-21 14:31:17', '2022-06-21 14:31:17', NULL, 101),
(118, 'Documento de prueba 1', 'Test Document 1', 'documentos_emision/ES/EmisionTest.pdf', 'documentos_emision/EN/EmisionTest.pdf', '2022-06-21 14:31:17', '2022-06-21 14:31:17', NULL, 102),
(119, 'Documento de prueba 1', 'Test Document 1', 'documentos_emision/ES/EmisionTest.pdf', 'documentos_emision/EN/EmisionTest.pdf', '2022-06-21 14:31:17', '2022-06-21 14:31:17', NULL, 103),
(120, 'Documento de prueba 1', 'Test Document 1', 'documentos_emision/ES/EmisionTest.pdf', 'documentos_emision/EN/EmisionTest.pdf', '2022-06-21 14:31:17', '2022-06-21 14:31:17', NULL, 104),
(121, 'Documento de prueba 1', 'Test Document 1', 'documentos_emision/ES/EmisionTest.pdf', 'documentos_emision/EN/EmisionTest.pdf', '2022-06-21 14:31:17', '2022-06-21 14:31:17', NULL, 105),
(122, 'Documento de prueba 1', 'Test Document 1', 'documentos_emision/ES/EmisionTest.pdf', 'documentos_emision/EN/EmisionTest.pdf', '2022-06-21 14:31:17', '2022-06-21 14:31:17', NULL, 106),
(123, 'Documento de prueba 1', 'Test Document 1', 'documentos_emision/ES/EmisionTest.pdf', 'documentos_emision/EN/EmisionTest.pdf', '2022-06-21 14:31:17', '2022-06-21 14:31:17', NULL, 107),
(124, 'Documento de prueba 1', 'Test Document 1', 'documentos_emision/ES/EmisionTest.pdf', 'documentos_emision/EN/EmisionTest.pdf', '2022-06-21 14:31:17', '2022-06-21 14:31:17', NULL, 108),
(125, 'Documento de prueba 1', 'Test Document 1', 'documentos_emision/ES/EmisionTest.pdf', 'documentos_emision/EN/EmisionTest.pdf', '2022-06-21 14:31:17', '2022-06-21 14:31:17', NULL, 109),
(126, 'Documento de prueba 1', 'Test Document 1', 'documentos_emision/ES/EmisionTest.pdf', 'documentos_emision/EN/EmisionTest.pdf', '2022-06-21 14:31:17', '2022-06-21 14:31:17', NULL, 110),
(127, 'Documento de prueba 1', 'Test Document 1', 'documentos_emision/ES/EmisionTest.pdf', 'documentos_emision/EN/EmisionTest.pdf', '2022-06-21 14:31:17', '2022-06-21 14:31:17', NULL, 111),
(128, 'Documento de prueba 1', 'Test Document 1', 'documentos_emision/ES/EmisionTest.pdf', 'documentos_emision/EN/EmisionTest.pdf', '2022-06-21 14:31:17', '2022-06-21 14:31:17', NULL, 112),
(129, 'Documento de prueba 1', 'Test Document 1', 'documentos_emision/ES/EmisionTest.pdf', 'documentos_emision/EN/EmisionTest.pdf', '2022-06-21 14:31:18', '2022-06-21 14:31:18', NULL, 113),
(130, 'Documento de prueba 1', 'Test Document 1', 'documentos_emision/ES/EmisionTest.pdf', 'documentos_emision/EN/EmisionTest.pdf', '2022-06-21 14:31:18', '2022-06-21 14:31:18', NULL, 114),
(131, 'Documento de prueba 1', 'Test Document 1', 'documentos_emision/ES/EmisionTest.pdf', 'documentos_emision/EN/EmisionTest.pdf', '2022-06-21 14:31:18', '2022-06-21 14:31:18', NULL, 115),
(132, 'Documento de prueba 1', 'Test Document 1', 'documentos_emision/ES/EmisionTest.pdf', 'documentos_emision/EN/EmisionTest.pdf', '2022-06-21 14:31:18', '2022-06-21 14:31:18', NULL, 116),
(133, 'Documento de prueba 1', 'Test Document 1', 'documentos_emision/ES/EmisionTest.pdf', 'documentos_emision/EN/EmisionTest.pdf', '2022-06-21 14:31:18', '2022-06-21 14:31:18', NULL, 117),
(134, 'Documento de prueba 1', 'Test Document 1', 'documentos_emision/ES/EmisionTest.pdf', 'documentos_emision/EN/EmisionTest.pdf', '2022-06-21 14:31:18', '2022-06-21 14:31:18', NULL, 118),
(135, 'Documento de prueba 1', 'Test Document 1', 'documentos_emision/ES/EmisionTest.pdf', 'documentos_emision/EN/EmisionTest.pdf', '2022-06-21 14:31:18', '2022-06-21 14:31:18', NULL, 119),
(136, 'Documento de prueba 1', 'Test Document 1', 'documentos_emision/ES/EmisionTest.pdf', 'documentos_emision/EN/EmisionTest.pdf', '2022-06-21 14:31:18', '2022-06-21 14:31:18', NULL, 120),
(137, 'Documento de prueba 1', 'Test Document 1', 'documentos_emision/ES/EmisionTest.pdf', 'documentos_emision/EN/EmisionTest.pdf', '2022-06-21 14:31:18', '2022-06-21 14:31:18', NULL, 129),
(138, 'Documento de prueba 1', 'Test Document 1', 'documentos_emision/ES/EmisionTest.pdf', 'documentos_emision/EN/EmisionTest.pdf', '2022-06-21 14:31:18', '2022-06-21 14:31:18', NULL, 130),
(139, 'Documento de prueba 1', 'Test Document 1', 'documentos_emision/ES/EmisionTest.pdf', 'documentos_emision/EN/EmisionTest.pdf', '2022-06-21 14:31:18', '2022-06-21 14:31:18', NULL, 131),
(140, 'Documento de prueba 1', 'Test Document 1', 'documentos_emision/ES/EmisionTest.pdf', 'documentos_emision/EN/EmisionTest.pdf', '2022-06-21 14:31:18', '2022-06-21 14:31:18', NULL, 132),
(141, 'Documento de prueba 1', 'Test Document 1', 'documentos_emision/ES/EmisionTest.pdf', 'documentos_emision/EN/EmisionTest.pdf', '2022-06-21 14:31:18', '2022-06-21 14:31:18', NULL, 133),
(142, 'Documento de prueba 1', 'Test Document 1', 'documentos_emision/ES/EmisionTest.pdf', 'documentos_emision/EN/EmisionTest.pdf', '2022-06-21 14:31:18', '2022-06-21 14:31:18', NULL, 134),
(143, 'Documento de prueba 1', 'Test Document 1', 'documentos_emision/ES/EmisionTest.pdf', 'documentos_emision/EN/EmisionTest.pdf', '2022-06-21 14:31:18', '2022-06-21 14:31:18', NULL, 135),
(144, 'Documento de prueba 1', 'Test Document 1', 'documentos_emision/ES/EmisionTest.pdf', 'documentos_emision/EN/EmisionTest.pdf', '2022-06-21 14:31:18', '2022-06-21 14:31:18', NULL, 136),
(145, 'Documento de prueba 1', 'Test Document 1', 'documentos_emision/ES/EmisionTest.pdf', 'documentos_emision/EN/EmisionTest.pdf', '2022-06-21 14:31:18', '2022-06-21 14:31:18', NULL, 137),
(146, 'Documento de prueba 1', 'Test Document 1', 'documentos_emision/ES/EmisionTest.pdf', 'documentos_emision/EN/EmisionTest.pdf', '2022-06-21 14:31:18', '2022-06-21 14:31:18', NULL, 138),
(147, 'Documento de prueba 1', 'Test Document 1', 'documentos_emision/ES/EmisionTest.pdf', 'documentos_emision/EN/EmisionTest.pdf', '2022-06-21 14:31:18', '2022-06-21 14:31:18', NULL, 139),
(148, 'Documento de prueba 1', 'Test Document 1', 'documentos_emision/ES/EmisionTest.pdf', 'documentos_emision/EN/EmisionTest.pdf', '2022-06-21 14:31:18', '2022-06-21 14:31:18', NULL, 140),
(149, 'Documento de prueba 1', 'Test Document 1', 'documentos_emision/ES/EmisionTest.pdf', 'documentos_emision/EN/EmisionTest.pdf', '2022-06-21 14:31:18', '2022-06-21 14:31:18', NULL, 141),
(150, 'Documento de prueba 1', 'Test Document 1', 'documentos_emision/ES/EmisionTest.pdf', 'documentos_emision/EN/EmisionTest.pdf', '2022-06-21 14:31:18', '2022-06-21 14:31:18', NULL, 142),
(151, 'Documento de prueba 1', 'Test Document 1', 'documentos_emision/ES/EmisionTest.pdf', 'documentos_emision/EN/EmisionTest.pdf', '2022-06-21 14:31:18', '2022-06-21 14:31:18', NULL, 143),
(152, 'Documento de prueba 1', 'Test Document 1', 'documentos_emision/ES/EmisionTest.pdf', 'documentos_emision/EN/EmisionTest.pdf', '2022-06-21 14:31:18', '2022-06-21 14:31:18', NULL, 144),
(153, 'Documento de prueba 1', 'Test Document 1', 'documentos_emision/ES/EmisionTest.pdf', 'documentos_emision/EN/EmisionTest.pdf', '2022-06-21 14:31:19', '2022-06-21 14:31:19', NULL, 145),
(154, 'Documento de prueba 1', 'Test Document 1', 'documentos_emision/ES/EmisionTest.pdf', 'documentos_emision/EN/EmisionTest.pdf', '2022-06-21 14:31:19', '2022-06-21 14:31:19', NULL, 146),
(155, 'Documento de prueba 1', 'Test Document 1', 'documentos_emision/ES/EmisionTest.pdf', 'documentos_emision/EN/EmisionTest.pdf', '2022-06-21 14:31:19', '2022-06-21 14:31:19', NULL, 147),
(156, 'Documento de prueba 1', 'Test Document 1', 'documentos_emision/ES/EmisionTest.pdf', 'documentos_emision/EN/EmisionTest.pdf', '2022-06-21 14:31:19', '2022-06-21 14:31:19', NULL, 148),
(157, 'Documento de prueba 1', 'Test Document 1', 'documentos_emision/ES/EmisionTest.pdf', 'documentos_emision/EN/EmisionTest.pdf', '2022-06-21 14:31:19', '2022-06-21 14:31:19', NULL, 149),
(158, 'Documento de prueba 1', 'Test Document 1', 'documentos_emision/ES/EmisionTest.pdf', 'documentos_emision/EN/EmisionTest.pdf', '2022-06-21 14:31:19', '2022-06-21 14:31:19', NULL, 150),
(159, 'Documento de prueba 1', 'Test Document 1', 'documentos_emision/ES/EmisionTest.pdf', 'documentos_emision/EN/EmisionTest.pdf', '2022-06-21 14:31:19', '2022-06-21 14:31:19', NULL, 151),
(160, 'Documento de prueba 1', 'Test Document 1', 'documentos_emision/ES/EmisionTest.pdf', 'documentos_emision/EN/EmisionTest.pdf', '2022-06-21 14:31:19', '2022-06-21 14:31:19', NULL, 152),
(161, 'Documento de prueba 1', 'Test Document 1', 'documentos_emision/ES/EmisionTest.pdf', 'documentos_emision/EN/EmisionTest.pdf', '2022-06-21 14:31:19', '2022-06-21 14:31:19', NULL, 153),
(162, 'Documento de prueba 1', 'Test Document 1', 'documentos_emision/ES/EmisionTest.pdf', 'documentos_emision/EN/EmisionTest.pdf', '2022-06-21 14:31:19', '2022-06-21 14:31:19', NULL, 154),
(163, 'Documento de prueba 1', 'Test Document 1', 'documentos_emision/ES/EmisionTest.pdf', 'documentos_emision/EN/EmisionTest.pdf', '2022-06-21 14:31:19', '2022-06-21 14:31:19', NULL, 155),
(164, 'Documento de prueba 1', 'Test Document 1', 'documentos_emision/ES/EmisionTest.pdf', 'documentos_emision/EN/EmisionTest.pdf', '2022-06-21 14:31:19', '2022-06-21 14:31:19', NULL, 156),
(165, 'Documento de prueba 1', 'Test Document 1', 'documentos_emision/ES/EmisionTest.pdf', 'documentos_emision/EN/EmisionTest.pdf', '2022-06-21 14:31:19', '2022-06-21 14:31:19', NULL, 157),
(166, 'Documento de prueba 1', 'Test Document 1', 'documentos_emision/ES/EmisionTest.pdf', 'documentos_emision/EN/EmisionTest.pdf', '2022-06-21 14:31:19', '2022-06-21 14:31:19', NULL, 158),
(167, 'Documento de prueba 1', 'Test Document 1', 'documentos_emision/ES/EmisionTest.pdf', 'documentos_emision/EN/EmisionTest.pdf', '2022-06-21 14:31:19', '2022-06-21 14:31:19', NULL, 159),
(168, 'Documento de prueba 1', 'Test Document 1', 'documentos_emision/ES/EmisionTest.pdf', 'documentos_emision/EN/EmisionTest.pdf', '2022-06-21 14:31:19', '2022-06-21 14:31:19', NULL, 160),
(169, 'TEST222', 'TEST222', 'documentos_emision/ES/TEST222 09072022 001856 ES.pdf', 'documentos_emision/EN/TEST222 09072022 001856 EN.pdf', '2022-07-09 00:18:56', '2022-07-09 00:19:08', '2022-07-09 00:19:08', 177),
(170, 'TEST 11', 'TEST 11', 'documentos_emision/ES/TEST 11 11072022 181814 ES.pdf', 'documentos_emision/EN/TEST 11 11072022 181814 EN.pdf', '2022-07-11 18:18:14', '2022-07-11 18:18:14', NULL, 178);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `emisiones`
--

CREATE TABLE `emisiones` (
  `id` int(11) NOT NULL,
  `categoria_id` int(11) DEFAULT NULL,
  `titulo_ES` varchar(255) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `deleted_at` datetime DEFAULT NULL,
  `titulo_EN` varchar(255) DEFAULT NULL,
  `contenido_ES` longtext DEFAULT NULL,
  `contenido_EN` longtext DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `encabezados`
--

CREATE TABLE `encabezados` (
  `id` int(11) NOT NULL,
  `pagina` varchar(45) DEFAULT NULL,
  `titulo_ES` varchar(255) DEFAULT NULL,
  `subtitulo_ES` varchar(255) DEFAULT NULL,
  `descripcion_ES` longtext DEFAULT NULL,
  `titulo_EN` varchar(255) DEFAULT NULL,
  `subtitulo_EN` varchar(255) DEFAULT NULL,
  `descripcion_EN` longtext DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `uri` varchar(45) DEFAULT NULL,
  `imagen` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Volcado de datos para la tabla `encabezados`
--

INSERT INTO `encabezados` (`id`, `pagina`, `titulo_ES`, `subtitulo_ES`, `descripcion_ES`, `titulo_EN`, `subtitulo_EN`, `descripcion_EN`, `created_at`, `updated_at`, `uri`, `imagen`) VALUES
(1, 'EDUCACION FINANCIERA', 'Conozcamos sobre la titularización', 'Educación financiera', 'Velit dignissim sodales ut eu sem integer. Et leo duis ut diam quam. Viverra suspendisse potenti nullam ac tortor vitae purus faucibus ornare. Facilisis volutpat est velit egestas dui id. Tellus id interdum velit laoreet id donec ultrices. Urna molestie at elementum eu facilisis sed odio. Tempor nec feugiat nisl pretium. Porttitor lacus luctus accumsan tortor posuere ac ut consequat.', 'Learn about securitization', 'financial education', 'Velit dignissim sodales ut eu sem integer. Et leo duis ut diam quam. Viverra suspendisse potenti nullam ac tortor vitae purus faucibus ornare. Facilisis volutpat est velit egestas dui id. Tellus id interdum velit laoreet id donec ultrices. Urna molestie at elementum eu facilisis sed odio. Tempor nec feugiat nisl pretium. Porttitor lacus luctus accumsan tortor posuere ac ut consequat.', NULL, '2022-06-15 11:46:04', 'educacion-financiera', 'imagenes_encabezados/pBiAPtDCXMID0ogwWAlu9HqP7bGbG7S43MCImvmP.png'),
(2, 'SOBRE TITULARICE', 'Quienes somos', 'Titularice', 'La titularizadora es consistente con el compromiso de nuestros accionistas para contribuir al desarrollo de los mercados de capitales de Colombia y de la región. Titularice fue creada con base en los valores y políticas generales de nuestros accionistas.', 'Who we are', 'Titularice', 'The securitization company is consistent with the commitment of our shareholders to contribute to the development of the capital markets of Colombia and the region. Titularice was created based on the general values and policies of our shareholders.', NULL, '2022-07-15 18:30:22', 'nuestra-titularizadora', 'imagenes_encabezados/PS7aNqE4YqFGvQ1UeiXbc6BoZfNNL2YL1ZqD8Gej.png'),
(3, 'ACCIONISTAS', 'Nuestros Accionistas', NULL, 'Nuestros accionistas son la Organización Delima con participación del 40 por ciento de acciones, y Organización Hencorp, con el 60% de participación', 'Our Shareholders', NULL, 'Our shareholders are the Delima Organization with participation of 40 percent of shares, and Hencorp Organization, with 60% participation', NULL, '2022-07-14 16:39:51', 'accionistas_', 'imagenes_encabezados/uop6QVoCWRRAWOY4Ak6WAwa1sLiMcZNTu4wAuGNC.png'),
(4, 'GOBIERNO CORPORATIVO', 'Gobierno Corporativo', NULL, 'Titularice, tiene como eje fundamental de su actuación su compromiso activo con los originadores e inversionistas, que se materializan través de la transparencia y ética corporativa.<br><br>\r\nNuestro Código de Buen Gobierno Corporativo tiene como propósito compilar las normas de gobierno corporativo que, como complemento de la normatividad legal y estatutaria aplicable, rigen el funcionamiento de la Sociedad estableciendo principios, políticas y normas que rigen su administración. <br><br>\r\nComités de Apoyo Junta Directiva: la Junta Directiva estableció los siguientes comités que apoyan su gestión y permiten vigilar la implementación y desarrollo de las políticas definidas. <br><br>\r\nComité de Auditoría: El objetivo principal del Comité de Auditoría es prestar apoyo a la gestión que realiza la Junta Directiva en la implementación y supervisión del SCI, el cual debe estar conforme con la ley y las disposiciones de la Superintendencia Financiera de Colombia. El comité estará integrado por lo menos por tres (3) miembros de la Junta Directiva u órgano equivalente, quienes deberán tener experiencia, ser conocedores de los temas relacionados con las funciones asignadas al mismo. <br><br>\r\nEl comité estará conformado, en su mayoría por miembros que, en los términos previstos en los Estatutos de la Sociedad, tengan calidad de independientes. <br><br>\r\nTitularice S.A. en su calidad de emisor del mercado de valores y en cumplimiento de la Circular Externa 028 de 2014, reporta anualmente la encuesta de mejores prácticas corporativas \"Código País\" a la Superintendencia Financiera de Colombia. A continuación, se relacionan los reportes divulgados: <br><br><br>', 'Corporate Governance', NULL, 'Titularice has as its fundamental axis of action its active commitment to originators and investors, which materializes through transparency and corporate ethics.<br><br>\r\nThe purpose of our Code of Good Corporate Governance is to compile the corporate governance rules that, as a complement to the applicable legal and statutory regulations, govern the operation of the Company, establishing principles, policies and rules that govern its administration. <br><br>\r\nBoard of Directors Support Committees: The Board of Directors established the following committees that support its management and allow monitoring the implementation and development of the defined policies. <br><br>\r\nAudit Committee: The main objective of the Audit Committee is to support the management carried out by the Board of Directors in the implementation and supervision of the SCI, which must be in accordance with the law and the provisions of the Financial Superintendence of Colombia. The committee will be composed of at least three (3) members of the Board of Directors or equivalent body, who must have experience, be knowledgeable about issues related to the functions assigned to it. <br><br>\r\nThe committee will be made up mostly of members who, under the terms provided in the Company\'s Bylaws, are independent. <br><br>\r\nTitle S.A. in its capacity as issuer of the stock market and in compliance with External Circular Letter 028 of 2014, annually reports the survey of best corporate practices \"Country Code\" to the Financial Superintendence of Colombia. The disclosed reports are listed below: <br><br><br>', NULL, '2022-07-14 17:03:25', 'gobierno-corporativo', 'imagenes_encabezados/SC3g8SjsVG9ABshzcO6JoDnIv6YHO6m7sNVAnZEb.png'),
(5, 'JUNTA DIRECTIVA', 'Junta Directiva', NULL, 'La Junta Directiva es el órgano rector de Titularice, obligado a obrar de buena fe en interés de la Sociedad, de manera independiente y honesta, con la debida diligencia y cuidado, procurando siempre que sus decisiones sean en el mejor interés de la Sociedad y de todos los accionistas. Todo esto, promoviendo el cumplimiento de las leyes y demás normas aplicables, los Estatutos, el Código de Buen Gobierno Corporativo y demás reglas acogidas por la Sociedad.', 'Board of Directors', NULL, 'The Board of Directors is the governing body of Titularice, obliged to act in good faith in the interest of the Company, independently and honestly, with due diligence and care, always ensuring that its decisions are in the best interest of the Company and of all shareholders. All this, promoting compliance with the laws and other applicable regulations, the Bylaws, the Code of Good Corporate Governance and other rules adopted by the Company.', NULL, '2022-07-14 17:33:30', 'junta-directiva', 'imagenes_encabezados/60JdPeS6PaY4nAaqhrgK0tAHLKEaB9SqFtN1cz60.png'),
(6, 'ORGANIZACION EMPRESARIAL', 'Lorem ipsum dolor sit amet', 'organización Empresarial', 'Morbi a neque leo. Donec id consectetur lorem. Quisque lacinia risus facilisis, mollis sem et, ornare nisl. Phasellus nec neque id libero auctor pretium. Morbi pretium, urna at sagittis posuere, sapien lacus pellentesque turpis, a sodales erat neque ac ligula. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Ut accumsan rutrum lectus vitae ullamcorper. Orci varius natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Nulla tempor a quam eu fermentum. Praesent sed est lacus. Proin dapibus sed quam ut convallis. Suspendisse at libero justo. Curabitur libero risus, malesuada vel rhoncus a, aliquam quis nibh.', 'Lorem ipsum dolor sit amet', 'business organization', 'Morbi a neque leo. Donec id consectetur lorem. Quisque lacinia risus facilisis, mollis sem et, ornare nisl. Phasellus nec neque id libero auctor pretium. Morbi pretium, urna at sagittis posuere, sapien lacus pellentesque turpis, a sodales erat neque ac ligula. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Ut accumsan rutrum lectus vitae ullamcorper. Orci varius natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Nulla tempor a quam eu fermentum. Praesent sed est lacus. Proin dapibus sed quam ut convallis. Suspendisse at libero justo. Curabitur libero risus, malesuada vel rhoncus a, aliquam quis nibh.', NULL, '2022-06-15 11:17:02', 'organizacion', 'imagenes_encabezados/rYfTbmNELBTLC6UfoMP6rjeKCIvyDXpcTJDNM1w7.png'),
(7, 'NORMATIVIDAD', 'Normatividad', NULL, '<p>La normatividad aplicable a las Sociedades Titularizadoras de Activos No\r\nHipotecarios se encuentra contemplada en los Capítulos 1 y 2, Título2, Libro 21,\r\nParte 2, ambas del Decreto Único 2555 de 2010.</p>\r\n<p>La titularización no hipotecaria parte del artículo 72 de la Ley 1328 de 2009\r\ncon la modificación incorporada a través de la Ley 1753 de 2015, artículo 169 y\r\nel marco legal de las emisiones de títulos no hipotecarios se encuentra en los\r\nCapítulos 1 a 4, artículo 10, Libro 6, Parte 5 del Decreto Único 2555 de 2010.</p>\r\n\r\n<p>Los documentos contenidos en los enlaces que se ponen a disposición de los\r\ninteresados se suministran únicamente con fines informativos. Por tal razón,\r\nTitularice S.A. no se hace responsable de la precisión y el contenido de la\r\ninformación.</p>\r\n\r\n<p>Algunas de las normas mencionadas a continuación simplemente se\r\nenuncian y se recomienda consultar su contenido en las páginas web de la\r\nSuperintendencia Financiera de Colombia y el Ministerio de Hacienda\r\nquienes realizan una actualización permanente de su contenido.</p>', 'Normativity', NULL, '<p>The regulations applicable to the Securitization Companies of Assets\r\nMortgage loans is contemplated in Chapters 1 and 2, Title 2, Book 21,\r\nPart 2, both of Sole Decree 2555 of 2010.</p>\r\n<p>Non-mortgage securitization is part of article 72 of Law 1328 of 2009\r\nwith the modification incorporated through Law 1753 of 2015, article 169 and\r\nThe legal framework for the issuance of non-mortgage securities is found in the\r\nChapters 1 to 4, Article 10, Book 6, Part 5 of Single Decree 2555 of 2010.</p>\r\n\r\n<p>The documents contained in the links that are made available to\r\ninterested parties are provided for informational purposes only. For that reason,\r\nTitle S.A. is not responsible for the accuracy and content of the\r\ninformation.</p>\r\n\r\n<p>Some of the rules mentioned below are simply\r\nenunciate and it is recommended to consult its content on the web pages of the\r\nFinancial Superintendence of Colombia and the Ministry of Finance\r\nwho carry out a permanent update of their content.</p>', NULL, '2022-07-14 18:58:24', 'normatividad', 'imagenes_encabezados/FedkTPP02vb0wOHFNfWZ6yFQBWtaitow7VTXHkVc.png'),
(8, 'INFORMACION FINANCIERA', 'Información Financiera', NULL, 'Seleccione un año para ver cada uno de los estados.', 'Financial Information', 'financial information', 'Select a year to view each of the states.', NULL, '2022-07-14 19:06:05', 'informacion-financiera', 'imagenes_encabezados/ntquTwXNHOJ7V1OoFiD9cp0XnFQWf6X00Vh0vIGM.png'),
(9, 'RIESGOS', 'Políticas', 'riesgos', 'Titularice cuenta con los Sistemas de Administración de Riesgo necesarios para la gestión y control de los riesgos involucrados en la titularización de activos.', 'Policies', 'risks', 'Titularice has the necessary Risk Management Systems for the management and control of the risks involved in the securitization of assets.', NULL, '2022-07-14 19:32:59', 'riesgos', 'imagenes_encabezados/dLoNqXcYZ8zibXhpzWbwello39CSE6RkhwEr5par.png'),
(10, 'RELACION CON INVERSIONISTAS', 'Relación con Inversionistas', NULL, 'Con el ánimo de solucionar sus inquietudes, puede contactarnos en cualquier momento a través de cualquiera de nuestros canales.', 'Investor Relations', NULL, 'In order to resolve your concerns, you can contact us at any time through any of our channels.', NULL, '2022-07-14 22:22:16', 'relacion-inversionistas', 'imagenes_encabezados/eHmCD6kQs4TtsXWG54EIinYtSCp5LEzbhGgGXkP5.png'),
(11, 'EMISIONES', 'Lorem ipsum dolor sit amet', 'emisiones', NULL, 'Lorem ipsum dolor sit amet', 'emissions', NULL, NULL, '2022-06-15 11:51:05', 'emisiones', 'imagenes_encabezados/PQexdb6LmgsXpAM4zKDn0bJzWPXrO1ZqlRAVWe9K.png'),
(12, 'CONTACTENOS', '¿Deseas explorar la titularización o invertir en nuestros instrumentos?', 'Titularice', 'Nuestro equipo está capacitado para brindarte la mejor asesoría acerca de cómo titularizar o invertir en nuestros instrumentos.', 'Do you want to explore securitization or invest in our instruments?', 'Titularice', 'Our team is trained to provide you with the best advice on how to securitize or invest in our instruments.', NULL, '2022-07-14 23:15:23', 'contactenos', 'imagenes_encabezados/9vWniaOJylZqT9al2MnIQ0ZbZosjGNa7KJcBrPaO.png'),
(13, 'REPORTES', 'Informes de Gobierno Corporativo', 'titularice', NULL, 'Corporate Governance Reports', 'titularice', NULL, NULL, '2022-07-14 17:22:56', 'reportes', NULL),
(14, 'REPORTES_PAIS', 'Encuesta Código País', 'titularice', NULL, 'Survey Code Country', 'titularice', NULL, NULL, NULL, 'reportes_pais', NULL);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `failed_jobs`
--

CREATE TABLE `failed_jobs` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `uuid` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `connection` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `queue` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `payload` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `exception` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `failed_at` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `filemanager`
--

CREATE TABLE `filemanager` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `ext` varchar(4) COLLATE utf8mb4_unicode_ci NOT NULL,
  `file_size` double(8,2) NOT NULL,
  `user_id` bigint(20) UNSIGNED NOT NULL,
  `absolute_url` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `extra` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_bin DEFAULT NULL CHECK (json_valid(`extra`)),
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `gastos`
--

CREATE TABLE `gastos` (
  `id` int(11) NOT NULL,
  `nombre` varchar(64) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `deleted_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `gastos`
--

INSERT INTO `gastos` (`id`, `nombre`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 'test', '2021-03-23 12:57:48', '2021-03-23 12:57:53', '2021-03-23 12:57:53'),
(2, 'Parqueo', '2021-03-24 09:58:02', '2021-03-24 09:58:02', NULL),
(3, 'Reparacion llanta', '2021-03-24 09:58:32', '2021-03-24 09:58:32', NULL),
(4, 'Compra de llantas', '2021-03-24 09:58:45', '2021-03-24 13:34:59', NULL),
(5, 'Gasolina', '2021-03-24 09:59:25', '2021-03-24 09:59:25', NULL),
(6, 'Renta', '2021-03-24 13:35:16', '2021-03-24 13:35:16', NULL),
(7, 'Viáticos', '2021-03-24 13:35:32', '2021-03-24 13:35:32', NULL),
(8, 'Pago ayudante', '2021-03-25 07:30:11', '2021-03-25 07:30:11', NULL),
(9, 'Pago Salvador Barillas', '2021-03-25 07:30:31', '2021-03-25 07:30:31', NULL),
(10, 'Otras reparaciones', '2021-05-04 07:50:53', '2021-05-04 07:50:53', NULL),
(11, 'Pago empleados (día domingo)', '2021-05-25 09:30:34', '2021-05-25 09:30:34', NULL),
(12, 'Pago Motorista', '2021-06-18 08:03:18', '2021-06-18 08:03:18', NULL),
(13, 'Gastos Varios', '2021-07-02 13:45:39', '2021-07-02 13:45:39', NULL);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `gobierno_corporativo`
--

CREATE TABLE `gobierno_corporativo` (
  `id` int(11) NOT NULL,
  `titulo_ES` varchar(255) DEFAULT NULL,
  `descripcion_ES` longtext DEFAULT NULL,
  `nombre_documento_ES` varchar(255) DEFAULT NULL,
  `titulo_EN` varchar(255) DEFAULT NULL,
  `descripcion_EN` longtext DEFAULT NULL,
  `nombre_documento_EN` varchar(255) DEFAULT NULL,
  `documento_url_ES` varchar(255) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `deleted_at` datetime DEFAULT NULL,
  `documento_url_EN` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Volcado de datos para la tabla `gobierno_corporativo`
--

INSERT INTO `gobierno_corporativo` (`id`, `titulo_ES`, `descripcion_ES`, `nombre_documento_ES`, `titulo_EN`, `descripcion_EN`, `nombre_documento_EN`, `documento_url_ES`, `created_at`, `updated_at`, `deleted_at`, `documento_url_EN`) VALUES
(6, 'Titulo de prueba', 'Nulla nisi mauris, facilisis in dignissim sit amet, accumsan vel lacus. Aliquam nec blandit magna, ac ullamcorper tortor. Mauris commodo porttitor aliquam. Aliquam suscipit blandit tortor, sed luctus sapien rutrum ac. Suspendisse mollis ante lectus, ut vulputate turpis vehicula at. Sed sagittis lobortis massa, non pharetra sapien laoreet id. Donec consequat convallis sem, non hendrerit libero condimentum luctus. Donec lobortis elit venenatis justo commodo consectetur. Aliquam a ante vel magna congue tempor. Nam blandit urna non ante tempor rutrum. Donec ullamcorper dui vitae massa vehicula rhoncus.', 'Documento de prueba', 'Test title', 'Nulla nisi mauris, facilisis in dignissim sit amet, accumsan vel lacus. Aliquam nec blandit magna, ac ullamcorper tortor. Mauris commodo porttitor aliquam. Aliquam suscipit blandit tortor, sed luctus sapien rutrum ac. Suspendisse mollis ante lectus, ut vulputate turpis vehicula at. Sed sagittis lobortis massa, non pharetra sapien laoreet id. Donec consequat convallis sem, non hendrerit libero condimentum luctus. Donec lobortis elit venenatis justo commodo consectetur. Aliquam a ante vel magna congue tempor. Nam blandit urna non ante tempor rutrum. Donec ullamcorper dui vitae massa vehicula rhoncus.', 'Test document', 'documentos_gobierno_corporativo/Documento de prueba 21062022 153042 ES.pdf', '2022-06-13 17:11:14', '2022-06-21 15:30:42', NULL, 'documentos_gobierno_corporativo/Test document 21062022 153042 EN.pdf'),
(10, 'Titulo de prueba', 'Nulla nisi mauris, facilisis in dignissim sit amet, accumsan vel lacus. Aliquam nec blandit magna, ac ullamcorper tortor. Mauris commodo porttitor aliquam. Aliquam suscipit blandit tortor, sed luctus sapien rutrum ac. Suspendisse mollis ante lectus, ut vulputate turpis vehicula at. Sed sagittis lobortis massa, non pharetra sapien laoreet id. Donec consequat convallis sem, non hendrerit libero condimentum luctus. Donec lobortis elit venenatis justo commodo consectetur. Aliquam a ante vel magna congue tempor. Nam blandit urna non ante tempor rutrum. Donec ullamcorper dui vitae massa vehicula rhoncus.', 'Documento de prueba', 'Test title', 'Nulla nisi mauris, facilisis in dignissim sit amet, accumsan vel lacus. Aliquam nec blandit magna, ac ullamcorper tortor. Mauris commodo porttitor aliquam. Aliquam suscipit blandit tortor, sed luctus sapien rutrum ac. Suspendisse mollis ante lectus, ut vulputate turpis vehicula at. Sed sagittis lobortis massa, non pharetra sapien laoreet id. Donec consequat convallis sem, non hendrerit libero condimentum luctus. Donec lobortis elit venenatis justo commodo consectetur. Aliquam a ante vel magna congue tempor. Nam blandit urna non ante tempor rutrum. Donec ullamcorper dui vitae massa vehicula rhoncus.', 'Test document', 'documentos_gobierno_corporativo/Documento de prueba 21062022 153042 ES.pdf', '2022-06-13 17:11:14', '2022-06-21 15:30:42', NULL, 'documentos_gobierno_corporativo/Test document 21062022 153042 EN.pdf'),
(11, 'Titulo de prueba', 'Nulla nisi mauris, facilisis in dignissim sit amet, accumsan vel lacus. Aliquam nec blandit magna, ac ullamcorper tortor. Mauris commodo porttitor aliquam. Aliquam suscipit blandit tortor, sed luctus sapien rutrum ac. Suspendisse mollis ante lectus, ut vulputate turpis vehicula at. Sed sagittis lobortis massa, non pharetra sapien laoreet id. Donec consequat convallis sem, non hendrerit libero condimentum luctus. Donec lobortis elit venenatis justo commodo consectetur. Aliquam a ante vel magna congue tempor. Nam blandit urna non ante tempor rutrum. Donec ullamcorper dui vitae massa vehicula rhoncus.', 'Documento de prueba', 'Test title', 'Nulla nisi mauris, facilisis in dignissim sit amet, accumsan vel lacus. Aliquam nec blandit magna, ac ullamcorper tortor. Mauris commodo porttitor aliquam. Aliquam suscipit blandit tortor, sed luctus sapien rutrum ac. Suspendisse mollis ante lectus, ut vulputate turpis vehicula at. Sed sagittis lobortis massa, non pharetra sapien laoreet id. Donec consequat convallis sem, non hendrerit libero condimentum luctus. Donec lobortis elit venenatis justo commodo consectetur. Aliquam a ante vel magna congue tempor. Nam blandit urna non ante tempor rutrum. Donec ullamcorper dui vitae massa vehicula rhoncus.', 'Test document', 'documentos_gobierno_corporativo/Documento de prueba 21062022 153042 ES.pdf', '2022-06-13 17:11:14', '2022-06-21 15:30:42', NULL, 'documentos_gobierno_corporativo/Test document 21062022 153042 EN.pdf'),
(12, 'Titulo de prueba', 'Nulla nisi mauris, facilisis in dignissim sit amet, accumsan vel lacus. Aliquam nec blandit magna, ac ullamcorper tortor. Mauris commodo porttitor aliquam. Aliquam suscipit blandit tortor, sed luctus sapien rutrum ac. Suspendisse mollis ante lectus, ut vulputate turpis vehicula at. Sed sagittis lobortis massa, non pharetra sapien laoreet id. Donec consequat convallis sem, non hendrerit libero condimentum luctus. Donec lobortis elit venenatis justo commodo consectetur. Aliquam a ante vel magna congue tempor. Nam blandit urna non ante tempor rutrum. Donec ullamcorper dui vitae massa vehicula rhoncus.', 'Documento de prueba', 'Test title', 'Nulla nisi mauris, facilisis in dignissim sit amet, accumsan vel lacus. Aliquam nec blandit magna, ac ullamcorper tortor. Mauris commodo porttitor aliquam. Aliquam suscipit blandit tortor, sed luctus sapien rutrum ac. Suspendisse mollis ante lectus, ut vulputate turpis vehicula at. Sed sagittis lobortis massa, non pharetra sapien laoreet id. Donec consequat convallis sem, non hendrerit libero condimentum luctus. Donec lobortis elit venenatis justo commodo consectetur. Aliquam a ante vel magna congue tempor. Nam blandit urna non ante tempor rutrum. Donec ullamcorper dui vitae massa vehicula rhoncus.', 'Test document', 'documentos_gobierno_corporativo/Documento de prueba 21062022 153042 ES.pdf', '2022-06-13 17:11:14', '2022-06-21 15:30:42', NULL, 'documentos_gobierno_corporativo/Test document 21062022 153042 EN.pdf'),
(13, 'Titulo de prueba', 'Nulla nisi mauris, facilisis in dignissim sit amet, accumsan vel lacus. Aliquam nec blandit magna, ac ullamcorper tortor. Mauris commodo porttitor aliquam. Aliquam suscipit blandit tortor, sed luctus sapien rutrum ac. Suspendisse mollis ante lectus, ut vulputate turpis vehicula at. Sed sagittis lobortis massa, non pharetra sapien laoreet id. Donec consequat convallis sem, non hendrerit libero condimentum luctus. Donec lobortis elit venenatis justo commodo consectetur. Aliquam a ante vel magna congue tempor. Nam blandit urna non ante tempor rutrum. Donec ullamcorper dui vitae massa vehicula rhoncus.', 'Documento de prueba', 'Test title', 'Nulla nisi mauris, facilisis in dignissim sit amet, accumsan vel lacus. Aliquam nec blandit magna, ac ullamcorper tortor. Mauris commodo porttitor aliquam. Aliquam suscipit blandit tortor, sed luctus sapien rutrum ac. Suspendisse mollis ante lectus, ut vulputate turpis vehicula at. Sed sagittis lobortis massa, non pharetra sapien laoreet id. Donec consequat convallis sem, non hendrerit libero condimentum luctus. Donec lobortis elit venenatis justo commodo consectetur. Aliquam a ante vel magna congue tempor. Nam blandit urna non ante tempor rutrum. Donec ullamcorper dui vitae massa vehicula rhoncus.', 'Test document', 'documentos_gobierno_corporativo/Documento de prueba 21062022 153042 ES.pdf', '2022-06-13 17:11:14', '2022-06-21 15:30:42', NULL, 'documentos_gobierno_corporativo/Test document 21062022 153042 EN.pdf');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `informes_financieros_categorias`
--

CREATE TABLE `informes_financieros_categorias` (
  `id` int(11) NOT NULL,
  `categoria_ES` varchar(255) DEFAULT NULL,
  `categoria_EN` varchar(45) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `deleted_at` varchar(45) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Volcado de datos para la tabla `informes_financieros_categorias`
--

INSERT INTO `informes_financieros_categorias` (`id`, `categoria_ES`, `categoria_EN`, `created_at`, `updated_at`, `deleted_at`) VALUES
(2, 'Balance', 'Balance', '2022-06-17 17:22:18', '2022-06-17 17:22:18', NULL),
(3, 'Pérdida y ganancia', 'Profit and loss', '2022-06-17 17:23:01', '2022-06-17 17:23:01', NULL),
(4, 'Indicadores de gestión', 'Management indicators', '2022-06-17 17:23:26', '2022-06-17 17:23:26', NULL),
(6, 'Relación de solvencia', 'solvency ratio', '2022-06-17 17:24:25', '2022-06-17 17:24:25', NULL),
(10, 'Estados Financieros Auditados', 'Financially audited States', NULL, NULL, NULL),
(11, 'Estados Financieros Intermedios', 'Interim Financial Statements', NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `informes_financieros_documentos`
--

CREATE TABLE `informes_financieros_documentos` (
  `id` int(11) NOT NULL,
  `nombre_ES` varchar(255) DEFAULT NULL,
  `nombre_EN` varchar(255) DEFAULT NULL,
  `url_ES` varchar(255) DEFAULT NULL,
  `url_EN` varchar(255) DEFAULT NULL,
  `informes_financieros_categoria_id` int(11) DEFAULT NULL,
  `anio` int(11) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `deleted_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Volcado de datos para la tabla `informes_financieros_documentos`
--

INSERT INTO `informes_financieros_documentos` (`id`, `nombre_ES`, `nombre_EN`, `url_ES`, `url_EN`, `informes_financieros_categoria_id`, `anio`, `created_at`, `deleted_at`, `updated_at`) VALUES
(12, 'Septiembre', 'September', 'documentos_informes_financieros/ES/Febrero%17062022%180932%ES.xlsx', 'documentos_informes_financieros/EN/Febrero%17062022%180932%EN.xlsx', 2, 2022, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `inversionistas`
--

CREATE TABLE `inversionistas` (
  `id` int(11) NOT NULL,
  `nombre` varchar(255) DEFAULT NULL,
  `puesto_laboral_ES` varchar(255) DEFAULT NULL,
  `puesto_laboral_EN` varchar(255) DEFAULT NULL,
  `correo` varchar(255) DEFAULT NULL,
  `telefono` varchar(45) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `deleted_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Volcado de datos para la tabla `inversionistas`
--

INSERT INTO `inversionistas` (`id`, `nombre`, `puesto_laboral_ES`, `puesto_laboral_EN`, `correo`, `telefono`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 'Ronald Ventura', 'Gerente de Estructuración', 'Structuring Manager', 'rventura@titularice.com', '(315) 564 8052', '2022-07-06 20:26:09', '2022-07-06 20:26:09', NULL),
(2, 'Carlos Marciales Villamizar', 'Gerente de Riesgos', 'Risk Manager', 'cvillamizar@titularice.com', '(313) 409 4303', '2022-07-06 20:26:09', '2022-07-06 20:26:09', NULL);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `junta_directiva`
--

CREATE TABLE `junta_directiva` (
  `id` int(11) NOT NULL,
  `nombre` varchar(255) DEFAULT NULL,
  `compania` varchar(255) DEFAULT NULL,
  `puesto_laboral_ES` varchar(255) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updatet_at` datetime DEFAULT NULL,
  `deleted_at` datetime DEFAULT NULL,
  `puesto_laboral_EN` varchar(255) DEFAULT NULL,
  `foto` longtext DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Volcado de datos para la tabla `junta_directiva`
--

INSERT INTO `junta_directiva` (`id`, `nombre`, `compania`, `puesto_laboral_ES`, `created_at`, `updatet_at`, `deleted_at`, `puesto_laboral_EN`, `foto`) VALUES
(1, 'Eduardo Alfaro', 'Hencorp', 'Miembro Patrimonial', NULL, NULL, NULL, 'Patrimonial Member', 'junta_directiva/eduardo_alfaro.png'),
(2, 'Felipe Holguín', 'Hencorp', 'Miembro Patrimonial', NULL, NULL, NULL, 'Miembro Patrimonial', 'junta_directiva/felipe_holguin.png'),
(3, 'Juan José Echavarría', 'Ex – Gerente del Banco de la República', 'Miembro Independiente', NULL, NULL, NULL, 'Independent Member', 'junta_directiva/jose_echavarria.png'),
(4, 'Fuad Velásco', 'Socio de Nexus Capital Partners', 'Miembro Independiente', NULL, NULL, NULL, 'Independent Member', 'junta_directiva/fuad_velasco.png'),
(5, 'Marcela Mejía', 'CEO – SIDOC', 'Independent Member', NULL, NULL, NULL, 'Independent Member', 'junta_directiva/marcela_mejia.png');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(1, '2014_10_12_000000_create_users_table', 1),
(2, '2014_10_12_100000_create_password_resets_table', 1),
(3, '2019_08_19_000000_create_failed_jobs_table', 1),
(4, '2020_05_02_100001_create_filemanager_table', 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `modulos`
--

CREATE TABLE `modulos` (
  `id` int(11) NOT NULL,
  `nombre` varchar(256) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `deleted_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Volcado de datos para la tabla `modulos`
--

INSERT INTO `modulos` (`id`, `nombre`, `created_at`, `updated_at`, `deleted_at`) VALUES
(6, 'comparaciones', NULL, NULL, NULL),
(9, 'usuarios', NULL, NULL, NULL),
(10, 'roles', NULL, NULL, NULL),
(11, 'Accesos', NULL, NULL, NULL),
(12, 'Bitacoras', NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `municipios`
--

CREATE TABLE `municipios` (
  `id` int(11) NOT NULL,
  `nombre` varchar(64) DEFAULT NULL,
  `departamento_id` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `municipios`
--

INSERT INTO `municipios` (`id`, `nombre`, `departamento_id`) VALUES
(1, 'Ahuachapán', 1),
(2, 'Jujutla', 1),
(3, 'Atiquizaya', 1),
(4, 'Concepción de Ataco', 1),
(5, 'El Refugio', 1),
(6, 'Guaymango', 1),
(7, 'Apaneca', 1),
(8, 'San Francisco Menéndez', 1),
(9, 'San Lorenzo', 1),
(10, 'San Pedro Puxtla', 1),
(11, 'Tacuba', 1),
(12, 'Turín', 1),
(13, 'Candelaria de la Frontera', 2),
(14, 'Chalchuapa', 2),
(15, 'Coatepeque', 2),
(16, 'El Congo', 2),
(17, 'El Porvenir', 2),
(18, 'Masahuat', 2),
(19, 'Metapán', 2),
(20, 'San Antonio Pajonal', 2),
(21, 'San Sebastián Salitrillo', 2),
(22, 'Santa Ana', 2),
(23, 'Santa Rosa Guachipilín', 2),
(24, 'Santiago de la Frontera', 2),
(25, 'Texistepeque', 2),
(26, 'Acajutla', 3),
(27, 'Armenia', 3),
(28, 'Caluco', 3),
(29, 'Cuisnahuat', 3),
(30, 'Izalco', 3),
(31, 'Juayúa', 3),
(32, 'Nahuizalco', 3),
(33, 'Nahulingo', 3),
(34, 'Salcoatitán', 3),
(35, 'San Antonio del Monte', 3),
(36, 'San Julián', 3),
(37, 'Santa Catarina Masahuat', 3),
(38, 'Santa Isabel Ishuatán', 3),
(39, 'Santo Domingo de Guzmán', 3),
(40, 'Sonsonate', 3),
(41, 'Sonzacate', 3),
(42, 'Alegría', 11),
(43, 'Berlín', 11),
(44, 'California', 11),
(45, 'Concepción Batres', 11),
(46, 'El Triunfo', 11),
(47, 'Ereguayquín', 11),
(48, 'Estanzuelas', 11),
(49, 'Jiquilisco', 11),
(50, 'Jucuapa', 11),
(51, 'Jucuarán', 11),
(52, 'Mercedes Umaña', 11),
(53, 'Nueva Granada', 11),
(54, 'Ozatlán', 11),
(55, 'Puerto El Triunfo', 11),
(56, 'San Agustín', 11),
(57, 'San Buenaventura', 11),
(58, 'San Dionisio', 11),
(59, 'San Francisco Javier', 11),
(60, 'Santa Elena', 11),
(61, 'Santa María', 11),
(62, 'Santiago de María', 11),
(63, 'Tecapán', 11),
(64, 'Usulután', 11),
(65, 'Carolina', 13),
(66, 'Chapeltique', 13),
(67, 'Chinameca', 13),
(68, 'Chirilagua', 13),
(69, 'Ciudad Barrios', 13),
(70, 'Comacarán', 13),
(71, 'El Tránsito', 13),
(72, 'Lolotique', 13),
(73, 'Moncagua', 13),
(74, 'Nueva Guadalupe', 13),
(75, 'Nuevo Edén de San Juan', 13),
(76, 'Quelepa', 13),
(77, 'San Antonio del Mosco', 13),
(78, 'San Gerardo', 13),
(79, 'San Jorge', 13),
(80, 'San Luis de la Reina', 13),
(81, 'San Miguel', 13),
(82, 'San Rafael Oriente', 13),
(83, 'Sesori', 13),
(84, 'Uluazapa', 13),
(85, 'Arambala', 12),
(86, 'Cacaopera', 12),
(87, 'Chilanga', 12),
(88, 'Corinto', 12),
(89, 'Delicias de Concepción', 12),
(90, 'El Divisadero', 12),
(91, 'El Rosario (Morazán)', 12),
(92, 'Gualococti', 12),
(93, 'Guatajiagua', 12),
(94, 'Joateca', 12),
(95, 'Jocoaitique', 12),
(96, 'Jocoro', 12),
(97, 'Lolotiquillo', 12),
(98, 'Meanguera', 12),
(99, 'Osicala', 12),
(100, 'Perquín', 12),
(101, 'San Carlos', 12),
(102, 'San Fernando (Morazán)', 12),
(103, 'San Francisco Gotera', 12),
(104, 'San Isidro (Morazán)', 12),
(105, 'San Simón', 12),
(106, 'Sensembra', 12),
(107, 'Sociedad', 12),
(108, 'Torola', 12),
(109, 'Yamabal', 12),
(110, 'Yoloaiquín', 12),
(111, 'La Unión', 14),
(112, 'San Alejo', 14),
(113, 'Yucuaiquín', 14),
(114, 'Conchagua', 14),
(115, 'Intipucá', 14),
(116, 'San José', 14),
(117, 'El Carmen (La Unión)', 14),
(118, 'Yayantique', 14),
(119, 'Bolívar', 14),
(120, 'Meanguera del Golfo', 14),
(121, 'Santa Rosa de Lima', 14),
(122, 'Pasaquina', 14),
(123, 'Anamoros', 14),
(124, 'Nueva Esparta', 14),
(125, 'El Sauce', 14),
(126, 'Concepción de Oriente', 14),
(127, 'Polorós', 14),
(128, 'Lislique', 14),
(129, 'Antiguo Cuscatlán', 4),
(130, 'Chiltiupán', 4),
(131, 'Ciudad Arce', 4),
(132, 'Colón', 4),
(133, 'Comasagua', 4),
(134, 'Huizúcar', 4),
(135, 'Jayaque', 4),
(136, 'Jicalapa', 4),
(137, 'La Libertad', 4),
(138, 'Santa Tecla', 4),
(139, 'Nuevo Cuscatlán', 4),
(140, 'San Juan Opico', 4),
(141, 'Quezaltepeque', 4),
(142, 'Sacacoyo', 4),
(143, 'San José Villanueva', 4),
(144, 'San Matías', 4),
(145, 'San Pablo Tacachico', 4),
(146, 'Talnique', 4),
(147, 'Tamanique', 4),
(148, 'Teotepeque', 4),
(149, 'Tepecoyo', 4),
(150, 'Zaragoza', 4),
(151, 'Agua Caliente', 5),
(152, 'Arcatao', 5),
(153, 'Azacualpa', 5),
(154, 'Cancasque', 5),
(155, 'Chalatenango', 5),
(156, 'Citalá', 5),
(157, 'Comapala', 5),
(158, 'Concepción Quezaltepeque', 5),
(159, 'Dulce Nombre de María', 5),
(160, 'El Carrizal', 5),
(161, 'El Paraíso', 5),
(162, 'La Laguna', 5),
(163, 'La Palma', 5),
(164, 'La Reina', 5),
(165, 'Las Vueltas', 5),
(166, 'Nueva Concepción', 5),
(167, 'Nueva Trinidad', 5),
(168, 'Nombre de Jesús', 5),
(169, 'Ojos de Agua', 5),
(170, 'Potonico', 5),
(171, 'San Antonio de la Cruz', 5),
(172, 'San Antonio Los Ranchos', 5),
(173, 'San Fernando (Chalatenango)', 5),
(174, 'San Francisco Lempa', 5),
(175, 'San Francisco Morazán', 5),
(176, 'San Ignacio', 5),
(177, 'San Isidro Labrador', 5),
(178, 'Las Flores', 5),
(179, 'San Luis del Carmen', 5),
(180, 'San Miguel de Mercedes', 5),
(181, 'San Rafael', 5),
(182, 'Santa Rita', 5),
(183, 'Tejutla', 5),
(184, 'Cojutepeque', 7),
(185, 'Candelaria', 7),
(186, 'El Carmen (Cuscatlán)', 7),
(187, 'El Rosario (Cuscatlán)', 7),
(188, 'Monte San Juan', 7),
(189, 'Oratorio de Concepción', 7),
(190, 'San Bartolomé Perulapía', 7),
(191, 'San Cristóbal', 7),
(192, 'San José Guayabal', 7),
(193, 'San Pedro Perulapán', 7),
(194, 'San Rafael Cedros', 7),
(195, 'San Ramón', 7),
(196, 'Santa Cruz Analquito', 7),
(197, 'Santa Cruz Michapa', 7),
(198, 'Suchitoto', 7),
(199, 'Tenancingo', 7),
(200, 'Aguilares', 6),
(201, 'Apopa', 6),
(202, 'Ayutuxtepeque', 6),
(203, 'Cuscatancingo', 6),
(204, 'Ciudad Delgado', 6),
(205, 'El Paisnal', 6),
(206, 'Guazapa', 6),
(207, 'Ilopango', 6),
(208, 'Mejicanos', 6),
(209, 'Nejapa', 6),
(210, 'Panchimalco', 6),
(211, 'Rosario de Mora', 6),
(212, 'San Marcos', 6),
(213, 'San Martín', 6),
(214, 'San Salvador', 6),
(215, 'Santiago Texacuangos', 6),
(216, 'Santo Tomás', 6),
(217, 'Soyapango', 6),
(218, 'Tonacatepeque', 6),
(219, 'Zacatecoluca', 8),
(220, 'Cuyultitán', 8),
(221, 'El Rosario (La Paz)', 8),
(222, 'Jerusalén', 8),
(223, 'Mercedes La Ceiba', 8),
(224, 'Olocuilta', 8),
(225, 'Paraíso de Osorio', 8),
(226, 'San Antonio Masahuat', 8),
(227, 'San Emigdio', 8),
(228, 'San Francisco Chinameca', 8),
(229, 'San Pedro Masahuat', 8),
(230, 'San Juan Nonualco', 8),
(231, 'San Juan Talpa', 8),
(232, 'San Juan Tepezontes', 8),
(233, 'San Luis La Herradura', 8),
(234, 'San Luis Talpa', 8),
(235, 'San Miguel Tepezontes', 8),
(236, 'San Pedro Nonualco', 8),
(237, 'San Rafael Obrajuelo', 8),
(238, 'Santa María Ostuma', 8),
(239, 'Santiago Nonualco', 8),
(240, 'Tapalhuaca', 8),
(241, 'Cinquera', 9),
(242, 'Dolores', 9),
(243, 'Guacotecti', 9),
(244, 'Ilobasco', 9),
(245, 'Jutiapa', 9),
(246, 'San Isidro (Cabañas)', 9),
(247, 'Sensuntepeque', 9),
(248, 'Tejutepeque', 9),
(249, 'Victoria', 9),
(250, 'Apastepeque', 10),
(251, 'Guadalupe', 10),
(252, 'San Cayetano Istepeque', 10),
(253, 'San Esteban Catarina', 10),
(254, 'San Ildefonso', 10),
(255, 'San Lorenzo', 10),
(256, 'San Sebastián', 10),
(257, 'San Vicente', 10),
(258, 'Santa Clara', 10),
(259, 'Santo Domingo', 10),
(260, 'Tecoluca', 10),
(261, 'Tepetitán', 10),
(262, 'Verapaz', 10);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `normatividad_categorias`
--

CREATE TABLE `normatividad_categorias` (
  `id` int(11) NOT NULL,
  `categoria_ES` varchar(64) DEFAULT NULL,
  `categoria_EN` varchar(64) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `deleted_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Volcado de datos para la tabla `normatividad_categorias`
--

INSERT INTO `normatividad_categorias` (`id`, `categoria_ES`, `categoria_EN`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 'Categoria', 'Categoria', '2022-07-01 15:17:57', '2022-07-01 15:24:44', '2022-07-01 15:24:44'),
(2, 'Categoria2', 'Categoria2', '2022-07-01 15:18:32', '2022-07-01 15:23:53', '2022-07-01 15:23:53'),
(3, 'Circulares', 'Circulars', '2022-07-01 15:33:01', '2022-07-01 15:33:01', NULL),
(4, 'Decretos', 'Decrees', '2022-07-01 15:33:47', '2022-07-01 15:33:47', NULL),
(5, 'Leyes', 'Laws', '2022-07-01 15:34:16', '2022-07-01 15:34:16', NULL),
(6, 'Resoluciones', 'Resolutions', '2022-07-01 15:34:46', '2022-07-01 15:34:46', NULL);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `organizacion`
--

CREATE TABLE `organizacion` (
  `id` int(11) NOT NULL,
  `nombre` varchar(255) DEFAULT NULL,
  `puesto_laboral_ES` varchar(255) DEFAULT NULL,
  `puesto_laboral_EN` varchar(255) DEFAULT NULL,
  `foto_url` varchar(512) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `deleted_at` datetime DEFAULT NULL,
  `descripcion_ES` longtext DEFAULT NULL,
  `descripcion_EN` longtext DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Volcado de datos para la tabla `organizacion`
--

INSERT INTO `organizacion` (`id`, `nombre`, `puesto_laboral_ES`, `puesto_laboral_EN`, `foto_url`, `created_at`, `updated_at`, `deleted_at`, `descripcion_ES`, `descripcion_EN`) VALUES
(1, 'Jorge Alberto Linares', 'Presidente', 'President', 'imagenes_organizacion/perfil.jpg', NULL, NULL, NULL, 'Cuenta con 30 años de experiencia en el sector financiero de Colombia. Graduado de Economía de la Universidad Javeriana y con una Especialidad en Finanzas de la Universidad EAFIT – CESA. Actualmente se desempeña como líder de este proyecto. Hasta 2020 fungió como Vicepresidente de Gestión de Capital y Vicepresidente de Gestión Empresarial de Grupo Fundación Social. Además, se ha desempeñado como Vicepresidente Financiero de Banco Caja Social, así como otros puestos importantes de liderazgo en el sector financiero de Colombia. ', 'He has 30 years of experience in the Colombian financial sector. He graduated in Economics from the Javeriana University and with a Specialty in Finance from the EAFIT University – CESA. He currently serves as the leader of this project. Until 2020, he served as Vice President of Capital Management and Vice President of Business Management of Grupo Fundación Social. In addition, he has served as Financial Vice President of Banco Caja Social, as well as other important leadership positions in the Colombian financial sector.'),
(2, 'Ronald Ventura', 'Director de Estructuración', 'Structuring Director', 'imagenes_organizacion/perfil.jpg', NULL, NULL, NULL, 'Cuenta con 6 años de experiencia en el mercado bursátil, banca de inversión, proyectos de infraestructura y Asociaciones Público Privadas. Graduado con honores de la Ingeniería de Negocios de la Escuela Superior de Economía y Negocios (ESEN). Candidato al Nivel II del Programa CFA y Nivel II del Programa FRM. Fungió como Gerente de Riesgos para Hencorp Valores, Ltda., Titularizadora en El Salvador, así como de Hencorp, S.A. de C.V., Casa de Corredores de Bolsa, y Hencorp, S.A., Sociedad Gestora de Fondos de Inversión.', 'He has 6 years of experience in the stock market, investment banking, infrastructure projects and Public Private Partnerships. He graduated with honors from Business Engineering at the Higher School of Economics and Business (ESEN). Candidate for Level II of the CFA Program and Level II of the FRM Program. He served as Risk Manager for Hencorp Valores, Ltda., Titularizadora in El Salvador, as well as Hencorp, S.A. de C.V., Casa de Corredores de Bolsa, and Hencorp, S.A., Sociedad Gestora de Fondos de Inversión.'),
(3, 'Myriam Carrillo', 'Directora Administrativa y Contable', 'Administrative and Accounting Director', 'imagenes_organizacion/myriam_carrillo.png', NULL, NULL, NULL, 'Alto nivel de conocimientos y experiencia para dirigir áreas de análisis financiero - contable, fiscal y de Control, con 30 años de experiencia en la implementación y desarrollo de nuevos modelos de procesos contables y financieros. Destacada capacidad en habilidades gerenciales orientadas al logro mediante el trabajo en equipo, de alta motivación compromiso y aprendizaje continuo. Contador Público, Especializada en Finanzas, diplomado en NIIF. ', 'High level of knowledge and experience to lead areas of financial analysis - accounting, tax and Control, with 30 years of experience in the implementation and development of new models of accounting and financial processes. Outstanding capacity in management skills oriented to achievement through teamwork, highly motivated, committed and continuous learning. Public Accountant, Specialized in Finance, graduated in NIIF.'),
(4, 'Carlos Marciales', 'Director de Riesgos', 'Risk Director', 'imagenes_organizacion/carlos_marciales.png', NULL, NULL, NULL, 'Cuenta con 12 años de experiencia en el área de riesgo, valoración y estructuración del mercado de capitales de Colombia. Graduado de Ingeniería Industrial de la Universidad de Los Andes con una Maestría en Finanzas de la EAE Business School y de la Universidad Politécnica de Cataluña, candidato al nivel I del programa CFA.', 'He has 12 years of experience in the area of risk, valuation and structuring of the Colombian capital market. He graduated in Industrial Engineering from the Universidad de Los Andes with a Master\'s Degree in Finance from the EAE Business School and the Polytechnic University of Catalonia, a candidate for level I of the CFA program.'),
(5, 'David Cortés', 'Auditor Interno', 'Internal Auditor', 'imagenes_organizacion/david_cortes.png', NULL, NULL, NULL, 'Cuenta con 17 años de experiencia profesional liderando proyectos y procesos relacionados con auditoría interna, revisoría fiscal y consultoría en sistemas de gestión, en entidades del sector financiero, público y real. Es profesional en Contaduría Pública de la Universidad de Bogotá Jorge Tadeo Lozano y especialista en Finanzas de la Universidad del Rosario. Actualmente se desempeña como Auditor Interno de Titularice; previamente se desempeñó como Gerente de Consultoría en Modelos de Gestión en KPMG Advisory, Tax and Legal SAS; pasó por otras Entidades como Fiduciaria Popular S.A. y Amézquita y Cia. S.A. trabajando como Coordinador Contable y Auditor Financiero.', 'He has 17 years of professional experience leading projects and processes related to internal auditing, tax auditing and consulting in management systems, in entities of the financial, public and real sectors. He is a professional in Public Accounting from the Universidad de Bogotá Jorge Tadeo Lozano and a specialist in Finance from the Universidad del Rosario. He currently works as Internal Auditor of Titularice; he previously served as Consulting Manager in Management Models at KPMG Advisory, Tax and Legal SAS; he went through other Entities such as Fiduciaria Popular S.A. and Amézquita y Cia. S.A. working as Accounting Coordinator and Financial Auditor.'),
(9, 'Mauricio Pardo', 'Director de Tecnología', 'Chief Technology Officer', 'imagenes_organizacion/mauricio_pardo.png', NULL, NULL, NULL, 'Ha liderado por mas de 15 años proyectos de Tecnologia y Ciberseguridad. Ingeniero de Sistemas de la Universidad Nacional de Colombia, cuenta con un MBA del Inalde Business School y con maestrias en Seguridad de la Informacion y Arquitectura de Informacion de la Universidad de los Andes, fue Gerente consultor de EY atendiendo diferentes clientes del sector financiero en Latinoamerica, asi como Gerente de Aseguramiento Informatico y Continuidad del Negocio de Grupo Fundacion Social.', 'He has led Technology and Cybersecurity projects for more than 15 years. Systems Engineer from the National University of Colombia, has an MBA from the Inalde Business School and a master\'s degree in Information Security and Information Architecture from the Universidad de los Andes, he was an EY Consulting Manager serving different clients in the financial sector in Latin America. , as well as Manager of IT Assurance and Business Continuity of Grupo Fundacion Social.');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `permisos`
--

CREATE TABLE `permisos` (
  `id` int(11) NOT NULL,
  `rol_id` int(11) NOT NULL,
  `modulo_id` int(11) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `deleted_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `permisos`
--

INSERT INTO `permisos` (`id`, `rol_id`, `modulo_id`, `created_at`, `updated_at`, `deleted_at`) VALUES
(12, 2, 10, '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL),
(13, 2, 1, '2021-03-22 15:26:53', '2021-03-22 15:26:53', NULL),
(14, 2, 2, '2021-03-22 15:26:58', '2021-03-22 15:26:58', NULL),
(15, 2, 3, '2021-03-22 15:27:07', '2021-03-22 15:27:07', NULL),
(16, 2, 9, '2021-03-22 15:27:22', '2021-03-22 15:27:22', NULL),
(17, 2, 6, '2021-03-22 15:27:30', '2021-03-22 15:27:30', NULL),
(18, 2, 8, '2021-03-22 15:27:40', '2021-03-22 15:27:40', NULL),
(19, 2, 7, '2021-03-22 15:27:45', '2021-03-22 15:27:45', NULL),
(20, 6, 2, '2021-03-23 16:50:14', '2021-03-23 16:50:14', NULL),
(21, 6, 3, '2021-03-23 16:58:44', '2021-03-23 16:58:44', NULL);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `preguntas_frecuentes`
--

CREATE TABLE `preguntas_frecuentes` (
  `id` int(11) NOT NULL,
  `pregunta_ES` longtext NOT NULL,
  `respuesta_ES` longtext DEFAULT NULL,
  `respuesta_EN` longtext DEFAULT NULL,
  `pregunta_EN` longtext DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `deleted_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Volcado de datos para la tabla `preguntas_frecuentes`
--

INSERT INTO `preguntas_frecuentes` (`id`, `pregunta_ES`, `respuesta_ES`, `respuesta_EN`, `pregunta_EN`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 'qhttp://127.0.0.1:8000/preguntas_frecuentes/create', NULL, 'qhttp://127.0.0.1:8000/preguntas_frecuentes/create', NULL, '2022-05-26 15:40:42', '2022-05-26 16:16:07', '2022-05-26 16:16:07'),
(2, 'qhttp://127.0.0.1:8000/preguntas_frecuentes/create', NULL, 'qhttp://127.0.0.1:8000/preguntas_frecuentes/create', NULL, '2022-05-26 15:47:56', '2022-05-26 19:23:14', '2022-05-26 19:23:14'),
(3, 'qhttp://127.0.0.1:8000/preguntas_frecuentes/create', 'Vivamus vulputate ex ante. Morbi non enim enim. Morbi arcu tellus, dictum a leo non, condimentum porta libero. In quis porttitor felis. Nunc maximus metus turpis, in accumsan justo malesuada nec.', 'qhttp://127.0.0.1:8000/preguntas_frecuentes/create', 'Vivamus vulputate ex ante. Morbi non enim enim. Morbi arcu tellus, dictum a leo non, condimentum porta libero. In quis porttitor felis. Nunc maximus', '2022-05-26 15:48:11', '2022-06-15 18:51:37', '2022-06-15 18:51:37'),
(4, 'qhttp://127.0.0.1:8000/preguntas_frecuentes/create', NULL, 'qhttp://127.0.0.1:8000/preguntas_frecuentes/create', NULL, '2022-05-26 15:48:37', '2022-05-26 16:13:54', '2022-05-26 16:13:54'),
(5, 'http://127.0.0.1:8000/preguntas_frecuentes/5/edit', 'http://127.0.0.1:8000/preguntas_frecuentes/5/edit', 'http://127.0.0.1:8000/preguntas_frecuentes/5/edithttp://127.0.0.1:8000/preguntas_frecuentes/5/edit', 'http://127.0.0.1:8000/preguntas_frecuentes/5/edit', '2022-05-26 16:00:43', '2022-05-26 16:13:42', '2022-05-26 16:13:42'),
(6, 'Vivamus vulputate ex ante. Morbi non enim enim. Morbi arcu tellus, dictum a leo non, condimentum porta libero. In quis porttitor felis. Nunc maximus metus turpis, in accumsan justo malesuada nec.', 'Vivamus vulputate ex ante. Morbi non enim enim. Morbi arcu tellus, dictum a leo non, condimentum porta libero. In quis porttitor felis. Nunc maximus metus turpis, in accumsan justo malesuada nec.', 'Vivamus vulputate ex ante. Morbi non enim enim. Morbi arcu tellus, dictum a leo non, condimentum porta libero. In quis porttitor felis. Nunc maximus metus turpis, in accumsan justo malesuada nec.', 'Vivamus vulputate ex ante. Morbi non enim enim. Morbi arcu tellus, dictum a leo non, condimentum porta libero. In quis porttitor felis. Nunc maximus metus turpis, in accumsan justo malesuada nec.', '2022-05-26 16:34:14', '2022-05-26 19:47:52', '2022-05-26 19:47:52'),
(7, 'Vivamus vulputate ex ante. Morbi non enim enim. Morbi arcu tellu', 'Vivamus vulputate ex ante. Morbi non enim enim. Morbi arcu tellus, dictum a leo non, condimentum porta libero. In quis porttitor felis. Nunc maximus metus turpis, in accumsan justo malesuada nec.', 'Vivamus vulputate ex ante. Morbi non enim enim. Morbi arcu tellus, dictum a leo non, condimentum porta libero. In quis porttitor felis. Nunc maximus metus turpis, in accumsan justo malesuada nec.', 'Vivamus vulputate ex ante. Morbi non enim enim. Morbi arcu tellu', '2022-05-26 19:21:02', '2022-06-14 23:56:10', '2022-06-14 23:56:10'),
(8, '1111111111111111111111111111111111111111111111111111111111111111111111111111111111111', '1111111111111111111111111111111111111111111111111111111111111111111111111111111111111', '1111111111111111111111111111111111111111111111111111111111111111111111111111111111111', '1111111111111111111111111111111111111111111111111111111111111111111111111111111111111', '2022-05-26 19:39:38', '2022-05-26 19:46:50', '2022-05-26 19:46:50'),
(9, '¿Qué es la titularización?', 'De acuerdo con la definición de la Real Academia de la Lengua Española, la Titularización es \"Convertir determinados activos, generalmente préstamos, en valores negociables en el mercado\". James Rosenthal (1988) la define como \" el proceso cuidadosamente estructurado a través del cual créditos y otros activos son empaquetados, suscritos y vendidos en forma de títulos valores a Inversionistas\". Para conocer más sobre la Titularización, lo invitamos a visitar nuestra sección La Titularización.', 'According to the definition of the Royal Academy of the Spanish Language, Securitization is \"Converting certain assets, generally loans, into marketable securities\". James Rosenthal (1988) defines it as \"the carefully structured process through which credits and other assets are packaged, subscribed and sold in the form of securities to Investors\". To learn more about Securitization, we invite you to visit our Securitization section.', 'What is securitization?', '2022-06-15 18:52:58', '2022-06-15 18:52:58', NULL),
(10, '¿Qué es la titularización de cartera hipotecaria?', 'La titularización hipotecaria es un mecanismo previsto en la Ley 546 de 1999 (Ley de Vivienda), mediante el cual los establecimientos de crédito, las sociedades titularizadoras y otras entidades autorizadas, emiten títulos representativos de créditos otorgados para financiar la construcción y la adquisición de vivienda.', 'Mortgage securitization is a mechanism provided for in Law 546 of 1999 (Housing Law), through which credit establishments, securitization companies and other authorized entities issue titles representing credits granted to finance the construction and acquisition of housing .', 'What is mortgage portfolio securitization?', '2022-06-15 18:53:39', '2022-06-15 18:53:39', NULL),
(11, '¿Cuáles son los beneficios de la titularización hipotecaria?', 'Para todos como colombianos este mecanismo también ofrece beneficios que han sido identificados en países que han utilizado la titularización como una herramienta de crecimiento económico: 1. Desarrollo general del sector inmobiliario, dado el potencial de oferta de créditos que genera en la medida en que se otorgue mayor liquidez a los bancos para que estos a su vez la traduzcan en nuevos créditos de vivienda 2. Aumento de la actividad edificadora 3. Incremento del empleo 4. Crecimiento de la economía 5. Adquisición de vivienda propia por millones de colombianos con claras facilidades, lo que ofrecerá bienestar y mejor calidad de vida 6. Mayor desarrollo y crecimiento del mercado de capitales 7. La adquisición de vivienda por parte de las familias se financiará indirectamente con los recursos que ellas mismas tienen ahorrados en diversas entidades, tales como los fondos de pensiones.', 'For all Colombians, this mechanism also offers benefits that have been identified in countries that have used securitization as a tool for economic growth: 1. General development of the real estate sector, given the potential supply of credits that it generates to the extent that grant greater liquidity to banks so that they in turn translate it into new housing loans 2. Increase in building activity 3. Increase in employment 4. Growth of the economy 5. Acquisition of their own homes by millions of Colombians with clear facilities , which will offer well-being and a better quality of life 6. Greater development and growth of the capital market 7. The acquisition of housing by families will be financed indirectly with the resources that they themselves have saved in various entities, such as funds of pensions.', 'What are the benefits of mortgage securitization?', '2022-06-15 19:00:23', '2022-06-15 19:00:23', NULL),
(12, '¿Cuáles son los beneficios de la titularización hipotecaria y no hipotecaria?', '1. En la medida en que la Titularizadora compre más cartera y se emitan títulos, se dará mayor liquidez a las entidades para que estas a su vez ofrezcan mayores alternativas de crédito. 2. Con ello se logrará mayor eficiencia en la originación del crédito 3. Se estandarizará el proceso de otorgamiento de crédito, sus tasas a nivel nacional y la simplificación de trámites. 4. Al tener actualmente un crédito con una entidad vendedora de cartera, el deudor mantendrá las mismas condiciones actuales que tiene contratadas con la entidad.', '1. To the extent that the Securitization Company buys more portfolio and titles are issued, greater liquidity will be given to the entities so that they in turn offer greater credit alternatives. 2. With this, greater efficiency will be achieved in credit origination. 3. The credit granting process, its rates at the national level, and the simplification of procedures will be standardized. 4. By currently having a loan with a portfolio seller entity, the debtor will maintain the same current conditions that he has contracted with the entity.', 'What are the benefits of mortgage and non-mortgage securitization?', '2022-06-15 19:01:20', '2022-06-15 19:01:20', NULL),
(13, '¿Cuáles son los beneficios de la titularización hipotecaria, si usted es un inversionista?', 'Si usted es un potencial inversionista y desea adquirir títulos valores respaldados por créditos hipotecarios y no hipotecarios obtendrá: 1. Títulos hipotecarios y no hipotecarios con altas acalificaciones de riesgo. 2. Rendimientos financieros de acuerdo a las condiciones de emisión de los títulos 3.Liquidez a través del mercado secundario, dado que los títulos están inscritos en la Bolsa de Valores 4. Protección frente al riesgo de crédito de los créditos titularizados a través de la diversificación y de los mecanismos de cobertura. Para mayor detalle consulte nuestros prospectos en la sección Emisiones.', 'If you are a potential investor and wish to purchase mortgage and non-mortgage backed securities, you will obtain: 1. Mortgage and non-mortgage securities with high risk ratings. 2. Financial returns according to the conditions of issuance of the securities 3. Liquidity through the secondary market, given that the securities are registered in the Stock Exchange 4. Protection against the credit risk of securitized loans through the diversification and hedging mechanisms. For further details, consult our prospectuses in the Emissions section.', 'What are the benefits of mortgage securitization if you are an investor?', '2022-06-15 19:02:18', '2022-06-15 19:02:18', NULL),
(14, '¿Qué son los bonos hipotecarios?', 'Los bonos hipotecarios, son títulos valores de contenido crediticio, emitidos directamente por los establecimientos de crédito, y los cuales tienen como finalidad exclusiva cumplir los contratos de crédito para la construcción de vivienda y para su financiación a largo plazo. Estos títulos valores se encuentran respaldadosen primer lugar por el balance del establecimiento de crédito y, en segundo lugar, por un activo que son los créditos hipotecarios, propiedad del establecimiento de crédito acreedor y a su vez emisor de los bonos hipotecarios.', 'Mortgage bonds are securities with a credit content, issued directly by credit institutions, and whose sole purpose is to fulfill credit contracts for housing construction and long-term financing. These securities are backed in the first place by the credit establishment\'s balance sheet and, secondly, by an asset that is the mortgage loans, property of the creditor credit establishment and, in turn, the issuer of the mortgage bonds.', 'What are mortgage bonds?', '2022-06-15 19:02:55', '2022-06-15 19:02:55', NULL),
(15, '¿Qué son los títulos hipotecarios?', 'Los títulos hipotecarios son títulos valores que pueden ser de participación y/o de contenido crediticio, emitidos en desarrollo de procesos de titularización que pueden llevar a cabo las sociedades titularizadora, los establecimientos de crédito y demás entidades de que trata el art. 1 de la Ley 546 de 1999 y que están respaldado con cartera hipotecaria.', 'Mortgage securities are securities that can be participation and/or credit content, issued in the development of securitization processes that can be carried out by securitization companies, credit establishments and other entities referred to in art. 1 of Law 546 of 1999 and that are backed by a mortgage portfolio.', 'What are mortgage titles?', '2022-06-15 19:03:31', '2022-06-15 19:03:31', NULL);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `reportes`
--

CREATE TABLE `reportes` (
  `id` int(11) NOT NULL,
  `anio` int(11) DEFAULT NULL,
  `url_ES` varchar(255) DEFAULT NULL,
  `url_EN` varchar(255) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `deleted_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `reportes_pais`
--

CREATE TABLE `reportes_pais` (
  `id` int(11) NOT NULL,
  `anio` int(11) DEFAULT NULL,
  `url_ES` varchar(255) DEFAULT NULL,
  `url_EN` varchar(255) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `deleted_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `riesgos`
--

CREATE TABLE `riesgos` (
  `id` int(11) NOT NULL,
  `riesgo_ES` longtext DEFAULT NULL,
  `riesgo_EN` longtext DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `deleted_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Volcado de datos para la tabla `riesgos`
--

INSERT INTO `riesgos` (`id`, `riesgo_ES`, `riesgo_EN`, `created_at`, `updated_at`, `deleted_at`) VALUES
(10, 'Sistemas para la Administración de Riesgo en Titularice', 'Systems for Risk Management in Titularice', NULL, NULL, NULL),
(11, 'Sistema de Administración de Riesgo Operativo', 'Operational Risk Management System', NULL, NULL, NULL),
(12, 'Sistema de Administración de Riesgo LA/FT', 'ML/FT Risk Management System', NULL, NULL, NULL),
(13, 'Lineamientos y políticas claras para la gestión de riesgo de mercado y liquidez. ', 'Clear guidelines and policies for market and liquidity risk management.', NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `roles`
--

CREATE TABLE `roles` (
  `id` int(11) NOT NULL,
  `nombre` varchar(64) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `deleted_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `roles`
--

INSERT INTO `roles` (`id`, `nombre`, `created_at`, `updated_at`, `deleted_at`) VALUES
(2, 'Administrador', '2021-03-04 23:08:27', '2021-03-06 09:28:19', NULL),
(6, 'Asistente', '2021-03-23 16:32:54', '2021-03-23 16:40:58', NULL);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `users`
--

CREATE TABLE `users` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email_verified_at` timestamp NULL DEFAULT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `usuarios`
--

CREATE TABLE `usuarios` (
  `id` int(11) NOT NULL,
  `nickname` varchar(45) NOT NULL,
  `email` varchar(64) NOT NULL,
  `password` varchar(64) NOT NULL,
  `nombres` varchar(128) NOT NULL,
  `apellidos` varchar(45) NOT NULL,
  `rol_id` varchar(45) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `deleted_at` datetime DEFAULT NULL,
  `clave_autorizacion` varchar(45) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `usuarios`
--

INSERT INTO `usuarios` (`id`, `nickname`, `email`, `password`, `nombres`, `apellidos`, `rol_id`, `created_at`, `updated_at`, `deleted_at`, `clave_autorizacion`) VALUES
(1, 'admin', 'carlos@test.com', '$2y$10$s3sofRNOfwnKVCP1ppe44OtfroY7bhkfzGji0a89bhaLBcihcUF5W', 'Carlos', 'Valle', '2', '0000-00-00 00:00:00', '2021-03-23 13:03:26', NULL, '0794');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `zonas`
--

CREATE TABLE `zonas` (
  `id` int(11) NOT NULL,
  `nombre` varchar(64) NOT NULL,
  `municipio_id` int(11) NOT NULL,
  `created_at` datetime NOT NULL,
  `deleted_at` datetime DEFAULT NULL,
  `updated_at` datetime NOT NULL,
  `departamento_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `zonas`
--

INSERT INTO `zonas` (`id`, `nombre`, `municipio_id`, `created_at`, `deleted_at`, `updated_at`, `departamento_id`) VALUES
(1, 'test2', 13, '2021-03-24 11:37:37', '2021-03-24 11:38:11', '2021-03-24 11:38:11', 2);

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `accesos`
--
ALTER TABLE `accesos`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `accionistas`
--
ALTER TABLE `accionistas`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `articulos`
--
ALTER TABLE `articulos`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `bitacora`
--
ALTER TABLE `bitacora`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `categorias`
--
ALTER TABLE `categorias`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `circulares`
--
ALTER TABLE `circulares`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `configuraciones`
--
ALTER TABLE `configuraciones`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `departamentos`
--
ALTER TABLE `departamentos`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `documentos_emisiones`
--
ALTER TABLE `documentos_emisiones`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `emisiones`
--
ALTER TABLE `emisiones`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `encabezados`
--
ALTER TABLE `encabezados`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `failed_jobs`
--
ALTER TABLE `failed_jobs`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `failed_jobs_uuid_unique` (`uuid`);

--
-- Indices de la tabla `filemanager`
--
ALTER TABLE `filemanager`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `gastos`
--
ALTER TABLE `gastos`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `gobierno_corporativo`
--
ALTER TABLE `gobierno_corporativo`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `informes_financieros_categorias`
--
ALTER TABLE `informes_financieros_categorias`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `informes_financieros_documentos`
--
ALTER TABLE `informes_financieros_documentos`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `inversionistas`
--
ALTER TABLE `inversionistas`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `junta_directiva`
--
ALTER TABLE `junta_directiva`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `modulos`
--
ALTER TABLE `modulos`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `municipios`
--
ALTER TABLE `municipios`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `normatividad_categorias`
--
ALTER TABLE `normatividad_categorias`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `organizacion`
--
ALTER TABLE `organizacion`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`);

--
-- Indices de la tabla `permisos`
--
ALTER TABLE `permisos`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `preguntas_frecuentes`
--
ALTER TABLE `preguntas_frecuentes`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `reportes`
--
ALTER TABLE `reportes`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `reportes_pais`
--
ALTER TABLE `reportes_pais`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `riesgos`
--
ALTER TABLE `riesgos`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `roles`
--
ALTER TABLE `roles`
  ADD PRIMARY KEY (`id`,`nombre`);

--
-- Indices de la tabla `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_email_unique` (`email`);

--
-- Indices de la tabla `usuarios`
--
ALTER TABLE `usuarios`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `zonas`
--
ALTER TABLE `zonas`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `accesos`
--
ALTER TABLE `accesos`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `accionistas`
--
ALTER TABLE `accionistas`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT de la tabla `articulos`
--
ALTER TABLE `articulos`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `bitacora`
--
ALTER TABLE `bitacora`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `categorias`
--
ALTER TABLE `categorias`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT de la tabla `circulares`
--
ALTER TABLE `circulares`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT de la tabla `configuraciones`
--
ALTER TABLE `configuraciones`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=20;

--
-- AUTO_INCREMENT de la tabla `documentos_emisiones`
--
ALTER TABLE `documentos_emisiones`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=171;

--
-- AUTO_INCREMENT de la tabla `emisiones`
--
ALTER TABLE `emisiones`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `encabezados`
--
ALTER TABLE `encabezados`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=15;

--
-- AUTO_INCREMENT de la tabla `failed_jobs`
--
ALTER TABLE `failed_jobs`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `filemanager`
--
ALTER TABLE `filemanager`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT de la tabla `gastos`
--
ALTER TABLE `gastos`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;

--
-- AUTO_INCREMENT de la tabla `gobierno_corporativo`
--
ALTER TABLE `gobierno_corporativo`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;

--
-- AUTO_INCREMENT de la tabla `informes_financieros_categorias`
--
ALTER TABLE `informes_financieros_categorias`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;

--
-- AUTO_INCREMENT de la tabla `informes_financieros_documentos`
--
ALTER TABLE `informes_financieros_documentos`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=92;

--
-- AUTO_INCREMENT de la tabla `inversionistas`
--
ALTER TABLE `inversionistas`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT de la tabla `junta_directiva`
--
ALTER TABLE `junta_directiva`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT de la tabla `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT de la tabla `modulos`
--
ALTER TABLE `modulos`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;

--
-- AUTO_INCREMENT de la tabla `normatividad_categorias`
--
ALTER TABLE `normatividad_categorias`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT de la tabla `organizacion`
--
ALTER TABLE `organizacion`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT de la tabla `permisos`
--
ALTER TABLE `permisos`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=22;

--
-- AUTO_INCREMENT de la tabla `preguntas_frecuentes`
--
ALTER TABLE `preguntas_frecuentes`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=16;

--
-- AUTO_INCREMENT de la tabla `reportes`
--
ALTER TABLE `reportes`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `reportes_pais`
--
ALTER TABLE `reportes_pais`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `riesgos`
--
ALTER TABLE `riesgos`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;

--
-- AUTO_INCREMENT de la tabla `roles`
--
ALTER TABLE `roles`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT de la tabla `users`
--
ALTER TABLE `users`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `usuarios`
--
ALTER TABLE `usuarios`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=21;

--
-- AUTO_INCREMENT de la tabla `zonas`
--
ALTER TABLE `zonas`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
