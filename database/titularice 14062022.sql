-- phpMyAdmin SQL Dump
-- version 5.0.4
-- https://www.phpmyadmin.net/
--
-- Servidor: 127.0.0.1
-- Tiempo de generación: 15-06-2022 a las 05:19:18
-- Versión del servidor: 10.4.16-MariaDB
-- Versión de PHP: 7.4.12

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `titularice`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `accesos`
--

CREATE TABLE `accesos` (
  `id` int(11) NOT NULL,
  `usuario_id` int(11) NOT NULL,
  `ip` varchar(45) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `deleted_at` datetime DEFAULT NULL,
  `user_agent` varchar(512) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `accionistas`
--

CREATE TABLE `accionistas` (
  `id` int(11) NOT NULL,
  `nombre_ES` varchar(64) DEFAULT NULL,
  `porcentaje` varchar(45) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `deleted_at` datetime DEFAULT NULL,
  `nombre_EN` varchar(64) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Volcado de datos para la tabla `accionistas`
--

INSERT INTO `accionistas` (`id`, `nombre_ES`, `porcentaje`, `created_at`, `updated_at`, `deleted_at`, `nombre_EN`) VALUES
(1, 'Carlos Valles', '30%', '2022-06-06 02:51:24', '2022-06-06 03:01:23', '2022-06-06 03:01:23', 'Charlye Valles'),
(2, 'Carlos Valle', '10', '2022-06-07 15:50:33', '2022-06-07 15:50:33', NULL, 'Carlos Valle');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `bitacora`
--

CREATE TABLE `bitacora` (
  `id` int(11) NOT NULL,
  `entidad` varchar(128) NOT NULL,
  `accion` varchar(16) NOT NULL,
  `user_id` int(11) NOT NULL,
  `datos_salientes` longtext DEFAULT NULL,
  `datos_entrantes` longtext NOT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `deleted_at` datetime DEFAULT NULL,
  `tabla_padre_id` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `categorias`
--

CREATE TABLE `categorias` (
  `id` int(11) NOT NULL,
  `cartera` varchar(255) DEFAULT NULL,
  `categoria_ES` varchar(255) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `deleted_at` datetime DEFAULT NULL,
  `categoria_EN` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Volcado de datos para la tabla `categorias`
--

INSERT INTO `categorias` (`id`, `cartera`, `categoria_ES`, `created_at`, `updated_at`, `deleted_at`, `categoria_EN`) VALUES
(1, 'EMISIONES-CANCELADAS', 'Liquidación y emisión2', '2022-06-07 09:53:36', '2022-06-07 10:04:15', '2022-06-07 10:04:15', 'Liquidación y emisión2'),
(2, 'CARTERA-LIBRANZAS', 'Liquidación y emisión', '2022-06-07 10:05:00', '2022-06-07 10:05:00', NULL, 'Liquidación y emisión'),
(3, 'FLUJOS-FUTUROS', 'Información de la emisión', '2022-06-07 15:56:46', '2022-06-07 15:56:46', NULL, 'Información de la emisión'),
(4, 'FLUJOS-FUTUROS', 'Indicadores de Información del activo titularizador', '2022-06-07 15:58:12', '2022-06-07 15:58:12', NULL, 'Indicadores de Información del activo titularizador'),
(5, 'FLUJOS-FUTUROS', 'Universalidad', '2022-06-07 15:58:29', '2022-06-07 15:58:29', NULL, 'Universalidad'),
(6, 'FLUJOS-FUTUROS', 'Títulos', '2022-06-07 15:58:45', '2022-06-07 15:58:45', NULL, 'Títulos'),
(7, 'CARTERA-LIBRANZAS', 'Liquidación y emisión', '2022-06-07 15:59:07', '2022-06-07 15:59:07', NULL, 'Liquidación y emisión'),
(8, 'FLUJOS-FUTUROS', 'Información de la emisión', '2022-06-07 15:59:31', '2022-06-07 15:59:31', NULL, 'Información de la emisión'),
(9, 'CARTERA-LIBRANZAS', 'Indicadores de Información del activo titularizador', '2022-06-07 15:59:53', '2022-06-07 15:59:53', NULL, 'Indicadores de Información del activo titularizador'),
(10, 'CARTERA-LIBRANZAS', 'Universalidad', '2022-06-07 16:00:12', '2022-06-07 16:00:12', NULL, 'Universalidad'),
(11, 'CARTERA-LIBRANZAS', 'Títulos', '2022-06-07 16:01:03', '2022-06-07 16:01:03', NULL, 'Títulos'),
(12, 'CARTERA-CONSUMO-COMERCIAL', 'Liquidación y emisión', '2022-06-07 16:01:38', '2022-06-07 16:01:38', NULL, 'Liquidación y emisión'),
(13, 'CARTERA-CONSUMO-COMERCIAL', 'Información de la emisión', '2022-06-07 16:01:59', '2022-06-07 16:01:59', NULL, 'Información de la emisión'),
(14, 'CARTERA-CONSUMO-COMERCIAL', 'Indicadores de Información del activo titularizador', '2022-06-07 16:02:18', '2022-06-07 16:02:18', NULL, 'Indicadores de Información del activo titularizador'),
(15, 'CARTERA-CONSUMO-COMERCIAL', 'Universalidad', '2022-06-07 16:02:36', '2022-06-07 16:02:36', NULL, 'Universalidad'),
(16, 'CARTERA-CONSUMO-COMERCIAL', 'Títulos', '2022-06-07 16:02:55', '2022-06-07 16:02:55', NULL, 'Títulos'),
(17, 'CARTERA-CONSUMON-COMERCIAL', 'Liquidación y emisión', '2022-06-07 16:03:20', '2022-06-07 16:03:20', NULL, 'Liquidación y emisión'),
(18, 'CARTERA-VEHICULOS', 'Información de la emisión', '2022-06-07 16:03:40', '2022-06-07 16:03:57', NULL, 'Información de la emisión'),
(19, 'CARTERA-VEHICULOS', 'Indicadores de Información del activo titularizador', '2022-06-07 16:04:20', '2022-06-07 16:04:20', NULL, 'Indicadores de Información del activo titularizador'),
(20, 'CARTERA-VEHICULOS', 'Universalidad', '2022-06-07 16:05:02', '2022-06-07 16:05:02', NULL, 'Universalidad'),
(21, 'CARTERA-VEHICULOS', 'Títulos', '2022-06-07 16:05:25', '2022-06-07 16:05:25', NULL, 'Títulos'),
(22, 'EMISIONES-CANCELADAS', 'Información de la emisión y su cancelación', '2022-06-07 16:05:57', '2022-06-07 16:05:57', NULL, 'Información de la emisión y su cancelación');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `configuraciones`
--

CREATE TABLE `configuraciones` (
  `id` int(11) NOT NULL,
  `key` varchar(45) DEFAULT NULL,
  `value` varchar(1024) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `deleted_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Volcado de datos para la tabla `configuraciones`
--

INSERT INTO `configuraciones` (`id`, `key`, `value`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 'TELEFONO', '0000000000', NULL, '2022-05-26 23:13:14', NULL);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `departamentos`
--

CREATE TABLE `departamentos` (
  `id` int(11) NOT NULL,
  `nombre` varchar(256) DEFAULT NULL,
  `codigo` varchar(64) DEFAULT NULL,
  `zona_pais_id` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `departamentos`
--

INSERT INTO `departamentos` (`id`, `nombre`, `codigo`, `zona_pais_id`) VALUES
(1, 'Ahuachapán', 'SV-AH', 1),
(2, 'Santa Ana', 'SV-SA', 1),
(3, 'Sonsonate', 'SV-SO', 1),
(4, 'La Libertad', 'SV-LI', 2),
(5, 'Chalatenango', 'SV-CH', 2),
(6, 'San Salvador', 'SV-SS', 2),
(7, 'Cuscatlán', 'SV-CU', 3),
(8, 'La Paz', 'SV-PA', 3),
(9, 'Cabañas', 'SV-CA', 3),
(10, 'San Vicente', 'SV-SV', 3),
(11, 'Usulután', 'SV-US', 4),
(12, 'Morazán', 'SV-MO', 4),
(13, 'San Miguel', 'SV-SM', 4),
(14, 'La Unión', 'SV-UN', 4);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `emisiones`
--

CREATE TABLE `emisiones` (
  `id` int(11) NOT NULL,
  `categoria_id` int(11) DEFAULT NULL,
  `titulo_ES` varchar(255) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `deleted_at` datetime DEFAULT NULL,
  `titulo_EN` varchar(255) DEFAULT NULL,
  `contenido_ES` longtext DEFAULT NULL,
  `contenido_EN` longtext DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Volcado de datos para la tabla `emisiones`
--

INSERT INTO `emisiones` (`id`, `categoria_id`, `titulo_ES`, `created_at`, `updated_at`, `deleted_at`, `titulo_EN`, `contenido_ES`, `contenido_EN`) VALUES
(1, 2, 'Emision de prueba 0', '2022-06-08 00:09:07', '2022-06-08 00:09:07', NULL, NULL, NULL, NULL),
(2, 2, 'Emision de prueba 1', '2022-06-08 00:09:07', '2022-06-08 00:09:07', NULL, NULL, NULL, NULL),
(3, 2, 'Emision de prueba 2', '2022-06-08 00:09:07', '2022-06-08 00:09:07', NULL, NULL, NULL, NULL),
(4, 2, 'Emision de prueba 3', '2022-06-08 00:09:07', '2022-06-08 00:09:07', NULL, NULL, NULL, NULL),
(5, 2, 'Emision de prueba 4', '2022-06-08 00:09:07', '2022-06-08 00:09:07', NULL, NULL, NULL, NULL),
(6, 2, 'Emision de prueba 5', '2022-06-08 00:09:07', '2022-06-08 00:09:07', NULL, NULL, NULL, NULL),
(7, 2, 'Emision de prueba 6', '2022-06-08 00:09:07', '2022-06-08 00:09:07', NULL, NULL, NULL, NULL),
(8, 2, 'Emision de prueba 7', '2022-06-08 00:09:07', '2022-06-08 00:09:07', NULL, NULL, NULL, NULL),
(9, 3, 'Emision de prueba 0', '2022-06-08 00:09:07', '2022-06-08 00:09:07', NULL, NULL, NULL, NULL),
(10, 3, 'Emision de prueba 1', '2022-06-08 00:09:07', '2022-06-08 00:09:07', NULL, NULL, NULL, NULL),
(11, 3, 'Emision de prueba 2', '2022-06-08 00:09:07', '2022-06-08 00:09:07', NULL, NULL, NULL, NULL),
(12, 3, 'Emision de prueba 3', '2022-06-08 00:09:07', '2022-06-08 00:09:07', NULL, NULL, NULL, NULL),
(13, 3, 'Emision de prueba 4', '2022-06-08 00:09:07', '2022-06-08 00:09:07', NULL, NULL, NULL, NULL),
(14, 3, 'Emision de prueba 5', '2022-06-08 00:09:07', '2022-06-08 00:09:07', NULL, NULL, NULL, NULL),
(15, 3, 'Emision de prueba 6', '2022-06-08 00:09:07', '2022-06-08 00:09:07', NULL, NULL, NULL, NULL),
(16, 3, 'Emision de prueba 7', '2022-06-08 00:09:07', '2022-06-08 00:09:07', NULL, NULL, NULL, NULL),
(17, 4, 'Emision de prueba 0', '2022-06-08 00:09:07', '2022-06-08 00:09:07', NULL, NULL, NULL, NULL),
(18, 4, 'Emision de prueba 1', '2022-06-08 00:09:07', '2022-06-08 00:09:07', NULL, NULL, NULL, NULL),
(19, 4, 'Emision de prueba 2', '2022-06-08 00:09:07', '2022-06-08 00:09:07', NULL, NULL, NULL, NULL),
(20, 4, 'Emision de prueba 3', '2022-06-08 00:09:07', '2022-06-08 00:09:07', NULL, NULL, NULL, NULL),
(21, 4, 'Emision de prueba 4', '2022-06-08 00:09:07', '2022-06-08 00:09:07', NULL, NULL, NULL, NULL),
(22, 4, 'Emision de prueba 5', '2022-06-08 00:09:07', '2022-06-08 00:09:07', NULL, NULL, NULL, NULL),
(23, 4, 'Emision de prueba 6', '2022-06-08 00:09:07', '2022-06-08 00:09:07', NULL, NULL, NULL, NULL),
(24, 4, 'Emision de prueba 7', '2022-06-08 00:09:07', '2022-06-08 00:09:07', NULL, NULL, NULL, NULL),
(25, 5, 'Emision de prueba 0', '2022-06-08 00:09:07', '2022-06-08 00:09:07', NULL, NULL, NULL, NULL),
(26, 5, 'Emision de prueba 1', '2022-06-08 00:09:07', '2022-06-08 00:09:07', NULL, NULL, NULL, NULL),
(27, 5, 'Emision de prueba 2', '2022-06-08 00:09:07', '2022-06-08 00:09:07', NULL, NULL, NULL, NULL),
(28, 5, 'Emision de prueba 3', '2022-06-08 00:09:07', '2022-06-08 00:09:07', NULL, NULL, NULL, NULL),
(29, 5, 'Emision de prueba 4', '2022-06-08 00:09:07', '2022-06-08 00:09:07', NULL, NULL, NULL, NULL),
(30, 5, 'Emision de prueba 5', '2022-06-08 00:09:07', '2022-06-08 00:09:07', NULL, NULL, NULL, NULL),
(31, 5, 'Emision de prueba 6', '2022-06-08 00:09:07', '2022-06-08 00:09:07', NULL, NULL, NULL, NULL),
(32, 5, 'Emision de prueba 7', '2022-06-08 00:09:07', '2022-06-08 00:09:07', NULL, NULL, NULL, NULL),
(33, 6, 'Emision de prueba 0', '2022-06-08 00:09:07', '2022-06-08 00:09:07', NULL, NULL, NULL, NULL),
(34, 6, 'Emision de prueba 1', '2022-06-08 00:09:07', '2022-06-08 00:09:07', NULL, NULL, NULL, NULL),
(35, 6, 'Emision de prueba 2', '2022-06-08 00:09:07', '2022-06-08 00:09:07', NULL, NULL, NULL, NULL),
(36, 6, 'Emision de prueba 3', '2022-06-08 00:09:07', '2022-06-08 00:09:07', NULL, NULL, NULL, NULL),
(37, 6, 'Emision de prueba 4', '2022-06-08 00:09:07', '2022-06-08 00:09:07', NULL, NULL, NULL, NULL),
(38, 6, 'Emision de prueba 5', '2022-06-08 00:09:07', '2022-06-08 00:09:07', NULL, NULL, NULL, NULL),
(39, 6, 'Emision de prueba 6', '2022-06-08 00:09:07', '2022-06-08 00:09:07', NULL, NULL, NULL, NULL),
(40, 6, 'Emision de prueba 7', '2022-06-08 00:09:07', '2022-06-08 00:09:07', NULL, NULL, NULL, NULL),
(41, 7, 'Emision de prueba 0', '2022-06-08 00:09:07', '2022-06-08 00:09:07', NULL, NULL, NULL, NULL),
(42, 7, 'Emision de prueba 1', '2022-06-08 00:09:07', '2022-06-08 00:09:07', NULL, NULL, NULL, NULL),
(43, 7, 'Emision de prueba 2', '2022-06-08 00:09:07', '2022-06-08 00:09:07', NULL, NULL, NULL, NULL),
(44, 7, 'Emision de prueba 3', '2022-06-08 00:09:07', '2022-06-08 00:09:07', NULL, NULL, NULL, NULL),
(45, 7, 'Emision de prueba 4', '2022-06-08 00:09:07', '2022-06-08 00:09:07', NULL, NULL, NULL, NULL),
(46, 7, 'Emision de prueba 5', '2022-06-08 00:09:07', '2022-06-08 00:09:07', NULL, NULL, NULL, NULL),
(47, 7, 'Emision de prueba 6', '2022-06-08 00:09:07', '2022-06-08 00:09:07', NULL, NULL, NULL, NULL),
(48, 7, 'Emision de prueba 7', '2022-06-08 00:09:07', '2022-06-08 00:09:07', NULL, NULL, NULL, NULL),
(49, 8, 'Emision de prueba 0', '2022-06-08 00:09:07', '2022-06-08 00:09:07', NULL, NULL, NULL, NULL),
(50, 8, 'Emision de prueba 1', '2022-06-08 00:09:07', '2022-06-08 00:09:07', NULL, NULL, NULL, NULL),
(51, 8, 'Emision de prueba 2', '2022-06-08 00:09:07', '2022-06-08 00:09:07', NULL, NULL, NULL, NULL),
(52, 8, 'Emision de prueba 3', '2022-06-08 00:09:07', '2022-06-08 00:09:07', NULL, NULL, NULL, NULL),
(53, 8, 'Emision de prueba 4', '2022-06-08 00:09:07', '2022-06-08 00:09:07', NULL, NULL, NULL, NULL),
(54, 8, 'Emision de prueba 5', '2022-06-08 00:09:07', '2022-06-08 00:09:07', NULL, NULL, NULL, NULL),
(55, 8, 'Emision de prueba 6', '2022-06-08 00:09:07', '2022-06-08 00:09:07', NULL, NULL, NULL, NULL),
(56, 8, 'Emision de prueba 7', '2022-06-08 00:09:07', '2022-06-08 00:09:07', NULL, NULL, NULL, NULL),
(57, 9, 'Emision de prueba 0', '2022-06-08 00:09:07', '2022-06-08 00:09:07', NULL, NULL, NULL, NULL),
(58, 9, 'Emision de prueba 1', '2022-06-08 00:09:07', '2022-06-08 00:09:07', NULL, NULL, NULL, NULL),
(59, 9, 'Emision de prueba 2', '2022-06-08 00:09:07', '2022-06-08 00:09:07', NULL, NULL, NULL, NULL),
(60, 9, 'Emision de prueba 3', '2022-06-08 00:09:07', '2022-06-08 00:09:07', NULL, NULL, NULL, NULL),
(61, 9, 'Emision de prueba 4', '2022-06-08 00:09:07', '2022-06-08 00:09:07', NULL, NULL, NULL, NULL),
(62, 9, 'Emision de prueba 5', '2022-06-08 00:09:07', '2022-06-08 00:09:07', NULL, NULL, NULL, NULL),
(63, 9, 'Emision de prueba 6', '2022-06-08 00:09:07', '2022-06-08 00:09:07', NULL, NULL, NULL, NULL),
(64, 9, 'Emision de prueba 7', '2022-06-08 00:09:07', '2022-06-08 00:09:07', NULL, NULL, NULL, NULL),
(65, 10, 'Emision de prueba 0', '2022-06-08 00:09:07', '2022-06-08 00:09:07', NULL, NULL, NULL, NULL),
(66, 10, 'Emision de prueba 1', '2022-06-08 00:09:07', '2022-06-08 00:09:07', NULL, NULL, NULL, NULL),
(67, 10, 'Emision de prueba 2', '2022-06-08 00:09:07', '2022-06-08 00:09:07', NULL, NULL, NULL, NULL),
(68, 10, 'Emision de prueba 3', '2022-06-08 00:09:07', '2022-06-08 00:09:07', NULL, NULL, NULL, NULL),
(69, 10, 'Emision de prueba 4', '2022-06-08 00:09:07', '2022-06-08 00:09:07', NULL, NULL, NULL, NULL),
(70, 10, 'Emision de prueba 5', '2022-06-08 00:09:07', '2022-06-08 00:09:07', NULL, NULL, NULL, NULL),
(71, 10, 'Emision de prueba 6', '2022-06-08 00:09:07', '2022-06-08 00:09:07', NULL, NULL, NULL, NULL),
(72, 10, 'Emision de prueba 7', '2022-06-08 00:09:07', '2022-06-08 00:09:07', NULL, NULL, NULL, NULL),
(73, 11, 'Emision de prueba 0', '2022-06-08 00:09:07', '2022-06-08 00:09:07', NULL, NULL, NULL, NULL),
(74, 11, 'Emision de prueba 1', '2022-06-08 00:09:07', '2022-06-08 00:09:07', NULL, NULL, NULL, NULL),
(75, 11, 'Emision de prueba 2', '2022-06-08 00:09:07', '2022-06-08 00:09:07', NULL, NULL, NULL, NULL),
(76, 11, 'Emision de prueba 3', '2022-06-08 00:09:07', '2022-06-08 00:09:07', NULL, NULL, NULL, NULL),
(77, 11, 'Emision de prueba 4', '2022-06-08 00:09:07', '2022-06-08 00:09:07', NULL, NULL, NULL, NULL),
(78, 11, 'Emision de prueba 5', '2022-06-08 00:09:07', '2022-06-08 00:09:07', NULL, NULL, NULL, NULL),
(79, 11, 'Emision de prueba 6', '2022-06-08 00:09:07', '2022-06-08 00:09:07', NULL, NULL, NULL, NULL),
(80, 11, 'Emision de prueba 7', '2022-06-08 00:09:07', '2022-06-08 00:09:07', NULL, NULL, NULL, NULL),
(81, 12, 'Emision de prueba 0', '2022-06-08 00:09:07', '2022-06-08 00:09:07', NULL, NULL, NULL, NULL),
(82, 12, 'Emision de prueba 1', '2022-06-08 00:09:07', '2022-06-08 00:09:07', NULL, NULL, NULL, NULL),
(83, 12, 'Emision de prueba 2', '2022-06-08 00:09:07', '2022-06-08 00:09:07', NULL, NULL, NULL, NULL),
(84, 12, 'Emision de prueba 3', '2022-06-08 00:09:07', '2022-06-08 00:09:07', NULL, NULL, NULL, NULL),
(85, 12, 'Emision de prueba 4', '2022-06-08 00:09:07', '2022-06-08 00:09:07', NULL, NULL, NULL, NULL),
(86, 12, 'Emision de prueba 5', '2022-06-08 00:09:07', '2022-06-08 00:09:07', NULL, NULL, NULL, NULL),
(87, 12, 'Emision de prueba 6', '2022-06-08 00:09:07', '2022-06-08 00:09:07', NULL, NULL, NULL, NULL),
(88, 12, 'Emision de prueba 7', '2022-06-08 00:09:07', '2022-06-08 00:09:07', NULL, NULL, NULL, NULL),
(89, 13, 'Emision de prueba 0', '2022-06-08 00:09:07', '2022-06-08 00:09:07', NULL, NULL, NULL, NULL),
(90, 13, 'Emision de prueba 1', '2022-06-08 00:09:07', '2022-06-08 00:09:07', NULL, NULL, NULL, NULL),
(91, 13, 'Emision de prueba 2', '2022-06-08 00:09:07', '2022-06-08 00:09:07', NULL, NULL, NULL, NULL),
(92, 13, 'Emision de prueba 3', '2022-06-08 00:09:07', '2022-06-08 00:09:07', NULL, NULL, NULL, NULL),
(93, 13, 'Emision de prueba 4', '2022-06-08 00:09:07', '2022-06-08 00:09:07', NULL, NULL, NULL, NULL),
(94, 13, 'Emision de prueba 5', '2022-06-08 00:09:07', '2022-06-08 00:09:07', NULL, NULL, NULL, NULL),
(95, 13, 'Emision de prueba 6', '2022-06-08 00:09:07', '2022-06-08 00:09:07', NULL, NULL, NULL, NULL),
(96, 13, 'Emision de prueba 7', '2022-06-08 00:09:07', '2022-06-08 00:09:07', NULL, NULL, NULL, NULL),
(97, 14, 'Emision de prueba 0', '2022-06-08 00:09:07', '2022-06-08 00:09:07', NULL, NULL, NULL, NULL),
(98, 14, 'Emision de prueba 1', '2022-06-08 00:09:07', '2022-06-08 00:09:07', NULL, NULL, NULL, NULL),
(99, 14, 'Emision de prueba 2', '2022-06-08 00:09:07', '2022-06-08 00:09:07', NULL, NULL, NULL, NULL),
(100, 14, 'Emision de prueba 3', '2022-06-08 00:09:07', '2022-06-08 00:09:07', NULL, NULL, NULL, NULL),
(101, 14, 'Emision de prueba 4', '2022-06-08 00:09:07', '2022-06-08 00:09:07', NULL, NULL, NULL, NULL),
(102, 14, 'Emision de prueba 5', '2022-06-08 00:09:07', '2022-06-08 00:09:07', NULL, NULL, NULL, NULL),
(103, 14, 'Emision de prueba 6', '2022-06-08 00:09:07', '2022-06-08 00:09:07', NULL, NULL, NULL, NULL),
(104, 14, 'Emision de prueba 7', '2022-06-08 00:09:07', '2022-06-08 00:09:07', NULL, NULL, NULL, NULL),
(105, 15, 'Emision de prueba 0', '2022-06-08 00:09:07', '2022-06-08 00:09:07', NULL, NULL, NULL, NULL),
(106, 15, 'Emision de prueba 1', '2022-06-08 00:09:07', '2022-06-08 00:09:07', NULL, NULL, NULL, NULL),
(107, 15, 'Emision de prueba 2', '2022-06-08 00:09:07', '2022-06-08 00:09:07', NULL, NULL, NULL, NULL),
(108, 15, 'Emision de prueba 3', '2022-06-08 00:09:07', '2022-06-08 00:09:07', NULL, NULL, NULL, NULL),
(109, 15, 'Emision de prueba 4', '2022-06-08 00:09:07', '2022-06-08 00:09:07', NULL, NULL, NULL, NULL),
(110, 15, 'Emision de prueba 5', '2022-06-08 00:09:07', '2022-06-08 00:09:07', NULL, NULL, NULL, NULL),
(111, 15, 'Emision de prueba 6', '2022-06-08 00:09:07', '2022-06-08 00:09:07', NULL, NULL, NULL, NULL),
(112, 15, 'Emision de prueba 7', '2022-06-08 00:09:07', '2022-06-08 00:09:07', NULL, NULL, NULL, NULL),
(113, 16, 'Emision de prueba 0', '2022-06-08 00:09:07', '2022-06-08 00:09:07', NULL, NULL, NULL, NULL),
(114, 16, 'Emision de prueba 1', '2022-06-08 00:09:07', '2022-06-08 00:09:07', NULL, NULL, NULL, NULL),
(115, 16, 'Emision de prueba 2', '2022-06-08 00:09:07', '2022-06-08 00:09:07', NULL, NULL, NULL, NULL),
(116, 16, 'Emision de prueba 3', '2022-06-08 00:09:07', '2022-06-08 00:09:07', NULL, NULL, NULL, NULL),
(117, 16, 'Emision de prueba 4', '2022-06-08 00:09:07', '2022-06-08 00:09:07', NULL, NULL, NULL, NULL),
(118, 16, 'Emision de prueba 5', '2022-06-08 00:09:07', '2022-06-08 00:09:07', NULL, NULL, NULL, NULL),
(119, 16, 'Emision de prueba 6', '2022-06-08 00:09:07', '2022-06-08 00:09:07', NULL, NULL, NULL, NULL),
(120, 16, 'Emision de prueba 7', '2022-06-08 00:09:07', '2022-06-08 00:09:07', NULL, NULL, NULL, NULL),
(121, 17, 'Emision de prueba 0', '2022-06-08 00:09:07', '2022-06-08 00:09:07', NULL, NULL, NULL, NULL),
(122, 17, 'Emision de prueba 1', '2022-06-08 00:09:07', '2022-06-08 00:09:07', NULL, NULL, NULL, NULL),
(123, 17, 'Emision de prueba 2', '2022-06-08 00:09:07', '2022-06-08 00:09:07', NULL, NULL, NULL, NULL),
(124, 17, 'Emision de prueba 3', '2022-06-08 00:09:07', '2022-06-08 00:09:07', NULL, NULL, NULL, NULL),
(125, 17, 'Emision de prueba 4', '2022-06-08 00:09:07', '2022-06-08 00:09:07', NULL, NULL, NULL, NULL),
(126, 17, 'Emision de prueba 5', '2022-06-08 00:09:07', '2022-06-08 00:09:07', NULL, NULL, NULL, NULL),
(127, 17, 'Emision de prueba 6', '2022-06-08 00:09:07', '2022-06-08 00:09:07', NULL, NULL, NULL, NULL),
(128, 17, 'Emision de prueba 7', '2022-06-08 00:09:07', '2022-06-08 00:09:07', NULL, NULL, NULL, NULL),
(129, 18, 'Emision de prueba 0', '2022-06-08 00:09:07', '2022-06-08 00:09:07', NULL, NULL, NULL, NULL),
(130, 18, 'Emision de prueba 1', '2022-06-08 00:09:07', '2022-06-08 00:09:07', NULL, NULL, NULL, NULL),
(131, 18, 'Emision de prueba 2', '2022-06-08 00:09:07', '2022-06-08 00:09:07', NULL, NULL, NULL, NULL),
(132, 18, 'Emision de prueba 3', '2022-06-08 00:09:07', '2022-06-08 00:09:07', NULL, NULL, NULL, NULL),
(133, 18, 'Emision de prueba 4', '2022-06-08 00:09:07', '2022-06-08 00:09:07', NULL, NULL, NULL, NULL),
(134, 18, 'Emision de prueba 5', '2022-06-08 00:09:07', '2022-06-08 00:09:07', NULL, NULL, NULL, NULL),
(135, 18, 'Emision de prueba 6', '2022-06-08 00:09:07', '2022-06-08 00:09:07', NULL, NULL, NULL, NULL),
(136, 18, 'Emision de prueba 7', '2022-06-08 00:09:07', '2022-06-08 00:09:07', NULL, NULL, NULL, NULL),
(137, 19, 'Emision de prueba 0', '2022-06-08 00:09:07', '2022-06-08 00:09:07', NULL, NULL, NULL, NULL),
(138, 19, 'Emision de prueba 1', '2022-06-08 00:09:07', '2022-06-08 00:09:07', NULL, NULL, NULL, NULL),
(139, 19, 'Emision de prueba 2', '2022-06-08 00:09:07', '2022-06-08 00:09:07', NULL, NULL, NULL, NULL),
(140, 19, 'Emision de prueba 3', '2022-06-08 00:09:07', '2022-06-08 00:09:07', NULL, NULL, NULL, NULL),
(141, 19, 'Emision de prueba 4', '2022-06-08 00:09:07', '2022-06-08 00:09:07', NULL, NULL, NULL, NULL),
(142, 19, 'Emision de prueba 5', '2022-06-08 00:09:07', '2022-06-08 00:09:07', NULL, NULL, NULL, NULL),
(143, 19, 'Emision de prueba 6', '2022-06-08 00:09:07', '2022-06-08 00:09:07', NULL, NULL, NULL, NULL),
(144, 19, 'Emision de prueba 7', '2022-06-08 00:09:07', '2022-06-08 00:09:07', NULL, NULL, NULL, NULL),
(145, 20, 'Emision de prueba 0', '2022-06-08 00:09:07', '2022-06-08 00:09:07', NULL, NULL, NULL, NULL),
(146, 20, 'Emision de prueba 1', '2022-06-08 00:09:07', '2022-06-08 00:09:07', NULL, NULL, NULL, NULL),
(147, 20, 'Emision de prueba 2', '2022-06-08 00:09:07', '2022-06-08 00:09:07', NULL, NULL, NULL, NULL),
(148, 20, 'Emision de prueba 3', '2022-06-08 00:09:07', '2022-06-08 00:09:07', NULL, NULL, NULL, NULL),
(149, 20, 'Emision de prueba 4', '2022-06-08 00:09:07', '2022-06-08 00:09:07', NULL, NULL, NULL, NULL),
(150, 20, 'Emision de prueba 5', '2022-06-08 00:09:07', '2022-06-08 00:09:07', NULL, NULL, NULL, NULL),
(151, 20, 'Emision de prueba 6', '2022-06-08 00:09:07', '2022-06-08 00:09:07', NULL, NULL, NULL, NULL),
(152, 20, 'Emision de prueba 7', '2022-06-08 00:09:07', '2022-06-08 00:09:07', NULL, NULL, NULL, NULL),
(153, 21, 'Emision de prueba 0', '2022-06-08 00:09:07', '2022-06-08 00:09:07', NULL, NULL, NULL, NULL),
(154, 21, 'Emision de prueba 1', '2022-06-08 00:09:07', '2022-06-08 00:09:07', NULL, NULL, NULL, NULL),
(155, 21, 'Emision de prueba 2', '2022-06-08 00:09:07', '2022-06-08 00:09:07', NULL, NULL, NULL, NULL),
(156, 21, 'Emision de prueba 3', '2022-06-08 00:09:07', '2022-06-08 00:09:07', NULL, NULL, NULL, NULL),
(157, 21, 'Emision de prueba 4', '2022-06-08 00:09:07', '2022-06-08 00:09:07', NULL, NULL, NULL, NULL),
(158, 21, 'Emision de prueba 5', '2022-06-08 00:09:07', '2022-06-08 00:09:07', NULL, NULL, NULL, NULL),
(159, 21, 'Emision de prueba 6', '2022-06-08 00:09:07', '2022-06-08 00:09:07', NULL, NULL, NULL, NULL),
(160, 21, 'Emision de prueba 7', '2022-06-08 00:09:07', '2022-06-08 00:09:07', NULL, NULL, NULL, NULL),
(161, 22, 'Emision de prueba 0', '2022-06-08 00:09:07', '2022-06-08 00:09:07', NULL, NULL, NULL, NULL),
(162, 22, 'Emision de prueba 1', '2022-06-08 00:09:07', '2022-06-08 00:09:07', NULL, NULL, NULL, NULL),
(163, 22, 'Emision de prueba 2', '2022-06-08 00:09:07', '2022-06-08 00:09:07', NULL, NULL, NULL, NULL),
(164, 22, 'Emision de prueba 3', '2022-06-08 00:09:07', '2022-06-08 00:09:07', NULL, NULL, NULL, NULL),
(165, 22, 'Emision de prueba 4', '2022-06-08 00:09:07', '2022-06-08 00:09:07', NULL, NULL, NULL, NULL),
(166, 22, 'Emision de prueba 5', '2022-06-08 00:09:07', '2022-06-08 00:09:07', NULL, NULL, NULL, NULL),
(167, 22, 'Emision de prueba 6', '2022-06-08 00:09:07', '2022-06-08 00:09:07', NULL, NULL, NULL, NULL),
(168, 22, 'Emision de prueba 7', '2022-06-08 00:09:07', '2022-06-08 00:09:07', NULL, NULL, NULL, NULL),
(169, 6, 'prueba', '2022-06-08 02:15:02', '2022-06-08 02:15:56', '2022-06-08 02:15:56', NULL, NULL, NULL),
(170, 22, 'TEST100', '2022-06-08 09:35:19', '2022-06-08 09:35:19', NULL, NULL, NULL, NULL),
(171, 22, 'TEST25', '2022-06-13 16:15:21', '2022-06-13 16:15:21', NULL, NULL, NULL, NULL),
(172, 22, 'TEST32', '2022-06-13 16:18:13', '2022-06-13 16:18:13', NULL, NULL, NULL, NULL),
(173, 22, 'TEST', '2022-06-14 15:25:02', '2022-06-14 15:30:19', '2022-06-14 15:30:19', NULL, NULL, NULL),
(174, 3, NULL, '2022-06-14 17:28:10', '2022-06-14 17:28:10', NULL, NULL, NULL, NULL),
(175, 2, NULL, '2022-06-14 17:29:26', '2022-06-14 17:29:26', NULL, NULL, '<div id=\"lipsum\">\r\n<h1 align=\"center\">\r\nLorem ipsum dolor sit amet </h1><p><br>consectetur adipiscing elit. Phasellus \r\npellentesque, tortor nec accumsan sagittis, urna nisl ultrices neque, \r\nsit amet dapibus lacus mi ut nulla. Ut dignissim congue accumsan. \r\nSuspendisse quis arcu fringilla, molestie leo eget, consectetur mi. Nunc\r\n sit amet eros ac neque sollicitudin dignissim ut vitae velit. Mauris \r\nsem nisi, iaculis eget facilisis ac, sollicitudin in velit. Nam \r\nmalesuada mi sed dignissim ullamcorper. Donec nec lacus eget lacus \r\nfacilisis gravida nec ac enim. Mauris egestas elit in consequat \r\nsollicitudin. Suspendisse pulvinar aliquam odio eu posuere. Nam in \r\nconsectetur lectus. Sed posuere justo ipsum, non tristique odio vehicula\r\n et. In consectetur mollis turpis, a convallis dui placerat in. Aliquam \r\nlobortis massa nec aliquet varius. Cras et viverra enim. Integer aliquam\r\n nisi sit amet volutpat blandit.\r\n</p>\r\n<p>\r\nMauris vitae ullamcorper massa, non tristique tortor. Aenean id sem \r\nelementum, pulvinar urna in, laoreet arcu. Quisque quis diam bibendum, \r\ndictum lacus condimentum, vulputate nisi. Duis gravida risus vel erat \r\nblandit, vitae iaculis velit posuere. Donec nec iaculis est. Donec id \r\negestas elit. Pellentesque non ultrices nisl.\r\n</p>\r\n<p>\r\nInteger auctor nisl at nulla pellentesque condimentum. Etiam blandit \r\nimperdiet ex, eget ultrices augue aliquet et. Sed purus velit, vulputate\r\n nec egestas sit amet, posuere at ex. Class aptent taciti sociosqu ad \r\nlitora torquent per conubia nostra, per inceptos himenaeos. Donec ut \r\nante quis lacus luctus mattis. Cras hendrerit nunc risus, a malesuada ex\r\n dictum vel. Vestibulum ante ipsum primis in faucibus orci luctus et \r\nultrices posuere cubilia curae; Cras ut pharetra tellus. Praesent a arcu\r\n felis. Ut luctus suscipit mattis. Etiam sodales ultricies turpis, sit \r\namet pulvinar tortor auctor sed.\r\n</p>\r\n<p>\r\nMorbi a neque leo. Donec id consectetur lorem. Quisque lacinia risus \r\nfacilisis, mollis sem et, ornare nisl. Phasellus nec neque id libero \r\nauctor pretium. Morbi pretium, urna at sagittis posuere, sapien lacus \r\npellentesque turpis, a sodales erat neque ac ligula. Pellentesque \r\nhabitant morbi tristique senectus et netus et malesuada fames ac turpis \r\negestas. Ut accumsan rutrum lectus vitae ullamcorper. Orci varius \r\nnatoque penatibus et magnis dis parturient montes, nascetur ridiculus \r\nmus. Nulla tempor a quam eu fermentum. Praesent sed est lacus. Proin \r\ndapibus sed quam ut convallis. Suspendisse at libero justo. Curabitur \r\nlibero risus, malesuada vel rhoncus a, aliquam quis nibh.\r\n</p>\r\n<p>\r\n<span style=\"background-color: rgb(255, 255, 0); font-family: &quot;Impact&quot;;\"><font color=\"#000000\">Proin elementum, lorem sit amet interdum vulputate, justo lacus commodo \r\ndiam, quis eleifend dolor nunc id tortor. Vivamus fringilla enim lectus,\r\n ac rutrum ante gravida sed. Suspendisse in tincidunt enim. Cras in \r\nmaximus urna. Curabitur malesuada, nibh a tristique pellentesque, velit \r\nmauris aliquet velit, ac aliquam sapien odio ac nibh. Quisque quis \r\nsollicitudin enim. Aenean volutpat, tellus sollicitudin aliquam \r\nconsectetur, dui magna bibendum lacus, et rutrum sapien leo eget turpis.\r\n</font></span></p><p><br></p></div><br><table class=\"table table-bordered\"><tbody><tr><td>a<br></td><td>a<br></td></tr><tr><td>b<br></td><td>b<br></td></tr></tbody></table><div id=\"lipsum\"><p><br></p><ul><li>q7q</li><li>q</li><li>qq<br><img style=\"width: 171px;\" src=\"data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAKsAAACrCAYAAAAZ6GwZAAAACXBIWXMAACE4AAAhOAFFljFgAAAAAXNSR0IArs4c6QAAAARnQU1BAACxjwv8YQUAAA9FSURBVHgB7Z1Nchu3EscbI1m25EWYE2S8SFXsLCydwNTiVcXKwvYJTJ1A8glEnSDSCUSfwMrGevWyMHUCMYvYrmTh8QlCbyTHFqdfAwMy+hhghtSQBAb9q6IpEzNDSvxPo9EAugUw3oE//BSDEKv04yN60M9AP4tYNQrRhzTt0U89QPxVvP9vF2qCAMZprgkzipokwsYYl0jo+Bfi3dEheA6L1SEqEKYZgXvij6MX4DEs1jmBcbMBy8tNmIYwzfTg9HRdJN0+eMgiMFNnJMyIRJmSOC/6mKODEGbAKn2OV/S8Dh7ClrViSglz3njqErBYb4A7wsSE3rdHImwACulKrBae8vXrmvjrtx54BLsBJbEKU/bgM7vttTARj0GO9M/Ould9UDVQWxAHJNym8TKLizv07zPwCLasOShh3rmzSl/46py7cinCLgnzo3rOEaYNfLCxR09bxmufnt7zabAVvFgvCRPFQ3ql6YQwEXsU0E/ghpBgT8DsFmyKt6874AlBuQFWYY4G4zO5f6ciTAP79DjIbUEs9m0dorZiDVSY11laOoQvX/LFKsQ34BG1EKtTwiQx0vPvMA9h5iB6h31yBfIbEWPwCO/E6qQwBT2n0J23MOuO02JlYTIXcUasLEymiLmIlYXJTMLUxaqEeetWDEuLTRYmcxOmphIagZLVxF8oPNIExk0Qu+LdkTcrsCKYAmpuGuAVC9V5Yv1decFUxApR1AK5oJhxGyHkou8P1Ase+CDa6YjVs2AzAy0S7Rv8/j9OT79OR6yMj8Q0ED4hC/scHIXFylwmijquCnYecdYOMLNFkFtWdgeBJIr2SLDHroX2Zi5W8fb1JjBzQU/GPKWB1Y4aXJlpSB+Wnu+BQ7AbEBByVwBZyw7FVu/JxBf0im2XQExRgjY4BIs1UEiwe5AO1goEu6WssSOwWANG+aTpwLZpUG6SbIEjsFgDRydu27Uc8gQcgcXKyK0ve0Z3gKbMXXEFWKyM2vpCsYKXxgOWl5rgACxWJgOxa2wTi05Mw7JYmYxsra+J78ABWKxMxp07zmdmYbEyisxvdRsWK+MNLFbGG1isjDewWBlvYLEy3sBiZbyBxcp4A4uV8QYWK+MNXK1lDPQepgYMBg1YWLi6bC7h3FnThcVq4Eq5ylUQMsOMiFVjlN8hqQzTF6tSAxyPW2GFMcNivYAS6N3lFqTwZOI8XbL+anaufGzDygrg/cddeu0liZgzF94AFiuoRHJNvT25OZUCbEPxkkUm4R6SoPf1dhJmDIIeYEmRknjeqD3ys8p4KMRTlVfqwcYHeu+nwJQmSMuqMuYJcTDnlJzyM7wi0XbIPdhl96CY4Cwr3t/YopH8iUO5Y1s67eQOMFaCEau0pqrLF7CnBkE3viAeAqSb9PwSqqEtS1f6lNx31gQhVpUyvkq/NIVN8e7omXirUvFQ9CCtKn/Xqg95UudF7cWKP/z8nEQqk4zFUAWIL8T7y8V5Zf6oEoItG2t1Pk/qvKi1WJVQI+yU6vblVuRMcInlqI7KEZWDEmxW1NfGWmm3weE8qfOitmIdCbXwQExIpOuqakk2MxVbjtu1XmtpqW1JdCZvmKfKbUB8pq5XBAv2ErUUa+ajlhLqPk2Hrl0I0NtG5PtF4SWd2cRmXVXeKBLsIdy+vQbFlngo2CYw9RPrqKyRnb4eJG0P5+21IOL8i5IVPDvrQBlseaNoADUUnhS2ePt6u0SeVCnYVxwlqKNlzTI2x8b2rPtdvzpIovPM3a0QpRejFFpXOYN18b/KB8b1AregoQTrUK7UeVArserAemw+gASBuE4WLS9VTtN4XpGvepWvcGhsE+LaTaE+DxYKdhXuLgc9cVAbsepusm05pK+EmuN34vcbMq4ZG67cG3cqVPxF4hPGRGeNvDiqeg8hnlldAhTbIfuv9bGsWfdv5hw3jaJblMWPDSBMNkOF4tjYtrjYzHtZWVh7Jmr5ex5AoNRCrNT9t8Ae9N8Vfx7ZuuZHxrbz8y5MQpp2zY3mm6NEJmo5bbwNAVIXy2r25cgPJIvVBivGadi++Os3WypI8xWV6EzZpKNH1nPl5xW2fKliJ8TBlvdLBAutqhAv8cFPLfMFZDEz4wxXYj23EDVgypvnb6jVXyL9ZD5VuRFNQ2sDVlakdW1DQNRhPetOcbulA7HvCiChTclHlKu/btaxyahCGwLCazfAGsivP3FokQG/fVZbID8EAvv9PR9gidD3MD0NaaDlrVizLhCDGxFfQSbdCGahtr+WNYp4Z6hEhNO7+BsNEPiQwjv5bdl60TILT2LjjJBcJI0llhkWIUQrbz1A1oYvYIDl4rimGToBjyAQ/BUrmgL5NJf/zjJbdfFI+2i6V0UiCpptkt10vljTQSLe/68LZa7zYCOB3MiHiCEQvHQD9MITQyN+hLJEUWxsE6Ka/FS264jF8j43ommtgZxgcKKo2rTx02ddSC1fsuhCFaRpAlVQ1XWyRG8GztdgmiB+AgfwU6xCWEbAgwTKE8N8icc4NjG2jGOhbZjWI6gcCfPHV7GavxyMqum+B4NqrmPfLVuec5FYWmOoglu35fLEixa8n209dyOJXP1yXYnoA1TB4q2/wSUW0r7RtmA18WZdEnNNbbhMydX6/LnnUm5ZP8WKGJN1BUYjxDdQIYZtP3PHVzfAmbudmR2+zmCxWC9TPlznMfXbio3pPaiC86/fQjXEUA0xBI6vYk2MLbZA/1VsU7LXq7HMF1sEpKqIg+P4KdavlgA5YvlVSNbZpXQWYk1KH2mLLVc38eA0for17lJibqRIQRVUFWi3WXo8L+97CzQvWKEQEwSAl2LN4oGT7Ry9hM0iYWVrZWNjyzgTGBiZLGs/lDpbHk8KqKnBvLWcDbz/+FXJ8JZZkEI8p+D4Q7g55u5bLOzQe5TZmmLegYvpMVSImhBAfK595GOKuXbAETxeIojHxoXH1SxIXgWb0KpAWDLBlL9IFypC1wOTGRiHN0aLxLsFp6frLlhvf0NXjiyumDuTZozJQy5Ev+7+rMLycgscwFux6rxVCYSMzDYzYcYYA3Huq0JU4Q7dGN8nBaoq6+Mrv0KlGPx8Xs9aASrLdMAgVvv7Y24e2n7l7zMhXotVhbBsCczqDKa/Vl1CU1ei2R0lNUb1t113pVSn/+tZB7gLkaUYm1w8bCgHpJpXnzbgy5e/Ded2VRWXCcEfH78xbmw8Pf3WNsLWCefMebYQpmLtdMbFNjiI9wtZ1Cr2ovSQluIResFxYji3OWnGE3UT2Hbg2oSafV5bGs9uiCXg67HqSlpXM6p4BNgxD1QmzXjyzz9Nc6Owj+Czzxsb2xGrKr/pFbUQq7YytppSq9Ql/2JpN4tn0gkGIZ4Y2/DceHPgjz/Lz2m7QTqhlnuvz3pWWd3PVu1EFo8wlUlfWjJPMJiyqRTTNLZglHtzqM+HuG0+r0SVwxpTG7Eq37O4e2znCbYgqtAYNw8q3n8srXGc36j8zeTay9nnaoP1wrgbqlWV1GqnQAl3QJIrWEgtfisN0mCsD2KxxjmFhnXX3wY7u7qYcbDUbluLKjFZHHttk/V7cylKcPt2x3xRigqUtK76mjY/tzs6liIN8nNYu351IFnjwiIe9aeeVbFlsoaiqtMkQJmZT8cziycYylrXKLKEnP4N5Cvxr6ycqM9hI6uKOLPRv7qBHvwkV1u1XUsDP5XN92QtOqaukCzETDb8KwsnxBt6xCUO7+iBS2wt/iZLvVvim2otKMCJ7XyQMd0okt1+cZRhWL5zRn6q+ptdrX0rcE/8cfQCHKCelhX0qqzieqhDWvQlfdA5+s3HR9GBaZJAv26P50bRI1hYkGJ2TqiKBSFnzOJLrzlUgrO2YpWMBGvNwHeJFti3PMfUdefHa1dWDqB4u3S71HYZhN7Mhare1+CSOFJoo9ZilcgvnFwPmRKyKEpQlpbcNjMcnGU+3oa0qFXsTpAWdR/OTp1ZPOIStRfrEBUlSNNNgApSD8lZLXIbSKQncPeuTARXhVD7etHNdigbAMclGLFKVJwyHazlxTonZLWSXbByKV6artlWhzF1THlZgO5eW9SNd1U4qly0YDrosFSIK6gmISjLehFpZcmS3VOuQbmIQXVkc/yb8v1ZqOUJVqxDRqKV5YgAprtjNuvu17VIO8CMRXBugAldjuhQj/Ll7NYTyFZO3cQnlQOlrspxcHbW4YHTzWCxXkH7tB39GGYoiXVitIc0oyN3AFzPkCJoMgFV3v+PNFVF3Tx0OfxULSzWAnTKcvmYrovAFBK8z8r4A4uV8QYWK+MNLFbGG1isjDewWBlvYLEy3uBlnFXvm9qCaWemniVyKjZb1JIUHpplqN7R+7dkDHjfpXTq08I7y3ohYVl9hCoZbmAsyK012if170ZD+Xc40LkKao1/boBAJ7ZYTIm4MCW6effsFtQc/8SKIoY6Y68kKG/W2HBeDDXHR8uaQJ0pqhaI4vf819Pfoeb4J9YBVrOPykUQeoXrXPMT0PXpXHtWlxrgXTRAjpZpkLFG3d423WpOVBG5MSjkZsHjMnuwZOYY+v3l9vJt+ht8R+d9lDn/Q1iO6GXoSn8xtbckJkL9/XlSgPEGFivjDSxWxhtYrIw3sFgZb2CxMt7Au1tBr2KKCtNVVsfXQa/iatZBELxYVfGJopz+VXMrkqUyncko7QtBuwFqueGshTp6c1WXqwVMaUL3WZ/AfHkETGnCFiviJ5gnmH4LTGlCF2sH5glySqJxCFqsOjfqfGqhypTsnPZyLIKPBsjKfRS66gDMMHT1+XOP01+OD8dZYbTkLgHGaXgGi/EGFivjDSxWxhtYrIw3sFgZb2CxMt7AYmW8gcXKeAOLlfEGFivjDSxWRqHLgBoa57yUUsNiZYbExhbhRiI8FiuTEUXmJM147sTmRhYrA6NK4MYDIhYr4wiZVY3zG7HnSjpNFmvgqBL1AG3jASnugyOwWANGC/WV+QCVYbsLjsBiDRS8v7FFo/w3YI8CvHQpozZvawkIVWNrebkJsgyRoAEVouVgTMS7ozY4xMzFSl3PATCzBbGhSw+VLXQnaxysg2PMw7K2gJktQox1OJzjpvjTvYIa7LMyl0mBhHrkZPIN9lmZDDnyF+KZeP/a2VSc07GsQiTA+ANSLPXsbE28dVeokjGdmXJklZsXTuinBjCu0tdl4/d1GiXnmYpYJUqwC+IAcFRqnJkvcuVUQg+ynukxnH4+9C2F0f8BGI1hRfmI0Q8AAAAASUVORK5CYII=\" data-filename=\"this.png\"><br></li></ul></div><p></p>', '<div id=\"lipsum\">\r\n<h1 align=\"center\">\r\nLorem ipsum dolor sit amet </h1><p><br>consectetur adipiscing elit. Phasellus \r\npellentesque, tortor nec accumsan sagittis, urna nisl ultrices neque, \r\nsit amet dapibus lacus mi ut nulla. Ut dignissim congue accumsan. \r\nSuspendisse quis arcu fringilla, molestie leo eget, consectetur mi. Nunc\r\n sit amet eros ac neque sollicitudin dignissim ut vitae velit. Mauris \r\nsem nisi, iaculis eget facilisis ac, sollicitudin in velit. Nam \r\nmalesuada mi sed dignissim ullamcorper. Donec nec lacus eget lacus \r\nfacilisis gravida nec ac enim. Mauris egestas elit in consequat \r\nsollicitudin. Suspendisse pulvinar aliquam odio eu posuere. Nam in \r\nconsectetur lectus. Sed posuere justo ipsum, non tristique odio vehicula\r\n et. In consectetur mollis turpis, a convallis dui placerat in. Aliquam \r\nlobortis massa nec aliquet varius. Cras et viverra enim. Integer aliquam\r\n nisi sit amet volutpat blandit.\r\n</p>\r\n<p>\r\nMauris vitae ullamcorper massa, non tristique tortor. Aenean id sem \r\nelementum, pulvinar urna in, laoreet arcu. Quisque quis diam bibendum, \r\ndictum lacus condimentum, vulputate nisi. Duis gravida risus vel erat \r\nblandit, vitae iaculis velit posuere. Donec nec iaculis est. Donec id \r\negestas elit. Pellentesque non ultrices nisl.\r\n</p>\r\n<p>\r\nInteger auctor nisl at nulla pellentesque condimentum. Etiam blandit \r\nimperdiet ex, eget ultrices augue aliquet et. Sed purus velit, vulputate\r\n nec egestas sit amet, posuere at ex. Class aptent taciti sociosqu ad \r\nlitora torquent per conubia nostra, per inceptos himenaeos. Donec ut \r\nante quis lacus luctus mattis. Cras hendrerit nunc risus, a malesuada ex\r\n dictum vel. Vestibulum ante ipsum primis in faucibus orci luctus et \r\nultrices posuere cubilia curae; Cras ut pharetra tellus. Praesent a arcu\r\n felis. Ut luctus suscipit mattis. Etiam sodales ultricies turpis, sit \r\namet pulvinar tortor auctor sed.\r\n</p>\r\n<p>\r\nMorbi a neque leo. Donec id consectetur lorem. Quisque lacinia risus \r\nfacilisis, mollis sem et, ornare nisl. Phasellus nec neque id libero \r\nauctor pretium. Morbi pretium, urna at sagittis posuere, sapien lacus \r\npellentesque turpis, a sodales erat neque ac ligula. Pellentesque \r\nhabitant morbi tristique senectus et netus et malesuada fames ac turpis \r\negestas. Ut accumsan rutrum lectus vitae ullamcorper. Orci varius \r\nnatoque penatibus et magnis dis parturient montes, nascetur ridiculus \r\nmus. Nulla tempor a quam eu fermentum. Praesent sed est lacus. Proin \r\ndapibus sed quam ut convallis. Suspendisse at libero justo. Curabitur \r\nlibero risus, malesuada vel rhoncus a, aliquam quis nibh.\r\n</p>\r\n<p>\r\n<span style=\"background-color: rgb(255, 255, 0); font-family: &quot;Impact&quot;;\"><font color=\"#000000\">Proin elementum, lorem sit amet interdum vulputate, justo lacus commodo \r\ndiam, quis eleifend dolor nunc id tortor. Vivamus fringilla enim lectus,\r\n ac rutrum ante gravida sed. Suspendisse in tincidunt enim. Cras in \r\nmaximus urna. Curabitur malesuada, nibh a tristique pellentesque, velit \r\nmauris aliquet velit, ac aliquam sapien odio ac nibh. Quisque quis \r\nsollicitudin enim. Aenean volutpat, tellus sollicitudin aliquam \r\nconsectetur, dui magna bibendum lacus, et rutrum sapien leo eget turpis.\r\n</font></span></p><p><br></p></div><br><table class=\"table table-bordered\"><tbody><tr><td>a<br></td><td>a<br></td></tr><tr><td>b<br></td><td>b<br></td></tr></tbody></table><div id=\"lipsum\"><p><br></p><ul><li>q7q</li><li>q</li><li>qq<br><img style=\"width: 171px;\" src=\"data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAKsAAACrCAYAAAAZ6GwZAAAACXBIWXMAACE4AAAhOAFFljFgAAAAAXNSR0IArs4c6QAAAARnQU1BAACxjwv8YQUAAA9FSURBVHgB7Z1Nchu3EscbI1m25EWYE2S8SFXsLCydwNTiVcXKwvYJTJ1A8glEnSDSCUSfwMrGevWyMHUCMYvYrmTh8QlCbyTHFqdfAwMy+hhghtSQBAb9q6IpEzNDSvxPo9EAugUw3oE//BSDEKv04yN60M9AP4tYNQrRhzTt0U89QPxVvP9vF2qCAMZprgkzipokwsYYl0jo+Bfi3dEheA6L1SEqEKYZgXvij6MX4DEs1jmBcbMBy8tNmIYwzfTg9HRdJN0+eMgiMFNnJMyIRJmSOC/6mKODEGbAKn2OV/S8Dh7ClrViSglz3njqErBYb4A7wsSE3rdHImwACulKrBae8vXrmvjrtx54BLsBJbEKU/bgM7vttTARj0GO9M/Ould9UDVQWxAHJNym8TKLizv07zPwCLasOShh3rmzSl/46py7cinCLgnzo3rOEaYNfLCxR09bxmufnt7zabAVvFgvCRPFQ3ql6YQwEXsU0E/ghpBgT8DsFmyKt6874AlBuQFWYY4G4zO5f6ciTAP79DjIbUEs9m0dorZiDVSY11laOoQvX/LFKsQ34BG1EKtTwiQx0vPvMA9h5iB6h31yBfIbEWPwCO/E6qQwBT2n0J23MOuO02JlYTIXcUasLEymiLmIlYXJTMLUxaqEeetWDEuLTRYmcxOmphIagZLVxF8oPNIExk0Qu+LdkTcrsCKYAmpuGuAVC9V5Yv1decFUxApR1AK5oJhxGyHkou8P1Ase+CDa6YjVs2AzAy0S7Rv8/j9OT79OR6yMj8Q0ED4hC/scHIXFylwmijquCnYecdYOMLNFkFtWdgeBJIr2SLDHroX2Zi5W8fb1JjBzQU/GPKWB1Y4aXJlpSB+Wnu+BQ7AbEBByVwBZyw7FVu/JxBf0im2XQExRgjY4BIs1UEiwe5AO1goEu6WssSOwWANG+aTpwLZpUG6SbIEjsFgDRydu27Uc8gQcgcXKyK0ve0Z3gKbMXXEFWKyM2vpCsYKXxgOWl5rgACxWJgOxa2wTi05Mw7JYmYxsra+J78ABWKxMxp07zmdmYbEyisxvdRsWK+MNLFbGG1isjDewWBlvYLEy3sBiZbyBxcp4A4uV8QYWK+MNXK1lDPQepgYMBg1YWLi6bC7h3FnThcVq4Eq5ylUQMsOMiFVjlN8hqQzTF6tSAxyPW2GFMcNivYAS6N3lFqTwZOI8XbL+anaufGzDygrg/cddeu0liZgzF94AFiuoRHJNvT25OZUCbEPxkkUm4R6SoPf1dhJmDIIeYEmRknjeqD3ys8p4KMRTlVfqwcYHeu+nwJQmSMuqMuYJcTDnlJzyM7wi0XbIPdhl96CY4Cwr3t/YopH8iUO5Y1s67eQOMFaCEau0pqrLF7CnBkE3viAeAqSb9PwSqqEtS1f6lNx31gQhVpUyvkq/NIVN8e7omXirUvFQ9CCtKn/Xqg95UudF7cWKP/z8nEQqk4zFUAWIL8T7y8V5Zf6oEoItG2t1Pk/qvKi1WJVQI+yU6vblVuRMcInlqI7KEZWDEmxW1NfGWmm3weE8qfOitmIdCbXwQExIpOuqakk2MxVbjtu1XmtpqW1JdCZvmKfKbUB8pq5XBAv2ErUUa+ajlhLqPk2Hrl0I0NtG5PtF4SWd2cRmXVXeKBLsIdy+vQbFlngo2CYw9RPrqKyRnb4eJG0P5+21IOL8i5IVPDvrQBlseaNoADUUnhS2ePt6u0SeVCnYVxwlqKNlzTI2x8b2rPtdvzpIovPM3a0QpRejFFpXOYN18b/KB8b1AregoQTrUK7UeVArserAemw+gASBuE4WLS9VTtN4XpGvepWvcGhsE+LaTaE+DxYKdhXuLgc9cVAbsepusm05pK+EmuN34vcbMq4ZG67cG3cqVPxF4hPGRGeNvDiqeg8hnlldAhTbIfuv9bGsWfdv5hw3jaJblMWPDSBMNkOF4tjYtrjYzHtZWVh7Jmr5ex5AoNRCrNT9t8Ae9N8Vfx7ZuuZHxrbz8y5MQpp2zY3mm6NEJmo5bbwNAVIXy2r25cgPJIvVBivGadi++Os3WypI8xWV6EzZpKNH1nPl5xW2fKliJ8TBlvdLBAutqhAv8cFPLfMFZDEz4wxXYj23EDVgypvnb6jVXyL9ZD5VuRFNQ2sDVlakdW1DQNRhPetOcbulA7HvCiChTclHlKu/btaxyahCGwLCazfAGsivP3FokQG/fVZbID8EAvv9PR9gidD3MD0NaaDlrVizLhCDGxFfQSbdCGahtr+WNYp4Z6hEhNO7+BsNEPiQwjv5bdl60TILT2LjjJBcJI0llhkWIUQrbz1A1oYvYIDl4rimGToBjyAQ/BUrmgL5NJf/zjJbdfFI+2i6V0UiCpptkt10vljTQSLe/68LZa7zYCOB3MiHiCEQvHQD9MITQyN+hLJEUWxsE6Ka/FS264jF8j43ommtgZxgcKKo2rTx02ddSC1fsuhCFaRpAlVQ1XWyRG8GztdgmiB+AgfwU6xCWEbAgwTKE8N8icc4NjG2jGOhbZjWI6gcCfPHV7GavxyMqum+B4NqrmPfLVuec5FYWmOoglu35fLEixa8n209dyOJXP1yXYnoA1TB4q2/wSUW0r7RtmA18WZdEnNNbbhMydX6/LnnUm5ZP8WKGJN1BUYjxDdQIYZtP3PHVzfAmbudmR2+zmCxWC9TPlznMfXbio3pPaiC86/fQjXEUA0xBI6vYk2MLbZA/1VsU7LXq7HMF1sEpKqIg+P4KdavlgA5YvlVSNbZpXQWYk1KH2mLLVc38eA0for17lJibqRIQRVUFWi3WXo8L+97CzQvWKEQEwSAl2LN4oGT7Ry9hM0iYWVrZWNjyzgTGBiZLGs/lDpbHk8KqKnBvLWcDbz/+FXJ8JZZkEI8p+D4Q7g55u5bLOzQe5TZmmLegYvpMVSImhBAfK595GOKuXbAETxeIojHxoXH1SxIXgWb0KpAWDLBlL9IFypC1wOTGRiHN0aLxLsFp6frLlhvf0NXjiyumDuTZozJQy5Ev+7+rMLycgscwFux6rxVCYSMzDYzYcYYA3Huq0JU4Q7dGN8nBaoq6+Mrv0KlGPx8Xs9aASrLdMAgVvv7Y24e2n7l7zMhXotVhbBsCczqDKa/Vl1CU1ei2R0lNUb1t113pVSn/+tZB7gLkaUYm1w8bCgHpJpXnzbgy5e/Ded2VRWXCcEfH78xbmw8Pf3WNsLWCefMebYQpmLtdMbFNjiI9wtZ1Cr2ovSQluIResFxYji3OWnGE3UT2Hbg2oSafV5bGs9uiCXg67HqSlpXM6p4BNgxD1QmzXjyzz9Nc6Owj+Czzxsb2xGrKr/pFbUQq7YytppSq9Ql/2JpN4tn0gkGIZ4Y2/DceHPgjz/Lz2m7QTqhlnuvz3pWWd3PVu1EFo8wlUlfWjJPMJiyqRTTNLZglHtzqM+HuG0+r0SVwxpTG7Eq37O4e2znCbYgqtAYNw8q3n8srXGc36j8zeTay9nnaoP1wrgbqlWV1GqnQAl3QJIrWEgtfisN0mCsD2KxxjmFhnXX3wY7u7qYcbDUbluLKjFZHHttk/V7cylKcPt2x3xRigqUtK76mjY/tzs6liIN8nNYu351IFnjwiIe9aeeVbFlsoaiqtMkQJmZT8cziycYylrXKLKEnP4N5Cvxr6ycqM9hI6uKOLPRv7qBHvwkV1u1XUsDP5XN92QtOqaukCzETDb8KwsnxBt6xCUO7+iBS2wt/iZLvVvim2otKMCJ7XyQMd0okt1+cZRhWL5zRn6q+ptdrX0rcE/8cfQCHKCelhX0qqzieqhDWvQlfdA5+s3HR9GBaZJAv26P50bRI1hYkGJ2TqiKBSFnzOJLrzlUgrO2YpWMBGvNwHeJFti3PMfUdefHa1dWDqB4u3S71HYZhN7Mhare1+CSOFJoo9ZilcgvnFwPmRKyKEpQlpbcNjMcnGU+3oa0qFXsTpAWdR/OTp1ZPOIStRfrEBUlSNNNgApSD8lZLXIbSKQncPeuTARXhVD7etHNdigbAMclGLFKVJwyHazlxTonZLWSXbByKV6artlWhzF1THlZgO5eW9SNd1U4qly0YDrosFSIK6gmISjLehFpZcmS3VOuQbmIQXVkc/yb8v1ZqOUJVqxDRqKV5YgAprtjNuvu17VIO8CMRXBugAldjuhQj/Ll7NYTyFZO3cQnlQOlrspxcHbW4YHTzWCxXkH7tB39GGYoiXVitIc0oyN3AFzPkCJoMgFV3v+PNFVF3Tx0OfxULSzWAnTKcvmYrovAFBK8z8r4A4uV8QYWK+MNLFbGG1isjDewWBlvYLEy3uBlnFXvm9qCaWemniVyKjZb1JIUHpplqN7R+7dkDHjfpXTq08I7y3ohYVl9hCoZbmAsyK012if170ZD+Xc40LkKao1/boBAJ7ZYTIm4MCW6effsFtQc/8SKIoY6Y68kKG/W2HBeDDXHR8uaQJ0pqhaI4vf819Pfoeb4J9YBVrOPykUQeoXrXPMT0PXpXHtWlxrgXTRAjpZpkLFG3d423WpOVBG5MSjkZsHjMnuwZOYY+v3l9vJt+ht8R+d9lDn/Q1iO6GXoSn8xtbckJkL9/XlSgPEGFivjDSxWxhtYrIw3sFgZb2CxMt7Au1tBr2KKCtNVVsfXQa/iatZBELxYVfGJopz+VXMrkqUyncko7QtBuwFqueGshTp6c1WXqwVMaUL3WZ/AfHkETGnCFiviJ5gnmH4LTGlCF2sH5glySqJxCFqsOjfqfGqhypTsnPZyLIKPBsjKfRS66gDMMHT1+XOP01+OD8dZYbTkLgHGaXgGi/EGFivjDSxWxhtYrIw3sFgZb2CxMt7AYmW8gcXKeAOLlfEGFivjDSxWRqHLgBoa57yUUsNiZYbExhbhRiI8FiuTEUXmJM147sTmRhYrA6NK4MYDIhYr4wiZVY3zG7HnSjpNFmvgqBL1AG3jASnugyOwWANGC/WV+QCVYbsLjsBiDRS8v7FFo/w3YI8CvHQpozZvawkIVWNrebkJsgyRoAEVouVgTMS7ozY4xMzFSl3PATCzBbGhSw+VLXQnaxysg2PMw7K2gJktQox1OJzjpvjTvYIa7LMyl0mBhHrkZPIN9lmZDDnyF+KZeP/a2VSc07GsQiTA+ANSLPXsbE28dVeokjGdmXJklZsXTuinBjCu0tdl4/d1GiXnmYpYJUqwC+IAcFRqnJkvcuVUQg+ynukxnH4+9C2F0f8BGI1hRfmI0Q8AAAAASUVORK5CYII=\" data-filename=\"this.png\"><br></li></ul></div><p><br></p><p></p>');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `encabezados`
--

CREATE TABLE `encabezados` (
  `id` int(11) NOT NULL,
  `pagina` varchar(45) DEFAULT NULL,
  `titulo_ES` varchar(255) DEFAULT NULL,
  `subtitulo_ES` varchar(255) DEFAULT NULL,
  `descripcion_ES` varchar(255) DEFAULT NULL,
  `titulo_EN` varchar(255) DEFAULT NULL,
  `subtitulo_EN` varchar(255) DEFAULT NULL,
  `descripcion_EN` varchar(4255) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `uri` varchar(45) DEFAULT NULL,
  `imagen` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Volcado de datos para la tabla `encabezados`
--

INSERT INTO `encabezados` (`id`, `pagina`, `titulo_ES`, `subtitulo_ES`, `descripcion_ES`, `titulo_EN`, `subtitulo_EN`, `descripcion_EN`, `created_at`, `updated_at`, `uri`, `imagen`) VALUES
(1, 'EDUCACION FINANCIERA', '', '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(2, 'SOBRE TITULARICE', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(3, 'ACCIONISTAS', 'Lorem ipsum dolor sit amet', 'accionistas', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Nulla porttitor massa id neque aliquam vestibulum morbi blandit. Nibh sed pulvinar proin gravida hendrerit.', 'Lorem ipsum dolor sit amet', 'accionistas', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Nulla porttitor massa id neque aliquam vestibulum morbi blandit. Nibh sed pulvinar proin gravida hendrerit.', NULL, '2022-06-06 01:42:18', NULL, 'encabezados/q74BuduyxTrWEfo9Cp97VQXeWnt2ADPTMj7hW8Mw.png'),
(4, 'GOBIERNO CORPORATIVO', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(5, 'JUNTA DIRECTIVA', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(6, 'ORGANIZACION EMPRESARIAL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(7, 'NORMATIVIDAD', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(8, 'INFORMACION FINANCIERA', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(9, 'RIESGOS', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(10, 'RELACION CON INVERSIONISTAS', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(11, 'EMISIONES', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(12, 'CONTACTENOS', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `gastos`
--

CREATE TABLE `gastos` (
  `id` int(11) NOT NULL,
  `nombre` varchar(64) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `deleted_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `gastos`
--

INSERT INTO `gastos` (`id`, `nombre`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 'test', '2021-03-23 12:57:48', '2021-03-23 12:57:53', '2021-03-23 12:57:53'),
(2, 'Parqueo', '2021-03-24 09:58:02', '2021-03-24 09:58:02', NULL),
(3, 'Reparacion llanta', '2021-03-24 09:58:32', '2021-03-24 09:58:32', NULL),
(4, 'Compra de llantas', '2021-03-24 09:58:45', '2021-03-24 13:34:59', NULL),
(5, 'Gasolina', '2021-03-24 09:59:25', '2021-03-24 09:59:25', NULL),
(6, 'Renta', '2021-03-24 13:35:16', '2021-03-24 13:35:16', NULL),
(7, 'Viáticos', '2021-03-24 13:35:32', '2021-03-24 13:35:32', NULL),
(8, 'Pago ayudante', '2021-03-25 07:30:11', '2021-03-25 07:30:11', NULL),
(9, 'Pago Salvador Barillas', '2021-03-25 07:30:31', '2021-03-25 07:30:31', NULL),
(10, 'Otras reparaciones', '2021-05-04 07:50:53', '2021-05-04 07:50:53', NULL),
(11, 'Pago empleados (día domingo)', '2021-05-25 09:30:34', '2021-05-25 09:30:34', NULL),
(12, 'Pago Motorista', '2021-06-18 08:03:18', '2021-06-18 08:03:18', NULL),
(13, 'Gastos Varios', '2021-07-02 13:45:39', '2021-07-02 13:45:39', NULL);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `gobierno_corporativo`
--

CREATE TABLE `gobierno_corporativo` (
  `id` int(11) NOT NULL,
  `titulo_ES` varchar(255) DEFAULT NULL,
  `descripcion_ES` longtext DEFAULT NULL,
  `nombre_documento_ES` varchar(255) DEFAULT NULL,
  `titulo_EN` varchar(255) DEFAULT NULL,
  `descripcion_EN` longtext DEFAULT NULL,
  `nombre_documento_EN` varchar(255) DEFAULT NULL,
  `documento_url_ES` varchar(255) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `deleted_at` datetime DEFAULT NULL,
  `documento_url_EN` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Volcado de datos para la tabla `gobierno_corporativo`
--

INSERT INTO `gobierno_corporativo` (`id`, `titulo_ES`, `descripcion_ES`, `nombre_documento_ES`, `titulo_EN`, `descripcion_EN`, `nombre_documento_EN`, `documento_url_ES`, `created_at`, `updated_at`, `deleted_at`, `documento_url_EN`) VALUES
(1, 'TEST', 'TEST', 'TEST', 'TEST', 'TEST', 'TEST', 'gobierno_corporativob', '2022-06-06 17:02:47', '2022-06-06 18:35:14', '2022-06-06 18:35:14', NULL),
(2, 'TEST2', 'TEST2', 'TEST2', 'TEST2', 'TEST2', 'TEST2', 'gobierno_corporativo/TEST2 06062022.pdf', '2022-06-06 17:12:27', '2022-06-06 18:35:52', '2022-06-06 18:35:52', NULL),
(3, 'TEST3', 'TEST3', 'TEST3', 'TEST3', 'TEST3', 'TEST3', 'gobierno_corporativo/TEST3 06062022.docx', '2022-06-06 17:56:06', '2022-06-06 18:38:29', '2022-06-06 18:38:29', NULL),
(4, 'TEST4', 'TEST4', 'TEST4', 'TEST4', 'TEST4', 'TEST4', 'gobierno_corporativo/TEST4 06062022.pdf', '2022-06-06 18:13:44', '2022-06-06 18:34:33', '2022-06-06 18:34:33', NULL),
(5, 'TEST222', 'TEST222TEST222TEST222', 'TEST222', 'ceeeeeeee', 'eeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee', 'TEST', 'gobierno_corporativo/TEST222 13062022.xlsx', '2022-06-13 17:05:59', '2022-06-14 15:17:19', '2022-06-14 15:17:19', NULL),
(6, 'TEST222', 'TEST222', 'TEST222', 'TEST222', 'TEST222TEST222', 'TEST222', 'gobierno_corporativo/TEST222 13062022.pdf', '2022-06-13 17:11:14', '2022-06-13 17:11:14', NULL, NULL),
(7, 'TEST222111', 'TEST222111', 'TEST222111', 'TEST222111', 'TEST222111', 'TEST222111', 'documentos_gobierno_corporativo/TEST222111 14062022 151825 ES.pdf', '2022-06-13 17:16:03', '2022-06-14 15:18:50', '2022-06-14 15:18:50', 'documentos_gobierno_corporativo/TEST222111 14062022 151825 EN.pdf'),
(8, 'TEST222TEST222TEST222', 'TEST222', 'TEST222TEST222TEST222', 'TEST222TEST222TEST222', 'TEST222TEST222', 'TEST222TEST222', 'gobierno_corporativo/TEST222TEST222TEST222 13062022.pdf', '2022-06-13 17:20:39', '2022-06-14 15:18:02', '2022-06-14 15:18:02', NULL),
(9, 'TEST', 'TEST', 'TEST', 'TEST', 'TEST', 'TEST', 'documentos_gobierno_corporativo/TEST 14062022 151636 ES.xlsx', '2022-06-14 15:02:59', '2022-06-14 15:16:55', '2022-06-14 15:16:55', 'documentos_gobierno_corporativo/TEST 14062022 151636 EN.xls');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `modulos`
--

CREATE TABLE `modulos` (
  `id` int(11) NOT NULL,
  `nombre` varchar(256) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `deleted_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Volcado de datos para la tabla `modulos`
--

INSERT INTO `modulos` (`id`, `nombre`, `created_at`, `updated_at`, `deleted_at`) VALUES
(6, 'comparaciones', NULL, NULL, NULL),
(9, 'usuarios', NULL, NULL, NULL),
(10, 'roles', NULL, NULL, NULL),
(11, 'Accesos', NULL, NULL, NULL),
(12, 'Bitacoras', NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `municipios`
--

CREATE TABLE `municipios` (
  `id` int(11) NOT NULL,
  `nombre` varchar(64) DEFAULT NULL,
  `departamento_id` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `municipios`
--

INSERT INTO `municipios` (`id`, `nombre`, `departamento_id`) VALUES
(1, 'Ahuachapán', 1),
(2, 'Jujutla', 1),
(3, 'Atiquizaya', 1),
(4, 'Concepción de Ataco', 1),
(5, 'El Refugio', 1),
(6, 'Guaymango', 1),
(7, 'Apaneca', 1),
(8, 'San Francisco Menéndez', 1),
(9, 'San Lorenzo', 1),
(10, 'San Pedro Puxtla', 1),
(11, 'Tacuba', 1),
(12, 'Turín', 1),
(13, 'Candelaria de la Frontera', 2),
(14, 'Chalchuapa', 2),
(15, 'Coatepeque', 2),
(16, 'El Congo', 2),
(17, 'El Porvenir', 2),
(18, 'Masahuat', 2),
(19, 'Metapán', 2),
(20, 'San Antonio Pajonal', 2),
(21, 'San Sebastián Salitrillo', 2),
(22, 'Santa Ana', 2),
(23, 'Santa Rosa Guachipilín', 2),
(24, 'Santiago de la Frontera', 2),
(25, 'Texistepeque', 2),
(26, 'Acajutla', 3),
(27, 'Armenia', 3),
(28, 'Caluco', 3),
(29, 'Cuisnahuat', 3),
(30, 'Izalco', 3),
(31, 'Juayúa', 3),
(32, 'Nahuizalco', 3),
(33, 'Nahulingo', 3),
(34, 'Salcoatitán', 3),
(35, 'San Antonio del Monte', 3),
(36, 'San Julián', 3),
(37, 'Santa Catarina Masahuat', 3),
(38, 'Santa Isabel Ishuatán', 3),
(39, 'Santo Domingo de Guzmán', 3),
(40, 'Sonsonate', 3),
(41, 'Sonzacate', 3),
(42, 'Alegría', 11),
(43, 'Berlín', 11),
(44, 'California', 11),
(45, 'Concepción Batres', 11),
(46, 'El Triunfo', 11),
(47, 'Ereguayquín', 11),
(48, 'Estanzuelas', 11),
(49, 'Jiquilisco', 11),
(50, 'Jucuapa', 11),
(51, 'Jucuarán', 11),
(52, 'Mercedes Umaña', 11),
(53, 'Nueva Granada', 11),
(54, 'Ozatlán', 11),
(55, 'Puerto El Triunfo', 11),
(56, 'San Agustín', 11),
(57, 'San Buenaventura', 11),
(58, 'San Dionisio', 11),
(59, 'San Francisco Javier', 11),
(60, 'Santa Elena', 11),
(61, 'Santa María', 11),
(62, 'Santiago de María', 11),
(63, 'Tecapán', 11),
(64, 'Usulután', 11),
(65, 'Carolina', 13),
(66, 'Chapeltique', 13),
(67, 'Chinameca', 13),
(68, 'Chirilagua', 13),
(69, 'Ciudad Barrios', 13),
(70, 'Comacarán', 13),
(71, 'El Tránsito', 13),
(72, 'Lolotique', 13),
(73, 'Moncagua', 13),
(74, 'Nueva Guadalupe', 13),
(75, 'Nuevo Edén de San Juan', 13),
(76, 'Quelepa', 13),
(77, 'San Antonio del Mosco', 13),
(78, 'San Gerardo', 13),
(79, 'San Jorge', 13),
(80, 'San Luis de la Reina', 13),
(81, 'San Miguel', 13),
(82, 'San Rafael Oriente', 13),
(83, 'Sesori', 13),
(84, 'Uluazapa', 13),
(85, 'Arambala', 12),
(86, 'Cacaopera', 12),
(87, 'Chilanga', 12),
(88, 'Corinto', 12),
(89, 'Delicias de Concepción', 12),
(90, 'El Divisadero', 12),
(91, 'El Rosario (Morazán)', 12),
(92, 'Gualococti', 12),
(93, 'Guatajiagua', 12),
(94, 'Joateca', 12),
(95, 'Jocoaitique', 12),
(96, 'Jocoro', 12),
(97, 'Lolotiquillo', 12),
(98, 'Meanguera', 12),
(99, 'Osicala', 12),
(100, 'Perquín', 12),
(101, 'San Carlos', 12),
(102, 'San Fernando (Morazán)', 12),
(103, 'San Francisco Gotera', 12),
(104, 'San Isidro (Morazán)', 12),
(105, 'San Simón', 12),
(106, 'Sensembra', 12),
(107, 'Sociedad', 12),
(108, 'Torola', 12),
(109, 'Yamabal', 12),
(110, 'Yoloaiquín', 12),
(111, 'La Unión', 14),
(112, 'San Alejo', 14),
(113, 'Yucuaiquín', 14),
(114, 'Conchagua', 14),
(115, 'Intipucá', 14),
(116, 'San José', 14),
(117, 'El Carmen (La Unión)', 14),
(118, 'Yayantique', 14),
(119, 'Bolívar', 14),
(120, 'Meanguera del Golfo', 14),
(121, 'Santa Rosa de Lima', 14),
(122, 'Pasaquina', 14),
(123, 'Anamoros', 14),
(124, 'Nueva Esparta', 14),
(125, 'El Sauce', 14),
(126, 'Concepción de Oriente', 14),
(127, 'Polorós', 14),
(128, 'Lislique', 14),
(129, 'Antiguo Cuscatlán', 4),
(130, 'Chiltiupán', 4),
(131, 'Ciudad Arce', 4),
(132, 'Colón', 4),
(133, 'Comasagua', 4),
(134, 'Huizúcar', 4),
(135, 'Jayaque', 4),
(136, 'Jicalapa', 4),
(137, 'La Libertad', 4),
(138, 'Santa Tecla', 4),
(139, 'Nuevo Cuscatlán', 4),
(140, 'San Juan Opico', 4),
(141, 'Quezaltepeque', 4),
(142, 'Sacacoyo', 4),
(143, 'San José Villanueva', 4),
(144, 'San Matías', 4),
(145, 'San Pablo Tacachico', 4),
(146, 'Talnique', 4),
(147, 'Tamanique', 4),
(148, 'Teotepeque', 4),
(149, 'Tepecoyo', 4),
(150, 'Zaragoza', 4),
(151, 'Agua Caliente', 5),
(152, 'Arcatao', 5),
(153, 'Azacualpa', 5),
(154, 'Cancasque', 5),
(155, 'Chalatenango', 5),
(156, 'Citalá', 5),
(157, 'Comapala', 5),
(158, 'Concepción Quezaltepeque', 5),
(159, 'Dulce Nombre de María', 5),
(160, 'El Carrizal', 5),
(161, 'El Paraíso', 5),
(162, 'La Laguna', 5),
(163, 'La Palma', 5),
(164, 'La Reina', 5),
(165, 'Las Vueltas', 5),
(166, 'Nueva Concepción', 5),
(167, 'Nueva Trinidad', 5),
(168, 'Nombre de Jesús', 5),
(169, 'Ojos de Agua', 5),
(170, 'Potonico', 5),
(171, 'San Antonio de la Cruz', 5),
(172, 'San Antonio Los Ranchos', 5),
(173, 'San Fernando (Chalatenango)', 5),
(174, 'San Francisco Lempa', 5),
(175, 'San Francisco Morazán', 5),
(176, 'San Ignacio', 5),
(177, 'San Isidro Labrador', 5),
(178, 'Las Flores', 5),
(179, 'San Luis del Carmen', 5),
(180, 'San Miguel de Mercedes', 5),
(181, 'San Rafael', 5),
(182, 'Santa Rita', 5),
(183, 'Tejutla', 5),
(184, 'Cojutepeque', 7),
(185, 'Candelaria', 7),
(186, 'El Carmen (Cuscatlán)', 7),
(187, 'El Rosario (Cuscatlán)', 7),
(188, 'Monte San Juan', 7),
(189, 'Oratorio de Concepción', 7),
(190, 'San Bartolomé Perulapía', 7),
(191, 'San Cristóbal', 7),
(192, 'San José Guayabal', 7),
(193, 'San Pedro Perulapán', 7),
(194, 'San Rafael Cedros', 7),
(195, 'San Ramón', 7),
(196, 'Santa Cruz Analquito', 7),
(197, 'Santa Cruz Michapa', 7),
(198, 'Suchitoto', 7),
(199, 'Tenancingo', 7),
(200, 'Aguilares', 6),
(201, 'Apopa', 6),
(202, 'Ayutuxtepeque', 6),
(203, 'Cuscatancingo', 6),
(204, 'Ciudad Delgado', 6),
(205, 'El Paisnal', 6),
(206, 'Guazapa', 6),
(207, 'Ilopango', 6),
(208, 'Mejicanos', 6),
(209, 'Nejapa', 6),
(210, 'Panchimalco', 6),
(211, 'Rosario de Mora', 6),
(212, 'San Marcos', 6),
(213, 'San Martín', 6),
(214, 'San Salvador', 6),
(215, 'Santiago Texacuangos', 6),
(216, 'Santo Tomás', 6),
(217, 'Soyapango', 6),
(218, 'Tonacatepeque', 6),
(219, 'Zacatecoluca', 8),
(220, 'Cuyultitán', 8),
(221, 'El Rosario (La Paz)', 8),
(222, 'Jerusalén', 8),
(223, 'Mercedes La Ceiba', 8),
(224, 'Olocuilta', 8),
(225, 'Paraíso de Osorio', 8),
(226, 'San Antonio Masahuat', 8),
(227, 'San Emigdio', 8),
(228, 'San Francisco Chinameca', 8),
(229, 'San Pedro Masahuat', 8),
(230, 'San Juan Nonualco', 8),
(231, 'San Juan Talpa', 8),
(232, 'San Juan Tepezontes', 8),
(233, 'San Luis La Herradura', 8),
(234, 'San Luis Talpa', 8),
(235, 'San Miguel Tepezontes', 8),
(236, 'San Pedro Nonualco', 8),
(237, 'San Rafael Obrajuelo', 8),
(238, 'Santa María Ostuma', 8),
(239, 'Santiago Nonualco', 8),
(240, 'Tapalhuaca', 8),
(241, 'Cinquera', 9),
(242, 'Dolores', 9),
(243, 'Guacotecti', 9),
(244, 'Ilobasco', 9),
(245, 'Jutiapa', 9),
(246, 'San Isidro (Cabañas)', 9),
(247, 'Sensuntepeque', 9),
(248, 'Tejutepeque', 9),
(249, 'Victoria', 9),
(250, 'Apastepeque', 10),
(251, 'Guadalupe', 10),
(252, 'San Cayetano Istepeque', 10),
(253, 'San Esteban Catarina', 10),
(254, 'San Ildefonso', 10),
(255, 'San Lorenzo', 10),
(256, 'San Sebastián', 10),
(257, 'San Vicente', 10),
(258, 'Santa Clara', 10),
(259, 'Santo Domingo', 10),
(260, 'Tecoluca', 10),
(261, 'Tepetitán', 10),
(262, 'Verapaz', 10);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `permisos`
--

CREATE TABLE `permisos` (
  `id` int(11) NOT NULL,
  `rol_id` int(11) NOT NULL,
  `modulo_id` int(11) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `deleted_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `permisos`
--

INSERT INTO `permisos` (`id`, `rol_id`, `modulo_id`, `created_at`, `updated_at`, `deleted_at`) VALUES
(12, 2, 10, '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL),
(13, 2, 1, '2021-03-22 15:26:53', '2021-03-22 15:26:53', NULL),
(14, 2, 2, '2021-03-22 15:26:58', '2021-03-22 15:26:58', NULL),
(15, 2, 3, '2021-03-22 15:27:07', '2021-03-22 15:27:07', NULL),
(16, 2, 9, '2021-03-22 15:27:22', '2021-03-22 15:27:22', NULL),
(17, 2, 6, '2021-03-22 15:27:30', '2021-03-22 15:27:30', NULL),
(18, 2, 8, '2021-03-22 15:27:40', '2021-03-22 15:27:40', NULL),
(19, 2, 7, '2021-03-22 15:27:45', '2021-03-22 15:27:45', NULL),
(20, 6, 2, '2021-03-23 16:50:14', '2021-03-23 16:50:14', NULL),
(21, 6, 3, '2021-03-23 16:58:44', '2021-03-23 16:58:44', NULL);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `preguntas_frecuentes`
--

CREATE TABLE `preguntas_frecuentes` (
  `id` int(11) NOT NULL,
  `pregunta_ES` varchar(512) NOT NULL,
  `respuesta_ES` varchar(2048) DEFAULT NULL,
  `respuesta_EN` varchar(512) DEFAULT NULL,
  `pregunta_EN` varchar(2048) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `deleted_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Volcado de datos para la tabla `preguntas_frecuentes`
--

INSERT INTO `preguntas_frecuentes` (`id`, `pregunta_ES`, `respuesta_ES`, `respuesta_EN`, `pregunta_EN`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 'qhttp://127.0.0.1:8000/preguntas_frecuentes/create', NULL, 'qhttp://127.0.0.1:8000/preguntas_frecuentes/create', NULL, '2022-05-26 15:40:42', '2022-05-26 16:16:07', '2022-05-26 16:16:07'),
(2, 'qhttp://127.0.0.1:8000/preguntas_frecuentes/create', NULL, 'qhttp://127.0.0.1:8000/preguntas_frecuentes/create', NULL, '2022-05-26 15:47:56', '2022-05-26 19:23:14', '2022-05-26 19:23:14'),
(3, 'qhttp://127.0.0.1:8000/preguntas_frecuentes/create', 'Vivamus vulputate ex ante. Morbi non enim enim. Morbi arcu tellus, dictum a leo non, condimentum porta libero. In quis porttitor felis. Nunc maximus metus turpis, in accumsan justo malesuada nec.', 'qhttp://127.0.0.1:8000/preguntas_frecuentes/create', 'Vivamus vulputate ex ante. Morbi non enim enim. Morbi arcu tellus, dictum a leo non, condimentum porta libero. In quis porttitor felis. Nunc maximus', '2022-05-26 15:48:11', '2022-05-26 16:16:38', NULL),
(4, 'qhttp://127.0.0.1:8000/preguntas_frecuentes/create', NULL, 'qhttp://127.0.0.1:8000/preguntas_frecuentes/create', NULL, '2022-05-26 15:48:37', '2022-05-26 16:13:54', '2022-05-26 16:13:54'),
(5, 'http://127.0.0.1:8000/preguntas_frecuentes/5/edit', 'http://127.0.0.1:8000/preguntas_frecuentes/5/edit', 'http://127.0.0.1:8000/preguntas_frecuentes/5/edithttp://127.0.0.1:8000/preguntas_frecuentes/5/edit', 'http://127.0.0.1:8000/preguntas_frecuentes/5/edit', '2022-05-26 16:00:43', '2022-05-26 16:13:42', '2022-05-26 16:13:42'),
(6, 'Vivamus vulputate ex ante. Morbi non enim enim. Morbi arcu tellus, dictum a leo non, condimentum porta libero. In quis porttitor felis. Nunc maximus metus turpis, in accumsan justo malesuada nec.', 'Vivamus vulputate ex ante. Morbi non enim enim. Morbi arcu tellus, dictum a leo non, condimentum porta libero. In quis porttitor felis. Nunc maximus metus turpis, in accumsan justo malesuada nec.', 'Vivamus vulputate ex ante. Morbi non enim enim. Morbi arcu tellus, dictum a leo non, condimentum porta libero. In quis porttitor felis. Nunc maximus metus turpis, in accumsan justo malesuada nec.', 'Vivamus vulputate ex ante. Morbi non enim enim. Morbi arcu tellus, dictum a leo non, condimentum porta libero. In quis porttitor felis. Nunc maximus metus turpis, in accumsan justo malesuada nec.', '2022-05-26 16:34:14', '2022-05-26 19:47:52', '2022-05-26 19:47:52'),
(7, 'Vivamus vulputate ex ante. Morbi non enim enim. Morbi arcu tellu', 'Vivamus vulputate ex ante. Morbi non enim enim. Morbi arcu tellus, dictum a leo non, condimentum porta libero. In quis porttitor felis. Nunc maximus metus turpis, in accumsan justo malesuada nec.', 'Vivamus vulputate ex ante. Morbi non enim enim. Morbi arcu tellus, dictum a leo non, condimentum porta libero. In quis porttitor felis. Nunc maximus metus turpis, in accumsan justo malesuada nec.', 'Vivamus vulputate ex ante. Morbi non enim enim. Morbi arcu tellu', '2022-05-26 19:21:02', '2022-05-26 19:21:02', NULL),
(8, '1111111111111111111111111111111111111111111111111111111111111111111111111111111111111', '1111111111111111111111111111111111111111111111111111111111111111111111111111111111111', '1111111111111111111111111111111111111111111111111111111111111111111111111111111111111', '1111111111111111111111111111111111111111111111111111111111111111111111111111111111111', '2022-05-26 19:39:38', '2022-05-26 19:46:50', '2022-05-26 19:46:50');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `roles`
--

CREATE TABLE `roles` (
  `id` int(11) NOT NULL,
  `nombre` varchar(64) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `deleted_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `roles`
--

INSERT INTO `roles` (`id`, `nombre`, `created_at`, `updated_at`, `deleted_at`) VALUES
(2, 'Administrador', '2021-03-04 23:08:27', '2021-03-06 09:28:19', NULL),
(6, 'Asistente', '2021-03-23 16:32:54', '2021-03-23 16:40:58', NULL);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `usuarios`
--

CREATE TABLE `usuarios` (
  `id` int(11) NOT NULL,
  `nickname` varchar(45) NOT NULL,
  `email` varchar(64) NOT NULL,
  `password` varchar(64) NOT NULL,
  `nombres` varchar(128) NOT NULL,
  `apellidos` varchar(45) NOT NULL,
  `rol_id` varchar(45) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `deleted_at` datetime DEFAULT NULL,
  `clave_autorizacion` varchar(45) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `usuarios`
--

INSERT INTO `usuarios` (`id`, `nickname`, `email`, `password`, `nombres`, `apellidos`, `rol_id`, `created_at`, `updated_at`, `deleted_at`, `clave_autorizacion`) VALUES
(1, 'carlos', 'carlos@test.com', '$2y$10$s3sofRNOfwnKVCP1ppe44OtfroY7bhkfzGji0a89bhaLBcihcUF5W', 'Carlos', 'Valle', '2', '0000-00-00 00:00:00', '2021-03-23 13:03:26', NULL, '0794'),
(10, 'rocio', 'rocio.interiano@iprimaveral.com', '$2y$10$s3sofRNOfwnKVCP1ppe44OtfroY7bhkfzGji0a89bhaLBcihcUF5W', 'Rocio Carolina', 'Interiano Aguila', '2', '2021-03-06 08:02:14', '2022-03-14 21:49:38', '2022-03-14 21:49:38', NULL),
(11, 'marta.andrade', 'marta.andrade@iprimaveral.com', '$2y$10$q.Pb04mcDuoeUGl84jc9V.6bG8leDjVmWuH51j.44i1ZLaGO7RGx6', 'Marta Lisseth', 'Andrade García', '2', '2021-03-23 16:07:51', '2022-03-14 21:50:04', '2022-03-14 21:50:04', NULL),
(12, 'alejandra_erazo', 'alejandra_erazo@iprimaveral.com', '$2y$10$AE/1G7sP2xsHMeNueRCcGOhyLFJwsfn/q6WdlXfelUXv49NszKNXy', 'Alejandra Yamileth', 'Erazo', '6', '2021-03-23 16:44:52', '2022-03-14 21:51:02', '2022-03-14 21:51:02', NULL),
(13, 'marielos.paniagua', 'marielos.paniagua@iprimaveral.com', '$2y$10$Rp2JMqmwmV3DIKgx6TS0weFmdz2UwO/cHbIkmhvscZDbAVaISXptu', 'Marielos Alely', 'Paniagua Gallardo', '6', '2021-03-23 17:03:59', '2022-03-14 21:50:15', '2022-03-14 21:50:15', NULL),
(14, 'javier.interiano', 'javier.interiano@iprimaveral.com', '$2y$10$DOG1AhQddQugJFV25flYX.EjejihelKGBnBNDCeU7QHJxdw/d2.qS', 'Fernando Javier', 'Interiano Aguila', '2', '2021-03-23 17:13:19', '2022-03-14 21:50:27', '2022-03-14 21:50:27', '8822'),
(15, 'francisco.interiano', 'francisco.interiano@iprimaveral.com', '$2y$10$hSlc4LxAiBc5B1DQTycHZeuKweat9qANhBJvwmkxAuU9aS4axCAUm', 'Francisco Eduardo', 'Interiano Aguila', '6', '2021-03-23 17:45:08', '2022-03-14 21:50:37', '2022-03-14 21:50:37', NULL),
(16, 'monica_melara', 'monica_melara@iprimaveral.com', '$2y$10$v8MlHkzUB6aCl4kkAgIhVe1SEYXf0/0VKaAW8g08l0Fh7GZxEw5KS', 'Monica Mariela', 'Melara', '6', '2021-03-23 17:49:51', '2022-03-14 21:49:49', '2022-03-14 21:49:49', NULL),
(17, 'enrique.interiano', 'enrique.interiano@iprimaveral.com', '$2y$10$q8Gd0vknLnB4TuIrDIKrSeUZvp6Gmpc6QZ1ctEOywJCQ0lTgBREWu', 'Francisco Enrique', 'Interiano', '6', '2021-03-29 12:10:20', '2022-03-14 21:50:51', '2022-03-14 21:50:51', NULL),
(18, 'rventura', 'rventura@test.com', '$2y$10$RzDv7b8WX0/9C9nqWfuO7eTjyMVld7pG/m80yD9tc3oUpS.d1KAgi', 'Ronald', 'Ventura', '6', '2022-03-14 21:53:20', '2022-03-14 22:16:02', NULL, NULL),
(19, 'rventurad', 'rventura@test.comw', '$2y$10$AelY2vahhLAcSGxL1kInG.V7VP8Pm7b8wPNuOO0x.OeoFrTX5vcBq', 'test', 'test', '2', '2022-03-14 21:54:46', '2022-03-14 22:04:44', '2022-03-14 22:04:44', NULL),
(20, 'admin@admin.comx', 'admin@admin.comx', '$2y$10$b3jhqL768pZUCmXkqPIZsezoh3/MP.CD4BEztQe5Pg7M5iDj8FPGe', 'xsxs', 'xsxs', '2', '2022-03-14 22:01:52', '2022-03-14 22:05:02', '2022-03-14 22:05:02', NULL);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `zonas`
--

CREATE TABLE `zonas` (
  `id` int(11) NOT NULL,
  `nombre` varchar(64) NOT NULL,
  `municipio_id` int(11) NOT NULL,
  `created_at` datetime NOT NULL,
  `deleted_at` datetime DEFAULT NULL,
  `updated_at` datetime NOT NULL,
  `departamento_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `zonas`
--

INSERT INTO `zonas` (`id`, `nombre`, `municipio_id`, `created_at`, `deleted_at`, `updated_at`, `departamento_id`) VALUES
(1, 'test2', 13, '2021-03-24 11:37:37', '2021-03-24 11:38:11', '2021-03-24 11:38:11', 2);

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `accesos`
--
ALTER TABLE `accesos`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `accionistas`
--
ALTER TABLE `accionistas`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `bitacora`
--
ALTER TABLE `bitacora`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `categorias`
--
ALTER TABLE `categorias`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `configuraciones`
--
ALTER TABLE `configuraciones`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `departamentos`
--
ALTER TABLE `departamentos`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `emisiones`
--
ALTER TABLE `emisiones`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `encabezados`
--
ALTER TABLE `encabezados`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `gastos`
--
ALTER TABLE `gastos`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `gobierno_corporativo`
--
ALTER TABLE `gobierno_corporativo`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `modulos`
--
ALTER TABLE `modulos`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `municipios`
--
ALTER TABLE `municipios`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `permisos`
--
ALTER TABLE `permisos`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `preguntas_frecuentes`
--
ALTER TABLE `preguntas_frecuentes`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `roles`
--
ALTER TABLE `roles`
  ADD PRIMARY KEY (`id`,`nombre`);

--
-- Indices de la tabla `usuarios`
--
ALTER TABLE `usuarios`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `zonas`
--
ALTER TABLE `zonas`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `accesos`
--
ALTER TABLE `accesos`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `accionistas`
--
ALTER TABLE `accionistas`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT de la tabla `bitacora`
--
ALTER TABLE `bitacora`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `categorias`
--
ALTER TABLE `categorias`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=23;

--
-- AUTO_INCREMENT de la tabla `configuraciones`
--
ALTER TABLE `configuraciones`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT de la tabla `emisiones`
--
ALTER TABLE `emisiones`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=176;

--
-- AUTO_INCREMENT de la tabla `encabezados`
--
ALTER TABLE `encabezados`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;

--
-- AUTO_INCREMENT de la tabla `gastos`
--
ALTER TABLE `gastos`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;

--
-- AUTO_INCREMENT de la tabla `gobierno_corporativo`
--
ALTER TABLE `gobierno_corporativo`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT de la tabla `modulos`
--
ALTER TABLE `modulos`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;

--
-- AUTO_INCREMENT de la tabla `permisos`
--
ALTER TABLE `permisos`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=22;

--
-- AUTO_INCREMENT de la tabla `preguntas_frecuentes`
--
ALTER TABLE `preguntas_frecuentes`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT de la tabla `roles`
--
ALTER TABLE `roles`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT de la tabla `usuarios`
--
ALTER TABLE `usuarios`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=21;

--
-- AUTO_INCREMENT de la tabla `zonas`
--
ALTER TABLE `zonas`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
