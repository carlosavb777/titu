<?php

namespace App\Helpers;

use Auth;
use URL;
use Route;
use App\Http\Controllers\Controller;
use App\Models\Rol;
use App\Models\Modulos;
use App\Models\Permisos;
use App\Models\Encabezados;
use App\Models\Configuraciones;
use Illuminate\Http\Request;
use DB;

class ToolKit
{

    public static function verificar_permiso($modulo_nombre){


			if(Auth::user()->nickname == "admin"){return true;}



			$modulo = Modulos::where("code", $modulo_nombre)->first();


			if(is_null($modulo)){

				return false;
			}

			if(Permisos::where("modulo_id", $modulo->id)->where("rol_id", Auth::user()->rol_id)->first()){

				return true;
			}
			return false;

    }

		public static function ultimo_dia_mes($fecha = null){


				$fecha = new \DateTime( $fecha);
				return $fecha->format( 'Y-m-t' );
		}


		public static function obtener_dia($fecha = null){
      if(is_null($fecha)) $fecha = date("Y-m-d");

      return jddayofweek(cal_to_jd(CAL_GREGORIAN, date_format(date_create($fecha), "m"), date_format(date_create($fecha), "d"), date_format(date_create($fecha), "Y")) , 0 );
    }







		public static  function Mes($mes=null)
		{
			if($mes == null){
					$mes = date("m");
			}

			$meses = array("Enero", "Febrero", "Marzo", "Abril", "Mayo", "Junio", "Julio", "Agosto", "Septiembre", "Octubre", "Noviembre", "Diciembre");

			return $meses[$mes - 1];

		}



        public static   function Encabezado($seccion, $uri = null)
        {

            if (is_null($uri)) {

                $uri = Route::getCurrentRoute()->uri;
            }


            $contenido = 'Contenido no encontrado';
            $encabezado = Encabezados::where('uri', $uri)->first();
            if (!is_null($encabezado)) {
                if ($seccion == 'IMG_HEADER') {

                    $contenido = asset($encabezado->imagen);

                }elseif ($seccion == 'TITULO') {

                    (session()->get('selected_lang') != 'EN') ? $contenido = $encabezado->titulo_ES : $contenido = $encabezado->titulo_EN;

                }elseif ($seccion == 'SUBTITULO') {

                    (session()->get('selected_lang') != 'EN') ? $contenido = $encabezado->subtitulo_ES : $contenido = $encabezado->subtitulo_EN;

                }elseif ($seccion == 'DESCRIPCION') {
                    (session()->get('selected_lang') != 'EN') ? $contenido = $encabezado->descripcion_ES : $contenido = $encabezado->descripcion_EN;
                }
            }

            return $contenido;

        }


        public static   function Configuracion($key)
        {
            if (is_null( $config = Configuraciones::where('key', $key)->first())) {

                $val = '';

            }else{

                if (session()->get('selected_lang') != 'EN') {
                    $val = $config->value_ES;
                }else {
                    $val = $config->value_EN;
                }

            }

            return $val;

        }

    /**
     * Transforma una fecha dada como parametro a formato "12 de Enero de 2022" ej.
     * Si no se pasa fecha se usa la fecha actual.
     * @param string $fecha
     * @return string
     */
    public static function fechaCortaDMY(string $fecha = null)
    {
        $fecha  =  $fecha ?? date('Y-m-d');
        $time   = strtotime( $fecha );
        $dia    = date('d', $time);
        $mes    = strtolower( self::Mes( date('m', $time) ));
        $anio   = date('Y', $time);
        return "$dia de $mes $anio";
    }








}

