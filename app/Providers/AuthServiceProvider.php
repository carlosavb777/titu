<?php

namespace App\Providers;

use Illuminate\Foundation\Support\Providers\AuthServiceProvider as ServiceProvider;
use Illuminate\Support\Facades\Gate;
use App\Models\User;
use Auth;
use Session;
use App\Models\Modulos;
use App\Helpers\ToolKit;

class AuthServiceProvider extends ServiceProvider
{
    /**
     * The policy mappings for the application.
     *
     * @var array
     */
    protected $policies = [
        // 'App\Models\Model' => 'App\Policies\ModelPolicy',
    ];

    /**
     * Register any authentication / authorization services.
     *
     * @return void
     */
    public function boot()
    {
        $this->registerPolicies();
			// $modulos = Modulos::all();
			// foreach ($modulos as $modulo) {
			// 	session(['modulo' => $modulo->nombre]);

			// 	Gate::define($modulo->nombre, function () {

			// 		return ToolKit::verificar_permiso(session()->pull('modulo'));
			//  });

			// }

			Gate::define("usuarios", function () {

			 		return ToolKit::verificar_permiso("usuarios");
			});

            Gate::define("roles", function () {

                return ToolKit::verificar_permiso("roles");
            });

            Gate::define("accesos", function () {

                return ToolKit::verificar_permiso("accesos");
            });


            Gate::define("bitacoras", function () {

                return ToolKit::verificar_permiso("bitacoras");
            });

            Gate::define("contenidos_generales", function () {

                return ToolKit::verificar_permiso("contenidos_generales");
            });

            Gate::define("junta_directiva", function () {

                return ToolKit::verificar_permiso("junta_directiva");
            });

            Gate::define("equipo", function () {

                return ToolKit::verificar_permiso("equipo");
            });

            Gate::define("preguntas_frecuentes", function () {

                return ToolKit::verificar_permiso("preguntas_frecuentes");
            });

            Gate::define("encabezados", function () {

                return ToolKit::verificar_permiso("encabezados");
            });

            Gate::define("accionistas", function () {

                return ToolKit::verificar_permiso("accionistas");
            });

            Gate::define("reportes", function () {

                return ToolKit::verificar_permiso("reportes");
            });

            Gate::define("educacion_ financiera", function () {

                return ToolKit::verificar_permiso("educacion_ financiera");
            });

            Gate::define("emisiones", function () {

                return ToolKit::verificar_permiso("emisiones");
            });

            Gate::define("informacion_fiinanciera", function () {

                return ToolKit::verificar_permiso("informacion_fiinanciera");
            });

            Gate::define("normatividad", function () {

                return ToolKit::verificar_permiso("normatividad");
            });

            Gate::define("operaciones", function () {

                return ToolKit::verificar_permiso("operaciones");
            });


    }
}
