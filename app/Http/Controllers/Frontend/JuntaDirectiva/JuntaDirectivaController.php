<?php

namespace App\Http\Controllers\Frontend\JuntaDirectiva;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Junta_directiva;
use Illuminate\Support\Facades\Storage;
use Illuminate\Http\File;

class JuntaDirectivaController extends Controller
{

    public function MostrarJuntaDirectiva()
    {
        $data["directiva"] = Junta_directiva::all();
        //return json_encode($data);
        return view("Frontend/About/JuntaDirectiva", $data);
    }
}
