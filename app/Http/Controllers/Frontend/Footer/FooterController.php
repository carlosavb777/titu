<?php

namespace App\Http\Controllers\Frontend\Footer;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Organizacion;

class FooterController extends Controller
{


    public function Informacion()
    {

        return view('Frontend/Footer/Informacion');

    }

    public function PreguntasFrecuentes()
    {

        return view('Frontend/Footer/PreguntasFrecuentes');

    }

    public function PoliticasDatosPersonales()
    {

        return view('Frontend/Footer/PoliticasDatosPersonales');

    }

    public function TerminosCondiciones()
    {

        return view('Frontend/Footer/TerminosCondiciones');

    }

    public function EnlaceInteres()
    {

        $data["enlaces"] = Organizacion::all();

        return view('Frontend/Footer/EnlaceInteres', $data);

    }


    public function InformacionHechosRelevantes()
    {

        return view('Frontend/Footer/InformacionHechos');

    }


}
