<?php

namespace App\Http\Controllers\Frontend\Contactenos;

use App\Http\Controllers\Controller;
use App\Mail\ContactMail;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;

class ContactenosController extends Controller
{

    public function Contactenos()
    {

        return view('Frontend/Contactenos/Contactenos');

    }


    public function EnviarCorreo(Request $request)
    {
       $details=['Nombre'=>$request->nombre,
                'Correo'=>$request->correo,
                'Mensaje'=>$request->mensaje,
                'Concepto'=>$request->concepto

            ];
               Mail::to("titularicecontacto@gmail.com")->send(new ContactMail($details));
           return 'correo  a '.$request->correo;
    }
}
