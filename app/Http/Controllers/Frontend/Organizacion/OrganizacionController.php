<?php

namespace App\Http\Controllers\Frontend\Organizacion;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Organizacion;
use Illuminate\Support\Facades\Storage;
use Illuminate\Http\File;

class OrganizacionController extends Controller
{

    public function MostrarOrganizacion()
    {
        $data["organizacion"] = Organizacion::all();
        //return json_encode($data);
        return view("Frontend/About/Organizacion", $data);
    }

}
