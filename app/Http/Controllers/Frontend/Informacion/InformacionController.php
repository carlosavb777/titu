<?php

namespace App\Http\Controllers\Frontend\Informacion;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class InformacionController extends Controller
{
    public function PreguntasFrecuentes(Request $request)
    {
        return view('Frontend/About/PreguntasFrecuentes');
    }
}
