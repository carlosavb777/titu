<?php

namespace App\Http\Controllers\Frontend\EducacionFinanciera;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Blog;

class EducacionFinancieraController extends Controller
{


    public function EducacionFinanciera()
    {


        $data["blogs"] = Blog::all();

        return view('Frontend/EducacionFinanciera/EducacionFinanciera', $data);

    }


    public function Articulo($id)
    {

        $data["blog"] = Blog::find($id);

        $data['titulo'] = "Educación financiera";
        $data['subtitulo']= array('¿Qué es la titularización?','Ventajas de la titularización');
        return view('Frontend/EducacionFinanciera/Articulo')->with($data);

    }



}
