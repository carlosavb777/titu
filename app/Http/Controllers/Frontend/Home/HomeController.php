<?php

namespace App\Http\Controllers\Frontend\Home;

use Session;
use App;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Organizacion;

class HomeController extends Controller
{
    /**
     * Handle the incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function __invoke(Request $request)
    {



        if ( is_null(session()->get('selected_lang'))) {
            session()->put('selected_lang', strtoupper(App::getLocale()));
        }

        $data["organizacion"] = Organizacion::all();



        return view('Frontend/Home/Home', $data);
    }
}
