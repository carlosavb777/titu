<?php

namespace App\Http\Controllers\Frontend\About;

use App\Http\Controllers\Controller;
use App\Models\Accionistas;
use App\Models\Categorias;
use App\Models\Categorias_normatividad;
use App\Models\Emisiones;
use App\Models\Gobierno_corporativo;
use App\Models\InformacionFinancieraCategorias;
use App\Models\Riesgos;
use App\Models\Inversionistas;
use App\Models\Reportes;
use App\Models\Reportes_pais;
use Illuminate\Http\Request;
use Illuminate\Support\Arr;

class AboutController extends Controller
{


    public function NuestraTitularizadora()
    {

        return view('Frontend/About/NuestraTitularizadora');

    }


    public function Accionistas()
    {
        $accionistas = Accionistas::all();
        return view('Frontend/About/Accionistas', compact('accionistas'));

    }


    public function GobiernoCorporativo()
    {
        $gobierno_corporativo   = Gobierno_corporativo::all();
        $reportes               = Reportes::orderBy('anio', 'ASC')->get();
        $reportes_pais          = Reportes_pais::orderBy('anio', 'ASC')->get();
        return view("Frontend/About/GobiernoCorporativo", compact('gobierno_corporativo', 'reportes', 'reportes_pais', ));

    }


    public function Normatividad()
    {
        $categoriasNormatividad = Categorias_normatividad::all();
        return view('Frontend/About/Normatividad', compact('categoriasNormatividad'));

    }

    public function InformacionFinanciera($anio=null)
    {
        $anioActual = (int) date('Y');
        $anios      = range( $anioActual-5, $anioActual );
        $anioSelec  = $anio ?? $anioActual;
        rsort( $anios );
        $informes   = InformacionFinancieraCategorias::all();
        return view('Frontend/About/InformacionFinanciera', compact('informes','anios', 'anioSelec'));

    }

    public function Riesgos()
    {
        $data['Riesgos'] = Riesgos::all();
        return view('Frontend/About/Riesgos',$data);

    }

    public function RelacionInversionistas()
    {
        $data['Inversionistas'] = Inversionistas::all();
        return view('Frontend/About/RelacionInversionistas',$data);

    }



}
