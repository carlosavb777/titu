<?php

namespace App\Http\Controllers\Frontend\EmisionesFrontend;

use App\Http\Controllers\Controller;
use App\Models\Emisiones;
use App\Models\Documentos_emisiones;
use App\Models\Categorias;
use Illuminate\Http\Request;

class EmisionesFrontendController extends Controller
{


    public function Emisiones(Request $request, $info='')
    {



        $carteras = array('FLUJOS-FUTUROS',  'EMISIONES-CANCELADAS', 'CARTERA-LIBRANZAS','CARTERA-CONSUMO-COMERCIAL', 'CARTERA-VEHICULOS' );

        for ($i=0; $i < count($carteras) ; $i++) {

            $categorias = Categorias::where('cartera',  $carteras[$i])->get();
            $cartera_key = str_replace('-', '_', $carteras[$i]);
            $data[$cartera_key] = '';
            foreach ($categorias as $key => $categoria) {

                $data[$cartera_key] .= '
                <div class=" col-md-6 col-lg-6 mt-3 col-xs-12">
                    <div class="card simple-grey-card mt-2"  style="height: 100%;" >
                        <div class="card-body">
                            <a style="text-decoration: none !important;  color:black;" class="descargar-emision" color:black" href="'.(url('detalle-emision')).'/'.$categoria->id.'"  rel="noopener noreferrer">'.((session()->get('selected_lang') != 'EN') ? $categoria->categoria_ES : $categoria->categoria_EN).'</a>
                            ';

            //    foreach ($emisiones as $key2 => $emision) {



            //         $data[$cartera_key] .=  '<li> <a style="text-decoration: none !important;  color:black;" class="descargar-emision" color:black" href="'.(url('detalle-emision')).'/'.$emision->id.'"  rel="noopener noreferrer">'.((session()->get('selected_lang') == 'ES') ? $emision->titulo_ES : $emision->titulo_EN).'</a></li>';

            //    }

                $data[$cartera_key] .=  '
                            </ul>
                        </div>
                    </div>
                </div>';

            }





        }

        // if($info != 'emisiones-canceladas' && $info != ''){
        //     return redirect('/emisiones');
        // } else if( $info == 'emisiones-canceladas' ){
        //     return view('Frontend/About/EmisionesText');
        // }
         return view('Frontend/About/Emisiones', $data);
    }

    public function Emision($id){

        $data["emision"] = Categorias::find($id);


        return view('Frontend/About/EmisionesText', $data);

    }



}
