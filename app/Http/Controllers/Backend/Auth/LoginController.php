<?php

namespace App\Http\Controllers\Backend\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
use App\Models\Accesos;


class LoginController extends Controller
{


	public function LoginVista(Request $request)
	{


		return view("Backend/Login/Login");

	}


    public function Login(Request $request)
    {

			$credentials = $request->only('nickname', 'password');

			if (Auth::attempt(["nickname"=>$credentials["nickname"], "password"=>$credentials["password"]])) {
					$request->session()->regenerate();

					if(Auth::user()->id != 1){
						$log_access = New Accesos();
						$log_access->ip =  $request->ip();
						$log_access->user_agent =  $request->header("user-agent");
						$log_access->usuario_id = Auth::user()->id;
						$log_access->save();

					}



					return redirect()->intended('dashboard');
			}

			return back()->withErrors([
					'password' => 'Credenciales incorrectas.',
			]);



    }


		public function Logout(Request $request){

				Auth::logout();

				$request->session()->invalidate();

				$request->session()->regenerateToken();

				return redirect('/admin');

		}


		public function LoginUser(Request $request)
    {

			$credentials = $request->only('nickname', 'password');

			if (Auth::attempt(["nickname"=>$credentials["nickname"], "password"=>$credentials["password"]])) {

				return 0;

			}else{
				return 1;
			}

		}



}
