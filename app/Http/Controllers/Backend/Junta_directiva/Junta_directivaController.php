<?php

namespace App\Http\Controllers\Backend\Junta_directiva;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Junta_directiva;
use Illuminate\Support\Facades\Storage;
use Illuminate\Http\File;

class Junta_directivaController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data["empleados"] = Junta_directiva::all();

        return view("Backend/Junta_directiva/Lista", $data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view("Backend/Junta_directiva/Crear");
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {


        if(!is_null($request->id)){
            $empleado = Junta_directiva::find($request->id);
            $request->session()->flash('modificado', 'true');
         }else{
            $empleado = new Junta_directiva();
             $request->session()->flash('nuevo', 'true');
         }

         $empleado->nombre = $request->nombre;
         $empleado->compania = $request->compania;
         $empleado->puesto_laboral_ES = $request->puesto_laboral_ES;
         $empleado->puesto_laboral_EN = $request->puesto_laboral_EN;




         if (!is_null($request->file('foto')) ) {

            $file = $request->file('foto');
            $extension = $file->extension();
            $resultDoc =  Storage::putFileAs('/junta_directiva', new File($request->file('foto')), $empleado->nombre.' '.(date('dmY His')).' ES.'.$extension );
            $resultDoc = explode('/', $resultDoc);
            if(!is_null($request->id)){
                Storage::delete($empleado->foto);
            }
            $empleado->foto = 'junta_directiva/'.$resultDoc[1];
        }




        $empleado->save();

         return redirect("directiva/".$empleado->id);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $data["empleado"] = Junta_directiva::find($id);

        return view("Backend/Junta_directiva/Mostrar", $data);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $data["empleado"] = Junta_directiva::find($id);

        return view("Backend/Junta_directiva/Modificar", $data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request  $request)
    {
        $empleado =  Junta_directiva::find($request->id);
        Storage::delete($empleado->foto);
        $empleado->delete();
        $request->session()->flash('eliminado', 'true');
        return redirect("directiva");
    }
}
