<?php

namespace App\Http\Controllers\Backend\Ajax;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Municipios;
use App\Models\Zonas;
use App\Helpers\ToolKit;
use App\Models\Cuentas_cobrar;
use App\Models\Preguntas_frecuentes;
use App\Models\User as Usuarios;

class AjaxController extends Controller
{
	public function municipios(Request $request)
	{

		$municipios = Municipios::where("departamento_id", $request->id)->get();
		return json_encode($municipios);
	}

	public function zonas(Request $request)
	{

		$zonas = Zonas::where("municipio_id", $request->id)->get();
		return json_encode($zonas);
	}


	public function verificar_nickname(Request $request)
	{

		if (Usuarios::where("nickname", $request->nickname)->exists()) {

			return "EXISTE_NICKNAME";
		} elseif (Usuarios::where("email", $request->email)->exists()) {

			return "EXISTE_EMAIL";
		}

		return "NO_EXISTEN";
	}


	public function info_box_ruta(Request $request)
	{

		$info_box  = ToolKit::info_box_ruta($request->ruta_id);
		return json_encode($info_box);
	}


	/**
	 * Obtener los registros de las preguntas frecuentes dentro de la base de datos para crear un json que sera consumido dentro de la vista mediante ajax
	 *
	 * @param Request $request Objeto de la peticion
	 * @param string $lang lenguaje para mapear las preguntas
	 * @return mixed
	 */
	public function get_preguntas_frecuentes(Request $request, string $lang)
	{

		$preguntas_frecuentes = ucwords($lang) == 'ES'
								? Preguntas_frecuentes::select('id', 'pregunta_ES as pregunta', 'respuesta_ES as respuesta')->get()
								: Preguntas_frecuentes::select('id', 'pregunta_EN as pregunta', 'respuesta_EN as respuesta')->get();
		return json_encode( $preguntas_frecuentes );
	}
}
