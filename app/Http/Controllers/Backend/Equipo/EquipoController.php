<?php

namespace App\Http\Controllers\Backend\Equipo;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Organizacion;
use Illuminate\Support\Facades\Storage;
use Illuminate\Http\File;

class EquipoController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data["empleados"] = Organizacion::all();

        return view("Backend/Equipo/Lista", $data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view("Backend/Equipo/Crear");
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {


        if(!is_null($request->id)){
            $empleado = Organizacion::find($request->id);
            $request->session()->flash('modificado', 'true');
         }else{
            $empleado = new Organizacion();
             $request->session()->flash('nuevo', 'true');
         }

         $empleado->nombre = $request->nombre;
         $empleado->puesto_laboral_ES = $request->puesto_laboral_ES;





         if (!is_null($request->file('foto')) ) {

            $file = $request->file('foto');
            $extension = $file->extension();
            $resultDoc =  Storage::putFileAs('/imagenes_enlaces', new File($request->file('foto')), $empleado->nombre.' '.(date('dmY His')).' ES.'.$extension );
            $resultDoc = explode('/', $resultDoc);
            if(!is_null($request->id)){
                Storage::delete($empleado->foto_url);
            }
            $empleado->foto_url = 'imagenes_enlaces/'.$resultDoc[1];
        }




        $empleado->save();

         return redirect("enlace/".$empleado->id);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $data["empleado"] = Organizacion::find($id);

        return view("Backend/Equipo/Mostrar", $data);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $data["empleado"] = Organizacion::find($id);

        return view("Backend/Equipo/Modificar", $data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request  $request)
    {
        $empleado =  Organizacion::find($request->id);
        Storage::delete($empleado->foto_url);
        $empleado->delete();
        $request->session()->flash('eliminado', 'true');
        return redirect("enlace");
    }
}
