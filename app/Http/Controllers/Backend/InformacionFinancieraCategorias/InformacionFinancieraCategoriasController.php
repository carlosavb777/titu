<?php

namespace App\Http\Controllers\Backend\InformacionFinancieraCategorias;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Categorias;
use App\Models\InformacionFinancieraCategorias;

class InformacionFinancieraCategoriasController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data["categorias"] = InformacionFinancieraCategorias::all();

        return view("Backend/Informacion_financiaera_categorias/Lista", $data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view("Backend/Informacion_financiaera_categorias/Crear");
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        if(!is_null($request->id)){
            $categoria = InformacionFinancieraCategorias::find($request->id);
            $request->session()->flash('modificado', 'true');
         }else{
             $categoria = new InformacionFinancieraCategorias();
             $request->session()->flash('nuevo', 'true');
         }

         $categoria->categoria_ES = $request->categoria_ES;
         $categoria->categoria_EN = $request->categoria_EN;


         $categoria->save();

         return redirect("informes_financieros_categorias/".$categoria->id);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $data["categoria"] = InformacionFinancieraCategorias::find($id);

        return view("Backend/Informacion_financiaera_categorias/Mostrar", $data);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $data["categoria"] = InformacionFinancieraCategorias::find($id);

        return view("Backend/Informacion_financiaera_categorias/Modificar", $data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request  $request)
    {
        $categoria =  InformacionFinancieraCategorias::find($request->id);
        $categoria->delete();
        $request->session()->flash('eliminado', 'true');
        return redirect("informes_financieros_categorias");
    }
}
