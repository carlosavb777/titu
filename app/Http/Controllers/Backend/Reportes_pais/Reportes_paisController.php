<?php

namespace App\Http\Controllers\Backend\Reportes_pais;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Reportes_pais;
use Illuminate\Support\Facades\Storage;
use Illuminate\Http\File;

class Reportes_paisController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data["reportes"] = Reportes_pais::all();

        return view("Backend/Reportes_pais/Lista", $data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view("Backend/Reportes_pais/Crear");
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {




        if(!is_null($request->id)){
            $documento = Reportes_pais::find($request->id);
            $request->session()->flash('modificado', 'true');
         }else{
            $documento = new Reportes_pais();
            $documento->anio = date('Y');
             $request->session()->flash('nuevo', 'true');
         }




         if (!is_null($request->file('archivo_ES')) ) {


            $file = $request->file('archivo_ES');
            $extension_ES = $file->extension();
            $resultDoc_ES =  Storage::putFileAs('/reportes_pais', new File($request->file('archivo_ES')), $documento->anio.' '.(date('dmY His')).' ES.'.$extension_ES );
            $resultDoc_ES = explode('/', $resultDoc_ES);
            if(!is_null($request->id)){
                Storage::delete($documento->url_ES);
            }
            $documento->url_ES = 'reportes_pais/'.$resultDoc_ES[1];
        }


        if (!is_null($request->file('archivo_EN')) ) {

            $file = $request->file('archivo_EN');
            $extension_EN = $file->extension();
            $resultDoc_EN =  Storage::putFileAs('/reportes_pais', new File($request->file('archivo_EN')), $documento->anio.' '.(date('dmY His')).' EN.'.$extension_EN );
            $resultDoc_EN = explode('/', $resultDoc_EN);
            if(!is_null($request->id)){
                Storage::delete($documento->url_EN);
            }
            $documento->url_EN = 'reportes_pais/'.$resultDoc_EN[1];
        }


        $documento->save();

         return redirect("encuesta_pais/".$documento->id);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $data["documento"] = Reportes_pais::find($id);

        return view("Backend/Reportes_pais/Mostrar", $data);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $data["documento"] = Reportes_pais::find($id);

        return view("Backend/Reportes_pais/Modificar", $data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request  $request)
    {

    }
}
