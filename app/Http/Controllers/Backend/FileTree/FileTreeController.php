<?php

namespace App\Http\Controllers\Backend\FileTree;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class FileTreeController extends Controller
{
    /**
     * Método de conneción entre filetree y la estructura de carpetas para ser servida en el front-end
     *
     * @param Request $request
     * @return Html
     */
    public function Connector(Request $request)
    {
        $root = public_path() . DIRECTORY_SEPARATOR;
        $postDir = rawurldecode(public_path($request->get('dir')));

        if (file_exists($postDir))
        {

            $files = scandir($postDir);
            $returnDir = substr($postDir, strlen($root));
            natcasesort($files);

            if (count($files) > 2)
            { // The 2 accounts for . and ..
                echo "<ul class='jqueryFileTree'>";
                foreach ($files as $file)
                {
                    $htmlRel = htmlentities($returnDir . $file);
                    $htmlName = htmlentities($file);
                    $ext = preg_replace('/^.*\./', '', $file);
                    if (file_exists($postDir . $file) && $file != '.' && $file != '..')
                    {
                        if (is_dir($postDir . $file))
                        {
                            echo "<li class='directory collapsed'><a rel='" . $htmlRel . "/'>" . $htmlName . "</a></li>";
                        }
                        else
                        {
                            echo "<li class='file ext_{$ext}'><a rel='" . $htmlRel . "'>" . $htmlName . "</a></li>";
                        }
                    }
                }
                echo "</ul>";
            }
        }
    }
}
