<?php

namespace App\Http\Controllers\Backend\Articulos;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Articulos;
use Illuminate\Support\Facades\Storage;
use Illuminate\Http\File;


class ArticulosController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data["articulos"] = Articulos::all();

        return view("Backend/Articulos/Lista", $data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view("Backend/Articulos/Crear");
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {



        if(!is_null($request->id)){
            $emision = Articulos::find($request->id);
            $request->session()->flash('modificado', 'true');
         }else{
            $emision = new Articulos();
             $request->session()->flash('nuevo', 'true');
         }

         $emision->categoria_id = $request->categoria;
         $emision->titulo_ES = $request->titulo_ES;
         $emision->titulo_EN = $request->titulo_EN;
         $emision->contenido_ES = $request->contenido_ES;
         $emision->contenido_EN = $request->contenido_EN;

        $emision->save();

        return redirect("emision/".$emision->id);
    }



    public function adjuntar_documentos(Request $request)
    {



         if(!is_null($request->id)){
            $documento = Documentos_Articulos::find($request->id);
             $request->session()->flash('modificado', 'true');
          }else{
            $documento = new Documentos_Articulos();
             $request->session()->flash('nuevo_documento', 'true');
         }



         $documento->emision_id = $request->emision_id;
         $documento->nombre_ES = $request->nombre_ES;
         $documento->nombre_EN = $request->nombre_EN;

         if (!is_null($request->file('archivo_ES')) ) {

            $file_ES = $request->file('archivo_ES');
            $extension_ES = $file_ES->extension();
            $resultDoc_ES =  Storage::putFileAs('documentos_emision/ES', new File($request->file('archivo_ES')), $documento->nombre_ES.' '.(date('dmY His')).' ES.'.$extension_ES );

            $resultDoc_ES = explode('/', $resultDoc_ES);


            $file_EN = $request->file('archivo_EN');
            $extension_EN = $file_EN->extension();
            $resultDoc_EN =  Storage::putFileAs('documentos_emision/EN', new File($request->file('archivo_EN')), $documento->nombre_EN.' '.(date('dmY His')).' EN.'.$extension_EN );
            $resultDoc_EN = explode('/', $resultDoc_EN);


            if(!is_null($request->id)){

                Storage::delete($documento->documento_url_ES);
                Storage::delete($documento->documento_url_EN);

            }

            $documento->url_ES = 'documentos_emision/ES/'.$resultDoc_ES[2];
            $documento->url_EN = 'documentos_emision/EN/'.$resultDoc_EN[2];
        }


        $documento->save();

        return redirect("emision/".$documento->emision_id);
    }


    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $data["emision"] = Articulos::find($id);
        $data["categoria"] = Categorias::find($data["emision"]->categoria_id);
        $data["documentos"] = Documentos_Articulos::where('emision_id', $id)->get();

        return view("Backend/Articulos/Mostrar", $data);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $data["emision"] = Articulos::find($id);
        $data["categoria"] = Categorias::find($data["emision"]->categoria_id);
        $data["categorias"] = Categorias::where('cartera', $data["categoria"]->cartera)->get();
        $data["emision"] = Articulos::find($id);



        return view("Backend/Articulos/Modificar", $data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request  $request)
    {
        $emision =  Articulos::find($request->id);
        $documentos =Documentos_Articulos::where('emision_id', $request->id)->get();
        foreach ($documentos as $key => $value) {
            Storage::delete($value->url_ES);
            Storage::delete($value->url_EN);
        }

        $emision->delete();
        $request->session()->flash('eliminado', 'true');
        return redirect("emision");
    }

    public function eliminar_documento(Request  $request)
    {
        $documento =  Documentos_Articulos::find($request->id);
        Storage::delete($documento->url_ES);
        Storage::delete($documento->url_EN);
        $documento->delete();
        $request->session()->flash('documento_eliminado', 'true');
        return redirect("emision/".$documento->emision_id);
    }
}
