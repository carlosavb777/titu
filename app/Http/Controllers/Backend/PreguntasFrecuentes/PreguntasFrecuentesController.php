<?php

namespace App\Http\Controllers\Backend\PreguntasFrecuentes;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Preguntas_frecuentes;

class PreguntasFrecuentesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data["preguntas_frecuentes"] = Preguntas_frecuentes::all();

        return view("Backend/Preguntas_frecuentes/Lista", $data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view("Backend/Preguntas_frecuentes/Crear");
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        if(!is_null($request->id)){
            $pregunta = Preguntas_frecuentes::find($request->id);
            $request->session()->flash('modificado', 'true');
         }else{
             $pregunta = new Preguntas_frecuentes();
             $request->session()->flash('nuevo', 'true');
         }

         $pregunta->pregunta_ES = $request->pregunta_ES;
         $pregunta->respuesta_ES = $request->respuesta_ES;
         $pregunta->pregunta_EN = $request->pregunta_EN;
         $pregunta->respuesta_EN = $request->respuesta_EN;

       //  dd($pregunta);
         $pregunta->save();

         return redirect("preguntas_frecuentes/".$pregunta->id);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $data["pregunta"] = Preguntas_frecuentes::find($id);

        return view("Backend/Preguntas_frecuentes/Mostrar", $data);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $data["pregunta"] = Preguntas_frecuentes::find($id);

        return view("Backend/Preguntas_frecuentes/Modificar", $data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request  $request)
    {
        $pregunta =  Preguntas_frecuentes::find($request->id);
        $pregunta->delete();
        $request->session()->flash('eliminado', 'true');
        return redirect("preguntas_frecuentes");
    }
}
