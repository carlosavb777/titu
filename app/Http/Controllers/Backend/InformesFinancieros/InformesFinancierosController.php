<?php

namespace App\Http\Controllers\Backend\InformesFinancieros;

use App\Http\Controllers\Controller;
use App\Models\InformacionFinancieraCategorias;
use Illuminate\Http\Request;
use App\Models\Informes_financieros;
use Illuminate\Support\Facades\Storage;
use Illuminate\Http\File;
use League\CommonMark\Block\Element\Document;

class InformesFinancierosController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data["informes"] = Informes_financieros::select('informes_financieros_documentos.*', 'informes_financieros_categorias.categoria_ES as categoria')
                                            ->join('informes_financieros_categorias', 'informes_financieros_categorias.id', '=', 'informes_financieros_documentos.informes_financieros_categoria_id')
                                            ->get();

        return view("Backend/Informes_financieros/Lista", $data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $data["categorias"] = InformacionFinancieraCategorias::all();
        return view("Backend/Informes_financieros/Crear", $data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {



        if(!is_null($request->id)){
            $informe = Informes_financieros::find($request->id);
            $request->session()->flash('modificado', 'true');
         }else{
            $informe = new Informes_financieros();
             $request->session()->flash('nuevo', 'true');
         }



         $informe->anio = $request->anio;
         $informe->informes_financieros_categoria_id = $request->categoria_id;
         $informe->nombre_ES = $request->nombre_ES;
         $informe->nombre_EN = $request->nombre_EN;



         if (!is_null($request->file('archivo_ES')) ) {

            $file = $request->file('archivo_ES');
            $extension_ES = $file->extension();
            $resultDoc_ES =  Storage::putFileAs('/documentos_informes_financieros/ES/'.$informe->anio, new File($request->file('archivo_ES')), $informe->nombre_ES.' '.(date('dmY His')).' ES.'.$extension_ES );
            $resultDoc_ES = explode('/', $resultDoc_ES);
            if(!is_null($request->id)){
                Storage::delete($informe->url_ES);
            }
            $informe->url_ES = 'documentos_informes_financieros/ES/'.$informe->anio.'/'.$resultDoc_ES[3];
        }




        if (!is_null($request->file('archivo_EN')) ) {

            $file = $request->file('archivo_EN');
            $extension_EN = $file->extension();
            $resultDoc_EN =  Storage::putFileAs('/documentos_informes_financieros/EN/'.$informe->anio, new File($request->file('archivo_EN')), $informe->nombre_EN.' '.(date('dmY His')).' EN.'.$extension_EN );
            $resultDoc_EN = explode('/', $resultDoc_EN);
            if(!is_null($request->id)){
                Storage::delete($informe->url_EN);
            }
            $informe->url_EN = 'documentos_informes_financieros/EN/'.$informe->anio.'/'.$resultDoc_EN[3];

        }


        $informe->save();

        return redirect("informes_financieros/".$informe->id);



    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $data["informe"] = Informes_financieros::find($id);
        $data["categoria"] =InformacionFinancieraCategorias::find($data["informe"]->informes_financieros_categoria_id);

        return view("Backend/Informes_financieros/Mostrar", $data);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $data["informe"] = Informes_financieros::find($id);
        $data["categoria"] = InformacionFinancieraCategorias::find($data["informe"]->informes_financieros_categoria_id);
        $data["categorias"] = InformacionFinancieraCategorias::all();




        return view("Backend/Informes_financieros/Modificar", $data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request  $request)
    {
        $informe =  Informes_financieros::find($request->id);
        Storage::delete($informe->url_ES);
        Storage::delete($informe->url_EN);
        $informe->delete();
        $request->session()->flash('eliminado', 'true');
        return redirect("informes_financieros");
    }


}
