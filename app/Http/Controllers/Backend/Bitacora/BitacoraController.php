<?php

namespace App\Http\Controllers\Backend\Bitacora;

use App\Http\Controllers\Controller;
use App\Models\Bitacora;
use App\Models\Modulos;
use App\Models\Permisos;
use App\Models\Rutas;
use App\Models\Productos;
use App\Models\Gastos;
use App\Models\Abonos_cuentas_cobrar;
use App\Models\Cuentas_cobrar;
use App\Models\User;
use App\Models\Accesos;
use Illuminate\Http\Request;

class BitacoraController extends Controller
{

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {




			$fecha = new \DateTime(date("Y-m-d"));
			$data["fecha_fin"] = $fecha->format("Y-m-d");
			$fecha->modify('-15 days');
			$data["fecha_inicio"] = $fecha->format("Y-m-d");

		//	dd($data["fecha_inicio"], $data["fecha_fin"]);

			if(!is_null($request->fecha_inicio)){
				$data["fecha_inicio"] = $request->fecha_inicio;
			}

			if(!is_null($request->fecha_fin)){
				$data["fecha_fin"] = $request->fecha_fin;
			}

			$data["usuarios"] = User::all();
			$data["usuario_id"] = "";

			$data["bitacoras"] = Bitacora::select("bitacora.*", "usuarios.nombres", "usuarios.apellidos")
																		->join("usuarios", "usuarios.id", "=", "bitacora.user_id")
																		->where("bitacora.created_at", ">=", $data["fecha_inicio"])
																		->where("bitacora.created_at", "<=", $data["fecha_fin"]." 23:59:59");

			if(!is_null($request->usuario)){

				$data["bitacoras"] = $data["bitacoras"]->where("bitacora.user_id", $request->usuario);
				$data["usuario_id"] = $request->usuario;
			}

			$data["bitacoras"] = $data["bitacoras"]->get();

        return view("Backend/Bitacora/Lista", $data);
    }






    /**
     * Display the specified resource.
     *
     * @param  \App\Models\roles  $roles
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {


        $data["bitacora"] = Bitacora::select("bitacora.*", "usuarios.nombres", "usuarios.apellidos")
																			->join("usuarios", "usuarios.id", "=", "bitacora.user_id")
																			->where("bitacora.id", $id)
																			->first();

				$modelo = json_decode($data["bitacora"]->datos_entrantes);
				$data["cliente"] = null;
				if($data["bitacora"]->entidad == "Ventas" && $data["bitacora"]->accion == "CREATE"){

						$data["cliente"] = Rutas::find($modelo->ruta_id);

				}
				$data["producto"] = null;
				if($data["bitacora"]->entidad == "Detalle_ventas" && $data["bitacora"]->accion == "CREATE"){

					$data["producto"] = Productos::find($modelo->producto_id);

				}
				$data["gasto"] = null;
				if($data["bitacora"]->entidad == "Detalle_gastos" && $data["bitacora"]->accion == "CREATE"){

					$data["gasto"] = Gastos::find($modelo->gasto_id);

				}
				$data["cuenta"] = null;
				if($data["bitacora"]->entidad == "Abonos_cuentas_cobrar" && $data["bitacora"]->accion == "CREATE"){

					$data["cuenta"] = Cuentas_cobrar::find($modelo->cuenta_cobrar_id);

				}


        return view("Backend/Bitacora/Mostrar", $data);
    }


		public function accesos(Request $request)
		{

				$fecha = new \DateTime(date("Y-m-d"));
				$data["fecha_fin"] = $fecha->format("Y-m-d");
				$fecha->modify('-15 days');
				$data["fecha_inicio"] = $fecha->format("Y-m-d");

			//	dd($data["fecha_inicio"], $data["fecha_fin"]);

				if(!is_null($request->fecha_inicio)){
					$data["fecha_inicio"] = $request->fecha_inicio;
				}

				if(!is_null($request->fecha_fin)){
					$data["fecha_fin"] = $request->fecha_fin;
				}

				$data["usuarios"] = User::all();
				$data["usuario_id"] = "";

				$data["accesos"] = Accesos::select("accesos.*", "usuarios.nombres", "usuarios.apellidos")
																			->join("usuarios", "usuarios.id", "=", "accesos.usuario_id")
																			->where("accesos.created_at", ">=", $data["fecha_inicio"])
																			->where("accesos.created_at", "<=", $data["fecha_fin"]." 23:59:59");

				if(!is_null($request->usuario)){

					$data["accesos"] = $data["accesos"]->where("accesos.usuario_id", $request->usuario);
					$data["usuario_id"] = $request->usuario;
				}

				$data["accesos"] = $data["accesos"]->get();

				return view("Backend/Bitacora/ListaAccesos", $data);

		}


}
