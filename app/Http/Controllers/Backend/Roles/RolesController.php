<?php

namespace App\Http\Controllers\Backend\Roles;

use App\Http\Controllers\Controller;
use App\Models\Roles;
use App\Models\Modulos;
use App\Models\Permisos;
use Illuminate\Http\Request;

class RolesController extends Controller
{

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

        $data["roles"] = Roles::all();

        return view("Backend/Roles/Lista", $data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view("Backend/Roles/Crear");
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        if(!is_null($request->id)){
           $rol = Roles::find($request->id);
           $request->session()->flash('modificado', 'true');
        }else{
            $rol = new Roles();
            $request->session()->flash('nuevo', 'true');
        }

        $rol->nombre = $request->nombre;
        $rol->save();

        return redirect("roles/".$rol->id);

    }

    public function guardar_permiso(Request $request)
    {
        if(!Permisos::where("rol_id", $request->rol_id)->where("modulo_id", $request->modulo_id)->first()){

          $permiso = new Permisos();
          $permiso->rol_id = $request->rol_id;
          $permiso->modulo_id = $request->modulo_id;
          $permiso->save();
          $request->session()->flash('nuevo_permiso', 'true');

        }

        return redirect("roles/".$request->rol_id);

    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\roles  $roles
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $data["rol"] = Roles::find($id);
        $data["modulos"] = Modulos::all();
        $data["permisos"] = Permisos::select("modulos.nombre as modulo", "permisos.*")
                                    ->join("modulos", "modulos.id", "=", "permisos.modulo_id")
                                    ->where("permisos.rol_id", $id)->get();


        return view("Backend/Roles/Mostrar", $data);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\roles  $roles
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {

        $data["rol"] = Roles::find($id);

        return view("Backend/Roles/Modificar", $data);

    }



    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\roles  $roles
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request)
    {

       $rol =  Roles::find($request->id);
       $rol->delete();
       $request->session()->flash('eliminado', 'true');
       return redirect("roles");
    }


    public function eliminar_permiso(Request $request)
    {

      $permiso =  Permisos::find($request->id);
      $rol_id = $permiso->rol_id;
      $permiso->delete();
      $request->session()->flash('permiso_eliminado', 'true');

       return redirect("roles/".$rol_id);

    }
}
