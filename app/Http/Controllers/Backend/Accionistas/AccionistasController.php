<?php

namespace App\Http\Controllers\Backend\Accionistas;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Accionistas;

class AccionistasController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data["accionistas"] = Accionistas::all();

        return view("Backend/Accionistas/Lista", $data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view("Backend/Accionistas/Crear");
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        if(!is_null($request->id)){
            $accionista = Accionistas::find($request->id);
            $request->session()->flash('modificado', 'true');
         }else{
             $accionista = new Accionistas();
             $request->session()->flash('nuevo', 'true');
         }

         $accionista->nombre_ES = $request->nombre_ES;
         $accionista->porcentaje = $request->porcentaje;
         $accionista->nombre_EN = $request->nombre_EN;

       //  dd($accionista);
         $accionista->save();

         return redirect("accionistas/".$accionista->id);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $data["accionista"] = Accionistas::find($id);

        return view("Backend/Accionistas/Mostrar", $data);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $data["accionista"] = Accionistas::find($id);

        return view("Backend/Accionistas/Modificar", $data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request  $request)
    {
        $accionista =  Accionistas::find($request->id);
        $accionista->delete();
        $request->session()->flash('eliminado', 'true');
        return redirect("accionistas");
    }
}
