<?php

namespace App\Http\Controllers\Backend\Gobierno_corporativo;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Gobierno_corporativo;
use Illuminate\Support\Facades\Storage;
use Illuminate\Http\File;

class Gobierno_corporativoController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data["gobierno_corporativo"] = Gobierno_corporativo::all();

        return view("Backend/Gobierno_corporativo/Lista", $data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view("Backend/Gobierno_corporativo/Crear");
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {




        if(!is_null($request->id)){
            $documento = Gobierno_corporativo::find($request->id);
            $request->session()->flash('modificado', 'true');
         }else{
            $documento = new Gobierno_corporativo();
             $request->session()->flash('nuevo', 'true');
         }

         $documento->titulo_ES = $request->titulo_ES;
         $documento->descripcion_ES = $request->descripcion_ES;
         $documento->nombre_documento_ES = $request->nombre_documento_ES;

         $documento->titulo_EN = $request->titulo_EN;
         $documento->descripcion_EN = $request->descripcion_EN;
         $documento->nombre_documento_EN = $request->nombre_documento_EN;


         if (!is_null($request->file('archivo_ES')) ) {

            $file = $request->file('archivo_ES');
            $extension_ES = $file->extension();
            $resultDoc_ES =  Storage::putFileAs('/documentos_gobierno_corporativo', new File($request->file('archivo_ES')), $documento->nombre_documento_ES.' '.(date('dmY His')).' ES.'.$extension_ES );
            $resultDoc_ES = explode('/', $resultDoc_ES);
            if(!is_null($request->id)){
                Storage::delete($documento->documento_url_ES);
            }
            $documento->documento_url_ES = 'documentos_gobierno_corporativo/'.$resultDoc_ES[1];
        }


        if (!is_null($request->file('archivo_EN')) ) {

            $file = $request->file('archivo_EN');
            $extension_EN = $file->extension();
            $resultDoc_EN =  Storage::putFileAs('/documentos_gobierno_corporativo', new File($request->file('archivo_EN')), $documento->nombre_documento_EN.' '.(date('dmY His')).' EN.'.$extension_EN );
            $resultDoc_EN = explode('/', $resultDoc_EN);
            if(!is_null($request->id)){
                Storage::delete($documento->documento_url_EN);
            }
            $documento->documento_url_EN = 'documentos_gobierno_corporativo/'.$resultDoc_EN[1];
        }


        $documento->save();

         return redirect("gobierno_corporativo/".$documento->id);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $data["documento"] = Gobierno_corporativo::find($id);

        return view("Backend/Gobierno_corporativo/Mostrar", $data);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $data["documento"] = Gobierno_corporativo::find($id);

        return view("Backend/Gobierno_corporativo/Modificar", $data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request  $request)
    {
        $documento =  Gobierno_corporativo::find($request->id);
        Storage::delete($documento->documento_url_ES);
        Storage::delete($documento->documento_url_EN);
        $documento->delete();
        $request->session()->flash('eliminado', 'true');
        return redirect("gobierno_corporativo");
    }
}
