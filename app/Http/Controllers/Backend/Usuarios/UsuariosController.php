<?php

namespace App\Http\Controllers\Backend\Usuarios;

use App\Http\Controllers\Controller;
use App\Models\User as Usuarios;
use App\Models\Roles;
use Illuminate\Http\Request;

class UsuariosController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

        $data["usuarios"] = Usuarios::select("usuarios.*", "roles.nombre as rol")
																		->join("roles", "roles.id", "=", "usuarios.rol_id")
																		->get();


        return view("Backend/Usuarios/Lista", $data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
			$data["roles"] = Roles::all();
        return view("Backend/Usuarios/Crear", $data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        if(!is_null($request->id)){
           $usuario = Usuarios::find($request->id);
           $request->session()->flash('modificado', 'true');
        }else{
            $usuario = new Usuarios();
            $request->session()->flash('nuevo', 'true');
        }

        $usuario->nombres = $request->nombres;
        $usuario->apellidos = $request->apellidos;
        $usuario->nickname = $request->nickname;
        $usuario->email = $request->correo;
        $usuario->clave_autorizacion = $request->clave;
        if  (!empty($request->password) ){

            $usuario->password = bcrypt($request->password);
        }

				$usuario->rol_id = $request->rol;
        $usuario->save();

        return redirect("usuarios/".$usuario->id);

    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\usuarios  $usuarios
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
			$data["usuario"] = Usuarios::select("usuarios.*", "roles.nombre as rol")
																->join("roles", "roles.id", "=", "usuarios.rol_id")
																->where("usuarios.id", $id)
																->first();

        return view("Backend/Usuarios/Mostrar", $data);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\usuarios  $usuarios
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {

			$data["usuario"] = Usuarios::select("usuarios.*", "roles.id as rol")
																->join("roles", "roles.id", "=", "usuarios.rol_id")
																->where("usuarios.id", $id)
																->first();
				$data["roles"] = Roles::all();
        return view("Backend/Usuarios/Modificar", $data);

    }



    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\usuarios  $usuarios
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request)
    {

       $usuario =  Usuarios::find($request->id);
       $usuario->delete();
       $request->session()->flash('eliminado', 'true');
       return redirect("usuarios");
    }
}
