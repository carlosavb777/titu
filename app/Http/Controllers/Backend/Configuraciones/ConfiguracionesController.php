<?php

namespace App\Http\Controllers\Backend\Configuraciones;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Configuraciones;
use Illuminate\Support\Facades\Storage;
use Illuminate\Http\File;

class ConfiguracionesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data["configuraciones"] = Configuraciones::all();

        return view("Backend/Configuraciones/Lista", $data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        $configuracion = Configuraciones::find($request->id);
        $request->session()->flash('modificado', 'true');

        if (!is_null($request->file('archivo_ES')) ) {

            $file = $request->file('archivo_ES');
            $extension_ES = $file->extension();
            $resultDoc_ES =  Storage::putFileAs('/documentos_generales', new File($request->file('archivo_ES')), $configuracion->key.' '.(date('dmY His')).' ES.'.$extension_ES );
            $resultDoc_ES = explode('/', $resultDoc_ES);
            if(!is_null($request->id)){
                Storage::delete($configuracion->value_ES);
            }
            $configuracion->value_ES = 'documentos_generales/'.$resultDoc_ES[1];
        }else{

            $configuracion->value_ES = $request->value_ES;

        }


        if (!is_null($request->file('archivo_EN')) ) {

            $file = $request->file('archivo_EN');
            $extension_EN = $file->extension();
            $resultDoc_EN =  Storage::putFileAs('/documentos_generales', new File($request->file('archivo_EN')), $configuracion->key.' '.(date('dmY His')).' EN.'.$extension_EN );
            $resultDoc_EN = explode('/', $resultDoc_EN);
            if(!is_null($request->id)){
                Storage::delete($configuracion->value_EN);
            }
            $configuracion->value_EN = 'documentos_generales/'.$resultDoc_EN[1];
        }else {
            $configuracion->value_EN = $request->value_EN;
        }







        $configuracion->save();

         return redirect("configuraciones/".$configuracion->id);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $data["configuracion"] = Configuraciones::find($id);

        return view("Backend/Configuraciones/Mostrar", $data);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $data["configuracion"] = Configuraciones::find($id);

        return view("Backend/Configuraciones/Modificar", $data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
