<?php

namespace App\Http\Controllers\Backend\Blog;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Blog;
use Illuminate\Support\Facades\Storage;
use Illuminate\Http\File;
use League\CommonMark\Block\Element\Document;

class BlogController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data["blogs"] = Blog::all();

        return view("Backend/Blog/Lista", $data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {

        $data['categorias'] = '';

        return view("Backend/Blog/Crear", $data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {



        if(!is_null($request->id)){
            $blog = Blog::find($request->id);
            $request->session()->flash('modificado', 'true');
         }else{
            $blog = new Blog();
             $request->session()->flash('nuevo', 'true');
         }

         $blog->descripcion_corta_ES = $request->descripcion_corta_ES;
         $blog->descripcion_corta_EN = $request->descripcion_corta_EN;
         $blog->titulo_ES = $request->titulo_ES;
         $blog->titulo_EN = $request->titulo_EN;
         $blog->contenido_ES = $request->contenido_ES;
         $blog->contenido_EN = $request->contenido_EN;
         if ($request->file('imagen')) {
            $resultImg =  Storage::putFile('imagenes_blog', new File($request->file('imagen')), 'public' );
            $ruta = explode('/', $resultImg);

            $blog->imagen = 'imagenes_blog/'.$ruta[1];
        }

        $blog->save();

        return redirect("blog/".$blog->id);
    }



    public function adjuntar_documentos(Request $request)
    {



         if(!is_null($request->id)){
            $documento = Documentos_Blog::find($request->id);
             $request->session()->flash('modificado', 'true');
          }else{
            $documento = new Documentos_Blog();
             $request->session()->flash('nuevo_documento', 'true');
         }



         $documento->blog_id = $request->blog_id;
         $documento->nombre_ES = $request->nombre_ES;
         $documento->nombre_EN = $request->nombre_EN;

         if (!is_null($request->file('archivo_ES')) ) {

            $file_ES = $request->file('archivo_ES');
            $extension_ES = $file_ES->extension();
            $resultDoc_ES =  Storage::putFileAs('documentos_blog/ES', new File($request->file('archivo_ES')), $documento->nombre_ES.' '.(date('dmY His')).' ES.'.$extension_ES );

            $resultDoc_ES = explode('/', $resultDoc_ES);


            $file_EN = $request->file('archivo_EN');
            $extension_EN = $file_EN->extension();
            $resultDoc_EN =  Storage::putFileAs('documentos_blog/EN', new File($request->file('archivo_EN')), $documento->nombre_EN.' '.(date('dmY His')).' EN.'.$extension_EN );
            $resultDoc_EN = explode('/', $resultDoc_EN);


            if(!is_null($request->id)){

                Storage::delete($documento->documento_url_ES);
                Storage::delete($documento->documento_url_EN);

            }

            $documento->url_ES = 'documentos_blog/ES/'.$resultDoc_ES[2];
            $documento->url_EN = 'documentos_blog/EN/'.$resultDoc_EN[2];
        }


        $documento->save();

        return redirect("blog/".$documento->blog_id);
    }


    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $data["blog"] = Blog::find($id);


        return view("Backend/Blog/Mostrar", $data);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $data["blog"] = Blog::find($id);




        return view("Backend/Blog/Modificar", $data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request  $request)
    {
        $blog =  Blog::find($request->id);
        $blog->delete();
        $request->session()->flash('eliminado', 'true');
        return redirect("blog");
    }


}
