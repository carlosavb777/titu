<?php

namespace App\Http\Controllers\Backend\Categorias;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Categorias;
use File;

class CategoriasController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data["categorias"] = Categorias::all();

        return view("Backend/Categorias/Lista", $data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view("Backend/Categorias/Crear");
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        if(!is_null($request->id)){
            $categoria = Categorias::find($request->id);
            $request->session()->flash('modificado', 'true');
         }else{

             $categoria = new Categorias();
             $request->session()->flash('nuevo', 'true');


            $path1 = public_path().'/documentos_emision/EN/'.$request->url_EN;
            File::makeDirectory($path1, $mode = 0777, true, true);

            $path2 = public_path().'/documentos_emision/ES/'.$request->url_ES;
            File::makeDirectory($path2, $mode = 0777, true, true);
         }

         $categoria->categoria_ES = $request->categoria_ES;
         $categoria->cartera = $request->cartera;
         $categoria->categoria_EN = $request->categoria_EN;
         $categoria->url_EN = $request->url_EN;
         $categoria->url_ES = $request->url_ES;




         $categoria->save();

         return redirect("categorias/".$categoria->id);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $data["categoria"] = Categorias::find($id);

        return view("Backend/Categorias/Mostrar", $data);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $data["categoria"] = Categorias::find($id);

        return view("Backend/Categorias/Modificar", $data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request  $request)
    {
        $categoria =  Categorias::find($request->id);
        $categoria->delete();
        $request->session()->flash('eliminado', 'true');
        return redirect("categorias");
    }
}
