<?php

namespace App\Http\Controllers\Backend\Normatividad;

use App\Http\Controllers\Controller;
use App\Models\Categorias_normatividad;
use Illuminate\Http\Request;
use App\Models\Circulares;
use Illuminate\Support\Facades\Storage;
use Illuminate\Http\File;
use League\CommonMark\Block\Element\Document;

class CircularesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data["circulares"] = Circulares::select('circulares.*', 'categorias.categoria_ES as categoria')
                                        ->join('categorias', 'categorias.id', '=', 'circulares.categoria_id')
                                        ->get();

        return view("Backend/Circulares/Lista", $data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {

        $data['categorias'] = Categorias_normatividad::all();

        return view("Backend/Circulares/Crear", $data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {



        if(!is_null($request->id)){
            $circular = Circulares::find($request->id);
            $request->session()->flash('modificado', 'true');
         }else{
            $circular = new Circulares();
             $request->session()->flash('nuevo', 'true');
         }

         $circular->descripcion_corta_ES = $request->descripcion_corta_ES;
         $circular->descripcion_corta_EN = $request->descripcion_corta_EN;
         $circular->categoria_id = $request->categoria;
         $circular->titulo_ES = $request->titulo_ES;
         $circular->titulo_EN = $request->titulo_EN;
         $circular->contenido_ES = $request->contenido_ES;
         $circular->contenido_EN = $request->contenido_EN;
        $circular->save();

        return redirect("circulares/".$circular->id);
    }



    public function adjuntar_documentos(Request $request)
    {



         if(!is_null($request->id)){
            $documento = Documentos_Circulares::find($request->id);
             $request->session()->flash('modificado', 'true');
          }else{
            $documento = new Documentos_Circulares();
             $request->session()->flash('nuevo_documento', 'true');
         }



         $documento->circular_id = $request->circular_id;
         $documento->nombre_ES = $request->nombre_ES;
         $documento->nombre_EN = $request->nombre_EN;

         if (!is_null($request->file('archivo_ES')) ) {

            $file_ES = $request->file('archivo_ES');
            $extension_ES = $file_ES->extension();
            $resultDoc_ES =  Storage::putFileAs('documentos_circular/ES', new File($request->file('archivo_ES')), $documento->nombre_ES.' '.(date('dmY His')).' ES.'.$extension_ES );

            $resultDoc_ES = explode('/', $resultDoc_ES);


            $file_EN = $request->file('archivo_EN');
            $extension_EN = $file_EN->extension();
            $resultDoc_EN =  Storage::putFileAs('documentos_circular/EN', new File($request->file('archivo_EN')), $documento->nombre_EN.' '.(date('dmY His')).' EN.'.$extension_EN );
            $resultDoc_EN = explode('/', $resultDoc_EN);


            if(!is_null($request->id)){

                Storage::delete($documento->documento_url_ES);
                Storage::delete($documento->documento_url_EN);

            }

            $documento->url_ES = 'documentos_circular/ES/'.$resultDoc_ES[2];
            $documento->url_EN = 'documentos_circular/EN/'.$resultDoc_EN[2];
        }


        $documento->save();

        return redirect("circular/".$documento->circular_id);
    }


    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $data["circular"] = Circulares::find($id);
        $data["categoria"] = Categorias_normatividad::find($data["circular"]->categoria_id);

        return view("Backend/Circulares/Mostrar", $data);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $data["circular"] = Circulares::find($id);
        $data["categorias"] = Categorias_normatividad::all();



        return view("Backend/Circulares/Modificar", $data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request  $request)
    {
        $circular =  Circulares::find($request->id);
        $circular->delete();
        $request->session()->flash('eliminado', 'true');
        return redirect("circulares");
    }

    public function eliminar_documento(Request  $request)
    {
        $documento =  Documentos_Circulares::find($request->id);
        Storage::delete($documento->url_ES);
        Storage::delete($documento->url_EN);
        $documento->delete();
        $request->session()->flash('documento_eliminado', 'true');
        return redirect("circular/".$documento->circular_id);
    }
}
