<?php

namespace App\Http\Controllers\Backend\Inversionistas;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Inversionistas;
use Illuminate\Support\Facades\Storage;
use Illuminate\Http\File;

class InversionistasController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data["empleados"] = Inversionistas::all();



        return view("Backend/Inversionistas/Lista", $data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view("Backend/Inversionistas/Crear");
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {


        if(!is_null($request->id)){
            $empleado = Inversionistas::find($request->id);
            $request->session()->flash('modificado', 'true');
         }else{
            $empleado = new Inversionistas();
             $request->session()->flash('nuevo', 'true');
         }

         $empleado->nombre = $request->nombre;
         $empleado->puesto_laboral_ES = $request->puesto_laboral_ES;
         $empleado->puesto_laboral_EN = $request->puesto_laboral_EN;
         $empleado->correo = $request->correo;
         $empleado->telefono = $request->telefono;


        $empleado->save();

         return redirect("inversionistasback/".$empleado->id);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $data["empleado"] = Inversionistas::find($id);

        return view("Backend/Inversionistas/Mostrar", $data);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $data["empleado"] = Inversionistas::find($id);

        return view("Backend/Inversionistas/Modificar", $data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request  $request)
    {
        $empleado =    Inversionistas::find($request->id);
        $empleado->delete();
        $request->session()->flash('eliminado', 'true');
        return redirect("inversionistasback");
    }
}
