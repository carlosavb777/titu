<?php

namespace App\Http\Controllers\Backend\Encabezados;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Encabezados;
use Illuminate\Http\File;
use Illuminate\Support\Facades\Storage;

class EncabezadosController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

        $data["encabezados"] = Encabezados::all();

        return view("Backend/Encabezados/Lista", $data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {


       // dd($request->file('imagen_header'));

        //Storage::put('public', $request->file('imagen_header'));
//dd();





        $encabezado= Encabezados::find($request->id);

        $encabezado->titulo_ES = $request->titulo_ES;
        $encabezado->subtitulo_ES = $request->subtitulo_ES;
        $encabezado->descripcion_ES = $request->descripcion_ES;
        $encabezado->titulo_EN = $request->titulo_EN;
        $encabezado->subtitulo_EN = $request->subtitulo_EN;
        $encabezado->descripcion_EN = $request->descripcion_EN;
        if ($request->file('imagen_header')) {
            $resultImg =  Storage::putFile('imagenes_encabezados', new File($request->file('imagen_header')), 'public' );
            $ruta = explode('/', $resultImg);

            $encabezado->imagen = 'imagenes_encabezados/'.$ruta[1];
        }

        $encabezado->save();

        $request->session()->flash('modificado', 'true');
         return redirect("encabezados/".$encabezado->id);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $data["encabezado"] = Encabezados::find($id);

        return view("Backend/Encabezados/Mostrar", $data);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $data["encabezado"] = Encabezados::find($id);

        return view("Backend/Encabezados/Modificar", $data);
    }


}
