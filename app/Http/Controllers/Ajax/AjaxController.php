<?php

namespace App\Http\Controllers\Ajax;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Municipios;
use App\Models\User as Usuarios;
use App\Helpers\ToolKit;
use App\Models\Categorias;

class AjaxController extends Controller
{
    public function municipios(Request $request)
    {

        $municipios = Municipios::where("departamento_id", $request->id)->get();
        return json_encode($municipios);
    }

		public function obtener_categorias(Request $request){

        $categorias = Categorias::where("cartera", $request->cartera)->get();
        return json_encode($categorias);
    }


		public function verificar_nickname(Request $request)
		{

				if(Usuarios::where("nickname", $request->nickname)->exists()){

					return "EXISTE_NICKNAME";

				}elseif(Usuarios::where("email", $request->email)->exists()){

					return "EXISTE_EMAIL";

				}

				return "NO_EXISTEN";


		}





}
