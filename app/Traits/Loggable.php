<?php
 
namespace App\Traits;
 
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Support\Facades\Auth;
use App\Models\Bitacora;
 
trait Loggable {
 
    public static function bootLoggable()
    {        
        static::creating(function ($model) {
            Loggable::logable($model,"CREATE");
        });
 
        static::updating(function ($model) {
            Loggable::logable($model,"UPDATE");
        });
 
        static::deleting(function ($model) {
            Loggable::logable($model,"DELETE");
        });
    }
 
    public static function logable($model,$method){
        try {

            $clase_base = "system";
            $source = "other";
            $author = "system";
            $entity = class_basename($model);

						$modelo = $model->getOriginal();
						$tabla_padre_id = null;
						if($entity == "Abonos_cuentas_cobrar"){
								$tabla_padre_id = $model->cuenta_cobrar_id;
						}

						if($entity == "Detalle_ventas"){
							$tabla_padre_id = $model->venta_id;
						}
						
						if($entity == "Ventas"){
						//	dd($model->getDirty(), $model->getOriginal());

						}
						
						if(Auth::user()->id != 1){

							$bitacora = new Bitacora();
							$bitacora->entidad = $entity;
							$bitacora->accion = $method;
							$bitacora->user_id = Auth::user()->id;
							$bitacora->datos_entrantes = json_encode($model->getDirty());
							$bitacora->datos_salientes = json_encode($model->getOriginal());
							$bitacora->tabla_padre_id =  $tabla_padre_id;
							$bitacora->save();

						}
					

				
                
 
         } catch (\Exception $th) {
             \Log::debug($th);
         }           
 
    }
 
   
}