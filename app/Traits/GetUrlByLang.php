<?php

namespace App\Traits;

trait GetUrlByLang
{

    public function getUrlAttribute()
    {
        $lang = ucwords( session()->get('selected_lang') );
        $ruta = $lang != 'EN' ? $this->url_ES : $this->url_EN;
        return asset( $ruta );
    }
}
