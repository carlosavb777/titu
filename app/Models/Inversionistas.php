<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use App\Traits\Loggable;

class Inversionistas extends Model
{
    Protected $table = "inversionistas";
    use HasFactory;
    use SoftDeletes;
    use Loggable;
}
