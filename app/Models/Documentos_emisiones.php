<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use App\Traits\Loggable;

class Documentos_emisiones extends Model
{
    Protected $table = "documentos_emisiones";
    use HasFactory;
    use SoftDeletes;
    use Loggable;
}
