<?php

namespace App\Models;

use App\Helpers\ToolKit;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use App\Traits\Loggable;

class Circulares extends Model
{
    Protected $table = "circulares";

    use HasFactory;
    use SoftDeletes;
    use Loggable;

    public function getTituloAttribute()
    {
        $lang = $this->getLang();
        return $lang != 'EN' ? $this->titulo_ES : $this->titulo_EN;
    }

    public function getDescripcionCortaAttribute()
    {
        $lang = $this->getLang();
        return $lang != 'EN' ? $this->descripcion_corta_ES : $this->descripcion_corta_EN;
    }

    public function getContenidoAttribute()
    {
        $lang = $this->getLang();
        return $lang != 'EN' ? $this->contenido_ES : $this->contenido_EN;
    }

    public function getFechaAttribute()
    {
        return ToolKit::fechaCortaDMY( $this->created_at );
    }

    private function getLang() : string
    {
        return ucwords( session()->get('selected_lang') );
    }
}
