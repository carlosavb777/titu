<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use App\Traits\Loggable;

class Categorias_normatividad extends Model
{
    Protected $table = "normatividad_categorias";

    use HasFactory;
    use SoftDeletes;
    use Loggable;

    public function getCategoriaAttribute()
    {
        $lang = ucwords( session()->get('selected_lang') );
        return $lang != 'EN' ? $this->categoria_ES : $this->categoria_EN;
    }

    public function circulares()
    {
        return $this->hasMany( Circulares::class, 'categoria_id', 'id' );
    }

}
