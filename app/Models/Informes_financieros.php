<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use App\Traits\Loggable;
use App\Traits\GetUrlByLang;

class Informes_financieros extends Model
{
    Protected $table = "informes_financieros_documentos";
    use HasFactory;
    use SoftDeletes;
    use Loggable;
    use GetUrlByLang;

    public function getNombreAttribute()
    {
        $locale = ucwords(session()->get('selected_lang'));
        return $locale != 'EN' ? $this->nombre_ES : $this->nombre_EN;
    }


}
