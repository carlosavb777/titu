<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use App\Traits\Loggable;

class InformacionFinancieraCategorias extends Model
{
    Protected $table = "informes_financieros_categorias";
    use HasFactory;
    use SoftDeletes;
    use Loggable;

    public function getCategoriaAttribute()
    {
        $locale = ucwords(session()->get('selected_lang'));
        return $locale != 'EN' ? $this->categoria_ES : $this->categoria_EN;
    }

    public function documentos()
    {
        return $this->hasMany(Informes_financieros::class, 'informes_financieros_categoria_id', 'id');
    }
}
