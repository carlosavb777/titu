<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use App\Traits\Loggable;

class Junta_directiva extends Model
{
    use HasFactory;
    use SoftDeletes;
    use Loggable;

    Protected $table = "junta_directiva";
}
