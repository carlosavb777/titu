<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use App\Traits\Loggable;
use App\Traits\GetUrlByLang;


class Reportes_pais extends Model
{
    use HasFactory;
    use SoftDeletes;
    use Loggable;
    use GetUrlByLang;

    Protected $table = "reportes_pais";

    protected $fillable = [
        'anio',
        'url_ES',
        'url_EN'
    ];

}
