<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use App\Traits\Loggable;

class Gobierno_corporativo extends Model
{

    protected $table = "gobierno_corporativo";
    use HasFactory;
    use SoftDeletes;
    use Loggable;
}
