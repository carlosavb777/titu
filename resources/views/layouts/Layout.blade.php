<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="_token" content="{{csrf_token()}}" />
    @yield('titulo')
</head>

<!-- Estilos -->
<link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3" crossorigin="anonymous">
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.1.1/css/all.min.css" type="text/css">
<link rel="stylesheet" href="{{ asset('css/frontend/home_style.css') }}" type="text/css">
@yield('styles')
<style>

</style>
<nav class="navbar navbar-dark cabecera">
  <div class="container-fluid nav-contain">
      <div class="row w-100">
          <div class="col-lg-3 col-sm-3 col-md-3  d-inline-flex align-items-center correo-nav">
              <i class="fa-solid fa-envelope"></i><span class="ms-2">{{ App\Helpers\ToolKit::Configuracion('CORREO') }}</span>
          </div>
          <div class="col-lg-8 col-sm-8 col-md-8  d-inline-flex align-items-center telefono-nav">
          <i class="fa-solid fa-phone"></i><span class="ms-2">{{ App\Helpers\ToolKit::Configuracion('TELEFONO1') }} </span>
          </div>
          <div class="col-lg-1 col-sm-1 col-md-1  text-end" style="height:30px;">
            <!-- ES <i class="fa-solid fa-angle-down"></i> -->
            <div class="custom-sel">
                <input type="text" class="input-lang" style="right: 100000px; position: absolute;">
                <a class="selected">{{ session()->get('selected_lang') ? session()->get('selected_lang') : 'ES' }} <i class="fa-solid fa-angle-down"></i></a>
                <a class="hidden" href="{{ route('lang.switch', 'es') }}">ES </a>
                <a class="hidden" href="{{ route('lang.switch', 'en') }}">EN</a>
            </div>
          </div>
      </div>
  </div>
</nav>

<nav id="navcontainer" class="navbar navbar-expand-lg navbar-light menu">
  <div class="container-fluid nav-contain">
    <a class="navbar-brand" href="{{url('/')}}">
        <img src="{{ asset('img/logo.png') }}" alt="">
    </a>
    <button id="navdeployer" class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
      <span class="navbar-toggler-icon"></span>
    </button>
    <div class="collapse navbar-collapse" id="navbarSupportedContent">
      <form class="search-form-movil">
        <span class="input-group-text icono-buscador"><i class="fa-solid fa-magnifying-glass"></i></span>
        <input id="buscador" class="form-control me-2 buscador" type="text" placeholder="{{__('menu.buscar')}}" aria-label="Buscador">
      </form>
      <ul class="navbar-nav me-auto mb-2 mb-lg-0">
        <li class="nav-item lang-movil-control">
          <a class="nav-link {{ session()->get('selected_lang') != 'ES' ? 'unselected-lang' : ''}}" href="{{ route('lang.switch', 'es') }}">
            {!! session()->get('selected_lang') != 'EN' ? '<i class="fa-solid fa-check"></i>' : ''  !!} ESPAÑOL
          </a>
        </li>
        <li class="nav-item lang-movil-control">
          <a class="nav-link {{ session()->get('selected_lang') != 'EN' ? 'unselected-lang' : ''}}" href="{{ route('lang.switch', 'en') }}">
            {!! session()->get('selected_lang') == 'EN' ? '<i class="fa-solid fa-check"></i>' : ''  !!} ENGLISH
          </a>
        </li>
        <li class="nav-item dropdown menu-opcion">
          <a
            class="nav-link dropdown-toggle"
            id="aboutDropdown"
            role="button"
            data-toggle="dropdown"
            aria-haspopup="true"
            aria-expanded="false"
            href="#">{{__('home.nosotros')}}
            <i class="fa-solid fa-angle-down"></i>
          </a>
          <div class="dropdown-menu nv-dp-menu" aria-labelledby="aboutDropdown">
            <a class="dropdown-item nav-menu-item" href="{{url('nuestra-titularizadora')}}">{{__('menu.nuestra-titularizadora')}}</a>
            <a class="dropdown-item nav-menu-item" href="{{url('accionistas_')}}">{{__('menu.accionistas')}}</a>
            <a class="dropdown-item nav-menu-item" href="{{url('gobierno-corporativo')}}">{{__('menu.gobierno-corporativo')}}</a>
            <a class="dropdown-item nav-menu-item" href="{{url('junta-directiva')}}">{{__('menu.junta-directiva')}}</a>
            <a class="dropdown-item nav-menu-item" href="{{url('organizacion')}}">{{__('menu.organizacion')}}</a>
            <a class="dropdown-item nav-menu-item" href="{{url('normatividad')}}">{{__('menu.normatividad')}}</a>
            <a class="dropdown-item nav-menu-item" href="{{url('informacion-financiera')}}">{{__('menu.informacion-financiera')}}</a>
            <a class="dropdown-item nav-menu-item" href="{{url('riesgos')}}">{{__('menu.riesgos')}}</a>
            <a class="dropdown-item nav-menu-item" href="{{url('relacion-inversionistas')}}">{{__('menu.relacion-con-inversionistas')}}</a>
          </div>
        </li>
        <li class="nav-item">
          <a class="nav-link" href="{{url('educacion-financiera')}}">{{__('home.educacion')}} </a>
        </li>
        <li class="nav-item">
          <a class="nav-link" href="{{url('emisiones')}}">{{__('home.emisiones')}}</a>
        </li>
        <li class="nav-item">
          <a class="nav-link" href="{{url('contactenos')}}">{{__('home.contactenos')}} </a>
        </li>
      </ul>
      <form class="search-form-web">
        <span class="input-group-text icono-buscador"><i class="fa-solid fa-magnifying-glass"></i></span>
        <input id="buscador" class="form-control me-2 buscador" type="text" placeholder="{{__('menu.buscar')}}" aria-label="Buscador">
      </form>
    </div>
  </div>
</nav>

<body>
<div>
     @yield('carrusel')
</div>
<div class="container-fluid">
     @yield('contenido')
</div>
<div>
     @yield('banner')
</div>
</body>

<footer class="footer" > <!-- footer -->
    <div class="row" id="footer-row"  >
        <div class="col-1 col-sm-1 col-md-1 col-lg-1 col-xl-1" style="max-width: 50px;" >
            <div>
                 {{-- <img class="vigilado2-logo" src="{{ asset('img/vigilado2.png') }}"> --}}
            </div>
         </div>
        <div class="col-11 col-sm-4 col-md-4 col-lg-4 col-xl-4 divider1" >
            <img class="titularice-logo" src="{{ asset('img/logo-blanco.png') }}" alt="">
            <ul>
                <p><i class="fa-solid fa-location-dot"></i><span> {{ App\Helpers\ToolKit::Configuracion('DIRECCION') }} </span></p>
                <p><i class="fa-solid fa-envelope"></i><span> {{ App\Helpers\ToolKit::Configuracion('CORREO') }}</span></p>
                <p><i class="fa-solid fa-phone"></i><span> {{ App\Helpers\ToolKit::Configuracion('TELEFONO1') }}</span></p>
            </ul>
        </div>
        <div class="col-12 col-sm-2 col-md-2 col-lg-3 col-xl-3 divider2" >
            <ul>
                <li><a href="{{url('nuestra-titularizadora')}}" >{{__('footer.texto1')}}</a></li>
                <li><a href="{{url('educacion-financiera')}}" >{{__('footer.texto2')}}</a></li>
                <li><a href="{{url('emisiones')}}" >{{__('footer.texto3')}}</a></li>
                {{-- <li><a href="{{url('informacion-Hechos')}}"  >{{__('footer.texto4')}}</a></li> --}}
                <li><a href="{{url('contactenos')}}" >{{__('footer.texto5')}}</a></li>
            </ul>
        </div>
        <div class="col-12 col-sm-3 col-md-3 col-lg-3 col-xl-3 divider3" >
            <ul>
                <li><a href="{{url('preguntas-frecuentes')}}" >{{__('footer.texto6')}}</a></li>
                 <li><a target="_blank" href="{{ App\Helpers\ToolKit::Configuracion('POLITICAS_DATOS_PERSONALES') }}" >{{__('footer.texto7')}}</a></li>
                <li><a href="{{url('terminos-condiciones')}}" >{{__('footer.texto8')}}</a></li>
                <li><a href="{{url('enlace-interes')}}" >{{__('footer.texto9')}}</a></li>
            </ul>
        </div>
        <div class="col-12 col-sm-2 col-md-2 col-lg-1 col-xl-1 divider4" >
            <span><b>{{__('footer.texto10')}}</b></span><br>
            <a href="http://www.linkedin.com/company/titularice" target="_blank" ><i class="fa-brands fa-linkedin"></i></a>
            {{-- <a href="{{ App\Helpers\ToolKit::Configuracion('INSTAGRAM_URL') }}" target="_blank" ><i class="fa-brands fa-instagram" class="instagram-icon" ></i></a> --}}
            {{-- <a href="{{ App\Helpers\ToolKit::Configuracion('FACEBOOK_URL') }}" target="_blank" ><i class="fa-brands fa-facebook-f" class="facebook-icon" ></i></a> --}}
        </div>
    </div>
</footer>
<div class="copyright">© Copyright {{date('Y')}} - Titularice </div>

</html>
<!-- JS -->
<script src="https://code.jquery.com/jquery-3.6.1.js" integrity="sha256-3zlB5s2uwoUzrXK3BT7AX3FyvojsraNFxCc2vC/7pNI=" crossorigin="anonymous"></script>
<script src="https://cdn.jsdelivr.net/npm/popper.js@1.12.9/dist/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
<script src="https://cdn.jsdelivr.net/npm/bootstrap@4.0.0/dist/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>
<script type="text/javascript" src="{{ asset('js/general.js') }}"></script>
<script> const APP_URL = '{{ url("/") }}';  const APP_LANG = '{{ session()->get("selected_lang") }}';  </script>
<script>
    $(document).ready(function() {

      // Show dropdown
      $('.selected').click(function() {
        if( !$('.custom-sel').hasClass('show-sel') ){
          $('.custom-sel').addClass('show-sel');
          $('.custom-sel a').removeClass('hidden');
          $('.input-lang').focus();
        } else {
          $('.custom-sel').removeClass('show-sel');
          $('.custom-sel > .lang').addClass('hidden');
        }
      });

      $('.custom-sel a').click(function() {
          $('.custom-sel a:not(:first)').addClass('lang');
      });

      // Hide dropdown when not focused
      $('.input-lang').focusout(function( e ) {
          console.log( e );
          if (!$('.custom-sel a').hasClass('lang')) {

              $('.custom-sel').removeClass('show-sel');
              $('.custom-sel a:not(:first)').addClass('hidden');

          }


      });

});
</script>
@yield('js')
