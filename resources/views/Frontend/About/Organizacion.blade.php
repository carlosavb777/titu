@extends('layouts.Layout')
@section('titulo')
    <title>{{__('windowtitle.organizacion')}}</title>
@endsection
@section('styles')
<link rel="stylesheet" href="css/frontend/general.css" type="text/css">
<link rel="stylesheet" href="css/frontend/directive_board_style.css" type="text/css">

@endsection
@section('contenido')
<section class="banner-container-organizacion" style="background-image: url('{{ App\Helpers\ToolKit::Encabezado('IMG_HEADER') }}')">

</section>
<section class="directive-board-container">
    <div class="row">
        <div class="col titulos">
            <span class="titulo2">{{ App\Helpers\ToolKit::Encabezado('SUBTITULO') }}</span><br>
            {{-- <span class="titulo2">{{ App\Helpers\ToolKit::Encabezado('TITULO') }}</span> --}}
            <hr class="linea-amarilla mt-4">
        </div>
    </div>
    <div class="row">
        <div class="col parrafo1">
            <p>
                {{ App\Helpers\ToolKit::Encabezado('DESCRIPCION') }}
            </p>
        </div>
    </div>
</section>
<!-- <section class="directive-board-container" style="background-image: url('img/junta.png'); background-position: center;
    height: 600px;  background-repeat: no-repeat;">


    </section> -->
<section class="directive-board-container" style="padding-top:0px !important" >
 {{-- s   <div class="row">
        <div class="col titulos">
            <span class="titulo2">{{ App\Helpers\ToolKit::Encabezado('SUBTITULO') }}</span>
            <hr class="linea-amarilla mt-4">
        </div>
    </div> --}}
    {{-- <div class="row">
        <?php //$pos = 1 ?>
        @foreach ($organizacion as $item)
        <div class="col-lg-3 col-md-6 col-sm-12 card-top">
            <div class="foto">
                <img src="{{ asset($item->foto_url) }}" class="img-1 mt-1">
            </div>

            <div class="estructura-name">{{ $item->nombre }}</div>
            <div class="puesto">{{ session()->get('selected_lang') != 'EN' ? $item->puesto_laboral_ES : $item->puesto_laboral_EN }}</div>
        </div>

        @endforeach

    </div> --}}
{{--
    <div style="background-image: url({{asset('imagenes_organizacion/estructura.jpg')}}); background-repeat: no-repeat;
    background-position: center; background-size: cover; height: 375px;" ></div> --}}
    <div class="row" >
        <div class=" text-center" >
             <img src="{{asset(App\Helpers\ToolKit::Configuracion('ESTRUCTURA_ORGANIZACIONAL'))}}" alt="" class="img-fluid float-right">
        </div>
    </div>
</section>


@endsection
