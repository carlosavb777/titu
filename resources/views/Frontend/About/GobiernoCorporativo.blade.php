@extends('layouts.Layout')
@section('titulo')
    <title>{{__('windowtitle.gobierno')}}</title>
@endsection
@section('styles')
    <link rel="stylesheet" href="css/frontend/corporative_gob_style.css" type="text/css">
@endsection
@section('contenido')
    <section class="banner-container" style="background-image: url('{{ App\Helpers\ToolKit::Encabezado('IMG_HEADER') }}')">

    </section>

    <section class="corporative-container">
        <div class="row">
            <div class="col corporative">
                <span class="txt-sobre-nosotros">{{ App\Helpers\ToolKit::Encabezado('SUBTITULO') }}</span><br>
                <span class="titulo-sobre-nosotros">{{ App\Helpers\ToolKit::Encabezado('TITULO') }}</span>
                <hr class="linea-amarilla mt-4">
            </div>
        </div>
        <div class="row corporative-info">
            @if (session()->get('selected_lang') != 'EN')
                <p>Titularice, tiene como eje fundamental de su actuación el compromiso activo con los originadores e inversionistas, que se materializan través de la transparencia y ética corporativa.
                </p>

                <p>Nuestro <a style="text-decoration: none" href="{{ url( App\Helpers\ToolKit::Configuracion('CODIGO_BUEN_GOBIERNO')) }}"> Código de Buen Gobierno
                        Corporativo</a> tiene como propósito compilar las normas de gobierno corporativo que, como complemento de la normatividad aplicable, rigen el funcionamiento de la Sociedad estableciendo principios, políticas y normas que rigen su administración.
                </p>

                <p>Comités de Apoyo Junta Directiva: la Junta Directiva estableció los siguientes comités que apoyan su gestión y permiten vigilar la implementación y desarrollo de las políticas definidas.
                </p>

                <p>Comité de Auditoría: El objetivo principal del Comité de Auditoría es prestar apoyo a la gestión que realiza la Junta Directiva en la implementación y supervisión del SCI, el cual debe estar conforme con la ley y las disposiciones de la Superintendencia Financiera de Colombia.
                </p>

                <p>El comité estará integrado por lo menos por tres (3) miembros de la Junta Directiva u órgano equivalente, quienes deberán tener experiencia, ser conocedores de los temas relacionados con las funciones asignadas al mismo.
                </p>
                <p>El comité está conformado, en su mayoría por miembros que, en los términos previstos en los Estatutos de la Sociedad, tengan calidad de independientes.
                </p>
                <p>
                    Comité de Riesgos: El objetivo principal del Comité de Riesgos es asistir a la Junta Directiva en el cumplimiento de sus responsabilidades de supervisión en relación con la gestión de riesgos.
                </p>
                <p>
                    El comité está integrado por tres (3) miembros de la Junta Directiva, de los cuales un (1) miembro será independiente y dos (2) serán patrimoniales.
                </p>
                <p>
                    El comité estará conformado, en su mayoría por miembros que, en los términos previstos en los Estatutos de la Sociedad, tengan calidad de independientes.
                </p>

                <br><br><br>
            @else
            <p>Titularice, has as a fundamental axis of its action the active commitment with originators and investors, which materialize through transparency and corporate ethics.
            </p>

            <p>Our<a style="text-decoration: none" href="{{ url( App\Helpers\ToolKit::Configuracion('CODIGO_BUEN_GOBIERNO')) }}"> Good Governance Code
                    Corporate</a> has the purpose of compiling the corporate governance rules that, as a complement to the applicable regulations, govern the operation of the Company, establishing principles, policies and rules that govern its administration.
            </p>

            <p>Support Committees Board of Directors: The Board of Directors established the following committees that support its management and allow monitoring the implementation and development of the defined policies.
            </p>

            <p>Audit Committee: The main objective of the Audit Committee is to provide support to the management carried out by the Board of Directors in the implementation and supervision of the SCI, which must be in accordance with the law and the provisions of the Financial Superintendence of Colombia .
            </p>

            <p>The committee will be made up of at least three (3) members of the Board of Directors or equivalent body, who must have experience and be knowledgeable about the issues related to the functions assigned to it.
            </p>
            <p>The committee is made up, for the most part, of members who, under the terms provided in the Company's Bylaws, are independent.
            </p>
            <p>
                Risk Committee: The main objective of the Risk Committee is to assist the Board of Directors in fulfilling its supervisory responsibilities in relation to risk management.
            </p>
            <p>
                The committee is made up of three (3) members of the Board of Directors, of which one (1) member will be independent and two (2) will be patrimonial.
            </p>
            <p>
                The committee will be made up, for the most part, of members who, under the terms provided in the Company's Bylaws, are independent.
            </p>
            @endif

        </div>
    </section>

    <section class="reports-container">
        <div class="row">
            <div class="col-lg-3 col-md-12 col-sm-12">
                <span
                    class="txt-sobre-nosotros">{{ App\Helpers\ToolKit::Encabezado('SUBTITULO', 'reportes_pais') }}</span><br>
                <span
                    class="titulo-sobre-nosotros">{{ App\Helpers\ToolKit::Encabezado('TITULO', 'reportes_pais') }}</span>
                <hr class="linea-amarilla mt-4">
            </div>
            <div class="col reports-filter">
                <p>
                    {{ App\Helpers\ToolKit::Encabezado('DESCRIPCION', 'reportes_pais') }}
                </p>
                <p>{{ __('about/GobiernoCorporativo.txt1') }}</p>

                {{-- begin: carrusel de reportes para pantallas desktop --}}
                <div id="carrousel-anios" class="carousel desktop-display" data-ride="carousel" data-interval="false"
                    data-pause="hover">
                    <div class="carousel-inner">
                        @foreach ($reportes_pais->chunk(3) as $key => $chunk)
                            <div class="carousel-item {{ $key == 0 ? 'active' : '' }}">
                                <div class="row justify-content-center">
                                    @foreach ($chunk as $reporte)
                                        <div class="col-md-3">
                                            <div class="card anio-card">
                                                <div class="card-body">
                                                    <a href="{{ $reporte->url }}" target="_blank"
                                                        rel="noopener noreferrer" style="all: unset;">
                                                        <i class="fa-solid fa-calendar"></i>
                                                        <span>{{ $reporte->anio }}</span>
                                                    </a>
                                                </div>
                                            </div>
                                        </div>
                                    @endforeach
                                </div>
                            </div>
                        @endforeach
                    </div>
                    <a class="carousel-control-prev" href="#carrousel-anios" role="button" data-slide="prev">
                        <i class="fa-solid fa-chevron-left" style="color:#14264C"></i>
                        <span class="sr-only">{{ __('home.texto27') }}</span>
                    </a>
                    <a class="carousel-control-next" href="#carrousel-anios" role="button" data-slide="next">
                        <i class="fa-solid fa-chevron-right" style="color:#14264C"></i>
                        <span class="sr-only">{{ __('home.texto28') }}</span>
                    </a>
                </div>
                {{-- end: carrusel de reportes para pantallas desktop --}}

                {{-- begin: carrusel de reportes para pantallas movil --}}
                <div id="carrousel-anios-sm" class="carousel mobile-display" data-ride="carousel" data-interval="false"
                    data-pause="hover">
                    <div class="carousel-inner">
                        @foreach ($reportes_pais as $key => $reporte)
                            <div class="carousel-item {{ $key == 0 ? 'active' : '' }}">
                                <div class="card anio-card">
                                    <div class="card-body">
                                        <a href="{{ $reporte->url }}" target="_blank" rel="noopener noreferrer"
                                            style="all: unset;">
                                            <i class="fa-solid fa-calendar"></i>
                                            <span>{{ $reporte->anio }}</span>
                                        </a>
                                    </div>
                                </div>
                            </div>
                        @endforeach
                    </div>
                    <a class="carousel-control-prev" href="#carrousel-anios-sm" role="button" data-slide="prev">
                        <i class="fa-solid fa-chevron-left" style="color:#14264C"></i>
                        <span class="sr-only">{{ __('home.texto27') }}</span>
                    </a>
                    <a class="carousel-control-next" href="#carrousel-anios-sm" role="button" data-slide="next">
                        <i class="fa-solid fa-chevron-right" style="color:#14264C"></i>
                        <span class="sr-only">{{ __('home.texto28') }}</span>
                    </a>
                </div>
                {{-- end: carrusel de reportes para pantallas movil --}}

            </div>
        </div>
        <br> <br> <br> <br> <br>
        <div class="row">
            <div class="col-lg-3 col-md-12 col-sm-12">
                <span
                    class="txt-sobre-nosotros">{{ App\Helpers\ToolKit::Encabezado('SUBTITULO', 'reportes') }}</span><br>
                <span class="titulo-sobre-nosotros">{{ App\Helpers\ToolKit::Encabezado('TITULO', 'reportes') }}</span>
                <hr class="linea-amarilla mt-4">
            </div>
            <div class="col reports-filter">
                <p>
                    {{ App\Helpers\ToolKit::Encabezado('DESCRIPCION', 'reportes') }}
                </p>
                <p>{{ __('about/GobiernoCorporativo.txt1') }}</p>

                {{-- begin: carrusel de reportes para pantallas desktop --}}
                <div id="reportes-carrousel-anios" class="carousel desktop-display" data-ride="carousel" data-interval="false"
                    data-pause="hover">
                    <div class="carousel-inner">
                        @foreach ($reportes->chunk(3) as $key => $chunk)
                            <div class="carousel-item {{ $key == 0 ? 'active' : '' }}">
                                <div class="row justify-content-center">
                                    @foreach ($chunk as $reporte)
                                        <div class="col-md-3">
                                            <div class="card anio-card">
                                                <div class="card-body">
                                                    <a href="{{ $reporte->url }}" target="_blank"
                                                        rel="noopener noreferrer" style="all: unset;">
                                                        <i class="fa-solid fa-calendar"></i>
                                                        <span>{{ $reporte->anio }}</span>
                                                    </a>
                                                </div>
                                            </div>
                                        </div>
                                    @endforeach
                                </div>
                            </div>
                        @endforeach
                    </div>
                    <a class="carousel-control-prev" href="#reportes-carrousel-anios" role="button" data-slide="prev">
                        <i class="fa-solid fa-chevron-left" style="color:#14264C"></i>
                        <span class="sr-only">{{ __('home.texto27') }}</span>
                    </a>
                    <a class="carousel-control-next" href="#reportes-carrousel-anios" role="button" data-slide="next">
                        <i class="fa-solid fa-chevron-right" style="color:#14264C"></i>
                        <span class="sr-only">{{ __('home.texto28') }}</span>
                    </a>
                </div>
                {{-- end: carrusel de reportes para pantallas desktop --}}

                {{-- begin: carrusel de reportes para pantallas movil --}}
                <div id="reportes-carrousel-anios-sm" class="carousel mobile-display" data-ride="carousel" data-interval="false"
                    data-pause="hover">
                    <div class="carousel-inner">
                        @foreach ($reportes as $key => $reporte)
                            <div class="carousel-item {{ $key == 0 ? 'active' : '' }}">
                                <div class="card anio-card">
                                    <div class="card-body">
                                        <a href="{{ $reporte->url }}" target="_blank" rel="noopener noreferrer"
                                            style="all: unset;">
                                            <i class="fa-solid fa-calendar"></i>
                                            <span>{{ $reporte->anio }}</span>
                                        </a>
                                    </div>
                                </div>
                            </div>
                        @endforeach
                    </div>
                    <a class="carousel-control-prev" href="#reportes-carrousel-anios-sm" role="button" data-slide="prev">
                        <i class="fa-solid fa-chevron-left" style="color:#14264C"></i>
                        <span class="sr-only">{{ __('home.texto27') }}</span>
                    </a>
                    <a class="carousel-control-next" href="#reportes-carrousel-anios-sm" role="button" data-slide="next">
                        <i class="fa-solid fa-chevron-right" style="color:#14264C"></i>
                        <span class="sr-only">{{ __('home.texto28') }}</span>
                    </a>
                </div>
                {{-- end: carrusel de reportes para pantallas movil --}}

            </div>
        </div>
    </section>

    {{-- <script src="{{ asset('js/gobierno-coorporativo.js') }}"></script> --}}
@endsection
