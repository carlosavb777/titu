@extends('layouts.Layout')
@section('titulo')
    <title>{{__('windowtitle.normatividad')}}</title>
@endsection
@section('styles')
<link rel="stylesheet" href="css/frontend/general.css" type="text/css">
<link rel="stylesheet" href="css/frontend/normativa_style.css" type="text/css">

@endsection

@section('contenido')
<!-- @include('layouts.Construccion') -->
<section class="banner-container" style="background-image: url('{{ App\Helpers\ToolKit::Encabezado('IMG_HEADER') }}')">

</section>

<section class="seccion-principal-normativa main-web">
    <div class="row">
        <div class="col titulos">
            <span class="titulo1">{{ App\Helpers\ToolKit::Encabezado('SUBTITULO') }}</span><br>
            <span class="titulo2">{{ App\Helpers\ToolKit::Encabezado('TITULO') }}</span>
            <hr class="linea-amarilla mt-4">
        </div>
    </div>
    <div class="row">
        <div class="col parrafo1">
            {!! App\Helpers\ToolKit::Encabezado('DESCRIPCION') !!}
        </div>
    </div>
    <br><br><br>
    <div class="row ajuste-margen">
        <div class="col">
            <ul class="nav nav-tabs card-header-tabs titu-tabs" data-id="myTab" role="tablist">
                @foreach ($categoriasNormatividad as $key => $categoria)
                <li class="nav-item">
                    <a class="nav-link tab-nav-link {{ $key == 0 ? 'active-tab-link' : '' }}"
                        data-toggle="tab"
                        data-id="categoria-{{ $categoria->id }}"
                        href="#categoria-{{ $categoria->id }}"
                        role="tab"
                        aria-controls="categoria-{{ $categoria->id }}"
                        aria-selected="true">{{ $categoria->categoria }}
                    </a>
                </li>
                @endforeach
            </ul>
        </div>
    </div>

</section>
<section class="normativa-board-container main-web">


    {{-- <form class="d-flex">
            <span class="input-group-text icono-buscador-normativa"><i class="fa-solid fa-magnifying-glass"></i></span>
            <input class="form-control me-2 buscador-normativa" type="text" placeholder="Buscar..." aria-label="Buscador">
        </form><br>
    --}}
    <div class="row">
        <div class="tab-content">
            @foreach($categoriasNormatividad as $key => $categoria)

            <div class="tab-pane {{ $key == 0 ? 'active' : '' }}" id="categoria-{{ $categoria->id }}">
                <div class="row content-normatividad">
                    <div class="col-md-12  header-normativa">
                        <span>{{ $categoria->categoria }}</span>
                    </div>

                </div>
                @forelse ($categoria->circulares as $circular)
                <div class="card-normatividad-2 mb-3">
                    <div class="card-body">
                        <h5 class="card-titulo">{{ $circular->titulo }}</h5>
                        <p class="card-texto">
                            {{-- <small class="text-muted">{{ $circular->fecha }}</small> --}}
                        </p>
                        <hr class="linea-gris-oscuro">
                        {!! $circular->contenido !!}
                    </div>
                </div>
                @empty
                <div class="card-normatividad-2 mb-3">
                    <div class="card-body">
                        <h5 class="card-titulo"></h5>
                        <p class="card-texto"><small class="text-muted"></small></p>
                        <hr class="linea-gris-oscuro">
                        <p>Sin nada que mostrar.</p>
                    </div>
                </div>
                @endforelse
            </div>
            @endforeach
        </div>
    </div>


</section>
<section class="main-section mobile-view">
    <div class="row ">
        <div class="col titulos">
            <span class="titulo1">Circulares</span>
            <span class="titulo2"></span>
            <hr class="linea-amarilla mt-4">
        </div>
    </div>
    <div class="row">
        <div id="accordion">
            @foreach ($categoriasNormatividad as $key => $categoria)
            <div class="card simple-grey-card-accordion">
                <div class="card-header" id="heading{{ $categoria->id }}">
                    <h5 class="mb-0">
                        <button class="btn btn-link" data-toggle="collapse" data-target="#categoria{{ $categoria->id }}" aria-expanded="true" aria-controls="heading{{ $categoria->id }}">
                            {{ $categoria->categoria }}
                        </button>
                        <i class="fa fa-plus"></i>
                    </h5>
                </div>
                <div id="categoria{{ $categoria->id }}" class="collapse" aria-labelledby="heading{{ $categoria->id }}" data-parent="#accordion">
                    <div class="row mt-3">
                        @forelse ($categoria->circulares as $circular)
                        <div class="card-normatividad-2 mb-3">
                            <div class="card-body">
                                <h5 class="card-titulo">{{ $circular->titulo }}</h5>
                                {{-- <p class="card-texto"><small class="text-muted">{{ $circular->fecha }}</small></p> --}}
                                <hr class="linea-gris-oscuro">
                                {!! $circular->contenido !!}
                            </div>
                        </div>
                        @empty
                        <div class="card-normatividad-2 mb-3">
                            <div class="card-body">
                                <h5 class="card-titulo"></h5>
                                <p class="card-texto"><small class="text-muted"></small></p>
                                <hr class="linea-gris-oscuro">
                                <p>No hay circulares que mostrar.</p>
                            </div>
                        </div>
                        @endforelse
                    </div>
                </div>
            </div>
            @endforeach

        </div>
    </div>
</section>

@endsection
