@extends('layouts.Layout')
@section('titulo')
    <title>{{__('windowtitle.accionistas')}}</title>
@endsection
@section('styles')
<link rel="stylesheet" href="css/frontend/shareholders_style.css" type="text/css">
@endsection
@section('contenido')
    <section class="banner-container" style="background-image: url('{{ App\Helpers\ToolKit::Encabezado('IMG_HEADER') }}')">

    </section>

    <section class="shareholders-container">
        <div class="row">
            <div class="col shareholders">
                <span class="txt-sobre-nosotros">{{ App\Helpers\ToolKit::Encabezado('SUBTITULO') }}</span><br>
                <span class="titulo-sobre-nosotros">{{ App\Helpers\ToolKit::Encabezado('TITULO') }}</span>
                <hr class="linea-amarilla mt-4">
            </div>
        </div>
        <div class="row">
            <div class="col shareholders-text">
                <p>
                    {!! App\Helpers\ToolKit::Encabezado('DESCRIPCION') !!}
                </p>
            </div>
        </div>
        {{-- <div class="row">
            <div class="col our-shareholders mt-5">
                <span>{{__('about/Accionistas.txt1')}}</span>
            </div>
        </div> --}}

        <div class="row mt-5">
            <div class="col shareholders-cards">
                @foreach ($accionistas as $i => $accionista)
                <div class="shareholder-card" data-pair="{{ ($i+1) % 2 == 0 ? 'true' : 'false' }}">
                    <div class="card-body">
                        <h2>{{__('about/Accionistas.txt2')}}</h2>
                        <span class="shareholder-name">{{ $accionista->nombre_ES }}</span>
                        <span>{{ $accionista->porcentaje }}</span>
                    </div>
                </div>
                @endforeach
            </div>
        </div>

    </section>
@endsection



