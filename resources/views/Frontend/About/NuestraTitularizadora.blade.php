
@extends('layouts.Layout')
@section('titulo')
    <title>{{__('windowtitle.about')}}</title>
@endsection
@section('styles')
<link rel="stylesheet" href="css/frontend/about_titularice_style.css" type="text/css">
@endsection
@section('contenido')
    <section class="video-container" style="background-image: url('{{ App\Helpers\ToolKit::Encabezado('IMG_HEADER') }}')">
    </section>

    <section class="about-container">

        <div class="row">
            <div class="col-lg-6 col-sm-12 col-md-12 about-titularice">
                <span class="txt-sobre-nosotros">{{ App\Helpers\ToolKit::Encabezado('SUBTITULO') }}</span><br>
                <span class="titulo-sobre-nosotros">{{ App\Helpers\ToolKit::Encabezado('TITULO') }}</span>

                <hr class="linea-amarilla">
            </div>
        </div>

        <div class="row">
            <div class="col-lg-8 col-sm-12 col-md-12 about-text">
                <p>
                    {{ App\Helpers\ToolKit::Encabezado('DESCRIPCION') }}
                </p>
            </div>
            <div class="col-lg-4 col-sm-12 col-md-12">
                <p class="phrase">
                    {{ App\Helpers\ToolKit::Configuracion('FRASE_IMPORTANTE') }}
                </p>
            </div>
        </div>

        {{-- <div class="row">
            <div class="col-lg-12 col-sm-12 col-md-12 about-text">
                <p>
                    Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.
                    Nulla porttitor massa id neque aliquam vestibulum morbi blandit. Nibh sed pulvinar proin gravida hendrerit lectus a.
                    Hac habitasse platea dictumst quisque sagittis.
                    Diam maecenas ultricies mi eget mauris pharetra et ultrices neque.
                </p>
            </div>
        </div> --}}
    </section>

    <section class="target-container">

        {{-- <div class="row">
            <div class="col-lg-3 col-sm-3 col-md-12 about-titularice">
                <span class="txt-sobre-nosotros">Objetivo</span><br>
                <span class="titulo-sobre-nosotros">Objetivo</span>
                <hr class="linea-amarilla">
            </div>
            <div class="col-lg-9 col-sm-12 col-md-12 about-titularice">
                <p>
                    {{ App\Helpers\ToolKit::Configuracion('OBJETIVO') }}
                </p>
            </div>
        </div> --}}

        <div class="row about-titularice">
            <div class="col-lg-4 col-sm-12 col-md-12">
                <img class="img-fluid mb-2" src="img/santiago-boada.png" alt="Santiago Boada">
            </div>
            <div class="col-lg-8 col-sm-12 col-md-12">
                <div class="card target-card p-4 mb-4">
                    <div class="card-body">
                        <h2 class="card-title">{{__('about/NuestraTitularizadora.txt1')}}</h2>
                        <hr class="blue-line">
                      <p>
                        {!! App\Helpers\ToolKit::Configuracion('MISION') !!}
                      </p>
                    </div>
                </div>
                {{-- <div class="card target-card p-4 mt-2 mb-4 mt-fix">
                    <div class="card-body">
                        <h2 class="card-title">{{__('about/NuestraTitularizadora.txt2')}}</h2>
                        <hr class="blue-line">
                      <p>
                        {{ App\Helpers\ToolKit::Configuracion('VISION') }}
                      </p>
                    </div>
                </div> --}}
            </div>
        </div>

    </section>

    <section id="assesorement-container">
        <div class="col-lg-6 col-sm-12 col-md-12 assesorement-question">
            <span>{{__('about/NuestraTitularizadora.txt3')}}</span><br>
            <hr class="linea-amarilla">
            <a href="{{url('contactenos')}}" class="btn contactenos">{{__('about/NuestraTitularizadora.txt4')}}</a>
        </div>
    </section>

@endsection


