@extends('layouts.Layout')
@section('titulo')
    <title>{{__('windowtitle.inversionistas')}}</title>
@endsection
@section('styles')
<link rel="stylesheet" href="css/frontend/general.css" type="text/css">
<link rel="stylesheet" href="css/frontend/shareholder_rel_style.css" type="text/css">

@endsection
@section('contenido')
    <section class="banner-container" style="background-image: url('{{ App\Helpers\ToolKit::Encabezado('IMG_HEADER') }}')">

    </section>
    <section class="rel-container">
        <div class="row">
            <div class="col rel-head">
                <span class="txt-sobre-nosotros">{{ App\Helpers\ToolKit::Encabezado('SUBTITULO') }}</span><br>
                <span class="titulo-sobre-nosotros">{{ App\Helpers\ToolKit::Encabezado('TITULO') }}</span>
                <hr class="linea-amarilla mt-4">
            </div>
        </div>
        <div class="row">
            <div class="col">
                <p class="text-justify">
                    {{ App\Helpers\ToolKit::Encabezado('DESCRIPCION') }}
                </p>
            </div>
        </div>
        <div class="row mt-5">
        @foreach ($Inversionistas as $item)
            <div class="col-lg-4 col-md-4 col-sm-6">
                <div class="card shldr-rel-card mt-1">
                    <div class="card-body">
                        <span class="shrl-icon"><i class="fa-solid fa-user-circle"></i></span>
                        <span class="shldr-name">{{ $item->nombre }}</span>
                        <span class="shldr-job">{{ session()->get('selected_lang') != 'EN' ? $item->puesto_laboral_ES : $item->puesto_laboral_EN }}</span>
                        <span class="shldr-email"><i class="fa-solid fa-envelope"></i>&nbsp;{{ $item->correo }}</span>
                        <span class="shldr-phone"><i class="fa-solid fa-phone"></i>&nbsp;{{ $item->telefono }}</span>
                    </div>
                </div>
            </div>
        @endforeach
        </div>
        <div class="row mt-4 mb-2">
            <div class="col">
                <p class="text-justify">
                    {{__('about/RelacionInversionistas.txt1')}}
                </p>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-3 col-md-6 col-sm-12">
                <div style="cursor:pointer" class="card calendar-card mt-1" data-url="{{ App\Helpers\ToolKit::Configuracion('INVERSIONISTAS_CALENDARIO_EVENTOS') }}">
                    <div class="card-body">
                        <p class="emision-btn mt-3">{{__('about/RelacionInversionistas.txt2')}}</p>
                    </div>
                </div>
            </div>
            {{-- <div class="col-lg-3 col-md-6 col-sm-12">
                <div style="cursor:pointer" class="card calendar-card mt-1" data-url="{{ App\Helpers\ToolKit::Configuracion('INVERSIONISTAS_ANALISTA_DE_INVERSIONES') }}">
                    <div class="card-body">
                        <p class="mt-3">{{__('about/RelacionInversionistas.txt3')}}</p>
                    </div>
                </div>
            </div> --}}
        </div>

    </section>
@endsection
@section('js')
    <script>

        $('.calendar-card').on('click', function () {
            window.open($(this).data('url'), '_blank');
        });

    </script>

 @stop
