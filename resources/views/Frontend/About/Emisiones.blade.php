@extends('layouts.Layout')
@section('titulo')
    <title>{{__('windowtitle.emisiones')}}</title>
@endsection
@section('styles')
    <link rel="stylesheet" href="{{ asset('css/frontend/general.css') }}" type="text/css">
    <link rel="stylesheet" href="{{ asset('css/frontend/emisiones_style.css') }}" type="text/css">
@endsection
@section('contenido')
<style>
    .descargar-emision:hover{
        color: blue !important;
    }
</style>
    <section class="banner-container" id="emisiones-banner" style="background-image: url('{{ App\Helpers\ToolKit::Encabezado('IMG_HEADER') }}')">

    </section>
    <section class="main-section main-web">
        <div class="row">
            <div class="col titulos">
                <span class="titulo1">{{ App\Helpers\ToolKit::Encabezado('SUBTITULO') }}</span>
                <span class="titulo2">{{ App\Helpers\ToolKit::Encabezado('TITULO') }}</span>
                <hr class="linea-amarilla mt-4">
            </div>
        </div>
        <div class="row">
            <div class="col">
                <ul class="nav nav-tabs card-header-tabs titu-tabs">
                    <li class="nav-item">
                        <a class="nav-link tab-nav-link active-tab-link" href="#item01" data-toggle="collapse" data-id="item01"
                            role="button" aria-expanded="false" aria-controls="item01">{{__('about/Emisiones.nav-txt1')}}</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link tab-nav-link" href="#item02" data-toggle="collapse" data-id="item02" role="button"
                            aria-expanded="false" aria-controls="item02">{{__('about/Emisiones.nav-txt2')}}</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link tab-nav-link" href="#item03" data-toggle="collapse" data-id="item03" role="button"
                            aria-expanded="false" aria-controls="item03">{{__('about/Emisiones.nav-txt3')}}</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link tab-nav-link" href="#item04" data-toggle="collapse" data-id="item04" role="button"
                            aria-expanded="false" aria-controls="item04">{{__('about/Emisiones.nav-txt4')}}</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link tab-nav-link" href="#item05" data-toggle="collapse" data-id="item05" role="button"
                            aria-expanded="false" aria-controls="item05">{{__('about/Emisiones.nav-txt5')}}</a>
                    </li>
                </ul>
            </div>
        </div>
        <div class="tab-link-box mt-5 mb-5"  id="item01" data-show="true">
            <div class="row ">

              {!! $FLUJOS_FUTUROS !!}

            </div>
        </div>
        <div class="tab-link-box mt-5 mb-5" id="item02" data-show="false">
            <div class="row ">
                {!! $CARTERA_LIBRANZAS !!}
            </div>
        </div>
        <div class="tab-link-box mt-5 mb-5" id="item03" data-show="false">
            <div class="row ">
                {!! $CARTERA_CONSUMO_COMERCIAL !!}
            </div>

        </div>
        <div class="tab-link-box mt-5 mb-5" id="item04" data-show="false">
            <div class="row ">
                {!! $CARTERA_VEHICULOS !!}
            </div>
        </div>
        <div class="tab-link-box mt-5 mb-5" id="item05" data-show="false">
            <div class="row ">
                {!! $EMISIONES_CANCELADAS !!}
            </div>
        </div>

    </section>
    <section class="main-section mobile-view" >
        <div class="row">
            <div class="col titulos">
                <span class="titulo1">emisiones</span>
                <span class="titulo2">Lorem ipsum dolor sit amet</span>
                <hr class="linea-amarilla mt-4">
            </div>
        </div>
        <div class="row">
            <div id="accordion">
                <div class="card simple-grey-card-accordion">
                  <div class="card-header mobile-accordion-item" id="headingOne" data-toggle="collapse" data-target="#collapseOne" aria-expanded="false" aria-controls="collapseOne">
                    <h5 class="mb-0">
                      <button class="btn btn-link" >
                        {{__('about/Emisiones.nav-txt1')}}
                      </button>
                      <i class="fa fa-plus"></i>
                    </h5>
                  </div>
                  <div id="collapseOne" class="collapse" aria-labelledby="headingOne" data-parent="#accordion">
                    <div class="row mt-3">
                        {!! $FLUJOS_FUTUROS !!}
                    </div>
                  </div>
                </div>
                <div class="card simple-grey-card-accordion">
                    <div class="card-header mobile-accordion-item" id="headingTwo" data-toggle="collapse" data-target="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo">
                      <h5 class="mb-0">
                        <button class="btn btn-link" >
                            {{__('about/Emisiones.nav-txt2')}}
                        </button>
                        <i class="fa fa-plus icon-button"></i>
                      </h5>
                    </div>
                    <div id="collapseTwo" class="collapse" aria-labelledby="headingTwo" data-parent="#accordion">
                      <div class="row ">
                        {!! $CARTERA_LIBRANZAS !!}
                      </div>
                    </div>
                </div>
                <div class="card simple-grey-card-accordion">
                    <div class="card-header mobile-accordion-item" id="headingThree" data-toggle="collapse" data-target="#collapseThree" aria-expanded="false" aria-controls="collapseThree">
                      <h5 class="mb-0">
                        <button class="btn btn-link" >
                            {{__('about/Emisiones.nav-txt3')}}
                        </button>
                        <i class="fa fa-plus"></i>
                      </h5>
                    </div>
                    <div id="collapseThree" class="collapse" aria-labelledby="headingThree" data-parent="#accordion">
                      <div class="row">
                        {!! $CARTERA_CONSUMO_COMERCIAL !!}
                      </div>
                    </div>
                </div>
                <div class="card simple-grey-card-accordion">
                    <div class="card-header mobile-accordion-item" id="headingFour" data-toggle="collapse" data-target="#collapseFour" aria-expanded="false" aria-controls="collapseFour">
                      <h5 class="mb-0">
                        <button class="btn btn-link" >
                            {{__('about/Emisiones.nav-txt4')}}
                        </button>
                        <i class="fa fa-plus"></i>
                      </h5>
                    </div>
                    <div id="collapseFour" class="collapse" aria-labelledby="headingFour" data-parent="#accordion">
                      <div class="row">
                        {!! $CARTERA_VEHICULOS !!}
                      </div>
                    </div>
                </div>
                <div class="card simple-grey-card-accordion">
                    <div class="card-header mobile-accordion-item" id="headingFive" data-toggle="collapse" data-target="#collapseFive" aria-expanded="false" aria-controls="collapseFive">
                      <h5 class="mb-0">
                        <button class="btn btn-link" >
                            {{__('about/Emisiones.nav-txt5')}}
                        </button>
                        <i class="fa fa-plus"></i>
                      </h5>
                    </div>
                    <div id="collapseFive" class="collapse" aria-labelledby="headingFive" data-parent="#accordion">
                        <div class="row ">
                            {!! $EMISIONES_CANCELADAS !!}
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <div class="modal-titularice modal fade auto-load-tituarice-modal" id="tituarice-modal" tabindex="-1" role="dialog"
        aria-labelledby="tituarice-modal" aria-hidden="true" data-show="true">
        <div class="modal-dialog modal-dialog-centered" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <img src="{{ asset('img/logo-blanco.png') }}" alt="">
                </div>
                <div class="modal-body">
                    <p class="text-justify">
                        {!!App\Helpers\ToolKit::Configuracion('POP_UP_EMISIONES')!!}
                    </p>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn contactenos btn-sm" aria-hidden="true" data-dismiss="modal"
                        aria-label="Close">{{__('about/Emisiones.modal-txt2')}}</button>
                </div>
            </div>
        </div>
    </div>
@endsection
@section('js')
    <script>
        /* Busca todos los elementos con la clase .mobile-accordion-item
        esta clase no figura en el css para evitar la funcionalidad donde no se necesita*/
        $('document').ready(function(){
            const accordionItems = $('.mobile-accordion-item');
            const lengthAccordionItems = accordionItems.length;

            /* reestablece el estilo inicial de todos los items del acordión */
            function initialStyle () {
                for (let i=0; i < lengthAccordionItems; i++) {
                    accordionItems.removeClass('h-active');
                    accordionItems.find('.fa').removeClass('fa-minus').addClass('fa-plus');
                }
            }

            /* Establece como item activo al item del menú clickeado
            a la vez reestablece el estilo inicial (inactivo) de los demás items */
            $('.mobile-accordion-item').on('click', function(){
                var clickedAccordionItem = $(this);
                var ariaExpanded = clickedAccordionItem.attr('aria-expanded');

                if (ariaExpanded == 'false') {
                    initialStyle();
                    $(this).addClass('h-active');
                    $(this).find('.fa').removeClass('fa-plus').addClass('fa-minus');
                } else {
                    initialStyle();
                }
            });
        });
    </script>
@endsection
