@extends('layouts.Layout')
{{-- @section('titulo', 'Emisiones') --}}
@section('styles')
    <link rel="stylesheet" href="{{ asset('css/frontend/general.css') }}" type="text/css">
    <link rel="stylesheet" href="{{ asset('css/frontend/emisiones_style.css') }}" type="text/css">
    <link rel="stylesheet" href="{{ asset('filetree/jQueryFileTree.min.css') }}">
@endsection
@section('contenido')
<style>
    .directory{
        margin: 15px !important;
    }
    UL.jqueryFileTree A{
        font-size: 17px !important;
        font-family: var(--bs-body-font-family);
    }

</style>
    <section class="main-section nav-p-content">
        <div class="row">
            <div class="col">
                <span class="titulo2"> @if(session()->get('selected_lang') != 'EN') {{ $emision->categoria_ES}} @else {{ $emision->categoria_EN}} @endif</span>
                <hr class="linea-amarilla mt-4">
            </div>
        </div>
        <div class="row" style="min-height: 350px">
            <div class="col">
                <div id="filetree">

                </div>
            </div>
        </div>



    </section>
@endsection
@section('css')

@endsection
@section('js')
<script src="https://code.jquery.com/jquery-3.6.0.min.js" integrity="sha256-/xUj+3OJU5yExlq6GSYGSHk7tPXikynS7ogEvDej/m4=" crossorigin="anonymous"></script>
<script src="{{ asset('filetree/jQueryFileTree.min.js') }}"></script>
<script>

    const ROOT_FOLDER = APP_LANG != 'ES' ? 'documentos_emision/EN/{{ $emision->url_EN }}' : 'documentos_emision/ES/{{ $emision->url_ES }}' ;

</script>
<script src="{{ asset('js/filetree.js') }}"></script>
@endsection
