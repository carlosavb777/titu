@extends('layouts.Layout')
@section('titulo')
    <title>{{__('windowtitle.riesgos')}}</title>
@endsection
@section('styles')
<link rel="stylesheet" href="{{ asset('css/frontend/general.css') }}" type="text/css">
<link rel="stylesheet" href="{{ asset('css/frontend/risks.css') }}" type="text/css">
@endsection
@section('contenido')
<section class="banner-container" id="riesgos-banner" style="background-image: url('{{ App\Helpers\ToolKit::Encabezado('IMG_HEADER') }}')">

</section>
<section class="main-section mb-5">
    <div class="row">
        <div class="col titulos">
            <span class="titulo1">{{ App\Helpers\ToolKit::Encabezado('SUBTITULO') }}</span>
            <span class="titulo2">{{ App\Helpers\ToolKit::Encabezado('TITULO') }}</span>
            <hr class="linea-amarilla mt-4">
        </div>
    </div>
    <div class="row mt-5 mb-5">
        <div class="col ">
            <p class="text-justify">
                {{ App\Helpers\ToolKit::Encabezado('DESCRIPCION') }}
            </p>


            @foreach ($Riesgos as $item)
            <ul>
                <p class="text-justify mt-4">
                    <li>{{ session()->get('selected_lang') != 'EN' ? $item->riesgo_ES : $item->riesgo_EN }}</li>
                </p>
            </ul>
            @endforeach

            <a style="text-decoration: none" href="{{ url( App\Helpers\ToolKit::Configuracion('RIESGOS_DOCUMENTO')) }}">  {{ session()->get('selected_lang') != 'EN' ? 'Descargar Gobierno de Riesgos' :  'Download Risk Governance' }}</li></a>
        </div>
    </div>
</section>
@endsection
