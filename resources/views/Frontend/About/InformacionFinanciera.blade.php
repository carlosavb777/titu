@extends('layouts.Layout')
@section('titulo')
    <title>{{__('windowtitle.info-financiera')}}</title>
@endsection
@section('styles')
<link rel="stylesheet" href="{{ asset('css/frontend/general.css') }}" type="text/css">
<link rel="stylesheet" href="{{ asset('css/frontend/finantial_info_style.css') }}" type="text/css">
@endsection
@section('contenido')
    <section class="banner-container" id="informacion-financiera-baner" style="background-image: url('{{ App\Helpers\ToolKit::Encabezado('IMG_HEADER', 'informacion-financiera') }}')">

    </section>
    <section class="collapse-container" id="documentos">
        <div class="row">
            <div class="col titulos">
                <span class="titulo1">{{ App\Helpers\ToolKit::Encabezado('SUBTITULO', 'informacion-financiera') }}</span>
                <span class="titulo2">{{ App\Helpers\ToolKit::Encabezado('TITULO', 'informacion-financiera') }}</span>
                <hr class="linea-amarilla mt-4">
            </div>
        </div>
        <div class="row mb-4">
            <p>
                {{ App\Helpers\ToolKit::Encabezado('DESCRIPCION', 'informacion-financiera') }}
            </p>
        </div>
        <div class="row">
            <div class="col">
                <select class="form-select select-input" id="anios-control" aria-placeholder="Seleccionar año">
                   @for ($anio=2022; $anio<=date('Y'); $anio++)
                    <option {{ $anio == $anioSelec ? 'selected' : '' }} value="{{ $anio }}">{{ $anio }}</option>
                    @endfor
                </select>
            </div>
        </div>

        <div class="row mt-5">
            <div class="col-lg-3 col-md-3 col-sm-12">
                @foreach ($informes as $i => $informe)
                <a class="collapse-link-item {{ $i == 0 ? 'collapse-link-active' : ''}}"
                    data-toggle="collapse"
                    href="#{{ 'informe'.$informe->id }}"
                    data-id="{{ 'informe'.$informe->id }}"
                    role="button"
                    aria-expanded="false"
                    aria-controls="{{ 'informe'.$informe->id }}">
                    {{ $informe->categoria }}
                </a>
                @endforeach
            </div>
            <div class="col">
                @foreach ($informes as $i => $informe)
                <div class="collapse collapse-box" id="{{ 'informe'.$informe->id }}" data-show="{{ $i == 0 ? 'true' : 'false'}}">
                    <strong>{{ $anioSelec }}</strong>
                    <p>
                        {{__('about/InformacionFinanciera.txt1')}}
                    </p>
                    <div class="row">
                        <div class="col">
                            <ul class="two-col-ul doc-ul">
                                @forelse ($informe->documentos->where('anio', (int) $anioSelec ) as $j => $documento)
                                    <li><a href="{{ $documento->url }}" target="_blank" rel="noopener noreferrer">{{ $documento->nombre }}</a></li>
                                @empty
                                    <li>No hay documentos del año seleccionado.</li>
                                @endforelse
                            </ul>
                        </div>
                    </div>
                </div>
                @endforeach
            </div>
        </div>
    </section>
@endsection
<script src="{{ asset('js/informacion-financiera.js')}}" type="text/javascript"></script>
