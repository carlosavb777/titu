@extends('layouts.Layout')
@section('titulo')
    <title>{{__('windowtitle.directiva')}}</title>
@endsection
@section('styles')
<link rel="stylesheet" href="css/frontend/general.css" type="text/css">
<link rel="stylesheet" href="css/frontend/directive_board_style.css" type="text/css">


@endsection
@section('contenido')
<section class="banner-container" style="background-image: url('{{ App\Helpers\ToolKit::Encabezado('IMG_HEADER') }}')">

</section>
<section class="seccion-principal">
    <div class="row">
        <div class="col titulos">
            <span class="titulo1">{{ App\Helpers\ToolKit::Encabezado('SUBTITULO') }}</span><br>
            <span class="titulo2">{{ App\Helpers\ToolKit::Encabezado('TITULO') }}</span>
            <hr class="linea-amarilla mt-4">
        </div>
    </div>
    <div class="row">
        <div class="col parrafo1">
            <p>
                {{ App\Helpers\ToolKit::Encabezado('DESCRIPCION') }}
            </p>
        </div>
    </div>
    {{-- <div class="row">
        <div class="col subtitulo1 mt-5">
            <span>Conoce nuestra junta directiva:</span>
        </div>
    </div>
    <div class="row">
        <div class="col parrafo-junta">
            <p>
                La Junta Directiva de Titularizadora Colombiana S.A. es el órgano encargado de
                proteger y maximizar el patrimonio de la Compañía, dirigir la estrategia corporativa y
                supervisar el adecuado desempeño de los administradores en cumplimiento del
                mandato encomendado por parte de los accionistas.
            </p>
        </div>
    </div> --}}
</section>

<section class="directive-board-container">

    <div class="row mt-2 card-rw">
        <div class="header-directive">
            <span>{{__('about/JuntaDirectiva.txt1')}}</span>
        </div>
    </div>
    <?php $count = 1 ?>
    <div class="row">
        @foreach ($directiva as $item)
        <div class="col-lg-4 col-md-6 col-sm-12">

            <div class="card directive-board-card2">
                <div class="card-body" style="margin-right: 10px;margin-left: 10px;">
                    <div class="foto">
                        <img src="{{ asset($item->foto) }}" class="img-1 mt-1">
                    </div>
                    <span class="directive-name">{{ $item->nombre }}</span>
                    <hr class="directive-name-line">
                    <span class="job">{{ session()->get('selected_lang') == 'ES' ? $item->puesto_laboral_ES : $item->puesto_laboral_EN }}</span>
                    <span class="company">{{ $item->compania }} </span>
                </div>
            </div>

        </div>
        @endforeach
    </div>

</section>

<section class="directive-board-info">


    <div class="row mt-4 mb-2">
        <div class="col">
            <p class="text-justify">
                {{__('about/JuntaDirectiva.txt2')}}
            </p>
        </div>
    </div>
    <div class="row">
        <div class="col-lg-3 col-md-6 col-sm-12 card-top">
            <div class="card documento-card mt-1" data-url="{{ App\Helpers\ToolKit::Configuracion('JUNTA_DIRECTIVA_ADMINISTRADORES') }}">
                <div style="cursor: pointer" class="card-body">
                    <p class="text-center mt-3 documento-texto">{{__('about/JuntaDirectiva.txt3')}}</p>
                </div>
            </div>
        </div>
        <div class="col-lg-3 col-md-6 col-sm-12 card-top">
            <div style="cursor: pointer" class="card documento-card mt-1" data-url="{{ App\Helpers\ToolKit::Configuracion('JUNTA_DIRECTIVA') }}">
                <div class="card-body">
                    <p class="text-center mt-3 documento-texto">{{__('about/JuntaDirectiva.txt4')}}</p>
                </div>
            </div>
        </div>
        <div class="col-lg-3 col-md-6 col-sm-1 card-top">
            <div style="cursor: pointer" class="card documento-card mt-1" data-url="{{ App\Helpers\ToolKit::Configuracion('JUNTA_DIRECTIVA_AUDITOR_INTERNO') }}">
                <div class="card-body">
                    <p class="text-center mt-3 documento-texto">{{__('about/JuntaDirectiva.txt5')}}</p>
                </div>
            </div>
        </div>
    </div>
</section>

@endsection
@section('js')
<script>
    $('.documento-card').on('click', function() {
        window.open($(this).data('url'), '_blank');
    });
</script>

@stop
