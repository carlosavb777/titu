@extends('layouts.Layout')
@section('titulo')
    <title>Titularice</title>
@endsection
@section('carrusel')
<div id="main-carousel" class="carousel slide" data-ride="carousel">
    <div class="carousel-indicators">
        <button type="button" data-target="#main-carousel" data-slide-to="0" class="active" aria-current="true" aria-label="Slide 1"></button>
        <button type="button" data-target="#main-carousel" data-slide-to="1" aria-label="Slide 2"></button>
        <button type="button" data-target="#main-carousel" data-slide-to="2" aria-label="Slide 3"></button>
    </div>
    <div class="carousel-inner">
        <div class="carousel-item active">
            <div class="shape"></div>
            <img  class="d-block w-100 slide-1" alt="...">
            <div class="informacion-carrusel">
                {!! App\Helpers\ToolKit::Configuracion('TITULO_CARRUSEL_1') !!}<br>
                <a href="{{url('contactenos')}}" class="btn contactenos">{{__('home.btncontacto')}}</a>
            </div>
        </div>
        <div class="carousel-item">
            <div class="shape"></div>
            <img  class="d-block w-100 slide-2" alt="...">
            <div class="informacion-carrusel">
                {!! App\Helpers\ToolKit::Configuracion('TITULO_CARRUSEL_2') !!}<br>
                <a href="{{url('contactenos')}}" class="btn contactenos">{{__('home.btncontacto')}}</a>
            </div>
        </div>
        <div class="carousel-item">
            <div class="shape"></div>
            <img  class="d-block w-100 slide-3" alt="...">
            <div class="informacion-carrusel">
                {!! App\Helpers\ToolKit::Configuracion('TITULO_CARRUSEL_3') !!}<br>
                <a href="{{url('contactenos')}}"  class="btn contactenos">{{__('home.btncontacto')}}</a>
            </div>
        </div>
    </div>
    <!-- <img src="img/banner.png" class="carusel-triangulo"> -->
</div>
@endsection


@section('contenido')

<div class="contenido-sobre-nosotros">
    <div class="row" style="--bs-gutter-x: 0;">
        <div class="col-lg-6 col-sm-12 col-md-12 sobre-nosotros">
            <span class="txt-sobre-nosotros">{{__('home.texto1')}}</span><br>
            <span class="titulo-sobre-nosotros">{{__('home.texto2')}}</span>
            <span class="titulo-sobre-nosotros">{{__('home.texto3')}}</span>
            <hr class="linea-amarilla">
            <span class="informacion-sobre-nosotros">{!!__('home.texto4')!!}</span><br><br><br>
            <a href="{{url('nuestra-titularizadora')}}" class="btn contactenos">{{__('home.texto5')}}</a>
        </div>
        <div class="col-lg-6 col-sm-12 col-md-12 tarjetas-sobre-nosotros">
            <div class="row" style="--bs-gutter-x: 0;">
                <div class="col-lg-6 col-sm-12 col-md-12">
                    <div class="card add-card-mt">
                        <div class="card-body">
                            <img src="img/homeicon-originador.png" style="padding: 10px;"><br>
                            <span class="titulo-card">{{__('home.texto6')}}</span>
                            <hr class="linea-azul">
                            <p>{{__('home.texto7')}}</p>
                        </div>
                    </div>
                </div>
                <div class="col-lg-6 col-sm-12 col-md-12">
                    <div class="card">
                        <div class="card-body">
                            <img src="img/inversionista.png" style="padding: 10px;"><br>
                            <span class="titulo-card">{{__('home.texto8')}}</span>
                            <hr class="linea-azul">
                            <p>{{__('home.texto9')}}</p>
                        </div>
                    </div>
                </div><br>
                <div class="col-lg-6 col-sm-12 col-md-12">
                    <div class="card">
                        <div class="card-body">
                            <img src="img/homeicon-titulos.png" style="padding: 10px;"><br>
                            <span class="titulo-card">{{__('home.texto10')}}</span>
                            <hr class="linea-azul">
                            <p>{{__('home.texto11')}}</p>
                        </div>
                    </div>
                </div>
                <div class="col-lg-6 col-sm-12 col-md-12">
                    <div class="card remove-card-mt">
                        <div class="card-body">
                            <img src="img/homeicon-activo.png" style="padding: 10px;"><br>
                            <span class="titulo-card">{{__('home.texto12')}}</span>
                            <hr class="linea-azul">
                            <p>{{__('home.texto13')}}</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>


@endsection

@section('banner')
<div class="banner" style="background-image: url('img/home-banner.png')">
    <div class="informacion-banner">
        <!-- <span>{{__('home.texto14')}}</span><br> -->
        <span style="font-size: 2em;font-weight:bold;">{{__('home.texto15')}}</span><br>
        <hr class="linea-amarilla">
    </div>
</div>

<div class="equipo">
    <div id="equipo-expertos" class="col-lg-6 col-sm-12 col-md-12">
        <span class="txt-sobre-nosotros">{{__('home.texto16')}}</span><br>
        <span class="titulo-equipo">{{__('home.texto17')}}</span>
        <hr class="linea-amarilla">
    </div>
    <div id="carrousel-equipo" class="carousel slide" data-ride="carousel">
        <div class="carousel-inner">

        @foreach ($organizacion as $empleado )


            <div class="carousel-item  @if ($loop->first) active @endif" style="text-align: -webkit-center;">
                <div class="tarjeta-de-equipo">
                    <div class="row">
                        <div class="col-lg-6 col-sm-12 col-md-12">
                            <img src="{{asset($empleado->foto_url)}}" class="gerente-image">
                            <span class="half-moon"></span>
                        </div>
                        <div class="col-lg-6 col-sm-12 col-md-12" style="place-self: center;">
                            <p style="text-align: justify;margin-top: 25px;">{{ session()->get('selected_lang') != 'EN' ? $empleado->descripcion_ES : $empleado->descripcion_EN }}</p>
                            <span class="nombre-integrante">{{$empleado->nombre}}</span><br>
                            <span class="posicion-integrante">{{ session()->get('selected_lang') != 'EN' ? $empleado->puesto_laboral_ES : $empleado->puesto_laboral_EN }}</span>
                        </div>
                    </div>
                </div>
            </div>
            @endforeach

        </div>
        <a class="carousel-control-prev" href="#carrousel-equipo" role="button" data-slide="prev">
            <i class="fa-solid fa-chevron-left" style="color:#14264C"></i>
            <span class="sr-only">{{__('home.texto27')}}</span>
        </a>
        <a class="carousel-control-next" href="#carrousel-equipo" role="button" data-slide="next">
            <i class="fa-solid fa-chevron-right" style="color:#14264C"></i>
            <span class="sr-only">{{__('home.texto28')}}</span>
        </a>
    </div>
</div>
</div>

<div class="banner-contacto" style="background-image: url('img/contacto.png')">
    <div class="tarjeta-contacto">
        <span class="titulo-contacto">{{__('home.texto29')}}</span><br>
        <span class="subtitulo-contacto">{{__('home.texto30')}}</span><br><br>
        <form id="contact-form">

            <div class="container">
                <div class="form-group">
                    <label for="nombre"></label>
                    <input type="text" class="form-control" id="nombre" placeholder="{{__('home.texto32')}}">
                    <p class="msg-error" id="validaNnombre" style="display:none;"></p>
                </div>
                <div class="form-group">
                    <label for="correo"></label>
                    <input type="email" class="form-control" id="correo" placeholder="{{__('home.texto33')}}">
                    <p class="msg-error" id="validaEmail" style="display:none;"></p>
                </div>
                <div class="form-group">
                    <label for="comentario"></label>
                    <textarea class="form-control" id="comentario" rows="3" placeholder="{{__('home.texto34')}}"></textarea>
                </div>
                <div class="form-group">
                    <label for="concepto"></label>
                    <select required name="concepto" id="concepto" class="form-control" style="background-color: #e9e9e9; color: #6c757d;">
                        <option value="" disabled selected >Selecciona</option>
                        <option value="Felicitación">Felicitación</option>
                        <option value="Sugerencias">Sugerencias</option>
                        <option value="Petición">Petición</option>
                        <option value="Queja">Queja</option>
                        <option value="Reclamo">Reclamo</option>
                    </select>
                </div>
                <div style="float: right;margin-top: 4%;">
                    <button type="button" onclick="enviarCorreo()" class="btn contactenos">{{__('home.texto31')}}</button>
                </div>
            </div>
            <div id="alerta"></div>
        </form>
    </div>
</div>

@endsection
@section('js')
<script>
 async   function enviarCorreo() {
    var todo_correcto = true;
    var nombre = await document.getElementById("nombre").value;
    var comentario = await document.getElementById("comentario").value;
    var expresion = /^[a-z][\w.-]+@\w[\w.-]+\.[\w.-]*[a-z][a-z]$/i;
    var email = await document.getElementById("correo").value;
    var concepto = await document.getElementById("concepto").value;



if (nombre == "" || concepto == "" ) {


    todo_correcto = false;
    console.log(todo_correcto);
    document.getElementById("validaNnombre").style.color = 'red';
    document.getElementById("validaNnombre").style.display = 'inline';

    if (nombre == "") {
        document.getElementById("validaNnombre").innerHTML =
        "El campo de nombre es requerido";
    }else{
        document.getElementById("validaNnombre").innerHTML =
        "Seleccione un concepto";
    }
    } else {
        document.getElementById("validaNnombre").innerHTML = "";
        if (email == "") {
            document.getElementById("validaEmail").style.color = 'red';
            document.getElementById("validaEmail").style.display = 'inline';
            document.getElementById("validaEmail").innerHTML =
                "El campo de correo es requerido";
            todo_correcto = false;
            console.log(todo_correcto);
        } else {
            document.getElementById("validaEmail").innerHTML = "";

            if (!expresion.test(email)) {
                document.getElementById("validaEmail").innerHTML =
                    "Ingrese un email válido";
                todo_correcto = false;
                console.log(todo_correcto);
            } else {
                document.getElementById("validaEmail").innerHTML = "";
                console.log(todo_correcto);
                if (comentario == "") {
                    todo_correcto = false;
                    console.log(todo_correcto);
                } else {
                    if (todo_correcto == true) {
                        var formdataEnviarCorreo = new FormData();
                        formdataEnviarCorreo.append("contactenos", this.nombre);
                        formdataEnviarCorreo.append("contactenos", this.email);
                       // console.log(...formdataEnviarCorreo.entries());

                       $.ajax({
                        url:  APP_URL +"/enviar-correo",
                        type: 'POST',
                        data: {
                            _token: '{{ csrf_token() }}',
                            nombre: document.getElementById("nombre").value,
                            correo: document.getElementById("correo").value,
                            mensaje:  document.getElementById("comentario").value,
                            concepto: concepto,

                        },
                        })
                            .done(function(data) {


                            });


                        setTimeout(() => {

                            const elemento = (document.getElementById(
                                "alerta"
                            ).innerHTML =
                                '<br><br><br><div id="msg" class="alert alert-success" role="alert">Correo enviado, pronto nos pondremos en contactó contigo</div>');
                            setTimeout(() => {
                                const msg = document.getElementById("msg");
                                msg.style.display = "none";
                            }, 3000);
                        }, 1000);
                    }
                }
            }
        }
    }
}

</script>
@endsection
