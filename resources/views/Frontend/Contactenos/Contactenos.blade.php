@extends('layouts.Layout')
@section('titulo')
    <title>{{__('windowtitle.contacto')}}</title>
@endsection
@section('styles')
<link rel="stylesheet" href="css/frontend/contact-us.css" type="text/css">
@endsection
@section('contenido')

<section class="banner-container" style="background-image: url('{{ App\Helpers\ToolKit::Encabezado('IMG_HEADER') }}')">
    <h1>{{__('Contactenos/Contactenos.txt1')}}</h1>
    <span>{{__('Contactenos/Contactenos.txt2')}}</span>
</section>

<section class="contact-us">
    <div class="row p-4">
        <div class="col-lg-6 col-md-6 col-sm-12">
            <div class="contact-info-head">
                <span class="txt-sobre-nosotros">{{ App\Helpers\ToolKit::Encabezado('SUBTITULO') }}</span><br>
                <span class="titulo-sobre-nosotros">{{ App\Helpers\ToolKit::Encabezado('TITULO') }}</span>
                <hr class="linea-amarilla mt-4">
            </div>
            <p class="text-justify mt-5 mb-5">
                {{ App\Helpers\ToolKit::Encabezado('DESCRIPCION') }}
            </p>
            <div class="contact-group">
                <h2><i class="fa-solid fa-location-dot"></i> {{__('Contactenos/Contactenos.txt3')}}</h2>
                <span>{{ App\Helpers\ToolKit::Configuracion('DIRECCION') }} </span>
            </div>
            <div class="contact-group">
                <h2><i class="fa-solid fa-envelope"></i> {{__('Contactenos/Contactenos.txt4')}}</h2>
                <span>{{ App\Helpers\ToolKit::Configuracion('CORREO') }}</span>
            </div>
            <div class="contact-group">
                <h2><i class="fa-solid fa-phone"></i> {{__('Contactenos/Contactenos.txt5')}}</h2>
                <span>{{ App\Helpers\ToolKit::Configuracion('TELEFONO1') }}</span>
            </div>
        </div>
        <div class="col-lg-6 col-md-6 col-sm-12">
            <div class="card">
                <div class="card-body p-5">
                    <h2>{{__('Contactenos/Contactenos.txt6')}}</h2>
                    <form >
                        <div class="form-group">
                            <label for="nombre"></label>
                            <input type="text" class="form-control contact-input" id="nombre" onblur="enviarCorreo()" placeholder="{{__('home.texto32')}}">
                            <p class="msg-error" id="validaNnombre"></p>
                        </div>
                        <div class="form-group">
                            <label for="correo"></label>
                            <input type="email" class="form-control contact-input" id="correo" onblur="enviarCorreo()" placeholder="{{__('home.texto33')}}">
                            <p class="msg-error" id="validaEmail"></p>
                        </div>
                        <div class="form-group">
                            <label for="comentario"></label>
                            <textarea class="form-control" rows="3" id="comentario" placeholder="{{__('home.texto34')}}"></textarea>
                        </div>
                        <div class="form-group">
                            <label for="concepto"></label>
                            <select required name="concepto" id="concepto" class="form-control" style="background-color: #e9e9e9; color: #6c757d;">
                                <option value="" disabled selected >Selecciona</option>
                                <option value="Felicitación">Felicitación</option>
                                <option value="Sugerencias">Sugerencias</option>
                                <option value="Petición">Petición</option>
                                <option value="Queja">Queja</option>
                                <option value="Reclamo">Reclamo</option>
                            </select>
                        </div>
                        <div style="float: right;margin-top: 4%;">
                            <button type="button" onclick="enviarCorreo()" class="btn contactenos">{{__('home.texto31')}}</button>
                        </div>
                        <div id="alerta"></div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</section>



@endsection
@section('js')
<script>
 async   function enviarCorreo() {
    var todo_correcto = true;
    var nombre = await document.getElementById("nombre").value;
    var comentario = await document.getElementById("comentario").value;
    var expresion = /^[a-z][\w.-]+@\w[\w.-]+\.[\w.-]*[a-z][a-z]$/i;
    var email = await document.getElementById("correo").value;
    var concepto = await document.getElementById("concepto").value;



    if (nombre == "" || concepto == "" ) {


        todo_correcto = false;
        console.log(todo_correcto);
        document.getElementById("validaNnombre").style.color = 'red';
        document.getElementById("validaNnombre").style.display = 'inline';

        if (nombre == "") {
            document.getElementById("validaNnombre").innerHTML =
            "El campo de nombre es requerido";
        }else{
            document.getElementById("validaNnombre").innerHTML =
            "Seleccione un concepto";
        }


    } else {
        document.getElementById("validaNnombre").innerHTML = "";
        if (email == "") {
            document.getElementById("validaEmail").style.color = 'red';
            document.getElementById("validaEmail").style.display = 'inline';
            document.getElementById("validaEmail").innerHTML =
                "El campo de correo es requerido";
            todo_correcto = false;
            console.log(todo_correcto);
        } else {
            document.getElementById("validaEmail").innerHTML = "";

            if (!expresion.test(email)) {
                document.getElementById("validaEmail").innerHTML =
                    "Ingrese un email válido";
                todo_correcto = false;
                console.log(todo_correcto);
            } else {
                document.getElementById("validaEmail").innerHTML = "";
                console.log(todo_correcto);
                if (comentario == "") {
                    todo_correcto = false;
                    console.log(todo_correcto);
                } else {
                    if (todo_correcto == true) {
                        var formdataEnviarCorreo = new FormData();
                        formdataEnviarCorreo.append("contactenos", this.nombre);
                        formdataEnviarCorreo.append("contactenos", this.email);
                       // console.log(...formdataEnviarCorreo.entries());

                       $.ajax({
                        url:  APP_URL +"/enviar-correo",
                        type: 'POST',
                        data: {
                            _token: '{{ csrf_token() }}',
                            nombre: document.getElementById("nombre").value,
                            correo: document.getElementById("correo").value,
                            mensaje:  document.getElementById("comentario").value,
                            concepto: concepto,

                        },
                        })
                            .done(function(data) {


                            });


                        setTimeout(() => {

                            const elemento = (document.getElementById(
                                "alerta"
                            ).innerHTML =
                                '<br><br><br><div id="msg" class="alert alert-success" role="alert">Correo enviado, pronto nos pondremos en contactó contigo</div>');
                            setTimeout(() => {
                                const msg = document.getElementById("msg");
                                msg.style.display = "none";
                            }, 3000);
                        }, 1000);
                    }
                }
            }
        }
    }
}

</script>
@endsection
