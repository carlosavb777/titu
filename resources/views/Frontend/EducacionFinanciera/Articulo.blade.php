@extends('layouts.Layout')
@section('titulo')
    <title>{{ __('windowtitle.edu-financiera') }}</title>
@endsection
@section('styles')
<link rel="stylesheet" href="{{asset('css/frontend/general.css')}}" type="text/css">
<link rel="stylesheet" href="{{asset('css/frontend/educacion_financiera_style.css')}}" type="text/css">
    <style>
        .banner-container-tituralize {
            width: 100% !important;
            height: 400px !important;
            background-position: bottom !important;
            background-repeat: no-repeat !important;
            background-size: cover !important;
        }
    </style>
@section('contenido')
    <!-- @include('layouts.Construccion') -->

    <section class="banner-container-tituralize" style='background: url("{{ asset($blog->imagen) }}");'>


    </section>


    <section class="seccion-principal">
        <div class="row">
            <div class="col titulos">
                <span class="titulo1">{{ $titulo }}</span><br>


                <span class="titulo2">{{ session()->get('selected_lang') != 'EN' ? $blog->titulo_ES : $blog->titulo_EN}}</span>

                <hr class="linea-amarilla mt-4">
            </div>
        </div>
        <div class="row">

            <div class="col parrafo1">
                <p>
                    {{ session()->get('selected_lang') != 'EN' ? $blog->descripcion_corta_ES: $blog->descripcion_corta_EN  }}
                </p>
            </div>


        </div>
    </section>
    <section class="edufinanciera-board-cards">
        <div class="row">
            <div class="col-12">
                {!! session()->get('selected_lang') != 'EN' ? $blog->contenido_ES : $blog->contenido_EN  !!}

            </div>
        </div>
    </section>

@endsection
