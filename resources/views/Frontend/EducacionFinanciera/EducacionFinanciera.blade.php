@extends('layouts.Layout')
@section('titulo')
    <title>{{ __('windowtitle.edu-financiera') }}</title>
@endsection
@section('styles')
    <link rel="stylesheet" href="{{asset('css/frontend/general.css')}}" type="text/css">
    <link rel="stylesheet" href="{{asset('css/frontend/educacion_financiera_style.css')}}" type="text/css">
@endsection

@section('contenido')

    <section class="banner-container" style="background-image: url('{{ App\Helpers\ToolKit::Encabezado('IMG_HEADER') }}')">

    </section>
    <section class="seccion-principal-edufinanciera">
        <div class="row">
            <div class="col titulos">
                <span class="titulo1">{{ App\Helpers\ToolKit::Encabezado('SUBTITULO') }}</span><br>
                <span class="titulo2">{{ App\Helpers\ToolKit::Encabezado('TITULO') }}</span>
                <hr class="linea-amarilla mt-4">
            </div>
        </div>
        <div class="row">
            <div class="col parrafo1">
                <p>
                    {{ App\Helpers\ToolKit::Encabezado('DESCRIPCION') }}
                </p>
            </div>
        </div>
    </section>
    <section class="edufinanciera-board-container">

        <div class="header-edufinanciera">
            <div class="row">


                @foreach ($blogs as $blog)
                    <div class="col-lg-6 col-sm-12 col-md-12">
                        <div class="card card-edu">
                            <img class="img-card1" style='background: url("{{$blog->imagen}}");'>
                            <div class="card-body card-body-1">
                                <!-- <p class="card-text">Some quick example text to build on the card title and make up the bulk of the card's content.</p> -->
                                <p class="margen-titulo">{{ session()->get('selected_lang') != 'EN' ? $blog->titulo_ES : $blog->titulo_EN}}</p>
                                <p class="margen-subtitulo">
                                    <span> {{ session()->get('selected_lang') != 'EN' ? $blog->descripcion_corta_ES: $blog->descripcion_corta_EN  }} </span>
                                </p>
                            </div>
                            <div class="card-footer">
                                <a href="{{ url('/articulo/'.$blog->id) }}"
                                    class="card-link">{{ __('eduFinanciera/eduFinanciera.txt1') }}</a>
                            </div>
                        </div>
                    </div>
                @endforeach
            </div>




            {{-- <div class="card card-edu">
                <img class="img-card1">
                <div class="card-body card-body-1">
                    <!-- <p class="card-text">Some quick example text to build on the card title and make up the bulk of the card's content.</p> -->
                    <p class="margen-titulo">¿Qué es titularización?</p>
                    <p class="margen-subtitulo">
                        <span> Es el agrupamiento o empaquetamiento de activos financieros para ser transformados en títulos
                            valores y vendidos o adquiridos por inversionistas. </span>
                    </p>
                </div>
                <div class="card-footer">
                    <a href="{{ url('que_es') }}" class="card-link">{{ __('eduFinanciera/eduFinanciera.txt1') }}</a>
                </div>
            </div>

            <div class="card card-edu">
                <img class="img-card2">
                <div class="card-body card-body-1">
                    <p class="margen-titulo">
                        Ventajas de la titularización
                    </p>
                    <p class="margen-subtitulo">
                        <span>Cursus sit amet dictum sit amet justo. Neque convallis a cras semper auctor.</span>
                    </p>
                </div>
                <div class="card-footer">
                    <a href="{{ url('ventaja') }}" class="card-link">{{ __('eduFinanciera/eduFinanciera.txt1') }}</a>
                </div>
            </div> --}}
        </div>
    </section>

@endsection
