@extends('layouts.Layout')
@section('titulo')
    <title>{{__('windowtitle.hechos')}}</title>
@endsection
@section('styles')
<link rel="stylesheet" href="css/frontend/general.css" type="text/css">
<link rel="stylesheet" href="css/frontend/hechos_relevantes_info_styl.css" type="text/css">

@endsection
@section('contenido')
<section class="banner-container">

</section>

<section class="seccion-principal-infohechos main-web">
    <div class="row">
        <div class="col titulos">
            <span class="titulo1">Información y hechos relevantes</span><br>
            <span class="titulo2">Lorem ipsum dolor sit amet</span>
            <hr class="linea-amarilla mt-4">
        </div>
    </div>
    <div class="row alinear">
        <div class="col">
            <ul class="nav nav-tabs card-header-tabs titu-tabs">
                <li class="nav-item">
                    <a class="nav-link active-tab-link" href="#item01" data-toggle="tab" data-id="item01" role="button" aria-expanded="false" aria-controls="item01">Flujos futuros</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="#item02" data-toggle="tab" data-id="item02" role="button" aria-expanded="false" aria-controls="item02">Cartera de libranza</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="#item03" data-toggle="tab" data-id="item03" role="button" aria-expanded="false" aria-controls="item03">Cartera de consumo y comercial</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="#item04" data-toggle="tab" data-id="item04" role="button" aria-expanded="false" aria-controls="item04">Cartera de vehículos</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="#item05" data-toggle="tab" data-id="item05" role="button" aria-expanded="false" aria-controls="item05">Emisiones canceladas</a>
                </li>
            </ul>
        </div>
    </div>
</section>
<section class="infohechos-board-container main-web">
    <div class="container">
        <form class="d-flex margen-buscador">
            <span class="input-group-text icono-buscador-infohechos"><i class="fa-solid fa-magnifying-glass"></i></span>
            <input class="form-control me-2 buscador-infohechos" type="text" placeholder="Buscar..." aria-label="Buscador">
        </form><br>
        <div class="tab-content">
            <div class="tab-pane active" id="item01">
                <div class="row">
                    <div class="card-infohechos-2 mb-3">
                        <div class="card-body">
                            <p class="card-categoria">Categoria</p>
                            <p class="card-fecha"><small class="text-muted">12 de abril 2021</small></p>
                            <h5 class="card-titulo">Nombre de información</h5>
                            <p>Interdum varius sit amet mattis vulputate enim. Sit amet consectetur adipiscing elit pellentesque habitant morbi tristique senectus. Ac tortor dignissim convallis aenean et tortor at risus.</p>
                        </div>
                    </div>
                </div>
            </div>
            <div class="tab-pane" id="item02">
                <div class="row">
                    <div class="card-infohechos-2 mb-3">
                        <div class="card-body">
                            <p class="card-categoria">Categoria</p>
                            <p class="card-fecha"><small class="text-muted">13 de abril 2021</small></p>
                            <h5 class="card-titulo">Nombre de información</h5>
                            <p>Interdum varius sit amet mattis vulputate enim. Sit amet consectetur adipiscing elit pellentesque habitant morbi tristique senectus. Ac tortor dignissim convallis aenean et tortor at risus.</p>
                        </div>
                    </div>
                </div>
            </div>
            <div class="tab-pane " id="item03">
                <div class="row">
                    <div class="card-infohechos-2 mb-3">
                        <div class="card-body">
                            <p class="card-categoria">Categoria</p>
                            <p class="card-fecha"><small class="text-muted">14 de abril 2021</small></p>
                            <h5 class="card-titulo">Nombre de información</h5>
                            <p>Interdum varius sit amet mattis vulputate enim. Sit amet consectetur adipiscing elit pellentesque habitant morbi tristique senectus. Ac tortor dignissim convallis aenean et tortor at risus.</p>
                        </div>
                    </div>
                </div>
            </div>
            <div class="tab-pane " id="item04">
                <div class="row">
                    <div class="card-infohechos-2 mb-3">
                        <div class="card-body">
                            <p class="card-categoria">Categoria</p>
                            <p class="card-fecha"><small class="text-muted">15 de abril 2021</small></p>
                            <h5 class="card-titulo">Nombre de información</h5>
                            <p>Interdum varius sit amet mattis vulputate enim. Sit amet consectetur adipiscing elit pellentesque habitant morbi tristique senectus. Ac tortor dignissim convallis aenean et tortor at risus.</p>
                        </div>
                    </div>
                </div>
            </div>
            <div class="tab-pane " id="item05">
                <div class="row">
                    <div class="card-infohechos-2 mb-3">
                        <div class="card-body">
                            <p class="card-categoria">Categoria</p>
                            <p class="card-fecha"><small class="text-muted">16 de abril 2021</small></p>
                            <h5 class="card-titulo">Nombre de información</h5>
                            <p>Interdum varius sit amet mattis vulputate enim. Sit amet consectetur adipiscing elit pellentesque habitant morbi tristique senectus. Ac tortor dignissim convallis aenean et tortor at risus.</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
</section>

<section class="main-section mobile-view">
    <div class="row ">
        <div class="col titulos">
            <span class="titulo1">Circulares</span>
            <span class="titulo2">Lorem ipsum dolor sit amet</span>
            <hr class="linea-amarilla mt-4">
        </div>
    </div>
    <div class="row">
        <div id="accordion">
            <div class="card simple-grey-card-accordion">
                <div class="card-header" id="headingOne">
                    <h5 class="mb-0">
                        <button class="btn btn-link" data-toggle="collapse" data-target="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
                            Flujos futuros
                        </button>
                        <i class="fa fa-plus"></i>
                    </h5>
                </div>
                <div id="collapseOne" class="collapse" aria-labelledby="headingOne" data-parent="#accordion">
                    <div class="row mt-3">
                        <div class="card-infohechos-2 mb-3">
                            <div class="card-body">
                                <p class="card-categoria">Categoria</p>
                                <p class="card-fecha"><small class="text-muted">12 de abril 2021</small></p>
                                <h5 class="card-titulo">Nombre de información</h5>
                                <p>Interdum varius sit amet mattis vulputate enim. Sit amet consectetur adipiscing elit pellentesque habitant morbi tristique senectus. Ac tortor dignissim convallis aenean et tortor at risus.</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="card simple-grey-card-accordion">
                <div class="card-header" id="headingOne">
                    <h5 class="mb-0">
                        <button class="btn btn-link" data-toggle="collapse" data-target="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo">
                            Cartera de libranza
                        </button>
                        <i class="fa fa-plus"></i>
                    </h5>
                </div>
                <div id="collapseTwo" class="collapse" aria-labelledby="headingOne" data-parent="#accordion">
                    <div class="row mt-3">
                        <div class="card-infohechos-2 mb-3">
                            <div class="card-body">
                                <p class="card-categoria">Categoria</p>
                                <p class="card-fecha"><small class="text-muted">13 de abril 2021</small></p>
                                <h5 class="card-titulo">Nombre de información</h5>
                                <p>Interdum varius sit amet mattis vulputate enim. Sit amet consectetur adipiscing elit pellentesque habitant morbi tristique senectus. Ac tortor dignissim convallis aenean et tortor at risus.</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="card simple-grey-card-accordion">
                <div class="card-header" id="headingOne">
                    <h5 class="mb-0">
                        <button class="btn btn-link" data-toggle="collapse" data-target="#collapseThree" aria-expanded="false" aria-controls="collapseThree">
                            Cartera de consumo y comercial
                        </button>
                        <i class="fa fa-plus"></i>
                    </h5>
                </div>
                <div id="collapseThree" class="collapse" aria-labelledby="headingOne" data-parent="#accordion">
                    <div class="row mt-3">
                        <div class="card-infohechos-2 mb-3">
                            <div class="card-body">
                                <p class="card-categoria">Categoria</p>
                                <p class="card-fecha"><small class="text-muted">14 de abril 2021</small></p>
                                <h5 class="card-titulo">Nombre de información</h5>
                                <p>Interdum varius sit amet mattis vulputate enim. Sit amet consectetur adipiscing elit pellentesque habitant morbi tristique senectus. Ac tortor dignissim convallis aenean et tortor at risus.</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="card simple-grey-card-accordion">
                <div class="card-header" id="headingFour">
                    <h5 class="mb-0">
                        <button class="btn btn-link" data-toggle="collapse" data-target="#collapseFour" aria-expanded="false" aria-controls="collapseFour">
                            Cartera de vehículos
                        </button>
                        <i class="fa fa-plus"></i>
                    </h5>
                </div>
                <div id="collapseFour" class="collapse" aria-labelledby="headingFour" data-parent="#accordion">
                    <div class="row mt-3">
                        <div class="card-infohechos-2 mb-3">
                            <div class="card-body">
                                <p class="card-categoria">Categoria</p>
                                <p class="card-fecha"><small class="text-muted">15 de abril 2021</small></p>
                                <h5 class="card-titulo">Nombre de información</h5>
                                <p>Interdum varius sit amet mattis vulputate enim. Sit amet consectetur adipiscing elit pellentesque habitant morbi tristique senectus. Ac tortor dignissim convallis aenean et tortor at risus.</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="card simple-grey-card-accordion">
                <div class="card-header h-active" id="headingFive">
                    <h5 class="mb-0">
                        <button class="btn btn-link" data-toggle="collapse" data-target="#collapseFive" aria-expanded="false" aria-controls="collapseFive">
                            Emisiones canceladas
                        </button>
                        <i class="fa fa-plus"></i>
                    </h5>
                </div>
                <div id="collapseFive" class="collapse" aria-labelledby="headingFive" data-parent="#accordion">
                    <div class="row mt-3">
                        <div class="card-infohechos-2 mb-3">
                            <div class="card-body">
                                <p class="card-categoria">Categoria</p>
                                <p class="card-fecha"><small class="text-muted">16 de abril 2021</small></p>
                                <h5 class="card-titulo">Nombre de información</h5>
                                <p>Interdum varius sit amet mattis vulputate enim. Sit amet consectetur adipiscing elit pellentesque habitant morbi tristique senectus. Ac tortor dignissim convallis aenean et tortor at risus.</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

        </div>
    </div>
</section>

@endsection