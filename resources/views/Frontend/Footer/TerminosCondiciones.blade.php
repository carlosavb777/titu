@extends('layouts.Layout')
@section('titulo')
    <title>{{__('windowtitle.tyc')}}</title>
@endsection
@section('styles')
<link rel="stylesheet" href="css/frontend/general.css" type="text/css">
<link rel="stylesheet" href="css/frontend/shareholder_rel_style.css" type="text/css">
@endsection

@section('contenido')
<section class="seccion-principal nav-p-content">
    <div class="row">
        <div class="col">
            <h1 class="titulo2">{{__('footer/TerminosCondiciones.txt1')}}</h1>
            <hr class="linea-amarilla">
        </div>
    </div>
    {!! App\Helpers\ToolKit::Configuracion('TERMINOS_CONDICIONES') !!}
</section>
@endsection

