@extends('layouts.Layout')
@section('titulo')
    <title>{{__('windowtitle.enlaces')}}</title>
@endsection
@section('styles')
<link rel="stylesheet" href="css/frontend/general.css" type="text/css">
<link rel="stylesheet" href="css/frontend/enlace_interes_styl.css" type="text/css">

@endsection
@section('contenido')
<!-- @include('layouts.Construccion') -->
<br><br>
<section class="seccion-principal-enlaces">
    <div class="row">
        <div class="col-lg-6 col-sm-12 col-md-12 about-titularice">
            <span class="titulo-sobre-nosotros">{{ App\Helpers\ToolKit::Encabezado('SUBTITULO') }}</span>
            <hr class="linea-amarilla">
        </div>
    </div>
    <div class="row">
        <div class="col parrafo1">
            <p>
                {{ App\Helpers\ToolKit::Encabezado('DESCRIPCION') }}
            </p>
        </div>
    </div>
    <div class="row">
        <h5 class="titulo-section">{{__('footer/EnlaceInteres.txt1')}}</h5>
    </div>

    <div class="logo">
        <ul class="logo-set">

                    @foreach ($enlaces as $enlace)
                    <li ><a class="logoi" target="_blank" style="background-image: url('{{ asset($enlace->foto_url) }}');" href="{{$enlace->puesto_laboral_ES}}">
                        <i class="fa fa-img1 fa-fw"></i></a></li>
                    @endforeach
        </ul>
    </div>


</section>
@endsection
