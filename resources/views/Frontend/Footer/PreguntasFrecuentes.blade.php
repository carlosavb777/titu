@extends('layouts.Layout')
@section('titulo')
    <title>{{__('windowtitle.faq')}}</title>
@endsection
@section('styles')
<link rel="stylesheet" href="css/frontend/general.css" type="text/css">
<link rel="stylesheet" href="css/frontend/frec_questions_style.css" type="text/css">
@endsection
@section('contenido')
<section class="questions-container">
    <div class="row">
        <div class="col shareholders">
            <span class="titulo-sobre-nosotros">{{ App\Helpers\ToolKit::Encabezado('SUBTITULO') }}</span>
            <hr class="linea-amarilla mt-4">
        </div>
    </div>
    <div class="row">
        <p>
            {{ App\Helpers\ToolKit::Encabezado('DESCRIPCION') }}
        </p>
    </div>
    <div class="row mt-5">
        <div class="col-lg-3 col-md-3 col-sm-12" id="preguntas-container">
            <a class="collapse-link-item collapse-link-active" data-toggle="collapse" href="#question01" data-id="question01" role="button" aria-expanded="false" aria-controls="question01">
                ¿Qué es la titularización?
            </a>
            <a class="collapse-link-item" data-toggle="collapse" href="#question02" data-id="question02" role="button" aria-expanded="false" aria-controls="question02">
                ¿Qué es la titularización de cartera hipotecaria?
            </a>
            <a class="collapse-link-item" data-toggle="collapse" href="#question03" data-id="question03" role="button" aria-expanded="false" aria-controls="question03">
                ¿Cuáles son las partes intervinientes en un proceso de titularización hipotecaria y no hipotecaria?
            </a>
            <a class="collapse-link-item" data-toggle="collapse" href="#question04" data-id="question04" role="button" aria-expanded="false" aria-controls="question04">
                ¿Cuáles son los beneficios de la titularización hipotecaria?
            </a>
            <a class="collapse-link-item" data-toggle="collapse" href="#question05" data-id="question05" role="button" aria-expanded="false" aria-controls="question05">
                ¿Cuáles son los beneficios de la titularización hipotecaria y no hipotecaria?
            </a>
            <a class="collapse-link-item" data-toggle="collapse" href="#question06" data-id="question06" role="button" aria-expanded="false" aria-controls="question06">
                ¿Cuáles son los beneficios de la titularización hipotecaria, si usted es un inversionista?
            </a>
            <a class="collapse-link-item" data-toggle="collapse" href="#question07" data-id="question07" role="button" aria-expanded="false" aria-controls="question07">
                ¿Qué son los bonos hipotecarios?
            </a>
            <a class="collapse-link-item" data-toggle="collapse" href="#question08" data-id="question08" role="button" aria-expanded="false" aria-controls="question08">
                ¿Qué son los títulos hipotecarios?
            </a>
        </div>
        <div class="col" id="respuestas-container">
            <div class="collapse collapse-box" id="question01" data-show="true">
                <strong>¿Qué es la titularización?</strong>
                <p>
                    De acuerdo con la definición de la Real Academia de la Lengua Española, la Titularización es "Convertir determinados activos, generalmente préstamos, en valores negociables en el mercado". James Rosenthal (1988) la define como " el proceso cuidadosamente estructurado a través del cual créditos y otros activos son empaquetados, suscritos y vendidos en forma de títulos valores a Inversionistas". Para conocer más sobre la Titularización, lo invitamos a visitar nuestra sección La Titularización.
                </p>
            </div>
            <div class="collapse collapse-box" id="question02" data-show="false">
                <strong>¿Qué es la titularización de cartera hipotecaria?</strong>
                <p>
                    La titularización hipotecaria es un mecanismo previsto en la Ley 546 de 1999 (Ley de Vivienda), mediante el cual los establecimientos de crédito, las sociedades titularizadoras y otras entidades autorizadas, emiten títulos representativos de créditos otorgados para financiar la construcción y la adquisición de vivienda.
                </p>
            </div>
            <div class="collapse collapse-box" id="question03" data-show="false">
                <strong>¿Cuáles son las partes intervinientes en un proceso de titularización hipotecaria y no hipotecaria?</strong>
                <p>
                    En el proceso de titularización hipotecaria y no hipotecaria intervienen el Originador (entidades que originan los créditos a los deudores), los Administradores Autorizados (entidades autorizadas para administrar el recaudo de los créditos), el Administrador Maestro (entidad que vigila el recaudo por parte de los Administradores Autorizados y realiza la liquidación y pago de los títulos), el Emisor (quien emite los títulos y en el caso de los TIP´s, TIS, TER, TIL y TIV es la Titularizadora Colombiana), Inversionistas (personas naturales o jurídicas) suscriptores o adquirientes de los títulos.
                </p>
            </div>
            <div class="collapse collapse-box" id="question04" data-show="false">
                <strong>¿Cuáles son los beneficios de la titularización hipotecaria?</strong>
                <p>
                    Para todos como colombianos este mecanismo también ofrece beneficios que han sido identificados en países que han utilizado la titularización como una herramienta de crecimiento económico: 1. Desarrollo general del sector inmobiliario, dado el potencial de oferta de créditos que genera en la medida en que se otorgue mayor liquidez a los bancos para que estos a su vez la traduzcan en nuevos créditos de vivienda 2. Aumento de la actividad edificadora 3. Incremento del empleo 4. Crecimiento de la economía 5. Adquisición de vivienda propia por millones de colombianos con claras facilidades, lo que ofrecerá bienestar y mejor calidad de vida 6. Mayor desarrollo y crecimiento del mercado de capitales 7. La adquisición de vivienda por parte de las familias se financiará indirectamente con los recursos que ellas mismas tienen ahorrados en diversas entidades, tales como los fondos de pensiones.
                </p>
            </div>
            <div class="collapse collapse-box" id="question05" data-show="false">
                <strong>¿Cuáles son los beneficios de la titularización hipotecaria y no hipotecaria?</strong>
                <p>
                    1. En la medida en que la Titularizadora compre más cartera y se emitan títulos, se dará mayor liquidez a las entidades para que estas a su vez ofrezcan mayores alternativas de crédito. 2. Con ello se logrará mayor eficiencia en la originación del crédito 3. Se estandarizará el proceso de otorgamiento de crédito, sus tasas a nivel nacional y la simplificación de trámites. 4. Al tener actualmente un crédito con una entidad vendedora de cartera, el deudor mantendrá las mismas condiciones actuales que tiene contratadas con la entidad.
                </p>
            </div>
            <div class="collapse collapse-box" id="question06" data-show="false">
                <strong>¿Cuáles son los beneficios de la titularización hipotecaria, si usted es un inversionista?</strong>
                <p>
                    Si usted es un potencial inversionista y desea adquirir títulos valores respaldados por créditos hipotecarios y no hipotecarios obtendrá: 1. Títulos hipotecarios y no hipotecarios con altas acalificaciones de riesgo. 2. Rendimientos financieros de acuerdo a las condiciones de emisión de los títulos 3.Liquidez a través del mercado secundario, dado que los títulos están inscritos en la Bolsa de Valores 4. Protección frente al riesgo de crédito de los créditos titularizados a través de la diversificación y de los mecanismos de cobertura. Para mayor detalle consulte nuestros prospectos en la sección Emisiones.
                </p>
            </div>
            <div class="collapse collapse-box" id="question07" data-show="false">
                <strong>¿Qué son los bonos hipotecarios?</strong>
                <p>
                    Los bonos hipotecarios, son títulos valores de contenido crediticio, emitidos directamente por los establecimientos de crédito, y los cuales tienen como finalidad exclusiva cumplir los contratos de crédito para la construcción de vivienda y para su financiación a largo plazo. Estos títulos valores se encuentran respaldadosen primer lugar por el balance del establecimiento de crédito y, en segundo lugar, por un activo que son los créditos hipotecarios, propiedad del establecimiento de crédito acreedor y a su vez emisor de los bonos hipotecarios.
                </p>
            </div>
            <div class="collapse collapse-box" id="question08" data-show="false">
                <strong>¿Qué son los títulos hipotecarios?</strong>
                <p>
                    Los títulos hipotecarios son títulos valores que pueden ser de participación y/o de contenido crediticio, emitidos en desarrollo de procesos de titularización que pueden llevar a cabo las sociedades titularizadora, los establecimientos de crédito y demás entidades de que trata el art. 1 de la Ley 546 de 1999 y que están respaldado con cartera hipotecaria.
                </p>
            </div>
        </div>
    </div>
</section>
@endsection
<script src="{{ asset('js/freq-questions.js')}}" type="text/javascript"></script>
