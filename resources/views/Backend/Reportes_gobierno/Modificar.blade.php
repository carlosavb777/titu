@extends('adminlte::page')

@section('title', 'Dashboard')

@section('content_header')
    <h1>Modificar reporte</h1>
@stop

@section('content')
    <div class="card card-primary">
        <div class="card-header">
            <h3 class="card-title">Edite los campos.</h3>
        </div>
        <!-- /.card-header   -->
        <div class="card-body">
            <form action="{{ url('informes_gobierno') }}" method="POST" enctype="multipart/form-data">
                @csrf
                <input type="hidden" name="id" value="{{$documento->id}}" >
                <div class="row">
                    <div class="col-2">
                        <div class="form-group">
                            <label>Año</label>
                            <input type="text" name="anio" onlyread disabled class="form-control" value="{{$documento->anio}}">
                        </div>
                    </div>
                    <div class="col-sm-5">
                        <div class="form-group">
                            <label>Adjuntar documento (ES)</label>  <a href="{{asset($documento->url_ES)}}" target="_blank" rel="noopener noreferrer">CLCIK AQUÍ PARA VER EL DOCUMENTO ACTUAL</a>

                            <div class="input-group">
                                <div class="custom-file">
                                    <input type="file" name="archivo_ES" class="custom-file-input" id="exampleInputFile">
                                    <label class="custom-file-label" for="exampleInputFile"></label>
                                </div>
                                <div class="input-group-append">
                                    <span class="input-group-text">Subir</span>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="col-sm-5">
                        <div class="form-group">
                            <label>Adjuntar documento (EN)</label> <a href="{{asset($documento->url_EN)}}" target="_blank" rel="noopener noreferrer">CLCIK AQUÍ PARA VER EL DOCUMENTO EN</a>
                            <div class="input-group">
                                <div class="custom-file">
                                    <input type="file" name="archivo_EN" class="custom-file-input" id="exampleInputFile2">
                                    <label class="custom-file-label" for="exampleInputFile2"></label>
                                </div>
                                <div class="input-group-append">
                                    <span class="input-group-text">Subir</span>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="card-footer">
                    <button type="submit" class="btn btn-lg btn-primary right guardando">Guardar</button>
                    <a href="{{ url('informes_gobierno') }}" class="btn btn-lg btn-default left">Salir</a>
                </div>
            </form>
            <!-- /.card-body -->
        </div>
    @stop

    @section('css')

    @stop

    @section('js')
        <script src="{{ asset('js/input-file-custom.js') }}"></script>
        <script>
            $(function() {
                bsCustomFileInput.init();
            });
        </script>
    @stop
