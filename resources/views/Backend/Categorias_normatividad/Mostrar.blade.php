@extends('adminlte::page')

@section('title', 'Dashboard')

@section('content_header')
    <h1>Categoría</h1>
@stop

@section('content')
<div class="card card-success">
    <div class="card-header">
      <h3 class="card-title">Detalles de la categoría</h3>
    </div>
    <!-- /.card-header -->
    <div class="card-body">

        <div class="row">
              <div class="col-sm-6">
                  <div class="form-group">
                    <label>Categoría (ES)</label>
                    <p>{{$categoria->categoria_ES}}</p>
                  </div>
                </div>
              <div class="col-sm-6">
                  <div class="form-group">
                      <label>Categoría (EN)</label>
                      <p>{{$categoria->categoria_EN}}</p>
                    </div>
                </div>
              </div>


    </div>
		<div class="card-footer card-footer-mostrar ">

      <a  href="{{url('normatividad_categorias/create')}}" class="btn btn-lg btn-primary right">Nueva categoría</a>
      <a style="margin-right: 5px;" href="{{url('normatividad_categorias')}}/{{$categoria->id}}/edit" class="btn btn-lg btn-default right">Modificar</a>
      <button style="margin-right: 5px;" type="button" id="eliminar" data-id="{{$categoria->id}}" class="btn btn-lg btn-danger right">Eliminar</button>
      <a  href="{{url('normatividad_categorias')}}" class="btn btn-lg btn-default left">Salir</a>
		</div>

    <!-- /.card-body -->
  </div>
@stop

@section('css')

@stop

@section('js')

    <script>


      $( "#eliminar" ).on( "click", function() {
       var id = $(this).data("id");
        alerta_eliminar("eliminar_normatividad_categorias/"+id, titulo = "Eliminar categoría", texto="¿Realmente quieres eliminar esta categoría?");
      });

      @if(Session::has('nuevo'))
					Swal.fire(
							'¡Creada!',
						'La categoría fue creada con éxito.',
						'success'
					)
			@endif

      @if(Session::has('modificado'))
					Swal.fire(
							'¡Modificada!',
						'La categoría fue modificada con éxito.',
						'success'
					)
			@endif

    </script>
@stop
