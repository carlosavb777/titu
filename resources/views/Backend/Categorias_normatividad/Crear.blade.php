@extends('adminlte::page')

@section('title', 'Dashboard')

@section('content_header')
    <h1>Nueva categoría</h1>
@stop

@section('content')
    <div class="card card-primary">
        <div class="card-header">
            <h3 class="card-title">Complete los campos.</h3>
        </div>
        <!-- /.card-header -->
        <div class="card-body">
            <form action="{{ url('normatividad_categorias') }}" method="POST">
                @csrf
                <div class="row">

                    <div class="col-sm-6">
                        <div class="form-group">
                            <label>Categoría (ES)</label>
                            <input type="text" name="categoria_ES" maxlength="512" required class="form-control"
                                placeholder="Ingrese una categoría">
                        </div>
                    </div>

                    <div class="col-sm-6">
                        <div class="form-group">
                            <label>Categoría (EN)</label>
                            <input type="text" name="categoria_EN" maxlength="512" required class="form-control"
                                placeholder="Ingrese una categoría">
                        </div>
                    </div>
                </div>

                <div class="card-footer">
                    <button type="submit" class="btn btn-lg btn-primary right guardando">Guardar</button>
                    <a href="{{ url('normatividad_categorias') }}" class="btn btn-lg btn-default left">Salir</a>
                </div>
            </form>
            <!-- /.card-body -->
        </div>
    @stop

    @section('css')

    @stop

    @section('js')
        <script></script>
    @stop
