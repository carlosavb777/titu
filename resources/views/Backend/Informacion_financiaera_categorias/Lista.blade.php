@extends('adminlte::page')

@section('title', 'Dashboard')

@section('content_header')
    <h1>Categorías</h1>
@stop

@section('content')
<div class="row">
	<div class="col-sm-12" >

			<div class="card">
					<div class="card-header">
						<h3 class="card-title">Selecciona un categoria de la lista para ver su detalle. </h3>
						<a href="{{url("informes_financieros_categorias/create")}}" class="btn btn-block btn-primary btn-sm btn-nuevo">Nueva categoría</a>
					</div>
					<!-- /.card-header -->
					<div class="card-body">
							<table id="tabla-categorias" class="table table-bordered table-striped dataTable dtr-inline" role="grid" aria-describedby="example1_info">
							<thead>
								<th>Categoría</th>
								<th>Acciones</th>
							</thead>
							<tbody>
								@foreach ($categorias as $categoria)
								<tr>
                                    <td>{{ $categoria->categoria_ES }}</td>
									<td style="float:right">
										<a href="{{url("informes_financieros_categorias")}}/{{$categoria->id}}" ><i class="iconos-lista fas fa-eye"></i></a>
										<a href="{{url("informes_financieros_categorias")}}/{{$categoria->id}}/edit" ><i class="iconos-lista fas fa-edit"></i></a>
										<a href="#" class="btn-eliminar" data-id="{{$categoria->id}}" ><i class="iconos-lista fas fa-trash"></i></a>  </td>
								</tr>

								@endforeach

							</tfoot>
						</table>
					</div>
					<!-- /.card-body -->
			</div>
	</div>
</div>

@stop

@section('css')

@stop
@section('plugins.Datatables', true)
@section('js')
    <script>
			$(document).ready( function () {
					$('#tabla-categorias').DataTable({
						language: {
									url: 'https://cdn.datatables.net/plug-ins/1.10.22/i18n/Spanish.json'
							},
							responsive: true
					});

					$( ".btn-eliminar" ).on( "click", function() {
						var id = $(this).data("id");
        		    alerta_eliminar("eliminar_informes_financieros_categorias/"+id, titulo = "Eliminar categoría", texto="¿Realmente quieres eliminar esta categoría?");

					});

					@if(Session::has('eliminado'))
					Swal.fire(
							'¡Eliminada!',
						'El categoría fue eliminada.',
						'success'
					)
					@endif

			});

	</script>
@stop
