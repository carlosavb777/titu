@extends('adminlte::page')

@section('title', 'Dashboard')

@section('content_header')

@if($permiso)
<div class="row">
    <div class="col-lg-3 col-6">
      <!-- small box -->
      <div class="small-box bg-info">
        <div class="inner">
          <h3> </h3>

          <p>Ventas totales </p>
        </div>
        <div class="icon">
          <i class="ion ion-bag"></i>
        </div>
        <a href="#" class="small-box-footer"></a>
      </div>
    </div>
    <!-- ./col -->
    <div class="col-lg-3 col-6">
      <!-- small box -->
      <div class="small-box bg-success">
        <div class="inner">
          <h3>$ </h3>

          <p>Ventas totales </p>
        </div>
        <div class="icon">
          <i class="ion ion-stats-bars"></i>
        </div>
        <a href="#" class="small-box-footer"></a>
      </div>
    </div>
    <!-- ./col -->
    <div class="col-lg-3 col-6">
      <!-- small box -->
      <div class="small-box bg-warning">
        <div class="inner">
          <h3></h3>

          <p>Clientes</p>
        </div>
        <div class="icon">
          <i class="ion ion-person-add"></i>
        </div>
        <a href="#" class="small-box-footer"></a>
      </div>
    </div>
    <!-- ./col -->
    <div class="col-lg-3 col-6">
      <!-- small box -->
      <div class="small-box bg-danger">
        <div class="inner">
          <h3>$ </h3>

          <p>Monto total créditos</p>
        </div>
        <div class="icon">
          <i class="ion ion-pie-graph"></i>
        </div>
        <a href="#" class="small-box-footer"></a>
      </div>
    </div>
    <!-- ./col -->
  </div>

  <div class="row">
    <div class="col-md-12">
      <div class="card">
        <div class="card-header">
          <h5 class="card-title">Ventas</h5>

          <!-- /  <div class="card-tools">
            <button type="button" class="btn btn-tool" data-card-widget="collapse">
              <i class="fas fa-minus"></i>
            </button>
            <div class="btn-group">
              <button type="button" class="btn btn-tool dropdown-toggle" data-toggle="dropdown">
                <i class="fas fa-wrench"></i>
              </button>
              <div class="dropdown-menu dropdown-menu-right" role="menu">
                <a href="#" class="dropdown-item">Action</a>
                <a href="#" class="dropdown-item">Another action</a>
                <a href="#" class="dropdown-item">Something else here</a>
                <a class="dropdown-divider"></a>
                <a href="#" class="dropdown-item">Separated link</a>
              </div>
            </div>
            <button type="button" class="btn btn-tool" data-card-widget="remove">
              <i class="fas fa-times"></i>
            </button>
          </div>.card-header -->
        </div>

        <div class="card-body">
          <div class="row">
            <div class="col-md-8">
              <p class="text-center">
                <strong>Ventas: </strong>
              </p>

              <div class="chart"><div class="chartjs-size-monitor"><div class="chartjs-size-monitor-expand"><div class=""></div></div><div class="chartjs-size-monitor-shrink"><div class=""></div></div></div>
                <!-- Sales Chart Canvas -->
                <canvas id="areaChart" height="225" style="height: 180px; display: block; width: 794px;" width="992" class="chartjs-render-monitor"></canvas>
              </div>
              <!-- /.chart-responsive -->
            </div>
            <!-- /.col -->
            <div class="col-md-4">
              <p class="text-center">
                <strong>Top 5 Clientes </strong>
              </p>

            <p>
              <i class="fas fa-user"></i>   00 <span style="float: right"><strong>$ 00</strong> </span>
            </p>




              <!-- /.progress-group -->
            </div>
            <!-- /.col -->
          </div>
          <!-- /.row -->
        </div>
        <!-- ./card-body -->
        <div class="card-footer">
          <div class="row">

            {{-- @foreach($productos_total as $producto) --}}
            <div class="col-sm-3 col-6">
              <div class="description-block border-right">
                <span class="description-percentage text-success hide"><i class="fas fa-caret-up"></i> 17%</span>
                <h5 class="description-header">00 </h5>
                <span class="description-text">--</span>
              </div>
              <!-- /.description-block -->
            </div>
            {{-- @endforeach --}}
            <!-- /.col -->

          </div>
          <!-- /.row -->
        </div>
        <!-- /.card-footer -->
      </div>
      <!-- /.card -->
    </div>
    <!-- /.col -->
  </div>


<div class="row">
  <div class="col-md-12">

    <div class="card card-dark card-outline">
      <div class="card-header">
        <h3 class="card-title">
          Inventario vendido este día.
        </h3>
      </div>
      <div class="card-body">
          <table class="table  table-striped">
            <thead class="thead-dark" >
              <tr>
                <th scope="col">Producto</th>
                <th scope="col">Cantidad vendida</th>
              </tr>
            </thead>
            <tbody>

                <tr>
                  <td>--</td>
                  <td>--</td>
                </tr>


            </tbody>
          </table>
      </div>
    </div>
  </div>
</div>


  <div class="row">
    <div class="col-md-12">
      <!-- Line chart -->
      <div class="card card-primary card-outline">
        <div class="card-header">
          <h3 class="card-title">
            <i class="far fa-chart-bar"></i>
            Total diario vendido de cada producto durante {{$mes_actual}}
          </h3>

          <div class="card-tools" style="margin-top: 15px;">
              {{-- @foreach ($productos as $producto)
                 <i style="color:{{$colores[$producto->id - 1]}}; margin:5px;" class="fas fa-circle"></i> {{$producto->nombre}}
              @endforeach --}}
          </div>
        </div>
        <div class="card-body">
          <div id="line-chart" style="height: 300px; padding: 0px; position: relative;"><canvas class="flot-base" width="714" height="375" style="direction: ltr; position: absolute; left: 0px; top: 0px; width: 571.6px; height: 300px;"></canvas><canvas class="flot-overlay" width="714" height="375" style="direction: ltr; position: absolute; left: 0px; top: 0px; width: 571.6px; height: 300px;"></canvas><div class="flot-svg" style="position: absolute; top: 0px; left: 0px; height: 100%; width: 100%; pointer-events: none;"><svg style="width: 100%; height: 100%;"><g class="flot-x-axis flot-x1-axis xAxis x1Axis" style="position: absolute; inset: 0px;"><text x="37" y="294" class="flot-tick-label tickLabel" style="position: absolute; text-align: center;">0</text><text x="114.42222222222223" y="294" class="flot-tick-label tickLabel" style="position: absolute; text-align: center;">2</text><text x="191.84444444444446" y="294" class="flot-tick-label tickLabel" style="position: absolute; text-align: center;">4</text><text x="269.2666666666667" y="294" class="flot-tick-label tickLabel" style="position: absolute; text-align: center;">6</text><text x="346.6888888888889" y="294" class="flot-tick-label tickLabel" style="position: absolute; text-align: center;">8</text><text x="420.13454861111114" y="294" class="flot-tick-label tickLabel" style="position: absolute; text-align: center;">10</text><text x="497.5567708333334" y="294" class="flot-tick-label tickLabel" style="position: absolute; text-align: center;">12</text></g><g class="flot-y-axis flot-y1-axis yAxis y1Axis" style="position: absolute; inset: 0px;"><text x="1" y="269" class="flot-tick-label tickLabel" style="position: absolute; text-align: right;">-1.5</text><text x="1" y="226.66666666666669" class="flot-tick-label tickLabel" style="position: absolute; text-align: right;">-1.0</text><text x="1" y="184.33333333333334" class="flot-tick-label tickLabel" style="position: absolute; text-align: right;">-0.5</text><text x="5.984375" y="15" class="flot-tick-label tickLabel" style="position: absolute; text-align: right;">1.5</text><text x="5.984375" y="142" class="flot-tick-label tickLabel" style="position: absolute; text-align: right;">0.0</text><text x="5.984375" y="99.66666666666667" class="flot-tick-label tickLabel" style="position: absolute; text-align: right;">0.5</text><text x="5.984375" y="57.333333333333336" class="flot-tick-label tickLabel" style="position: absolute; text-align: right;">1.0</text></g></svg></div></div>
        </div>
        <!-- /.card-body-->
      </div>
      <!-- /.card -->



    </div>
  </div>


  <div class="card">
    <div class="card-header border-0">
      <div class="d-flex justify-content-between">
        <h3 class="card-title">Ventas totales mes de  por día</h3>
        <a href="javascript:void(0);"></a>
      </div>
    </div>
    <div class="card-body">
      <div class="d-flex">
        <p class="d-flex flex-column">
          <span class="text-bold text-lg">$ </span>
          <span>Venta promedio</span>
        </p>
     <!--    <p class="ml-auto d-flex flex-column text-right hide">
          <span class="text-success">
            <i class="fas fa-arrow-up"></i> 33.1%
          </span>
          <span class="text-muted">Since last month</span>
        </p>-->
      </div>
      <!-- /.d-flex -->

      <div class="position-relative mb-4"><div class="chartjs-size-monitor"><div class="chartjs-size-monitor-expand"><div class=""></div></div><div class="chartjs-size-monitor-shrink"><div class=""></div></div></div>
        <canvas id="sales-chart" height="250" style="display: block; height: 200px; width: 572px;" width="715" class="chartjs-render-monitor"></canvas>
      </div>

      <!--  <div class="d-flex flex-row justify-content-end ">
        <span class="mr-2">
          <i class="fas fa-square text-primary"></i> This year
        </span>

        <span>
          <i class="fas fa-square text-gray"></i> Last year
        </span>
      </div>-->
    </div>
  </div>
@else


<h1>Bienvenido </h1>
@endif
  <br>
  <br>
  <br>
@stop


@section('content')

@stop

@section('css')
    <link rel="stylesheet" href="{{ asset('css/backend/backend_style.css') }}">
@stop

@section('plugins.Chartjs', true)
@section('js')
    {{-- <script> console.log('Hi!'); </script>
    <script src="{{ asset("js/flot1.js")}}" ></script>
    <script src="{{ asset("js/flot2.js")}}" ></script>
    <script src="{{ asset("js/flot3.js")}}" ></script>
    <script>
      $(function () {
        /* ChartJS
         * -------
         * Here we will create a few charts using ChartJS
         */

        //--------------
        //- AREA CHART -
        //--------------
        // var ventas = {{ json_encode($productos_ventas) }}


        var arreglo = {{ json_encode($venta_mensual_anio_actual) }}
        var arreglo2 = {{ json_encode($venta_mensual_anio_anterior) }}
        var colores = ["red", "blue", "yellow", "green", "gray","orange", "purple", "pink", "black", "black", "black", "black"];

        //console.log(arreglo);
        // Get context with jQuery - using jQuery's .get() method.
        var areaChartCanvas = $('#areaChart').get(0).getContext('2d')

        var areaChartData = {
          labels  : ['Enero', 'Febrero', 'Marzo', 'Abril', 'Mayo', 'Junio', 'Julio', 'Agosto', 'Septiembre', 'Octubre', 'Noviembre', 'Diciembre'],
          datasets: [
            {
              label               : '2021',
              backgroundColor     : 'rgba(60,141,188,0.9)',
              borderColor         : 'rgba(60,141,188,0.8)',
              pointRadius          : false,
              pointColor          : '#3b8bba',
              pointStrokeColor    : 'rgba(60,141,188,1)',
              pointHighlightFill  : '#fff',
              pointHighlightStroke: 'rgba(60,141,188,1)',
              data                : arreglo
            },
            {
              label               : '2020',
              backgroundColor     : 'rgba(210, 214, 222, 1)',
              borderColor         : 'rgba(210, 214, 222, 1)',
              pointRadius         : false,
              pointColor          : 'rgba(210, 214, 222, 1)',
              pointStrokeColor    : '#c1c7d1',
              pointHighlightFill  : '#fff',
              pointHighlightStroke: 'rgba(220,220,220,1)',
              data                : arreglo2
            },
          ]
        }

        var areaChartOptions = {
          maintainAspectRatio : false,
          responsive : true,
          legend: {
            display: true
          },
          scales: {
            xAxes: [{
              gridLines : {
                display : false,
              }
            }],
            yAxes: [{
              gridLines : {
                display : false,
              }
            }]
          }
        }

        // This will get the first returned node in the jQuery collection.
        var areaChart       = new Chart(areaChartCanvas, {
          type: 'line',
          data: areaChartData,
          options: areaChartOptions
        })



    /*
     * LINE CHART
     * ----------
     */
    //LINE randomly generated data
    var array = [];
    var contador = 0;
    @for ($i = 1; $i <= 5; $i++)
      var producto = {{ json_encode($productos_ventas[$i])}}
        console.log(producto);
        var d =
           {
          data : producto,
          color: colores[contador]
        }
        contador++;
        array.push(d);

    @endfor

      console.log(array);

    var sin = [],
        cos = []
    for (var i = 1; i <= 30 ; i++) {

      sin.push([i, i])
      cos.push([i, i])
    }
    var line_data1 = {
      data : sin,
      color: 'red'
    }
    var line_data2 = {
      data : cos,
      color: 'blue'
    }


    $.plot('#line-chart', array, {
      grid  : {
        hoverable  : true,
        borderColor: '#f3f3f3',
        borderWidth: 1,
        tickColor  : '#f3f3f3'
      },
      series: {
        shadowSize: 0,
        lines     : {
          show: true
        },
        points    : {
          show: true
        }
      },
      lines : {
        fill : false,
        color: ['#3c8dbc', '#f56954']
      },
      yaxis : {
        show: true
      },
      xaxis : {
        show: true
      }
    })
    //Initialize tooltip on hover
    $('<div class="tooltip-inner" id="line-chart-tooltip"></div>').css({
      position: 'absolute',
      display : 'none',
      opacity : 0.8
    }).appendTo('body')


    $('#line-chart').bind('plothover', function (event, pos, item) {
     // console.log(item);
      if (item) {
        var x = item.datapoint[0],
            y = item.datapoint[1].toFixed(2)

        $('#line-chart-tooltip').html('Día ' + x + ' $ ' + y)
          .css({
            top : item.pageY + 5,
            left: item.pageX + 5
          })
          .fadeIn(200)
      } else {
        $('#line-chart-tooltip').hide()
      }

    })
    /* END LINE CHART */




    var ticksStyle = {
    fontColor: '#495057',
    fontStyle: 'bold'
  }

  var mode = 'index'
  var intersect = true
  var dias_mes = {{$dias_mes}}
  var venta_diaria = {{$venta_diaria}}

  var $salesChart = $('#sales-chart')
  // eslint-disable-next-line no-unused-vars
  var salesChart = new Chart($salesChart, {
    type: 'bar',
    data: {
      labels: dias_mes,
      datasets: [
        {
          backgroundColor: '#007bff',
          borderColor: '#007bff',
          data: venta_diaria
        }

      ]
    },
    options: {
      maintainAspectRatio: false,
      tooltips: {
        mode: mode,
        intersect: intersect
      },
      hover: {
        mode: mode,
        intersect: intersect
      },
      legend: {
        display: false
      },
      scales: {
        yAxes: [{
          // display: false,
          gridLines: {
            display: true,
            lineWidth: '4px',
            color: 'rgba(0, 0, 0, .2)',
            zeroLineColor: 'transparent'
          },
          ticks: $.extend({
            beginAtZero: true,

            // Include a dollar sign in the ticks
            callback: function (value) {
              if (value >= 1000) {
                value /= 1000
                value += 'k'
              }

              return '$' + value
            }
          }, ticksStyle)
        }],
        xAxes: [{
          display: true,
          gridLines: {
            display: false
          },
          ticks: ticksStyle
        }]
      }
    }
  })


      });
    </script> --}}
@stop
