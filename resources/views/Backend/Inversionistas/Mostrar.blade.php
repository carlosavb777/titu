@extends('adminlte::page')

@section('title', 'Relación con inversionistas')

@section('content_header')
    <h1>Relación con inversionistas</h1>
@stop

@section('content')
<div class="card card-success">
    <div class="card-header">
      <h3 class="card-title">Detalles del empleado.</h3>
    </div>
    <!-- /.card-header -->
    <div class="card-body">

        <div class="row">
            <div class="col-sm-12">
                <div class="form-group">
                    <label>Nombre</label>
                  <p>  {{$empleado->nombre}}</p>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-sm-6">
                <div class="form-group">
                    <label>Puesto laboral (ES)</label>
                    <p>  {{$empleado->puesto_laboral_ES}}</p>
                </div>
            </div>
            <div class="col-sm-6">
                <div class="form-group">
                    <label>Puesto laboral (EN)</label>
                    <p>  {{$empleado->puesto_laboral_EN}}</p>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-6">
                <div class="form-group">
                    <label>Correo</label><br>
                    <p>  {{$empleado->correo}}</p>
                </div>
            </div>
            <div class="col-6">
                <div class="form-group">
                    <label>Telefóno</label><br>
                    <p>  {{$empleado->telefono}}</p>
                </div>
            </div>
        </div>



    </div>
		<div class="card-footer card-footer-mostrar ">
      <a  href="{{url('inversionistasback/create')}}" class="btn btn-lg btn-primary right">Nuevo empleado</a>
      <a style="margin-right: 5px;" href="{{url('inversionistasback')}}/{{$empleado->id}}/edit" class="btn btn-lg btn-default right">Modificar</a>
      <button style="margin-right: 5px;" type="button" id="eliminar" data-id="{{$empleado->id}}" class="btn btn-lg btn-danger right">Eliminar</button>
      <a  href="{{url('inversionistasback')}}" class="btn btn-lg btn-default left">Salir</a>
		</div>

    <!-- /.card-body -->
  </div>
@stop

@section('css')

@stop

@section('js')

    <script>


      $( "#eliminar" ).on( "click", function() {
       var id = $(this).data("id");
       alerta_eliminar("eliminar_inversionistasback/"+id, titulo = "Eliminar contacto", texto="¿Realmente quieres eliminar esta contacto?");
      });

      @if(Session::has('nuevo'))
					Swal.fire(
							'¡Creado!',
						'El contacto fue creado con éxito.',
						'success'
					)
			@endif

      @if(Session::has('modificado'))
					Swal.fire(
							'¡Modificado!',
						'El contacto fue modificado con éxito.',
						'success'
					)
			@endif

    </script>
@stop
