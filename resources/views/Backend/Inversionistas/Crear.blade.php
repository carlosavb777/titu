@extends('adminlte::page')

@section('title', 'Relación con inversionistas')

@section('content_header')
    <h1>Nuevo contacto</h1>
@stop

@section('content')
    <div class="card card-primary">
        <div class="card-header">
            <h3 class="card-title">Complete los campos.</h3>
        </div>
        <!-- /.card-header -->
        <div class="card-body">
            <form action="{{ url('inversionistasback') }}" method="POST" enctype="multipart/form-data">
                @csrf
                <div class="row">
                    <div class="col-sm-12">
                        <div class="form-group">
                            <label>Nombre</label>
                            <input type="text" name="nombre" maxlength="512" required class="form-control"
                                placeholder="Ingrese un nombre">
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-6">
                        <div class="form-group">
                            <label>Correo</label>
                            <input type="text" name="correo" maxlength="512" required class="form-control"
                                placeholder="Ingrese un correo">
                        </div>
                    </div>
                    <div class="col-sm-6">
                        <div class="form-group">
                            <label>Teléfono</label>
                            <input type="text" name="telefono" maxlength="512" required class="form-control"
                                placeholder="Ingrese número telefónico">
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-6">
                        <div class="form-group">
                            <label>Puesto laboral (ES)</label>
                            <input type="text" name="puesto_laboral_ES" maxlength="512" required class="form-control"
                                placeholder="Ingrese un puesto laboral en español">
                        </div>
                    </div>
                    <div class="col-sm-6">
                        <div class="form-group">
                            <label>Puesto laboral (EN)</label>
                            <input type="text" name="puesto_laboral_EN" maxlength="512" required class="form-control"
                                placeholder="Ingrese un puesto laboral en ingles">
                        </div>
                    </div>
                </div>


            </div>
                <div class="card-footer">
                    <button type="submit" class="btn btn-lg btn-primary right guardando">Guardar</button>
                    <a href="{{ url('inversionistasback') }}" class="btn btn-lg btn-default left">Salir</a>
                </div>
            </form>
            <!-- /.card-body -->
        </div>
    @stop

    @section('css')

    @stop

    @section('js')
        <script src="{{ asset('js/input-file-custom.js') }}"></script>
        <script>
            $(function() {
                bsCustomFileInput.init();
            });
        </script>
    @stop
