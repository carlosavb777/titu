@extends('adminlte::page')

@section('title', 'Dashboard')

@section('content_header')
    <h1>Emisión</h1>
@stop

@section('content')
    <div class="card card-success">
        <div class="card-header">
            <h3 class="card-title">Detalles de la emisión.</h3>
        </div>
        <!-- /.card-header -->
        <div class="card-body">

            <div class="row">
                <div class="col-sm-6">
                    <div class="form-group">
                        <label for="cartera">Cartera</label>
                        @if ($categoria->cartera == 'FLUJOS-FUTUROS')
                            <p>Flujos futuros</p>
                        @elseif ($categoria->cartera == 'CARTERA-LIBRANZAS')
                            <p>Cartera libranzas</p>
                        @elseif ($categoria->cartera == 'CARTERA-CONSUMO-COMERCIAL')
                            <p>Cartera de consumo y comercial</p>
                        @elseif ($categoria->cartera == 'CARTERA-VEHICULOS')
                            <p>Cartera vehículo</p>
                        @elseif ($categoria->cartera == 'EMISIONES-CANCELADAS')
                            <p>Emisiones canceladas</p>
                        @endif
                    </div>
                </div>
                <div class="col-sm-6">
                    <div class="form-group">
                        <label for="categorias">Cartegoría</label>
                        <p>{{ $categoria->categoria_ES }}</p>

                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-sm-6">
                    <label for="categorias">Titulo (ES)</label>
                    <p>{{ $emision->titulo_ES }}</p>
                </div>
                <div class="col-sm-6">
                    <a class="btn btn-info" style="color: white" data-toggle="modal" data-target="#modal-ES">
                        Ver contenido de la emisíon (ES)
                    </a>
                </div>
            </div>

            <div class="row">
                <div class="col-sm-6">
                    <label for="categorias">Titulo (EN)</label>
                    <p>{{ $emision->titulo_EN }}</p>
                </div>
                <div class="col-sm-6">
                    <a class="btn btn-info" style="color: white" data-toggle="modal" data-target="#modal-EN">
                        Ver contenido de la emisíon (EN)
                    </a>
                </div>
            </div>




            <div class="modal fade" id="modal-ES" tabindex="-1" role="dialog" aria-labelledby="exampleModalLongTitle"
                aria-hidden="true">
                <div class="modal-dialog" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h5 class="modal-title" id="exampleModalLongTitle">Contenido de la emisión (ES)</h5>
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                        <div class="modal-body">
                            {!! $emision->contenido_ES !!}
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal fade" id="modal-EN" tabindex="-1" role="dialog" aria-labelledby="exampleModalLongTitle2"
                aria-hidden="true">
                <div class="modal-dialog" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h5 class="modal-title" id="exampleModalLongTitle2">Contenido de la emisión (EN)</h5>
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                        <div class="modal-body">
                            {!! $emision->contenido_EN !!}
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
                        </div>
                    </div>
                </div>
            </div>
            {{-- <div class="row">
            <div class="col-sm-6">
                <div class="form-group">
                    <label>Nombre documento (ES)</label>
                  <p> {{ $emision->nombre_documento_ES }}</p>
                </div>
            </div>
            <div class="col-sm-6">
                <div class="form-group">
                    <label>Documento (ES)</label><br>
                    <a href="{{asset($emision->documento_url_ES)}}" target="_blank" rel="noopener noreferrer">CLCIK AQUÍ PARA VER EL DOCUMENTO</a>


                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-sm-6">
                <div class="form-group">
                    <label>Nombre documento (EN)</label>
                    <p> {{ $emision->nombre_documento_EN }}</p>
                </div>
            </div>
            <div class="col-sm-6">
                <div class="form-group">
                    <label>Documento (EN)</label><br>
                    <a href="{{asset($emision->documento_url_EN)}}" target="_blank" rel="noopener noreferrer">CLCIK AQUÍ PARA VER EL DOCUMENTO</a>

                </div>
            </div>
        </div> --}}



        </div>
        <div class="card-footer card-footer-mostrar ">

            <a href="{{ url('emision/create') }}" class="btn btn-lg btn-primary right">Nuevo documento</a>
            <a style="margin-right: 5px;" href="{{ url('emision') }}/{{ $emision->id }}/edit"
                class="btn btn-lg btn-default right">Modificar</a>
            <button style="margin-right: 5px;" type="button" id="eliminar" data-id="{{ $emision->id }}"
                class="btn btn-lg btn-danger right ">Eliminar</button>
            <a href="{{ url('emision') }}" class="btn btn-lg btn-default left">Salir</a>
        </div>

        <!-- /.card-body -->
    </div>

    <div class="card card-primary">

        <div class="card-header">
            <h3 class="card-title">Adjuntar documento a la emisión.</h3>
        </div>
        <!-- /.card-header -->
        <form action="{{ url('adjuntar_documentos') }}" method="POST" enctype="multipart/form-data">
            <input type="hidden" value="{{$emision->id}}" name="emision_id">
            @csrf
        <div class="card-body">

                 <div class="row">
                    <div class="col-sm-6">
                        <div class="form-group">
                            <label>Nombre documento (ES)</label>
                            <input type="text" name="nombre_ES" maxlength="512" required class="form-control"
                                placeholder="Ingrese un nombre de documento">
                        </div>
                    </div>
                    <div class="col-sm-6">
                        <div class="form-group">
                            <label>Adjuntar documento (ES)</label>

                            <div class="input-group">
                                <div class="custom-file">
                                    <input type="file" name="archivo_ES" class="custom-file-input" id="exampleInputFile">
                                    <label class="custom-file-label" for="exampleInputFile"></label>
                                </div>
                                <div class="input-group-append">
                                    <span class="input-group-text">Subir</span>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-6">
                        <div class="form-group">
                            <label>Nombre documento (EN)</label>
                            <input type="text" name="nombre_EN" maxlength="512" required class="form-control"
                                placeholder="Ingrese un nombre de documento">
                        </div>
                    </div>
                    <div class="col-sm-6">
                        <div class="form-group">
                            <label>Adjuntar documento (EN)</label>
                            <div class="input-group">
                                <div class="custom-file">
                                    <input type="file" name="archivo_EN" class="custom-file-input" id="exampleInputFile2">
                                    <label class="custom-file-label" for="exampleInputFile2"></label>
                                </div>
                                <div class="input-group-append">
                                    <span class="input-group-text">Subir</span>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
        </div>
        <div class="card-footer">
            <button type="submit" class="btn btn-lg btn-primary right guardando">Guardar</button>
        </div>
        </form>
    </div>

    <div class="card card-primary">
        <div class="card-header">
            <h3 class="card-title">Documentos de la emisión </h3>
        </div>
        <!-- /.card-header -->
        <div class="card-body">
                <table id="tabla-emisiones" class="table table-bordered table-striped dataTable dtr-inline" role="grid" aria-describedby="example1_info">
                <thead>
                    <th>Nombre (ES)</th>
                    <th>Documento (ES)</th>
                    <th>Nombre (EN)</th>
                    <th>Documento (EN)</th>
                    <th>Acciones</th>
                </thead>
                <tbody>
                    @foreach ($documentos as $documento)
                    <tr>
                        <td>{{ $documento->nombre_ES }}</td>
                        <td><a href="{{asset($documento->url_ES)}}" target="_blank" rel="noopener noreferrer">Ver documento</a></td>
                        <td>{{ $documento->nombre_EN }}</td>
                        <td><a href="{{asset($documento->url_EN)}}" target="_blank" rel="noopener noreferrer">Ver documento</a></td>
                        <td style="float:right">
                            {{-- <a href="{{url("emision")}}/{{$emision->id}}" ><i class="iconos-lista fas fa-eye"></i></a>
                            <a href="{{url("emision")}}/{{$emision->id}}/edit" ><i class="iconos-lista fas fa-edit"></i></a> --}}
                            <a href="{{url("eliminar_documento")}}/{{$documento->id}}" class="btn-eliminar eliminando" ><i class="iconos-lista fas fa-trash"></i></a>  </td>
                    </tr>

                    @endforeach

                </tbody>
            </table>
        </div>
        <!-- /.card-body -->
</div>






@stop

@section('css')

@stop
@section('plugins.Datatables', true)
@section('js')
<script src="{{ asset('js/input-file-custom.js') }}"></script>
    <script>
          bsCustomFileInput.init();

          $('#tabla-emisiones').DataTable({
            language: {
                        url: 'https://cdn.datatables.net/plug-ins/1.10.22/i18n/Spanish.json'
                },
                responsive: true
        });

        $("#eliminar").on("click", function() {
            var id = $(this).data("id");
            alerta_eliminar("eliminar_emision/" + id, titulo = "Eliminar emisión", texto =
                "¿Realmente quieres eliminar esta emisión?");
        });

        @if (Session::has('nuevo'))
            Swal.fire(
                '¡Creado!',
                'La emisión fue creada con éxito.',
                'success'
            )
        @endif

        @if (Session::has('nuevo_documento'))
            Swal.fire(
                '¡Creado!',
                'El documento fue creado con éxito.',
                'success'
            )
        @endif

        @if (Session::has('modificado'))
            Swal.fire(
                '¡Modificado!',
                'La emisión fue modificada con éxito.',
                'success'
            )
        @endif


					@if(Session::has('documento_eliminado'))
					Swal.fire(
							'¡Eliminado!',
						'El documento fue eliminado.',
						'success'
					)
					@endif
    </script>
@stop
