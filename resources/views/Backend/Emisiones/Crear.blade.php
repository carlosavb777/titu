@extends('adminlte::page')

@section('title', 'Dashboard')

@section('content_header')
    <h1>Nueva emisión</h1>

    <link href="https://cdn.jsdelivr.net/npm/summernote@0.8.18/dist/summernote-bs4.min.css" rel="stylesheet">
@stop

@section('content')
    <div class="card card-primary">
        <div class="card-header">
            <h3 class="card-title">Complete los campos.</h3>
        </div>

        <!-- /.card-header -->
        <div class="card-body">
            <form action="{{ url('emision') }}" method="POST" enctype="multipart/form-data">
                @csrf
                <div class="row">
                    <div class="col-sm-6">
                        <div class="form-group">
                            <label for="cartera">Cartera</label>
                            <select class="form-control" required name="cartera" id="cartera">
                                <option disabled selected >Seleccione una opción</option>
                                <option value="FLUJOS-FUTUROS">Flujos futuros</option>
                                <option value="CARTERA-LIBRANZAS">Cartera libranzas</option>
                                <option value="CARTERA-CONSUMO-COMERCIAL">Cartera de consumo y comercial</option>
                                <option value="CARTERA-VEHICULOS">Cartera vehículo</option>
                                <option value="EMISIONES-CANCELADAS">Emisiones canceladas</option>
                            </select>
                        </div>
                    </div>
                    <div class="col-sm-6">
                        <div class="form-group">
                            <label for="categorias">Cartegoría</label>
                            <select class="form-control" required name="categoria" id="categorias">
                            </select>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-12">
                        <div class="form-group">
                            <label>Titulo (ES)</label>
                            <input type="text" name="titulo_ES" maxlength="512" required class="form-control"
                                placeholder="Ingrese un titulo">
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-12">
                        <label for="categorias">Contenido de la emisión (ES)</label><br>
                        <textarea name="contenido_ES" class="summernote"></textarea>
                    </div>
                </div>
                <div class="row">
                    <div class="col-12">
                        <div class="form-group">
                            <label>Titulo (EN)</label>
                            <input type="text" name="titulo_EN" maxlength="512" required class="form-control"
                                placeholder="Ingrese un titulo">
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-12">
                        <label for="categorias">Contenido de la emisión (EN)</label><br>
                        <textarea name="contenido_EN" class="summernote"></textarea>
                    </div>
                </div>

                <div class="card-footer">
                    <button type="submit" class="btn btn-lg btn-primary right guardando">Guardar</button>
                    <a href="{{ url('emision') }}" class="btn btn-lg btn-default left">Salir</a>
                </div>
            </form>
            <!-- /.card-body -->
        </div>

    @stop

    @section('css')

    @stop

    @section('js')
        <script src="{{ asset('js/input-file-custom.js') }}"></script>
        <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js" integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" crossorigin="anonymous"></script>
        <script src="https://cdn.jsdelivr.net/npm/summernote@0.8.18/dist/summernote-bs4.min.js"></script>


        <script>
            $(function() {
                bsCustomFileInput.init();
                $('.summernote').summernote({
                    toolbar: [
                            ['style', ['style']],
                            ['font', ['bold', 'underline', 'clear']],
                            ['fontname', ['fontname']],
                            ['color', ['color']],
                            ['para', ['ul', 'ol', 'paragraph']],
                            ['table', ['table']],
                            ['insert', ['link', 'picture']],
                            ['view', ['fullscreen', 'help']],
                            ],
                    height: 300
                });


                $( "#cartera" ).on( "change", function( event) {
                    $( "#categorias").empty().append('<option>Cargando...</option>');

                        $.ajax({
                            url:  APP_URL +"/obtener_categorias",
                            type: 'POST',
                            data: {
                                cartera: $(this).val(),
                                '_token': $('meta[name="csrf-token"]').attr('content')
                            },
                            })
                        .done(function(data) {

                            data = jQuery.parseJSON(data);

                            var options = '<option disabled selected >Seleccione una opción</option>';

                            $.each( data, function( index, val ){
                                //console.log(val.categoria_ES);
                                options += '<option value="'+val.id+'">'+val.categoria_ES+'</option>';
                            });

                            $( "#categorias").empty().append(options);

                           // console.log(options);



                        });



                });




            });
        </script>
    @stop
