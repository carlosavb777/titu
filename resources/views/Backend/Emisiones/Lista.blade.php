@extends('adminlte::page')

@section('title', 'Dashboard')

@section('content_header')
    <h1>Documentos Emisiones</h1>
@stop

@section('content')
<div class="row">
	<div class="col-sm-12" >
        <div style="height: 600px;">
            <div id="fm"></div>
        </div>
	</div>
</div>

@stop

@section('css')
@stop
@section('js')
<script src="{{ asset('vendor/file-manager/js/file-manager.js') }}"></script>
@stop
