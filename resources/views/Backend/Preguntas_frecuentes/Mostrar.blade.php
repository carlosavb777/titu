@extends('adminlte::page')

@section('title', 'Dashboard')

@section('content_header')
    <h1>Pregunta frecuente</h1>
@stop

@section('content')
<div class="card card-success">
    <div class="card-header">
      <h3 class="card-title">Detalles de la pregunta frecuente.</h3>
    </div>
    <!-- /.card-header -->
    <div class="card-body">

    @csrf
    <div class="row">
        <div class="col-sm-12">
            <div class="form-group">
                <label>Pregunta (ES)</label>
                {{$pregunta->pregunta_ES}}
            </div>
            <div class="form-group">
                <label>Respuesta (ES)</label><br>
                {{$pregunta->respuesta_ES}}
            </div>
            </div>
        </div>
        <div class="row">
            <div class="col-sm-12">
                <div class="form-group">
                <label>Pregunta (EN)</label>
                {{$pregunta->pregunta_EN}}
                </div>
                <div class="form-group">
                <label>Respuesta (EN)</label><br>
                {{$pregunta->respuesta_EN}}
                </div>
            </div>
            </div>


    </div>
		<div class="card-footer card-footer-mostrar ">

      <a  href="{{url('preguntas_frecuentes/create')}}" class="btn btn-lg btn-primary right">Nueva pregunta</a>
      <a style="margin-right: 5px;" href="{{url('preguntas_frecuentes')}}/{{$pregunta->id}}/edit" class="btn btn-lg btn-default right">Modificar</a>
      <button style="margin-right: 5px;" type="button" id="eliminar" data-id="{{$pregunta->id}}" class="btn btn-lg btn-danger right">Eliminar</button>
      <a  href="{{url('preguntas_frecuentes')}}" class="btn btn-lg btn-default left">Salir</a>
		</div>

    <!-- /.card-body -->
  </div>
@stop

@section('css')

@stop

@section('js')

    <script>


      $( "#eliminar" ).on( "click", function() {
       var id = $(this).data("id");
        alerta_eliminar("eliminar_pregunta/"+id, titulo = "Eliminar pregunta", texto="¿Realmente quieres eliminar esta pregunta frecuente?");
      });

      @if(Session::has('nuevo'))
					Swal.fire(
							'¡Creado!',
						'La pregunta fue creada con éxito.',
						'success'
					)
			@endif

      @if(Session::has('modificado'))
					Swal.fire(
							'¡Modificado!',
						'La pregunta fue modificada con éxito.',
						'success'
					)
			@endif

    </script>
@stop
