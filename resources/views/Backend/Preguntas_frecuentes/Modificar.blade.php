@extends('adminlte::page')

@section('title', 'Dashboard')

@section('content_header')
    <h1>Modificar pregunta frecuente</h1>
@stop

@section('content')
<div class="card card-primary">
    <div class="card-header">
      <h3 class="card-title">Edite los campos.</h3>
    </div>
    <!-- /.card-header -->
    <div class="card-body">
      <form action="{{url('preguntas_frecuentes')}}" method="POST">
				@csrf
        <input type="hidden" name="id" value="{{$pregunta->id}}" >
        <div class="row">
            <div class="col-sm-12">
                <div class="form-group">
                  <label>Pregunta (ES)</label>
                  <input type="text" name="pregunta_ES" value="{{$pregunta->pregunta_ES}}" maxlength="512" required class="form-control" placeholder="Ingrese una pregunta" >
                </div>
                <div class="form-group">
                  <label>Respuesta (ES)</label><br>
                  <textarea name="respuesta_ES" id="" maxlength="2048" class="form-control">{{$pregunta->respuesta_ES}}</textarea>
                </div>
              </div>
            </div>
            <div class="row">
              <div class="col-sm-12">
                  <div class="form-group">
                    <label>Pregunta (EN)</label>
                    <input type="text" name="pregunta_EN" maxlength="512" value="{{$pregunta->pregunta_EN}}" required class="form-control" placeholder="Ingrese una pregunta" >
                  </div>
                  <div class="form-group">
                    <label>Respuesta (EN)</label><br>
                    <textarea name="respuesta_EN" id="" maxlength="2048" class="form-control">{{$pregunta->respuesta_EN}}</textarea>
                  </div>
                </div>
              </div>
    </div>
		<div class="card-footer">
			<button type="submit" class="btn btn-lg btn-primary right guardando">Guardar</button>
      <a  href="{{url('preguntas_frecuentes')}}" class="btn btn-lg btn-default left">Salir</a>
		</div>
	</form>
    <!-- /.card-body -->
  </div>
@stop

@section('css')

@stop

@section('js')
    <script> </script>
@stop
