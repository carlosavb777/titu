@extends('adminlte::page')

@section('title', 'Dashboard')

@section('content_header')
    <h1>Preguntas frecuentes</h1>
@stop

@section('content')
<div class="row">
	<div class="col-sm-12" >

			<div class="card">
					<div class="card-header">
						<h3 class="card-title">Selecciona una pregunta de la lista para ver su detalle. </h3>
						<a href="{{url("preguntas_frecuentes/create")}}" class="btn btn-block btn-primary btn-sm btn-nuevo">Nueva pregunta</a>
					</div>
					<!-- /.card-header -->
					<div class="card-body">
							<table id="tabla-preguntas" class="table table-bordered table-striped dataTable dtr-inline" role="grid" aria-describedby="example1_info">
							<thead>
								<th>Nombre</th>
								<th>Acciones</th>
							</thead>
							<tbody>
								@foreach ($preguntas_frecuentes as $pregunta)
								<tr>
									<td>{{ $pregunta->pregunta_ES }}</td>
									<td style="float:right">
										<a href="{{url("preguntas_frecuentes")}}/{{$pregunta->id}}" ><i class="iconos-lista fas fa-eye"></i></a>
										<a href="{{url("preguntas_frecuentes")}}/{{$pregunta->id}}/edit" ><i class="iconos-lista fas fa-edit"></i></a>
										<a href="#" class="btn-eliminar" data-id="{{$pregunta->id}}" ><i class="iconos-lista fas fa-trash"></i></a>  </td>
								</tr>

								@endforeach

							</tfoot>
						</table>
					</div>
					<!-- /.card-body -->
			</div>
	</div>
</div>

@stop

@section('css')

@stop
@section('plugins.Datatables', true)
@section('js')
    <script>
			$(document).ready( function () {
					$('#tabla-preguntas').DataTable({
						language: {
									url: 'https://cdn.datatables.net/plug-ins/1.10.22/i18n/Spanish.json'
							},
							responsive: true
					});

					$( ".btn-eliminar" ).on( "click", function() {
						var id = $(this).data("id");
        		alerta_eliminar("eliminar_pregunta/"+id, titulo = "Eliminar pregunta", texto="¿Realmente quieres eliminar esta pregunta frecuente?");

					});

					@if(Session::has('eliminado'))
					Swal.fire(
							'¡Eliminada!',
						'La pregunta fue eliminada.',
						'success'
					)
					@endif

			});

	</script>
@stop
