@extends('adminlte::page')

@section('title', 'Articulos')

@section('content_header')
    <h1>Nuevo articulo</h1>

    <link href="https://cdn.jsdelivr.net/npm/summernote@0.8.18/dist/summernote-bs4.min.css" rel="stylesheet">
@stop

@section('content')
    <div class="card card-primary">
        <div class="card-header">
            <h3 class="card-title">Complete los campos.</h3>
        </div>

        <!-- /.card-header -->
        <div class="card-body">
            <form action="{{ url('articulos') }}" method="POST" enctype="multipart/form-data">
                @csrf
                <div class="row">
                    <div class="col-12">
                        <div class="form-group">
                            <label>Titulo (ES)</label>
                            <input type="text" name="titulo_ES" maxlength="512" required class="form-control"
                                placeholder="Ingrese un titulo">
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-12">
                        <div class="form-group">
                            <label for="DC">Descripción corta (ES)</label><br>
                            <textarea id="DC" name="descripcion_corta_ES" class="form-control"></textarea>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-12">
                        <div class="form-group">
                        <label for="categorias">Contenido de la emisión (ES)</label><br>
                        <textarea name="contenido_ES" class="summernote"></textarea>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-12">
                        <div class="form-group">
                            <div class="form-group">
                                <label for="exampleInputFile">Imagen</label>
                                <div class="input-group">
                                    <div class="custom-file">
                                        <input type="file" name="imagen_header" class="custom-file-input"
                                            id="exampleInputFile">
                                        <label class="custom-file-label" for="exampleInputFile"></label>
                                    </div>
                                    <div class="input-group-append">
                                        <span class="input-group-text">Subir</span>
                                    </div>
                                </div>

                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-12">
                        <div class="form-group">
                            <label>Titulo (EN)</label>
                            <input type="text" name="titulo_EN" maxlength="512" required class="form-control"
                                placeholder="Ingrese un titulo">
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-12">
                        <div class="form-group">
                            <label>Descripción corta (EN)</label><br>
                            <textarea name="descripcion_corta_EN" class="form-control"></textarea>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-12">
                        <div class="form-group">
                            <label for="categorias">Contenido de la emisión (EN)</label><br>
                            <textarea name="contenido_EN" class="summernote"></textarea>
                        </div>
                    </div>
                </div>

                <div class="card-footer">
                    <button type="submit" class="btn btn-lg btn-primary right guardando">Guardar</button>
                    <a href="{{ url('articulos') }}" class="btn btn-lg btn-default left">Salir</a>
                </div>
            </form>
            <!-- /.card-body -->
        </div>
    </div>
@stop

@section('css')

@stop

@section('js')
    <script src="{{ asset('js/input-file-custom.js') }}"></script>
    <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js"
        integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" crossorigin="anonymous">
    </script>
    <script src="https://cdn.jsdelivr.net/npm/summernote@0.8.18/dist/summernote-bs4.min.js"></script>

    <script>
        $(function() {
            bsCustomFileInput.init();
            $('.summernote').summernote({
                toolbar: [
                    ['style', ['style']],
                    ['font', ['bold', 'underline', 'clear']],
                    ['fontname', ['fontname']],
                    ['color', ['color']],
                    ['para', ['ul', 'ol', 'paragraph']],
                    ['table', ['table']],
                    ['insert', ['link', 'picture']],
                    ['view', ['fullscreen', 'help']],
                ],
                height: 300
            });



        });
    </script>
@stop
