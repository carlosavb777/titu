@extends('adminlte::page')

@section('title', 'Dashboard')

@section('content_header')
    <h1>Circular</h1>
@stop

@section('content')
<div class="card card-primary">
    <div class="card-header">
        <h3 class="card-title">Complete los campos.</h3>
    </div>

    <!-- /.card-header -->
    <div class="card-body">
        <form action="{{ url('circulares') }}" method="POST" enctype="multipart/form-data">
            @csrf
            <div class="row">
                <div class="col-sm-12">
                    <div class="form-group">
                        <label for="categorias">Cartegoría</label><br>
                        {{$categoria->categoria_ES}}
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-12">
                    <div class="form-group">
                        <label>Titulo (ES)</label>
                        <p>{{$circular->titulo_ES}}</p>

                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-12">
                    <label for="DC" >Descripción corta (ES)</label><br>
                    <p>{{$circular->descripcion_corta_ES}}</p>
                </div>
            </div>
            <div class="row">
                <div class="col-12">
                    <label for="categorias">Contenido de la emisión (ES)</label><br>
                    <a class="btn btn-info" style="color: white" data-toggle="modal" data-target="#modal-ES">
                        Ver contenido de la circular (ES)
                    </a>
                </div>
            </div>
            <div class="row">
                <div class="col-12">
                    <div class="form-group">
                        <label>Titulo (EN)</label>
                        <p>{{$circular->titulo_EN}}</p>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-12">
                    <label >Descripción corta (EN)</label><br>
                    <p>{{$circular->descripcion_corta_EN}}</p>
                </div>
            </div>
            <div class="row">
                <div class="col-12">
                    <label for="categorias">Contenido de la emisión (EN)</label><br>
                    <a class="btn btn-info" style="color: white" data-toggle="modal" data-target="#modal-EN">
                        Ver contenido de la emisíon (EN)
                    </a>
                </div>
            </div>

            <div class="card-footer card-footer-mostrar ">

                <a href="{{ url('circulares/create') }}" class="btn btn-lg btn-primary right">Nueva circular</a>
                <a style="margin-right: 5px;" href="{{ url('circulares') }}/{{ $circular->id }}/edit"
                    class="btn btn-lg btn-default right">Modificar</a>
                <button style="margin-right: 5px;" type="button" id="eliminar" data-id="{{ $circular->id }}"
                    class="btn btn-lg btn-danger right ">Eliminar</button>
                <a href="{{ url('circulares') }}" class="btn btn-lg btn-default left">Salir</a>
            </div>
        </form>
        <!-- /.card-body -->
    </div>
</div>

<div class="modal fade" id="modal-ES" tabindex="-1" role="dialog" aria-labelledby="exampleModalLongTitle"
aria-hidden="true">
<div class="modal-dialog" role="document">
    <div class="modal-content">
        <div class="modal-header">
            <h5 class="modal-title" id="exampleModalLongTitle">Contenido de la circular (ES)</h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
        </div>
        <div class="modal-body">
            {!! $circular->contenido_ES !!}
        </div>
        <div class="modal-footer">
            <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
        </div>
    </div>
</div>
</div>
<div class="modal fade" id="modal-EN" tabindex="-1" role="dialog" aria-labelledby="exampleModalLongTitle2"
aria-hidden="true">
<div class="modal-dialog" role="document">
    <div class="modal-content">
        <div class="modal-header">
            <h5 class="modal-title" id="exampleModalLongTitle2">Contenido de la circular (EN)</h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
        </div>
        <div class="modal-body">
            {!! $circular->contenido_EN !!}
        </div>
        <div class="modal-footer">
            <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
        </div>
    </div>
</div>
</div>


@stop

@section('css')

@stop
@section('plugins.Datatables', true)
@section('js')
<script src="{{ asset('js/input-file-custom.js') }}"></script>
    <script>
          bsCustomFileInput.init();

          $('#tabla-circulares').DataTable({
            language: {
                        url: 'https://cdn.datatables.net/plug-ins/1.10.22/i18n/Spanish.json'
                },
                responsive: true
        });

        $("#eliminar").on("click", function() {
            var id = $(this).data("id");
            alerta_eliminar("eliminar_circulares/" + id, titulo = "Eliminar circular", texto =
                "¿Realmente quieres eliminar esta circular?");
        });

        @if (Session::has('nuevo'))
            Swal.fire(
                '¡Creado!',
                'La circular fue creada con éxito.',
                'success'
            )
        @endif

        @if (Session::has('nuevo_documento'))
            Swal.fire(
                '¡Creado!',
                'El documento fue creado con éxito.',
                'success'
            )
        @endif

        @if (Session::has('modificado'))
            Swal.fire(
                '¡Modificado!',
                'La circular fue modificada con éxito.',
                'success'
            )
        @endif

        @if(Session::has('documento_eliminado'))
        Swal.fire(
                '¡Eliminado!',
            'El documento fue eliminado.',
            'success'
        )
        @endif
    </script>
@stop
