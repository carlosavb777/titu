@extends('adminlte::page')

@section('title', 'Dashboard')

@section('content_header')
    <h1>Rol</h1>
@stop

@section('content')


<div class="card card-primary">
  <div class="card-header">
    <h3 class="card-title">Agregar módulos a este rol.</h3>
  </div>
  <!-- /.card-header -->
  <div class="card-body">
    <form action="{{url('guardar_permiso')}}" method="POST">
      @csrf
      <input type="hidden" value="{{$rol->id}}" name="rol_id">
      <div class="row">
        <div class="col-sm-12">
          <div class="form-group">
            <label for="modulos">Modulos</label>
            <select required id="modulos" name="modulo_id" class="form-control selector">
              <option  disabled selected>Seleccione un módulo</option>
              @foreach ($modulos as $modulo)
              <option value="{{$modulo->id}}">{{ $modulo->nombre}}</option>
              @endforeach
            </select>
          </div>
        </div>
      </div>
  </div>
  <div class="card-footer">
    <button type="submit" class="btn btn-lg btn-primary right guardando">Guardar</button>
  </div>
</form>
  <!-- /.card-body -->
</div>


<div class="card card-success">
    <div class="card-header">
      <h3 class="card-title">Permisos del rol.</h3>
    </div>
    <!-- /.card-header -->
    <div class="card-body">
      <form action="{{url('roles')}}" method="POST">
				@csrf
        <div class="row">
          <div class="col-sm-12">
            <div class="form-group">
              <label>Nombre</label>
              <p>{{$rol->nombre}}</p>
            </div>
          </div>
        </div>
        <div class="row">
          <div class="col-sm-12">
            <table class="table table-hover text-nowrap">
              <thead>
                <th>Modulo</th>
                <th>Acciones</th>
              </thead>
              <tbody>
                @foreach ($permisos as $permiso)
                <tr>
                  <td>{{ $permiso->modulo }}</td>
                  <td >
                    <a href="{{url("eliminar_permiso")}}/{{$permiso->id}}" class="btn-eliminar eliminando" data-id="{{$permiso->id}}" ><i class="iconos-lista fas fa-trash"></i></a>
                  </td>
                </tr>
                @endforeach
              </tbody>
            </table>
          </div>
          </div>
    </div>
		<div class="card-footer card-footer-mostrar ">

      <a  href="{{url('roles/create')}}" class="btn btn-lg btn-primary right">Nuevo rol</a>
      <a style="margin-right: 5px;" href="{{url('roles')}}/{{$rol->id}}/edit" class="btn btn-lg btn-default right">Modificar</a>
      <button style="margin-right: 5px;" type="button" id="eliminar" data-id="{{$rol->id}}" class="btn btn-lg btn-danger right">Eliminar</button>
      <a  href="{{url('roles')}}" class="btn btn-lg btn-default left">Salir</a>
		</div>
	</form>
    <!-- /.card-body -->
  </div>
@stop

@section('css')

@stop

@section('js')

    <script>


      $( "#eliminar" ).on( "click", function() {
       var id = $(this).data("id");
        alerta_eliminar("eliminar_permiso/"+id, titulo = "Eliminar permiso", texto="¿Realmente quieres eliminar este módulo?");
      });

      @if(Session::has('nuevo'))
					Swal.fire(
							'¡Creado!',
						'El rol fue creado con éxito.',
						'success'
					)
			@endif

      @if(Session::has('nuevo_permiso'))
					Swal.fire(
							'¡Creado!',
						'El permiso fue agregado con éxito.',
						'success'
					)
			@endif

      @if(Session::has('permiso_eliminado'))
              Swal.fire(
                  '¡Eliminado!',
                'El permiso fue eliminado con éxito.',
                'success'
              )
      @endif

      @if(Session::has('modificado'))
					Swal.fire(
							'¡Modificado!',
						'El rol fue modificado con éxito.',
						'success'
					)
			@endif

    </script>
@stop
