@extends('adminlte::page')

@section('title', 'Dashboard')

@section('content_header')
    <h1>Junta directiva</h1>
@stop

@section('content')
<div class="card card-success">
    <div class="card-header">
      <h3 class="card-title">Detalles del miembro.</h3>
    </div>
    <!-- /.card-header -->
    <div class="card-body">

        <div class="row">
            <div class="col-sm-6">
                <div class="form-group">
                    <label>Nombre</label>
                    <p>{{$empleado->nombre}}</p>
                </div>
            </div>
            <div class="col-sm-6">
                <div class="form-group">
                    <label>Compañia</label>
                    <p>{{$empleado->compania}}</p>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-sm-6">
                <div class="form-group">
                    <label>Puesto laboral (ES)</label>
                    <p>{{$empleado->puesto_laboral_ES}}</p>
                </div>
            </div>
            <div class="col-sm-6">
                <div class="form-group">
                    <label>Puesto laboral (EN)</label>
                    <p>{{$empleado->puesto_laboral_EN}}</p>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-sm-8 offset-2">
                <img src="{{asset($empleado->foto)}}" class="img-fluid" id="img" alt="Responsive image">
            </div>
        </div>

    </div>
		<div class="card-footer card-footer-mostrar ">
      <a  href="{{url('directiva/create')}}" class="btn btn-lg btn-primary right">Nuevo miembro</a>
      <a style="margin-right: 5px;" href="{{url('directiva')}}/{{$empleado->id}}/edit" class="btn btn-lg btn-default right">Modificar</a>
      <button style="margin-right: 5px;" type="button" id="eliminar" data-id="{{$empleado->id}}" class="btn btn-lg btn-danger right">Eliminar</button>
      <a  href="{{url('directiva')}}" class="btn btn-lg btn-default left">Salir</a>
		</div>

    <!-- /.card-body -->
  </div>
@stop

@section('css')

@stop

@section('js')

    <script>


      $( "#eliminar" ).on( "click", function() {
       var id = $(this).data("id");
        alerta_eliminar("eliminar_directiva/"+id, titulo = "Eliminar miembro", texto="¿Realmente quieres eliminar este miembro?");
      });

      @if(Session::has('nuevo'))
					Swal.fire(
							'¡Creado!',
						'El miembro fue creado con éxito.',
						'success'
					)
			@endif

      @if(Session::has('modificado'))
					Swal.fire(
							'¡Modificado!',
						'El miembro fue modificado con éxito.',
						'success'
					)
			@endif

    </script>
@stop
