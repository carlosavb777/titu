@extends('adminlte::page')

@section('title', 'Dashboard')

@section('content_header')
    <h1>Junta directiva</h1>
@stop

@section('content')
<div class="row">
	<div class="col-sm-12" >

			<div class="card">
					<div class="card-header">
						<h3 class="card-title">Selecciona un elemento de la lista para ver su detalle. </h3>
						<a href="{{url("directiva/create")}}" class="btn btn-block btn-primary btn-sm btn-nuevo">Nuevo miembro</a>
					</div>
					<!-- /.card-header -->
					<div class="card-body">
							<table id="tabla-directiva" class="table table-bordered table-striped dataTable dtr-inline" role="grid" aria-describedby="example1_info">
							<thead>
								<th>Nombre</th>
								<th>Puesto laboral</th>
								<th>Acciones</th>
							</thead>
							<tbody>
								@foreach ($empleados as $empleado)
								<tr>
									<td>{{ $empleado->nombre }}</td>
                                    <td>{{ $empleado->puesto_laboral_ES }}</td>
									<td style="float:right">
										<a href="{{url("directiva")}}/{{$empleado->id}}" ><i class="iconos-lista fas fa-eye"></i></a>
										<a href="{{url("directiva")}}/{{$empleado->id}}/edit" ><i class="iconos-lista fas fa-edit"></i></a>
										<a href="#" class="btn-eliminar" data-id="{{$empleado->id}}" ><i class="iconos-lista fas fa-trash"></i></a>  </td>
								</tr>

								@endforeach

							</tfoot>
						</table>
					</div>
					<!-- /.card-body -->
			</div>
	</div>
</div>

@stop

@section('css')

@stop
@section('plugins.Datatables', true)
@section('js')
    <script>
			$(document).ready( function () {
					$('#tabla-directiva').DataTable({
						language: {
									url: 'https://cdn.datatables.net/plug-ins/1.10.22/i18n/Spanish.json'
							},
							responsive: true
					});

					$( ".btn-eliminar" ).on( "click", function() {
						var id = $(this).data("id");
        		alerta_eliminar("eliminar_directiva/"+id, titulo = "Eliminar miembro", texto="¿Realmente quieres eliminar esta miembro?");

					});

					@if(Session::has('eliminado'))
					Swal.fire(
							'¡Eliminado!',
						'El miembro fue eliminado.',
						'success'
					)
					@endif

			});

	</script>
@stop
