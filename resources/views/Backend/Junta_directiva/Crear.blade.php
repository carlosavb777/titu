@extends('adminlte::page')

@section('title', 'Dashboard')

@section('content_header')
    <h1>Nuevo miembro</h1>
@stop

@section('content')
    <div class="card card-primary">
        <div class="card-header">
            <h3 class="card-title">Complete los campos.</h3>
        </div>
        <!-- /.card-header -->
        <div class="card-body">
            <form action="{{ url('directiva') }}" method="POST" enctype="multipart/form-data">
                @csrf
                <div class="row">
                    <div class="col-sm-6">
                        <div class="form-group">
                            <label>Nombre</label>
                            <input type="text" name="nombre" maxlength="512" required class="form-control"
                                placeholder="Ingrese un nombre">
                        </div>
                    </div>
                    <div class="col-sm-6">
                        <div class="form-group">
                            <label>Compañia</label>
                            <input type="text" name="compania" maxlength="512" required class="form-control"
                                placeholder="Ingrese un nombre de compañia">
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-6">
                        <div class="form-group">
                            <label>Puesto laboral (ES)</label>
                            <input type="text" name="puesto_laboral_ES" maxlength="512" required class="form-control"
                                placeholder="Ingrese un puesto laboral en español">
                        </div>
                    </div>
                    <div class="col-sm-6">
                        <div class="form-group">
                            <label>Puesto laboral (EN)</label>
                            <input type="text" name="puesto_laboral_EN" maxlength="512" required class="form-control"
                                placeholder="Ingrese un puesto laboral en ingles">
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-12">
                        <div class="form-group">
                            <label>Adjuntar foto</label>
                            <div class="input-group">
                                <div class="custom-file">
                                    <input type="file" name="foto" class="custom-file-input" id="exampleInputFile">
                                    <label class="custom-file-label" for="exampleInputFile"></label>
                                </div>
                                <div class="input-group-append">
                                    <span class="input-group-text">Subir</span>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
                <div class="card-footer">
                    <button type="submit" class="btn btn-lg btn-primary right guardando">Guardar</button>
                    <a href="{{ url('directiva') }}" class="btn btn-lg btn-default left">Salir</a>
                </div>
            </form>
            <!-- /.card-body -->
        </div>
    @stop

    @section('css')

    @stop

    @section('js')
        <script src="{{ asset('js/input-file-custom.js') }}"></script>
        <script>
            $(function() {
                bsCustomFileInput.init();
            });
        </script>
    @stop
