@extends('adminlte::page')

@section('title', 'Dashboard')

@section('content_header')
    <h1>Operaciones</h1>
@stop

@section('content')

<div class="row"> 
	<div class="col-sm-12" >
			<form action="{{url('filtrar_bitacoras')}}" method="POST">
				@csrf
			<div class="card">
				<div class="card-header bg-primary">
					<h3 class="card-title">Filtros</h3>
				</div>
				<div class="card-body">
					<div class="row">
						<div class="col-sm-4">
							<div class="form-group">
								<label for="usuario">Usuarios</label>
								<select  id="usuario" name="usuario" class="form-control selector">
									<option value="0" @if($usuario_id == "") selected @endif disabled>Selecciona un usuario</option>
									@foreach ($usuarios as $usuario)
									<option @if($usuario_id == $usuario->id) selected @endif   value="{{$usuario->id}}" class="departamento" >{{ $usuario->nombres }} {{ $usuario->apellidos }}</option>
									@endforeach
								</select>
							</div>
						</div>
						<div class="col-sm-4">
							<div class="form-group">
                <label>Fecha inicio</label>
                <input type="date" @if($fecha_inicio != "") value="{{$fecha_inicio}}" @endif  name="fecha_inicio" maxlength="64"  class="form-control" placeholder="Ingrese una fecha" >
              </div>
						</div>
						<div class="col-sm-4">
							<div class="form-group">
                <label>Fecha fin</label>
                <input type="date" name="fecha_fin" @if($fecha_fin != "") value="{{$fecha_fin}}"  @endif  maxlength="64"  class="form-control" placeholder="Ingrese una fecha" >
              </div>
						</div>
					</div>
				</div>
				<div class="card-footer card-footer-mostrar ">
          <button type="submit" class="btn btn-lg btn-primary right">Filtrar</button>
          <a  href="{{url('bitacora')}}" class="btn btn-lg btn-default left">Limpiar</a>				
        </div>
			</div>
		</form>
		</div>
</div>

<div class="row"> 
	<div class="col-sm-12" >

			<div class="card">
					<div class="card-header">
						<h3 class="card-title">Selecciona una operación de la lista para ver su detalle. </h3>
					
					</div>
					<!-- /.card-header -->
					<div class="card-body">
							<table id="tabla-bitacora" class="table table-bordered table-striped dataTable dtr-inline" role="grid" aria-describedby="example1_info">
							<thead>
								<th>ID</th>
								
								<th>Acción</th>
								<th>Entidad</th>
								<th>Usuario</th>
								<th>Acciones</th>
							</thead>
							<tbody>
								@foreach ($bitacoras as $bitacora)
								<tr>
									
									<td>{{ date_format(date_create($bitacora->created_at), "d/m/Y H:s:i")}}</td>
									<td>
										@if($bitacora->accion == "DELETE")
										Eliminar
										@elseif($bitacora->accion == "CREATE")
										Crear

										@elseif($bitacora->accion == "UPDATE")

										Actualizar
										@endif
										
										</td>
									<td>{{ ucfirst(str_replace("_", " ", $bitacora->entidad))}}</td>
									<td>{{$bitacora->nombres}} {{$bitacora->apellidos}}</td>
									<td style="float:right">
										<a href="{{url("bitacora/show")}}/{{$bitacora->id}}" ><i class="iconos-lista fas fa-eye"></i></a> 
									
								</tr>

								@endforeach

							</tfoot>
						</table>
					</div>
					<!-- /.card-body -->
			</div>
	</div>
</div>
     
@stop

@section('css')
    
@stop
@section('plugins.Datatables', true)
@section('js')
    <script>
			$(document).ready( function () {
					$('#tabla-bitacora').DataTable({
						language: {
									url: 'https://cdn.datatables.net/plug-ins/1.10.22/i18n/Spanish.json'
							},
							responsive: true,
							"order": [[ 0, "desc" ]],
					});

					$( ".btn-eliminar" ).on( "click", function() {
						var id = $(this).data("id");
        		alerta_eliminar("eliminar_rol/"+id, titulo = "Eliminar rol", texto="¿Realmente quieres eliminar este rol?");

					});

					@if(Session::has('eliminado'))
					Swal.fire(
							'¡Eliminado!',
						'El rol fue eliminado.',
						'success'
					)
					@endif

			});

	</script>
@stop