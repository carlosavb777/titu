@extends('adminlte::page')

@section('title', 'Dashboard')

@section('content_header')
    <h1>Operación</h1>
@stop

@section('content')




<div class="card card-success">
    <div class="card-header">
      <h3 class="card-title">Detalles</h3>
    </div>
    <!-- /.card-header --> 
    <div class="card-body">

        <div class="row">
          <div class="col-sm-12">
            <div class="card">
              <div class="card-body">
          
                  <div class="row">
                    <div class="col-sm-3">
                      <div class="form-group">
                        <label>Fecha</label>
                        <p>{{date_format(date_create($bitacora->created_at), "d/m/Y H:s:i")}}</p>
                      </div>
                    </div>
                    <div class="col-sm-3">
                      <div class="form-group">
                        <label>Acción</label>
                        @if($bitacora->accion == "DELETE")
									<p>Eliminar</p>
										@elseif($bitacora->accion == "CREATE")
									<p>Crear</p>	
										@elseif($bitacora->accion == "UPDATE")
									<p>Actualizar</p>	
										@endif
                      </div>
                    </div>
                    <div class="col-sm-3">
                      <div class="form-group">
                        <label>Entidad</label>
                        <p>{{ ucfirst(str_replace("_", " ", $bitacora->entidad))}}</p>
                      </div>
                    </div>
                    <div class="col-sm-3">
                      <div class="form-group">
                        <label>Usuario</label>
                        <p>{{$bitacora->nombres}} {{$bitacora->apellidos}}</p>
                      </div>
                    </div>
                  </div>
                  <div class="row">
                    <div class="col-sm-12">
                      <div class="form-group">
                        <label>Datos entrantes</label>
                         
                          <ul>
                            @if($bitacora->entidad == "Ventas" && !is_null($cliente))
                              <li><strong>Cliente:</strong>{{$cliente->nombre}}</li>
                            @endif
                            @if($bitacora->entidad == "Detalle_ventas" && !is_null($producto))
                              <li><strong>Producto:</strong> {{$producto->nombre}} </li>
                              
                            @endif
                            @if($bitacora->entidad == "Detalle_gastos" && !is_null($gasto))
                              <li><strong>Gasto:</strong>{{$gasto->nombre}}</li>
                            @endif
                            @if($bitacora->entidad == "Abonos_cuentas_cobrar" && !is_null($cuenta))
                              <li><strong>Numero cuenta cobrar:</strong> {{$cuenta->codigo}}</li>
                            @endif
                              @foreach (json_decode($bitacora->datos_entrantes) as $key => $item)
                                  <li><strong>{{ ucfirst(str_replace("_", " ", $key))}}:</strong> {{$item}}</li>
                                  @if($key == "venta_id")
                                  <li><strong>Enlace a la venta:</strong> <a href='{{ url("ventas") }}/{{$item}}'>Ir a la venta</a></li>
                                  @endif
                                  @if($key == "cuenta_cobrar_id")
                                  <li><strong>Enlace a la venta:</strong> <a href='{{ url("cuentas_cobrar") }}/{{$item}}'>Ir a la cuenta</a></li>
                                  @endif
                              @endforeach
                            </ul>
                      </div>
                    </div>
              </div>
              <div class="row" @if($bitacora->accion == "CREATE") style="display:none;" @endif>
                <div class="col-sm-12">
                  <div class="form-group">
                    <label>Datos salientes</label>
                    <p></p>
                    <ul>
                      @foreach (json_decode($bitacora->datos_salientes) as $key2 => $item2)
                          <li><strong>{{ ucfirst(str_replace("_", " ", $key2))}}:</strong> {{$item2}}</li>
                          @if($key2 == "venta_id")
                          <li><strong>Enlace a la venta:</strong> <a href='{{ url("ventas") }}/{{$item2}}'>Ir a la venta</a></li>
                          @endif
                          @if($key2 == "cuenta_cobrar_id")
                          <li><strong>Enlace a la venta:</strong> <a href='{{ url("cuentas_cobrar") }}/{{$item2}}'>Ir a la cuenta</a></li>
                          @endif
                      @endforeach
                    </ul>
                  </div>
                </div>
              </div>
          </div>
          </div>
        </div>
      </div>
    </div>
		<div class="card-footer card-footer-mostrar ">
    
         <a  href="{{url('bitacora')}}" class="btn btn-lg btn-default left">Salir</a>				
		</div>

    <!-- /.card-body -->
  </div>
@stop

@section('css')
    
@stop

@section('js')

   
@stop