@extends('adminlte::page')

@section('title', 'Dashboard')

@section('content_header')
    <h1>Encabezados</h1>
@stop

@section('content')
<div class="row">
	<div class="col-sm-12" >

			<div class="card">
					<div class="card-header">
						<h3 class="card-title">Selecciona una pagina para modificar su encabezado.</h3>
					</div>
					<!-- /.card-header -->
					<div class="card-body">
							<table id="tabla-encabezados" class="table table-bordered table-striped dataTable dtr-inline" role="grid" aria-describedby="example1_info">
							<thead>
                                <th>Pagina</th>
								<th>Titulo</th>
								<th>Acciones</th>
							</thead>
							<tbody>
								@foreach ($encabezados as $encabezado)
								<tr>
									<td>{{ $encabezado->pagina }}</td>
                                    <td>{{ $encabezado->titulo }}</td>
									<td style="float:right">
										<a href="{{url("encabezados")}}/{{$encabezado->id}}" ><i class="iconos-lista fas fa-eye"></i></a>
										<a href="{{url("encabezados")}}/{{$encabezado->id}}/edit" ><i class="iconos-lista fas fa-edit"></i></a>
										  </td>
								</tr>

								@endforeach

							</tfoot>
						</table>
					</div>
					<!-- /.card-body -->
			</div>
	</div>
</div>

@stop

@section('css')

@stop
@section('plugins.Datatables', true)
@section('js')
    <script>
			$(document).ready( function () {
					$('#tabla-encabezados').DataTable({
						language: {
									url: 'https://cdn.datatables.net/plug-ins/1.10.22/i18n/Spanish.json'
							},
							responsive: true
					});



			});

	</script>
@stop
