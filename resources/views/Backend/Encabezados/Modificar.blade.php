@extends('adminlte::page')

@section('title', 'Dashboard')

@section('content_header')
    <h1>Modificar encabezado</h1>
@stop

@section('content')
<div class="card card-primary">
    <div class="card-header">
      <h3 class="card-title">Edite los campos.</h3>
    </div>
    <!-- /.card-header -->
    <div class="card-body">
      <form action="{{url('encabezados')}}" method="POST" enctype="multipart/form-data" >
				@csrf
        <input type="hidden" name="id" value="{{$encabezado->id}}" >
        <div class="row">
            <div class="col-sm-8 offset-2">
                <img src="{{asset($encabezado->imagen)}}" class="img-fluid" id="img" alt="Responsive image">
            </div>
        </div><br><br>
        <div class="row">
            <div class="col-sm-4">
                <div class="form-group">
                  <label>Pagina</label>
                  <input type="text" readonly value="{{$encabezado->pagina}}"  disabled class="form-control" >
                </div>
            </div>
              <div class="col-sm-2">
                <div class="form-group">
                    <label>URL</label><br>
                    <input type="text" name="value" required readonly value="{{$encabezado->uri}}"  class="form-control"  >
                </div>
              </div>
              <div class="col-sm-6">
                <div class="form-group">
                    <div class="form-group">
                        <label for="exampleInputFile">Imagen de cabecera</label>
                        <div class="input-group">
                        <div class="custom-file">
                        <input type="file" name="imagen_header" class="custom-file-input" id="exampleInputFile">
                        <label class="custom-file-label" for="exampleInputFile"></label>
                        </div>
                        <div class="input-group-append">
                        <span class="input-group-text">Subir</span>
                        </div>
                        </div>

                </div>
              </div>
        </div>
    </div>
    <div class="row">
        <div class="col-sm-6">
            <div class="form-group">
              <label>Titulo (ES)</label>
              <input type="text" name="titulo_ES"  value="{{$encabezado->titulo_ES}}"  class="form-control" >
            </div>
        </div>
        <div class="col-sm-6">
            <div class="form-group">
              <label>SubTitulo (ES)</label>
              <input type="text" name="subtitulo_ES"  value="{{$encabezado->subtitulo_ES}}"  class="form-control" >
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-sm-12">
            <div class="form-group">
                <label>Descripción (ES)</label><br>
                <textarea name="descripcion_ES" id="" maxlength="2048" class="form-control">{{$encabezado->descripcion_ES}}</textarea>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-sm-6">
            <div class="form-group">
              <label>Titulo (EN)</label>
              <input type="text" name="titulo_EN"  value="{{$encabezado->titulo_EN}}"  class="form-control" >
            </div>
        </div>
        <div class="col-sm-6">
            <div class="form-group">
              <label>SubTitulo (EN)</label>
              <input type="text" name="subtitulo_EN"  value="{{$encabezado->subtitulo_EN}}"  class="form-control" >
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-sm-12">
            <div class="form-group">
                <label>Descripción (EN)</label><br>
                <textarea name="descripcion_EN" id="" maxlength="2048" class="form-control">{{$encabezado->descripcion_EN}}</textarea>
            </div>
        </div>
    </div>
		<div class="card-footer">
			<button type="submit" class="btn btn-lg btn-primary right guardando">Guardar</button>
      <a  href="{{url('encabezados')}}" class="btn btn-lg btn-default left">Salir</a>
		</div>
	</form>
    <!-- /.card-body -->
  </div>
@stop

@section('css')

@stop

@section('js')
<script src="{{asset('js/input-file-custom.js')}}"></script>
    <script>
$(function () {
  bsCustomFileInput.init();
});

    </script>
@stop
