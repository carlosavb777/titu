@extends('adminlte::page')

@section('title', 'Dashboard')

@section('content_header')
    <h1>Encabezado</h1>
@stop

@section('content')
<div class="card card-success">
    <div class="card-header">
      <h3 class="card-title">Detalles del encabezado.</h3>
    </div>
    <!-- /.card-header -->
    <div class="card-body">

    @csrf
            <div class="row">
                <div class="col-sm-8 offset-2">
                    <img src="{{asset($encabezado->imagen)}}" class="img-fluid" id="img" alt="Responsive image">
                </div>
            </div><br><br>
            <div class="row">
                <div class="col-sm-6">
                    <div class="form-group">
                    <label>Pagina</label>
                    <p>{{$encabezado->pagina}}</p>
                    </div>
                </div>
                <div class="col-sm-6">
                    <div class="form-group">
                        <label>URL</label><br>
                        <p>{{$encabezado->uri}}</p>
                    </div>
                </div>

        </div>
        <div class="row">
            <div class="col-sm-6">
                <div class="form-group">
                <label>Titulo (ES)</label>
                <p>{{$encabezado->titulo_ES}}</p>
                </div>
            </div>
            <div class="col-sm-6">
                <div class="form-group">
                <label>SubTitulo (ES)</label>
                <p>{{$encabezado->subtitulo_ES}}</p>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-sm-12">
                <div class="form-group">
                    <label>Descripción (ES)</label><br>
                    {{$encabezado->descripcion_ES}}
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-sm-6">
                <div class="form-group">
                <label>Titulo (EN)</label>
                <p>{{$encabezado->titulo_EN}}</p>
                </div>
            </div>
            <div class="col-sm-6">
                <div class="form-group">
                <label>SubTitulo (EN)</label>
                <p>{{$encabezado->subtitulo_EN}}</p>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-sm-12">
                <div class="form-group">
                    <label>Descripción (EN)</label><br>
                    <p>{{$encabezado->descripcion_EN}}</p>
                </div>
            </div>
        </div>
    </div>
		<div class="card-footer card-footer-mostrar ">

      <a style="margin-right: 5px;" href="{{url('encabezados')}}/{{$encabezado->id}}/edit" class="btn btn-lg btn-default right">Modificar</a>
      <a  href="{{url('encabezados')}}" class="btn btn-lg btn-default left">Salir</a>
		</div>

    <!-- /.card-body -->
  </div>
@stop

@section('css')

@stop

@section('js')

    <script>


      @if(Session::has('modificado'))
					Swal.fire(
							'¡Modificado!',
						'El encabezado fue modificado con éxito.',
						'success'
					)
			@endif

    </script>
@stop
