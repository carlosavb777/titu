@extends('adminlte::page')

@section('title', 'Categorias')

@section('content_header')
    <h1>Modificar la emisión</h1>
@stop

@section('content')
<div class="card card-primary">
    <div class="card-header">
      <h3 class="card-title">Edite los campos.</h3>
    </div>
    <!-- /.card-header -->
    <div class="card-body">
      <form action="{{url('categorias')}}" method="POST">
				@csrf
        <input type="hidden" name="id" value="{{$categoria->id}}" >
        <div class="row">
            <div class="col-sm-12">
              <div class="form-group">
                  <label for="exampleFormControlSelect1">Cartera</label>
                  <select class="form-control" name="cartera" id="exampleFormControlSelect1">
                    <option  @if ($categoria->cartera == 'FLUJOS-FUTUROS' ) selected @endif value="FLUJOS-FUTUROS">Flujos futuros</option>
                    <option  @if ($categoria->cartera == 'CARTERA-LIBRANZAS' ) selected @endif value="CARTERA-LIBRANZAS">Cartera libranzas</option>
                    <option  @if ($categoria->cartera == 'CARTERA-CONSUMO-COMERCIAL' ) selected @endif value="CARTERA-CONSUMO-COMERCIAL">Cartera de consumo y comercial</option>
                    <option  @if ($categoria->cartera == 'CARTERA-VEHICULOS' ) selected @endif value="CARTERA-VEHICULOS">Cartera vehículo</option>
                    <option  @if ($categoria->cartera == 'EMISIONES-CANCELADAS' ) selected @endif value="EMISIONES-CANCELADAS">Emisiones canceladas</option>
                  </select>
                </div>
              </div>
            </div>
            <div class="row">
                <div class="col-sm-6">
                    <div class="form-group">
                    <label>Emisión (ES)</label>
                    <input type="text" name="categoria_ES" value="{{$categoria->categoria_ES}}" maxlength="512" required class="form-control" placeholder="Ingrese una categoría" >
                    </div>
                </div>
                <div class="col-sm-6">
                    <div class="form-group">
                        <label>Dirección de carpeta (ES)</label>
                        <input type="text" name="url_ES" value="{{$categoria->url_ES}}" maxlength="512" required class="form-control" placeholder="carpeta_emision/" >
                      </div>
                  </div>
            </div>
            <div class="row">
                <div class="col-sm-6">
                    <div class="form-group">
                        <label>Emisión (EN)</label>
                        <input type="text" name="categoria_EN" value="{{$categoria->categoria_EN}}" maxlength="512" required class="form-control" placeholder="Ingrese una categoría" >
                    </div>
                </div>
                <div class="col-sm-6">
                    <div class="form-group">
                        <label>Dirección de carpeta (EN)</label>
                        <input type="text" name="url_EN" maxlength="512" value="{{$categoria->url_EN}}" required class="form-control" placeholder="carpeta_emision/" >
                      </div>
                  </div>
            </div>
    </div>
		<div class="card-footer">
			<button type="submit" class="btn btn-lg btn-primary right">Guardar</button>
      <a  href="{{url('categorias')}}" class="btn btn-lg btn-default left">Salir</a>
		</div>
	</form>
    <!-- /.card-body -->
  </div>
@stop

@section('css')

@stop

@section('js')
    <script> </script>
@stop
