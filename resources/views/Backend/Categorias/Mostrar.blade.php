@extends('adminlte::page')

@section('title', 'Dashboard')

@section('content_header')
    <h1>Emisión</h1>
@stop

@section('content')
<div class="card card-success">
    <div class="card-header">
      <h3 class="card-title">Detalles de la emisión</h3>
    </div>
    <!-- /.card-header -->
    <div class="card-body">

        <div class="row">
            <div class="col-sm-6">
              <div class="form-group">
                <label>Cartera</label><br>
                  @if ($categoria->cartera == 'FLUJOS-FUTUROS')

                  <p>Flujos futuros</p>

                  @elseif ($categoria->cartera == 'CARTERA-LIBRANZAS')
                  <p>Cartera libranzas</p>

                  @elseif ($categoria->cartera == 'CARTERA-CONSUMO-COMERCIAL')
                  <p>Cartera de consumo y comercial</p>

                  @elseif ($categoria->cartera == 'CARTERA-VEHICULOS')

                  <p>Cartera vehículo</p>

                  @elseif ($categoria->cartera == 'EMISIONES-CANCELADAS')

                  <p>Emisiones canceladas</p>

                  @endif

                </div>
              </div>
              <div class="col-sm-6">
                  <div class="form-group">
                    <label>Nombre (ES)</label>
                    <p>{{$categoria->categoria_ES}}</p>
                  </div>
                </div>
            </div>
            <div class="row">
                <div class="col-sm-6">
                    <div class="form-group">
                        <label>Dirección de carpeta (ES)</label>
                        <p>{{$categoria->url_ES}}</p>
                      </div>
                  </div>
              <div class="col-sm-6">
                  <div class="form-group">
                      <label>Nombre (EN)</label>
                      <p>{{$categoria->categoria_EN}}</p>
                    </div>
                </div>
              </div>
              <div class="row">
                <div class="col-sm-6">
                    <div class="form-group">
                        <label>Dirección de carpeta (EN)</label>
                        <p>{{$categoria->url_EN}}</p>
                      </div>
                  </div>
                </div>


    </div>
		<div class="card-footer card-footer-mostrar ">

      <a  href="{{url('categorias/create')}}" class="btn btn-lg btn-primary right">Nueva emisión</a>
      <a style="margin-right: 5px;" href="{{url('categorias')}}/{{$categoria->id}}/edit" class="btn btn-lg btn-default right">Modificar</a>
      <button style="margin-right: 5px;" type="button" id="eliminar" data-id="{{$categoria->id}}" class="btn btn-lg btn-danger right">Eliminar</button>
      <a  href="{{url('categorias')}}" class="btn btn-lg btn-default left">Salir</a>
		</div>

    <!-- /.card-body -->
  </div>
@stop

@section('css')

@stop

@section('js')

    <script>


      $( "#eliminar" ).on( "click", function() {
       var id = $(this).data("id");
        alerta_eliminar("eliminar_categoria/"+id, titulo = "Eliminar categoría", texto="¿Realmente quieres eliminar esta emisión?");
      });

      @if(Session::has('nuevo'))
					Swal.fire(
							'¡Creado!',
						'La emisión fue creada con éxito.',
						'success'
					)
			@endif

      @if(Session::has('modificado'))
					Swal.fire(
							'¡Modificado!',
						'La emisión fue modificada con éxito.',
						'success'
					)
			@endif

    </script>
@stop
