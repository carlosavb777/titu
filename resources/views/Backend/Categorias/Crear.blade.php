@extends('adminlte::page')

@section('title', 'Dashboard')

@section('content_header')
    <h1>Nueva emisión</h1>
@stop

@section('content')
<div class="card card-primary">
    <div class="card-header">
      <h3 class="card-title">Complete los campos.</h3>
    </div>
    <!-- /.card-header -->
    <div class="card-body">
      <form action="{{url('categorias')}}" method="POST">
		@csrf
        <div class="row">
          <div class="col-sm-12">
            <div class="form-group">
                <label for="exampleFormControlSelect1">Cartera</label>
                <select class="form-control" name="cartera" id="exampleFormControlSelect1">
                  <option value="FLUJOS-FUTUROS">Flujos futuros</option>
                  <option value="CARTERA-LIBRANZAS">Cartera libranzas</option>
                  <option value="CARTERA-CONSUMO-COMERCIAL">Cartera de consumo y comercial</option>
                  <option value="CARTERA-VEHICULOS">Cartera vehículo</option>
                  <option value="EMISIONES-CANCELADAS">Emisiones canceladas</option>
                </select>
              </div>
            </div>
          </div>
          <div class="row">
            <div class="col-sm-6">
                <div class="form-group">
                  <label>Nombre (ES)</label>
                  <input type="text" name="categoria_ES" maxlength="512" required class="form-control" placeholder="Ingrese una categoría" >
                </div>
              </div>
            <div class="col-sm-6">
                <div class="form-group">
                    <label>Dirección de carpeta (ES)</label>
                    <input type="text" name="url_ES" maxlength="512" required class="form-control" placeholder="carpeta_emision/" >
                  </div>
              </div>
            </div>
            <div class="row">
                <div class="col-sm-6">
                    <div class="form-group">
                        <label>Nombre (EN)</label>
                        <input type="text" name="categoria_EN" maxlength="512" required class="form-control" placeholder="Ingrese una categoría" >
                      </div>
                  </div>
                  <div class="col-sm-6">
                    <div class="form-group">
                        <label>Dirección de carpeta (EN)</label>
                        <input type="text" name="url_EN" maxlength="512" required class="form-control" placeholder="carpeta_emision/" >
                      </div>
                  </div>
                </div>

        <div class="card-footer">
          <button type="submit" class="btn btn-lg btn-primary right guardando">Guardar</button>
          <a  href="{{url('categorias')}}" class="btn btn-lg btn-default left">Salir</a>
        </div>
	  </form>
    <!-- /.card-body -->
  </div>
@stop

@section('css')

@stop

@section('js')
    <script> </script>
@stop
