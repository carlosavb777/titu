@extends('adminlte::page')

@section('title', 'Dashboard')

@section('content_header')
    <h1>Usuario</h1>
@stop

@section('content')
<div class="card card-primary">
  <div class="card-header">
    <h3 class="card-title">Complete los campos.</h3>
  </div>
  <!-- /.card-header -->
  <form action="{{url('usuarios')}}" method="POST">
  <div class="card-body">
      @csrf
      <div class="row">
        <div class="col-sm-6">
            <div class="form-group">
              <label>Nombres</label>
              <p>{{$usuario->nombres }}</p>
            </div>
          </div>
          <div class="col-sm-6">
            <div class="form-group">
              <label>Apellidos</label>
              <p>{{$usuario->apellidos }}</p>
            </div>
          </div>
        </div>
        <div class="row">
          <div class="col-sm-6">
            <div class="form-group">
              <label>Usuario</label>
              <p>{{$usuario->nickname }}</p>
            </div>
          </div>
          <div class="col-sm-6">
            <div class="form-group">
              <label>Contraseña</label>
              <p>------</p>
            </div>
          </div>
        </div>
        <div class="row">
          <div class="col-sm-6">
            <div class="form-group">
              <label>Correo</label>
              <p>{{$usuario->email }}</p>
            </div>
          </div>
          <div class="col-sm-6">
            <div class="form-group">
              <label>Rol</label>
              <p>{{$usuario->rol }}</p>
            </div>
          </div>
        </div>
    </div>
		<div class="card-footer card-footer-mostrar ">
    
      <a  href="{{url('usuarios/create')}}" class="btn btn-lg btn-primary right">Nuevo usuario</a>
      <a style="margin-right: 5px;" href="{{url('usuarios')}}/{{$usuario->id}}/edit" class="btn btn-lg btn-default right">Modificar</a>
      <button style="margin-right: 5px;" type="button" id="eliminar" data-id="{{$usuario->id}}" class="btn btn-lg btn-danger right">Eliminar</button>
      <a  href="{{url('usuarios')}}" class="btn btn-lg btn-default left">Salir</a>				
		</div>
	</form>
    <!-- /.card-body -->
  </div>
@stop

@section('css')
    
@stop

@section('js')

    <script>

    
      $( "#eliminar" ).on( "click", function() {
       var id = $(this).data("id");
        alerta_eliminar("eliminar_usuario/"+id, titulo = "Eliminar usuario", texto="¿Realmente quieres eliminar este usuario?");
      });
      
      @if(Session::has('nuevo'))
					Swal.fire(
							'¡Creado!',
						'El usuario fue creado con éxito.',
						'success'
					)
			@endif

      @if(Session::has('modificado'))
					Swal.fire(
							'¡Modificado!',
						'El usuario fue modificado con éxito.',
						'success'
					)
			@endif
    
    </script>
@stop