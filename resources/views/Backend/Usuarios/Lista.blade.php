@extends('adminlte::page')

@section('title', 'Dashboard')

@section('content_header')
    <h1>Usuarios</h1>
@stop

@section('content')
<div class="row"> 
	<div class="col-sm-12" >

			<div class="card">
					<div class="card-header">
						<h3 class="card-title">Selecciona un usuario de la lista para ver su detalle. </h3>
						<a href="{{url("usuarios/create")}}" class="btn btn-block btn-primary btn-sm btn-nuevo">Nuevo usuario</a>
					</div>
					<!-- /.card-header -->
					<div class="card-body">
							<table id="tabla-usuarios" class="table table-bordered table-striped dataTable dtr-inline" role="grid" aria-describedby="example1_info">
							<thead>
								<th>Usuario</th>
								<th>Nombres</th>
								<th>Apellidos</th>
								<th>Rol</th>
								<th>Acciones</th>
							</thead>
							<tbody>
								@foreach ($usuarios as $usuario)
								<tr>
									<td>{{ $usuario->nickname }}</td>
									<td>{{ $usuario->nombres }}</td>
									<td>{{ $usuario->apellidos }}</td>
									<td>@if($usuario->id == 1) Super-Admin @else {{ $usuario->rol }} @endif</td>
									<td style="float:right">
										@if($usuario->id != 1) <a href="{{url("usuarios")}}/{{$usuario->id}}" ><i class="iconos-lista fas fa-eye"></i></a> 
										<a href="{{url("usuarios")}}/{{$usuario->id}}/edit" ><i class="iconos-lista fas fa-edit"></i></a>
										<a href="#" class="btn-eliminar" data-id="{{$usuario->id}}" ><i class="iconos-lista fas fa-trash"></i></a>  </td>@endif
								</tr>

								@endforeach

							</tfoot>
						</table>
					</div>
					<!-- /.card-body -->
			</div>
	</div>
</div>
    
@stop

@section('css')
    
@stop
@section('plugins.Datatables', true)
@section('js')
    <script>
			$(document).ready( function () {
					$('#tabla-usuarios').DataTable({
						language: {
									url: 'https://cdn.datatables.net/plug-ins/1.10.22/i18n/Spanish.json'
							},
							responsive: true
					});

					$( ".btn-eliminar" ).on( "click", function() {
						var id = $(this).data("id");
        		alerta_eliminar("eliminar_usuario/"+id, titulo = "Eliminar usuario", texto="¿Realmente quieres eliminar este usuario?");

					});

					@if(Session::has('eliminado'))
					Swal.fire(
							'¡Eliminado!',
						'El usuario fue eliminado.',
						'success'
					)
					@endif

			});

	</script>
@stop