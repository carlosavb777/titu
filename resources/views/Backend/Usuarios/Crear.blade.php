@extends('adminlte::page')

@section('title', 'Dashboard')

@section('content_header')
    <h1>Nuevo usuario</h1>
@stop

@section('content')
<div class="card card-primary">
    <div class="card-header">
      <h3 class="card-title">Complete los campos.</h3>
    </div>
    <!-- /.card-header -->
    <form action="{{url('usuarios')}}" method="POST" id="form-usuarios">
    <div class="card-body">
				@csrf
        <div class="row">
          <div class="col-sm-6">
              <div class="form-group">
                <label>Nombres</label>
                <input type="text" name="nombres" maxlength="64" required class="form-control" placeholder="Ingrese nombres" >
              </div>
            </div>
            <div class="col-sm-6">
              <div class="form-group">
                <label>Apellidos</label>
                <input type="text" name="apellidos" maxlength="64" required class="form-control" placeholder="Ingrese apellidos" >
              </div>
            </div>
          </div>
          <div class="row">
            <div class="col-sm-6">
              <div class="form-group">
                <label>Usuario</label>
                <input type="text" id="nickname"  name="nickname" maxlength="64" required class="form-control" placeholder="Ingrese usuario" >
              </div>
            </div>
            <div class="col-sm-6">
              <div class="form-group">
                <label>Contraseña</label>
                <input type="password" name="password" maxlength="64" required class="form-control" placeholder="Ingrese una contraseña" >
              </div>
            </div>
          </div>
          <div class="row">
            <div class="col-sm-6">
              <div class="form-group">
                <label>Correo</label>
                <input type="email" id="email" name="correo" maxlength="64" required class="form-control" placeholder="Ingrese un correo" >
              </div>
            </div>
            <div class="col-sm-6">
              <div class="form-group">
                <label>Rol</label>
                <select required id="rol" name="rol" class="form-control selector">
                  <option  disabled selected>Seleccione un rol</option>
                  @foreach ($roles as $rol)
                  <option value="{{$rol->id}}" class="rol" >{{ $rol->nombre}}</option>
                  @endforeach
                </select>
              </div>
            </div>
          </div>
      </div>
        <div class="card-footer">
          <button type="submit" id="guardar" class="btn btn-lg btn-primary guardando right">Guardar</button>
          <a  href="{{url('usuarios')}}" class="btn btn-lg btn-default left">Salir</a>
        </div>
	  </form>
    <!-- /.card-body -->
  </div>
@stop

@section('css')

@stop

@section('js')
    <script>

  $( "#guardar" ).on( "click", function( event) {

    if(jQuery('#form-usuarios').get(0).checkValidity()){
          event.preventDefault();
          $.ajax({
          url:  APP_URL +"/verificar_nickname",
            type: 'POST',
            data: {
                nickname: $("#nickname").val(),
                email: $("#email").val(),
                '_token': $('meta[name="csrf-token"]').attr('content')
            },
          })
      .done(function(data) {

          if(data == "EXISTE_NICKNAME"){

              Swal.fire({
              title: "¡Credenciales existentes!",
              text: "Parece que el usuario ingresado ya existe.",
              icon: 'error',

            });

          }else if(data == "EXISTE_EMAIL"){

            Swal.fire({
              title: "¡Credenciales existentes!",
              text: "Parece que el correo ingresado ya existe.",
              icon: 'error',

            });

          }else{


            guardando();
               $("#form-usuarios").submit();
            }



      });

    }

      });
    </script>
@stop
