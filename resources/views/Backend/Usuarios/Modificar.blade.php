@extends('adminlte::page')

@section('title', 'Dashboard')

@section('content_header')
    <h1>Modificar usuario</h1>
@stop

@section('content')
<div class="card card-primary">
    <div class="card-header">
      <h3 class="card-title">Complete los campos.</h3>
    </div>
    <!-- /.card-header -->
    <form action="{{url('usuarios')}}" id="form-usuarios" method="POST">
    <div class="card-body">
				@csrf
        <input type="hidden" name="id" value="{{ $usuario->id }}">
        <input type="hidden" id="nickname-anterior" value="{{ $usuario->nickname }}">
        <input type="hidden" id="email-anterior" value="{{ $usuario->email }}">
        <div class="row">
          <div class="col-sm-6">
              <div class="form-group">
                <label>Nombres</label>
                <input type="text" name="nombres" value="{{ $usuario->nombres }}" maxlength="64" required class="form-control" placeholder="Ingrese nombres" >
              </div>
            </div>
            <div class="col-sm-6">
              <div class="form-group">
                <label>Apellidos</label>
                <input type="text" name="apellidos" value="{{ $usuario->apellidos }}" maxlength="64" required class="form-control" placeholder="Ingrese apellidos" >
              </div>
            </div>
          </div>
          <div class="row">
            <div class="col-sm-6">
              <div class="form-group">
                <label>Usuario</label>
                <input type="text" readonly id="nickname" name="nickname" value="{{ $usuario->nickname }}" maxlength="64" required class="form-control" placeholder="Ingrese usuario" >
              </div>
            </div>
            <div class="col-sm-6">
              <div class="form-group">
                <label>Contraseña</label>
                <input type="password" name="password" maxlength="64" minlength="4" class="form-control" placeholder="Ingrese una contraseña" >
              </div>
            </div>
          </div>
          <div class="row">
            <div class="col-sm-6">
              <div class="form-group">
                <label>Correo</label>
                <input type="email" readonly id="email" name="correo" maxlength="64" value="{{ $usuario->email }}" required class="form-control" placeholder="Ingrese un correo" >
              </div>
            </div>
            <div class="col-sm-6">
              <div class="form-group">
                <label>Rol</label>
                <select required id="rol" name="rol" class="form-control selector">
                  <option  disabled >Seleccione un rol</option>
                  @foreach ($roles as $rol)
                  <option @if($usuario->rol == $rol->id) selected @endif  value="{{$rol->id}}" class="rol" >{{ $rol->nombre}}</option>
                  @endforeach
                </select>
              </div>
            </div>
          </div>

      </div>
        <div class="card-footer">
          <button type="submit" id="guardar" class="btn btn-lg btn-primary guardando right">Guardar</button>
          <a  href="{{url('usuarios')}}" class="btn btn-lg btn-default left">Salir</a>
        </div>

	  </form>
    <!-- /.card-body -->
  </div>
@stop

@section('css')

@stop

@section('js')
<script>

  $( "#guardar" ).on( "click", function( event) {
    guardando();



      });
    </script>
@stop
