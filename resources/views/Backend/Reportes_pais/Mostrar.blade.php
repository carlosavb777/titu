@extends('adminlte::page')

@section('title', 'Dashboard')

@section('content_header')
    <h1>Reporte país</h1>
@stop

@section('content')
<div class="card card-success">
    <div class="card-header">
      <h3 class="card-title">Detalles del documento.</h3>
    </div>
    <!-- /.card-header -->
    <div class="card-body">

        <div class="row">
            <div class="col-sm-2">
                <div class="form-group">
                    <label>Año</label>
                    <p>{{$documento->anio}}</p>
                </div>
            </div>
            <div class="col-sm-5">
                <div class="form-group">
                    <label>Documento (ES)</label><br>
                    <a href="{{asset($documento->url_ES)}}" target="_blank" rel="noopener noreferrer">CLCIK AQUÍ PARA VER EL DOCUMENTO ES</a>

                </div>
            </div>
            <div class="col-sm-5">
                <div class="form-group">
                    <label>Documento (EN)</label><br>
                    <a href="{{asset($documento->url_EN)}}" target="_blank" rel="noopener noreferrer">CLCIK AQUÍ PARA VER EL DOCUMENTO EN</a>

                </div>
            </div>
        </div>


    </div>
		<div class="card-footer card-footer-mostrar ">

      <a style="margin-right: 5px;" href="{{url('encuesta_pais')}}/{{$documento->id}}/edit" class="btn btn-lg btn-default right">Modificar</a>
      <a  href="{{url('encuesta_pais')}}" class="btn btn-lg btn-default left">Salir</a>
		</div>

    <!-- /.card-body -->
  </div>
@stop

@section('css')

@stop

@section('js')

    <script>


      $( "#eliminar" ).on( "click", function() {
       var id = $(this).data("id");
        alerta_eliminar("eliminar_documento/"+id, titulo = "Eliminar documento", texto="¿Realmente quieres eliminar este documento?");
      });

      @if(Session::has('nuevo'))
					Swal.fire(
							'¡Creado!',
						'El reporte fue creado con éxito.',
						'success'
					)
			@endif

      @if(Session::has('modificado'))
					Swal.fire(
							'¡Modificado!',
						'El reporte fue modificado con éxito.',
						'success'
					)
			@endif

    </script>
@stop
