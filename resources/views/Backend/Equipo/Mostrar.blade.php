@extends('adminlte::page')

@section('title', 'Dashboard')

@section('content_header')
    <h1>Enlaces de interés</h1>
@stop

@section('content')
<div class="card card-success">
    <div class="card-header">
      <h3 class="card-title">Detalles del empleado.</h3>
    </div>
    <!-- /.card-header -->
    <div class="card-body">

        <div class="row">
            <div class="col-sm-12">
                <div class="form-group">
                    <label>Nombre</label>
                  <p>  {{$empleado->nombre}}</p>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-sm-12">
                <div class="form-group">
                    <label>Enlace</label>
                    <p> <a href="{{$empleado->puesto_laboral_ES}}"  target="_blank" rel="noopener noreferrer">Clicl aquí para ir al enlace</a> </p>
                </div>
            </div>

        </div>


        <div class="row">
            <div class="col-sm-8 offset-2">
                <img src="{{asset($empleado->foto_url)}}" class="img-fluid" id="img" alt="Responsive image">
            </div>
        </div>

    </div>
		<div class="card-footer card-footer-mostrar ">
      <a  href="{{url('enlace/create')}}" class="btn btn-lg btn-primary right">Nuevo enlace</a>
      <a style="margin-right: 5px;" href="{{url('enlace')}}/{{$empleado->id}}/edit" class="btn btn-lg btn-default right">Modificar</a>
      <button style="margin-right: 5px;" type="button" id="eliminar" data-id="{{$empleado->id}}" class="btn btn-lg btn-danger right">Eliminar</button>
      <a  href="{{url('enlace')}}" class="btn btn-lg btn-default left">Salir</a>
		</div>

    <!-- /.card-body -->
  </div>
@stop

@section('css')

@stop

@section('js')

    <script>


      $( "#eliminar" ).on( "click", function() {
       var id = $(this).data("id");
        alerta_eliminar("eliminar_enlace/"+id, titulo = "Eliminar enlace", texto="¿Realmente quieres eliminar este enlace?");
      });

      @if(Session::has('nuevo'))
					Swal.fire(
							'¡Creado!',
						'El enlace fue creado con éxito.',
						'success'
					)
			@endif

      @if(Session::has('modificado'))
					Swal.fire(
							'¡Modificado!',
						'El enlace fue modificado con éxito.',
						'success'
					)
			@endif

    </script>
@stop
