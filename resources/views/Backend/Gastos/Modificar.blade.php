@extends('adminlte::page')

@section('title', 'Dashboard')

@section('content_header')
    <h1>Modificar gasto</h1>
@stop

@section('content')
<div class="card card-primary">
    <div class="card-header">
      <h3 class="card-title">Edite los campos.</h3>
    </div>
    <!-- /.card-header -->
    <div class="card-body">
      <form action="{{url('gastos')}}" method="POST">
				@csrf
        <input type="hidden" name="id" value="{{$gasto->id}}" >
        <div class="row">
          <div class="col-sm-12">
            <div class="form-group">
              <label>Nombre</label>
              <input type="text" name="nombre" value="{{$gasto->nombre}}"  maxlength="64" required class="form-control" placeholder="Ingrese un nombre" >
            </div>
          </div>
        </div>
    </div>
		<div class="card-footer">
			<button type="submit" class="btn btn-lg btn-primary right">Guardar</button>
      <a  href="{{url('gastos')}}" class="btn btn-lg btn-default left">Salir</a>
		</div>
	</form>
    <!-- /.card-body -->
  </div>
@stop

@section('css')
    
@stop

@section('js')
    <script> </script>
@stop