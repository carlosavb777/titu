@extends('adminlte::page')

@section('title', 'Dashboard')

@section('content_header')
    <h1>Gasto</h1>
@stop

@section('content')
<div class="card card-success">
    <div class="card-header">
      <h3 class="card-title">Detalles del gasto.</h3>
    </div>
    <!-- /.card-header -->
    <div class="card-body">
      <form action="{{url('gastos')}}" method="POST">
				@csrf
        <div class="row">
          <div class="col-sm-12">
            <div class="form-group">
              <label>Nombre</label>
              <p>{{$gasto->nombre}}</p>
            </div>
          </div>
        </div>
	
     
    </div>
		<div class="card-footer card-footer-mostrar ">
    
      <a  href="{{url('gastos/create')}}" class="btn btn-lg btn-primary right">Nuevo gasto</a>
      <a style="margin-right: 5px;" href="{{url('gastos')}}/{{$gasto->id}}/edit" class="btn btn-lg btn-default right">Modificar</a>
      <button style="margin-right: 5px;" type="button" id="eliminar" data-id="{{$gasto->id}}" class="btn btn-lg btn-danger right">Eliminar</button>
      <a  href="{{url('gastos')}}" class="btn btn-lg btn-default left">Salir</a>				
		</div>
	</form>
    <!-- /.card-body -->
  </div>
@stop

@section('css')
    
@stop

@section('js')

    <script>

    
      $( "#eliminar" ).on( "click", function() {
       var id = $(this).data("id");
        alerta_eliminar("eliminar_gasto/"+id, titulo = "Eliminar gasto", texto="¿Realmente quieres eliminar este gasto?");
      });
      
      @if(Session::has('nuevo'))
					Swal.fire(
							'¡Creado!',
						'El gasto fue creado con éxito.',
						'success'
					)
			@endif

      @if(Session::has('modificado'))
					Swal.fire(
							'¡Modificado!',
						'El gasto fue modificado con éxito.',
						'success'
					)
			@endif
    
    </script>
@stop