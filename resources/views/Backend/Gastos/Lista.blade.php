@extends('adminlte::page')

@section('title', 'Dashboard')

@section('content_header')
    <h1>Gastos</h1>
@stop

@section('content')
<div class="row"> 
	<div class="col-sm-12" >

			<div class="card">
					<div class="card-header">
						<h3 class="card-title">Selecciona un gasto de la lista para ver su detalle. </h3>
						<a href="{{url("gastos/create")}}" class="btn btn-block btn-primary btn-sm btn-nuevo">Nuevo gasto</a>
					</div>
					<!-- /.card-header -->
					<div class="card-body">
							<table id="tabla-gastos" class="table table-bordered table-striped dataTable dtr-inline" role="grid" aria-describedby="example1_info">
							<thead>
								<th>Nombre</th>
								<th>Acciones</th>
							</thead>
							<tbody>
								@foreach ($gastos as $gasto)
								<tr>
									<td>{{ $gasto->nombre }}</td>
									<td style="float:right">
										<a href="{{url("gastos")}}/{{$gasto->id}}" ><i class="iconos-lista fas fa-eye"></i></a> 
										<a href="{{url("gastos")}}/{{$gasto->id}}/edit" ><i class="iconos-lista fas fa-edit"></i></a>
										<a href="#" class="btn-eliminar" data-id="{{$gasto->id}}" ><i class="iconos-lista fas fa-trash"></i></a>  </td>
								</tr>

								@endforeach

							</tfoot>
						</table>
					</div>
					<!-- /.card-body -->
			</div>
	</div>
</div>
    
@stop

@section('css')
    
@stop
@section('plugins.Datatables', true)
@section('js')
    <script>
			$(document).ready( function () {
					$('#tabla-gastos').DataTable({
						language: {
									url: 'https://cdn.datatables.net/plug-ins/1.10.22/i18n/Spanish.json'
							},
							responsive: true
					});

					$( ".btn-eliminar" ).on( "click", function() {
						var id = $(this).data("id");
        		alerta_eliminar("eliminar_gasto/"+id, titulo = "Eliminar gasto", texto="¿Realmente quieres eliminar este gasto?");

					});

					@if(Session::has('eliminado'))
					Swal.fire(
							'¡Eliminado!',
						'El gasto fue eliminado.',
						'success'
					)
					@endif

			});

	</script>
@stop