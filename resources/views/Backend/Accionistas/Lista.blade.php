@extends('adminlte::page')

@section('title', 'Dashboard')

@section('content_header')
    <h1>Accionistas</h1>
@stop

@section('content')
<div class="row">
	<div class="col-sm-12" >

			<div class="card">
					<div class="card-header">
						<h3 class="card-title">Selecciona un accionista de la lista para ver su detalle. </h3>
						<a href="{{url("accionistas/create")}}" class="btn btn-block btn-primary btn-sm btn-nuevo">Nuevo accionista</a>
					</div>
					<!-- /.card-header -->
					<div class="card-body">
							<table id="tabla-accionistas" class="table table-bordered table-striped dataTable dtr-inline" role="grid" aria-describedby="example1_info">
							<thead>
								<th>Nombre</th>
								<th>Porcentaje</th>
								<th>Acciones</th>
							</thead>
							<tbody>
								@foreach ($accionistas as $accionista)
								<tr>
									<td>{{ $accionista->nombre_ES }}</td>
                                    <td>{{ $accionista->porcentaje }}</td>
									<td style="float:right">
										<a href="{{url("accionistas")}}/{{$accionista->id}}" ><i class="iconos-lista fas fa-eye"></i></a>
										<a href="{{url("accionistas")}}/{{$accionista->id}}/edit" ><i class="iconos-lista fas fa-edit"></i></a>
										<a href="#" class="btn-eliminar" data-id="{{$accionista->id}}" ><i class="iconos-lista fas fa-trash"></i></a>  </td>
								</tr>

								@endforeach

							</tfoot>
						</table>
					</div>
					<!-- /.card-body -->
			</div>
	</div>
</div>

@stop

@section('css')

@stop
@section('plugins.Datatables', true)
@section('js')
    <script>
			$(document).ready( function () {
					$('#tabla-accionistas').DataTable({
						language: {
									url: 'https://cdn.datatables.net/plug-ins/1.10.22/i18n/Spanish.json'
							},
							responsive: true
					});

					$( ".btn-eliminar" ).on( "click", function() {
						var id = $(this).data("id");
        		alerta_eliminar("eliminar_accionista/"+id, titulo = "Eliminar accionista", texto="¿Realmente quieres eliminar esta accionista?");

					});

					@if(Session::has('eliminado'))
					Swal.fire(
							'¡Eliminado!',
						'El accionista fue eliminado.',
						'success'
					)
					@endif

			});

	</script>
@stop
