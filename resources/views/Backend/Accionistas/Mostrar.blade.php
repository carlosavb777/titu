@extends('adminlte::page')

@section('title', 'Dashboard')

@section('content_header')
    <h1>Accionista</h1>
@stop

@section('content')
<div class="card card-success">
    <div class="card-header">
      <h3 class="card-title">Detalles del accionista.</h3>
    </div>
    <!-- /.card-header -->
    <div class="card-body">

        <div class="row">
            <div class="col-sm-6">
                <div class="form-group">
                  <label>Nombre (ES)</label>
                 <p>{{$accionista->nombre_ES}}</p>
                </div>
              </div>
              <div class="col-sm-6">
                  <div class="form-group">
                    <label>Porcentaje</label>
                   <p>{{$accionista->porcentaje}}</p>
                  </div>
                </div>
            </div>
            <div class="row">
              <div class="col-sm-6">
                  <div class="form-group">
                    <label>Nombre (EN)</label>
                    <p>{{$accionista->nombre_EN}}</p>
                  </div>
                </div>
              </div>


    </div>
		<div class="card-footer card-footer-mostrar ">

      <a  href="{{url('accionistas/create')}}" class="btn btn-lg btn-primary right">Nuevo accionista</a>
      <a style="margin-right: 5px;" href="{{url('accionistas')}}/{{$accionista->id}}/edit" class="btn btn-lg btn-default right">Modificar</a>
      <button style="margin-right: 5px;" type="button" id="eliminar" data-id="{{$accionista->id}}" class="btn btn-lg btn-danger right">Eliminar</button>
      <a  href="{{url('accionistas')}}" class="btn btn-lg btn-default left">Salir</a>
		</div>

    <!-- /.card-body -->
  </div>
@stop

@section('css')

@stop

@section('js')

    <script>


      $( "#eliminar" ).on( "click", function() {
       var id = $(this).data("id");
        alerta_eliminar("eliminar_accionista/"+id, titulo = "Eliminar accionista", texto="¿Realmente quieres eliminar este accionista?");
      });

      @if(Session::has('nuevo'))
					Swal.fire(
							'¡Creado!',
						'El accionista fue creado con éxito.',
						'success'
					)
			@endif

      @if(Session::has('modificado'))
					Swal.fire(
							'¡Modificado!',
						'El accionista fue modificado con éxito.',
						'success'
					)
			@endif

    </script>
@stop
