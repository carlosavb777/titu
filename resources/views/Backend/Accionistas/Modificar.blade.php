@extends('adminlte::page')

@section('title', 'Accionistas')

@section('content_header')
    <h1>Modificar el accionista</h1>
@stop

@section('content')
<div class="card card-primary">
    <div class="card-header">
      <h3 class="card-title">Edite los campos.</h3>
    </div>
    <!-- /.card-header -->
    <div class="card-body">
      <form action="{{url('accionistas')}}" method="POST">
				@csrf
        <input type="hidden" name="id" value="{{$accionista->id}}" >
        <div class="row">
            <div class="col-sm-6">
                <div class="form-group">
                  <label>Nombre (ES)</label>
                  <input type="text" name="nombre_ES" maxlength="512" required value="{{$accionista->nombre_ES}}" class="form-control" placeholder="Ingrese un nombre" >
                </div>
              </div>
              <div class="col-sm-6">
                  <div class="form-group">
                    <label>Porcentaje</label>
                    <input type="text" name="porcentaje" maxlength="512" value="{{$accionista->porcentaje}}"  required class="form-control" placeholder="Ingrese un porcentaje" >
                  </div>
                </div>
            </div>
            <div class="row">
              <div class="col-sm-6">
                  <div class="form-group">
                    <label>Nombre (EN)</label>
                    <input type="text" name="nombre_EN" maxlength="512" required value="{{$accionista->nombre_EN}}"  class="form-control" placeholder="Ingrese un nombre" >
                  </div>
                </div>
              </div>
    </div>
		<div class="card-footer">
			<button type="submit" class="btn btn-lg btn-primary right">Guardar</button>
      <a  href="{{url('accionistas')}}" class="btn btn-lg btn-default left">Salir</a>
		</div>
	</form>
    <!-- /.card-body -->
  </div>
@stop

@section('css')

@stop

@section('js')
    <script> </script>
@stop
