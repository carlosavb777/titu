@extends('adminlte::page')

@section('title', 'Dashboard')

@section('content_header')
    <h1>Edu. Financiera</h1>
@stop

@section('content')
<div class="row">
	<div class="col-sm-12" >

			<div class="card">
					<div class="card-header">
						<h3 class="card-title">Selecciona un elemento de la lista para ver su detalle. </h3>
						<a href="{{url("blog/create")}}" class="btn btn-block btn-primary btn-sm btn-nuevo">Nueva entrada</a>
					</div>
					<!-- /.card-header -->
					<div class="card-body">
							<table id="tabla-blog" class="table table-bordered table-striped dataTable dtr-inline" role="grid" aria-describedby="example1_info">
							<thead>
                                <th>Fecha</th>
								<th>Titulo</th>
								<th>Acciones</th>
							</thead>
							<tbody>
								@foreach ($blogs as $blog)
								<tr>
                                    <td>{{ date_format($blog->created_at, 'd-m-Y')  }}</td>
									<td>{{ $blog->titulo_ES }}</td>
									<td style="float:right">
										<a href="{{url("blog")}}/{{$blog->id}}" ><i class="iconos-lista fas fa-eye"></i></a>
										<a href="{{url("blog")}}/{{$blog->id}}/edit" ><i class="iconos-lista fas fa-edit"></i></a>
										<a href="#" class="btn-eliminar" data-id="{{$blog->id}}" ><i class="iconos-lista fas fa-trash"></i></a>  </td>
								</tr>

								@endforeach

							</tbody>
						</table>
					</div>
					<!-- /.card-body -->
			</div>
	</div>
</div>

@stop

@section('css')

@stop
@section('plugins.Datatables', true)
@section('js')
    <script>
			$(document).ready( function () {
					$('#tabla-blog').DataTable({
						language: {
									url: 'https://cdn.datatables.net/plug-ins/1.10.22/i18n/Spanish.json'
							},
							responsive: true
					});

					$( ".btn-eliminar" ).on( "click", function() {
						var id = $(this).data("id");
        		    alerta_eliminar("eliminar_blog/"+id, titulo = "Eliminar entrada", texto="¿Realmente quieres eliminar esta entrada?");

					});

					@if(Session::has('eliminado'))
					Swal.fire(
							'¡Eliminada!',
						'La entrada fue eliminada.',
						'success'
					)
					@endif

			});

	</script>
@stop

