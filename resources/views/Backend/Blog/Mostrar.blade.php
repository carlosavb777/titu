@extends('adminlte::page')

@section('title', 'Dashboard')

@section('content_header')
    <h1>Edu. Financiera</h1>
@stop

@section('content')
<div class="card card-primary">
    <div class="card-header">
        <h3 class="card-title">Complete los campos.</h3>
    </div>

    <!-- /.card-header -->
    <div class="card-body">
        <form action="{{ url('blog') }}" method="POST" enctype="multipart/form-data">
            @csrf
            <div class="row">
                <div class="col-sm-12">
                    <div class="form-group">
                        <label for="categorias">Imagen</label><br>
                        <img src="{{asset($blog->imagen)}}" class="img-fluid" id="img" alt="Responsive image">
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-12">
                    <div class="form-group">
                        <label>Titulo (ES)</label>
                        <p>{{$blog->titulo_ES}}</p>

                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-12">
                    <label for="DC" >Descripción corta (ES)</label><br>
                    <p>{{$blog->descripcion_corta_ES}}</p>
                </div>
            </div>
            <div class="row">
                <div class="col-12">
                    <label for="categorias">Contenido del articulo (ES)</label><br>
                    <a class="btn btn-info" style="color: white" data-toggle="modal" data-target="#modal-ES">
                        Contenido del articulo (ES)
                    </a>
                </div>
            </div>
            <div class="row">
                <div class="col-12">
                    <div class="form-group">
                        <label>Titulo (EN)</label>
                        <p>{{$blog->titulo_EN}}</p>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-12">
                    <label >Descripción corta (EN)</label><br>
                    <p>{{$blog->descripcion_corta_EN}}</p>
                </div>
            </div>
            <div class="row">
                <div class="col-12">
                    <label for="categorias">Contenido del articulo (EN)</label><br>
                    <a class="btn btn-info" style="color: white" data-toggle="modal" data-target="#modal-EN">
                        Contenido del articulo (EN)
                    </a>
                </div>
            </div>
<br><br><br>
            <div class="card-footer card-footer-mostrar ">

                <a href="{{ url('blog/create') }}" class="btn btn-lg btn-primary right">Nueva entrada</a>
                <a style="margin-right: 5px;" href="{{ url('blog') }}/{{ $blog->id }}/edit"
                    class="btn btn-lg btn-default right">Modificar</a>
                <button style="margin-right: 5px;" type="button" id="eliminar" data-id="{{ $blog->id }}"
                    class="btn btn-lg btn-danger right ">Eliminar</button>
                <a href="{{ url('blog') }}" class="btn btn-lg btn-default left">Salir</a>
            </div>
        </form>
        <!-- /.card-body -->
    </div>
</div>

<div class="modal fade" id="modal-ES" tabindex="-1" role="dialog" aria-labelledby="exampleModalLongTitle"
aria-hidden="true">
<div class="modal-dialog" role="document">
    <div class="modal-content">
        <div class="modal-header">
            <h5 class="modal-title" id="exampleModalLongTitle">Contenido del articuloo (ES)</h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
        </div>
        <div class="modal-body">
            {!! $blog->contenido_ES !!}
        </div>
        <div class="modal-footer">
            <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
        </div>
    </div>
</div>
</div>
<div class="modal fade" id="modal-EN" tabindex="-1" role="dialog" aria-labelledby="exampleModalLongTitle2"
aria-hidden="true">
<div class="modal-dialog" role="document">
    <div class="modal-content">
        <div class="modal-header">
            <h5 class="modal-title" id="exampleModalLongTitle2">Contenido del articulo (EN)</h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
        </div>
        <div class="modal-body">
            {!! $blog->contenido_EN !!}
        </div>
        <div class="modal-footer">
            <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
        </div>
    </div>
</div>
</div>


@stop

@section('css')

@stop
@section('plugins.Datatables', true)
@section('js')
<script src="{{ asset('js/input-file-custom.js') }}"></script>
    <script>
          bsCustomFileInput.init();

          $('#tabla-bloges').DataTable({
            language: {
                        url: 'https://cdn.datatables.net/plug-ins/1.10.22/i18n/Spanish.json'
                },
                responsive: true
        });

        $("#eliminar").on("click", function() {
            var id = $(this).data("id");
            alerta_eliminar("eliminar_blog/" + id, titulo = "Eliminar entrada", texto =
                "¿Realmente quieres eliminar esta entrada?");
        });

        @if (Session::has('nuevo'))
            Swal.fire(
                '¡Creado!',
                'La entrada fue creada con éxito.',
                'success'
            )
        @endif

        @if (Session::has('nuevo_documento'))
            Swal.fire(
                '¡Creado!',
                'El documento fue creado con éxito.',
                'success'
            )
        @endif

        @if (Session::has('modificado'))
            Swal.fire(
                '¡Modificado!',
                'La entrada fue modificada con éxito.',
                'success'
            )
        @endif

        @if(Session::has('documento_eliminado'))
        Swal.fire(
                '¡Eliminado!',
            'El documento fue eliminado.',
            'success'
        )
        @endif
    </script>
@stop
