@extends('adminlte::page')

@section('title', 'Dashboard')

@section('content_header')
    <h1>Gobierno Corporativo</h1>
@stop

@section('content')
<div class="row">
	<div class="col-sm-12" >

			<div class="card">
					<div class="card-header">
						<h3 class="card-title">Selecciona un elemento de la lista para ver su detalle. </h3>
						<a href="{{url("gobierno_corporativo/create")}}" class="btn btn-block btn-primary btn-sm btn-nuevo">Nuevo documento</a>
					</div>
					<!-- /.card-header -->
					<div class="card-body">
							<table id="tabla-gobierno_corporativo" class="table table-bordered table-striped dataTable dtr-inline" role="grid" aria-describedby="example1_info">
							<thead>
								<th>Titulo</th>
								<th>Documento</th>
								<th>Acciones</th>
							</thead>
							<tbody>
								@foreach ($gobierno_corporativo as $documento)
								<tr>
									<td>{{ $documento->titulo_ES }}</td>
                                    <td>{{ $documento->nombre_documento_ES }}</td>
									<td style="float:right">
										<a href="{{url("gobierno_corporativo")}}/{{$documento->id}}" ><i class="iconos-lista fas fa-eye"></i></a>
										<a href="{{url("gobierno_corporativo")}}/{{$documento->id}}/edit" ><i class="iconos-lista fas fa-edit"></i></a>
										<a href="#" class="btn-eliminar" data-id="{{$documento->id}}" ><i class="iconos-lista fas fa-trash"></i></a>  </td>
								</tr>

								@endforeach

							</tfoot>
						</table>
					</div>
					<!-- /.card-body -->
			</div>
	</div>
</div>

@stop

@section('css')

@stop
@section('plugins.Datatables', true)
@section('js')
    <script>
			$(document).ready( function () {
					$('#tabla-gobierno_corporativo').DataTable({
						language: {
									url: 'https://cdn.datatables.net/plug-ins/1.10.22/i18n/Spanish.json'
							},
							responsive: true
					});

					$( ".btn-eliminar" ).on( "click", function() {
						var id = $(this).data("id");
        		alerta_eliminar("eliminar_documento/"+id, titulo = "Eliminar documento", texto="¿Realmente quieres eliminar esta documento?");

					});

					@if(Session::has('eliminado'))
					Swal.fire(
							'¡Eliminado!',
						'El documento fue eliminado.',
						'success'
					)
					@endif

			});

	</script>
@stop
