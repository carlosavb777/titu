@extends('adminlte::page')

@section('title', 'Accionistas')

@section('content_header')
    <h1>Modificar el documento</h1>
@stop

@section('content')
<div class="card card-primary">
    <div class="card-header">
      <h3 class="card-title">Edite los campos.</h3>
    </div>
    <!-- /.card-header -->
    <div class="card-body">
      <form action="{{url('gobierno_corporativo')}}" method="POST" enctype="multipart/form-data">
		@csrf
        <input type="hidden" name="id" value="{{$documento->id}}" >

        <div class="row">
            <div class="col-sm-6">
                <div class="form-group">
                    <label>Titulo (ES)</label>
                    <input type="text" value="{{$documento->titulo_ES}}" name="titulo_ES" maxlength="512" required class="form-control"
                        placeholder="Ingrese una titulo">
                </div>
            </div>
            <div class="col-sm-6">
                <div class="form-group">
                    <label>Nombre documento (ES)</label>
                    <input type="text" value="{{$documento->nombre_documento_ES}}"  name="nombre_documento_ES" maxlength="512" required class="form-control"
                        placeholder="Ingrese un nombre de documento">
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-sm-12">
                <div class="form-group">
                    <label>Adjuntar documento (ES)  <a href="{{asset($documento->documento_url_ES)}}" target="_blank" rel="noopener noreferrer">CLCIK AQUÍ PARA VER EL DOCUMENTO ACTUAL</a>
                    </label>
                    <div class="input-group">
                        <div class="custom-file">
                            <input type="file" name="archivo_ES" class="custom-file-input" id="exampleInputFile">
                            <label class="custom-file-label" for="exampleInputFile"></label>
                        </div>
                        <div class="input-group-append">
                            <span class="input-group-text">Subir</span>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-sm-12">
                <div class="form-group">
                    <label>Descripción (ES)</label><br>
                    <textarea class="form-control" name="descripcion_ES" id="" maxlength="2048">{{$documento->descripcion_ES}}</textarea>
                </div>
            </div>
        </div>



        <div class="row">
            <div class="col-sm-6">
                <div class="form-group">
                    <label>Titulo (EN)</label>
                    <input type="text" name="titulo_EN" value="{{$documento->titulo_EN}}"  maxlength="512" required class="form-control"
                        placeholder="Ingrese una titulo">
                </div>
            </div>
            <div class="col-sm-6">
                <div class="form-group">
                    <label>Nombre documento (EN)</label>
                    <input type="text" name="nombre_documento_EN" value="{{$documento->nombre_documento_EN}}"  maxlength="512" required class="form-control"
                        placeholder="Ingrese un nombre de documento">
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-sm-12">
                <div class="form-group">
                    <label>Adjuntar documento (EN) <a href="{{asset($documento->documento_url_EN)}}" target="_blank" rel="noopener noreferrer">CLCIK AQUÍ PARA VER EL DOCUMENTO ACTUAL</a>
                    </label>
                    <div class="input-group">
                        <div class="custom-file">
                            <input type="file" name="archivo_EN" class="custom-file-input" id="exampleInputFile2">
                            <label class="custom-file-label" for="exampleInputFile2"></label>
                        </div>
                        <div class="input-group-append">
                            <span class="input-group-text">Subir</span>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-sm-12">
                <div class="form-group">
                    <label>Descripción (EN)</label><br>
                    <textarea class="form-control" name="descripcion_EN" id="" maxlength="2048">{{$documento->descripcion_EN}}</textarea>
                </div>
            </div>
        </div>

    </div>
		<div class="card-footer">
			<button type="submit" class="btn btn-lg btn-primary right guardando">Guardar</button>
      <a  href="{{url('accionistas')}}" class="btn btn-lg btn-default left">Salir</a>
		</div>
	</form>
    <!-- /.card-body -->
  </div>
@stop

@section('css')

@stop

@section('js')
        <script src="{{ asset('js/input-file-custom.js') }}"></script>
        <script>
            $(function() {
                bsCustomFileInput.init();
            });
        </script>
    @stop
