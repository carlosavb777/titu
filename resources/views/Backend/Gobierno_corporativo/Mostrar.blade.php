@extends('adminlte::page')

@section('title', 'Dashboard')

@section('content_header')
    <h1>Gobierno corporativo</h1>
@stop

@section('content')
<div class="card card-success">
    <div class="card-header">
      <h3 class="card-title">Detalles del documento.</h3>
    </div>
    <!-- /.card-header -->
    <div class="card-body">

        <div class="row">
            <div class="col-sm-6">
                <div class="form-group">
                    <label>Titulo (ES)</label>
                    <p>{{$documento->titulo_ES}}</p>
                </div>
            </div>
            <div class="col-sm-6">
                <div class="form-group">
                    <label>Nombre documento (ES)</label>
                    <p>{{$documento->nombre_documento_ES}}</p>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-sm-12">
                <div class="form-group">
                    <label>Descripción (ES)</label><br>
                    <p>{{$documento->descripcion_ES}}</p>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-sm-12">
                <div class="form-group">
                    <label>Documento (ES)</label><br>
                    <a href="{{asset($documento->documento_url_ES)}}" target="_blank" rel="noopener noreferrer">CLCIK AQUÍ PARA VER EL DOCUMENTO ES</a>

                </div>
            </div>
        </div>


        <div class="row">
            <div class="col-sm-6">
                <div class="form-group">
                    <label>Titulo (EN)</label>
                    <p>{{$documento->titulo_EN}}</p>
                </div>
            </div>
            <div class="col-sm-6">
                <div class="form-group">
                    <label>Nombre documento (EN)</label>
                    <p>{{$documento->nombre_documento_EN}}</p>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-sm-12">
                <div class="form-group">
                    <label>Descripción (EN)</label><br>
                    <p>{{$documento->descripcion_EN}}</p>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-sm-12">
                <div class="form-group">
                    <label>Documento (EN)</label><br>
                    <a href="{{asset($documento->documento_url_EN)}}" target="_blank" rel="noopener noreferrer">CLCIK AQUÍ PARA VER EL DOCUMENTO EN</a>

                </div>
            </div>
        </div>


    </div>
		<div class="card-footer card-footer-mostrar ">

      <a  href="{{url('gobierno_corporativo/create')}}" class="btn btn-lg btn-primary right">Nuevo documento</a>
      <a style="margin-right: 5px;" href="{{url('gobierno_corporativo')}}/{{$documento->id}}/edit" class="btn btn-lg btn-default right">Modificar</a>
      <button style="margin-right: 5px;" type="button" id="eliminar" data-id="{{$documento->id}}" class="btn btn-lg btn-danger right">Eliminar</button>
      <a  href="{{url('gobierno_corporativo')}}" class="btn btn-lg btn-default left">Salir</a>
		</div>

    <!-- /.card-body -->
  </div>
@stop

@section('css')

@stop

@section('js')

    <script>


      $( "#eliminar" ).on( "click", function() {
       var id = $(this).data("id");
        alerta_eliminar("eliminar_documento/"+id, titulo = "Eliminar documento", texto="¿Realmente quieres eliminar este documento?");
      });

      @if(Session::has('nuevo'))
					Swal.fire(
							'¡Creado!',
						'El documento fue creado con éxito.',
						'success'
					)
			@endif

      @if(Session::has('modificado'))
					Swal.fire(
							'¡Modificado!',
						'El documento fue modificado con éxito.',
						'success'
					)
			@endif

    </script>
@stop
