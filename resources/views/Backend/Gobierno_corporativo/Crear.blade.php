@extends('adminlte::page')

@section('title', 'Dashboard')

@section('content_header')
    <h1>Nuevo documento</h1>
@stop

@section('content')
    <div class="card card-primary">
        <div class="card-header">
            <h3 class="card-title">Complete los campos.</h3>
        </div>
        <!-- /.card-header -->
        <div class="card-body">
            <form action="{{ url('gobierno_corporativo') }}" method="POST" enctype="multipart/form-data">
                @csrf
                <div class="row">
                    <div class="col-sm-6">
                        <div class="form-group">
                            <label>Titulo (ES)</label>
                            <input type="text" name="titulo_ES" maxlength="512" required class="form-control"
                                placeholder="Ingrese una titulo">
                        </div>
                    </div>
                    <div class="col-sm-6">
                        <div class="form-group">
                            <label>Nombre documento (ES)</label>
                            <input type="text" name="nombre_documento_ES" maxlength="512" required class="form-control"
                                placeholder="Ingrese un nombre de documento">
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-12">
                        <div class="form-group">
                            <label>Descripción (ES)</label><br>
                            <textarea class="form-control" name="descripcion_ES" id="" maxlength="2048"></textarea>
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-sm-12">
                        <div class="form-group">
                            <label>Adjuntar documento (ES)</label>

                            <div class="input-group">
                                <div class="custom-file">
                                    <input type="file" name="archivo_ES" class="custom-file-input" id="exampleInputFile">
                                    <label class="custom-file-label" for="exampleInputFile"></label>
                                </div>
                                <div class="input-group-append">
                                    <span class="input-group-text">Subir</span>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-sm-12">
                        <div class="form-group">
                            <label>Adjuntar documento (EN)</label>

                            <div class="input-group">
                                <div class="custom-file">
                                    <input type="file" name="archivo_EN" class="custom-file-input" id="exampleInputFile2">
                                    <label class="custom-file-label" for="exampleInputFile2"></label>
                                </div>
                                <div class="input-group-append">
                                    <span class="input-group-text">Subir</span>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>


                <div class="row">
                    <div class="col-sm-6">
                        <div class="form-group">
                            <label>Titulo (EN)</label>
                            <input type="text" name="titulo_EN" maxlength="512" required class="form-control"
                                placeholder="Ingrese una titulo">
                        </div>
                    </div>
                    <div class="col-sm-6">
                        <div class="form-group">
                            <label>Nombre documento (EN)</label>
                            <input type="text" name="nombre_documento_EN" maxlength="512" required class="form-control"
                                placeholder="Ingrese un nombre de documento">
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-12">
                        <div class="form-group">
                            <label>Descripción (EN)</label><br>
                            <textarea class="form-control" name="descripcion_EN" id="" maxlength="2048"></textarea>
                        </div>
                    </div>
                </div>

                <div class="card-footer">
                    <button type="submit" class="btn btn-lg btn-primary right guardando">Guardar</button>
                    <a href="{{ url('gobierno_corporativo') }}" class="btn btn-lg btn-default left">Salir</a>
                </div>
            </form>
            <!-- /.card-body -->
        </div>
    @stop

    @section('css')

    @stop

    @section('js')
        <script src="{{ asset('js/input-file-custom.js') }}"></script>
        <script>
            $(function() {
                bsCustomFileInput.init();
            });
        </script>
    @stop
