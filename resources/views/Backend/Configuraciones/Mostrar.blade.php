@extends('adminlte::page')

@section('title', 'Dashboard')

@section('content_header')
    <h1>Configuración</h1>
@stop

@section('content')
<div class="card card-success">
    <div class="card-header">
      <h3 class="card-title">Detalles de la configuración.</h3>
    </div>
    <!-- /.card-header -->
    <div class="card-body">

        <div class="row">
            <div class="col-sm-12">
                <div class="form-group">
                  <label>Configuración</label>
                  <input type="text" name="" value="{{$configuracion->key}}"  disabled readonly class="form-control" >
                </div>
            </div>
        </div>
        @if($configuracion->type == 'TEXT')
            <div class="row" >
                <div class="col-6">
                    <div class="form-group">
                        <label>Valor (ES)</label><br>
                        {{$configuracion->value_ES}}
                    </div>
                </div>
                <div class="col-6">
                    <div class="form-group">
                        <label>Valor (EN)</label><br>
                        {{$configuracion->value_EN}}
                    </div>
                </div>
            </div>

        @elseif($configuracion->type == 'TEXTAREA')

        <div class="row" >
            <div class="col-12">
                <div class="form-group">
                    <label>Valor (ES)</label><br>
                    {{$configuracion->value_ES}}
                </div>
            </div>
        </div>
        <div class="row" >

            <div class="col-12">
                <div class="form-group">
                    <label>Valor (EN)</label><br>
                    {{$configuracion->value_EN}}
                </div>
            </div>
        </div>

        @elseif($configuracion->type == 'FILE')

        <div class="row" >
            <div class="col-6">
                <div class="form-group">
                    <label>Documento (ES)</label><br><a href="{{asset($configuracion->value_ES)}}" target="_blank" > CLCIK AQUÍ PARA VER EL DOCUMENTO</a>
                </div>
            </div>
            <div class="col-6">
                <div class="form-group">
                    <label>Documento (EN)</label><br><a href="{{asset($configuracion->value_EN)}}" target="_blank" > CLCIK AQUÍ PARA VER EL DOCUMENTO</a>
                </div>
            </div>
        </div>
        @else
        <div class="row">
            <div class="col-sm-6">
                <a class="btn btn-info" style="color: white" data-toggle="modal" data-target="#modal-ES">
                    Ver contenido (ES)
                </a>
            </div>
            <div class="col-sm-6">
                <a class="btn btn-info" style="color: white" data-toggle="modal" data-target="#modal-EN">
                    Ver contenido (EN)
                </a>
            </div>
        </div>


        <div class="modal fade" id="modal-ES" tabindex="-1" role="dialog" aria-labelledby="exampleModalLongTitle"
                aria-hidden="true">
                <div class="modal-dialog" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h5 class="modal-title" id="exampleModalLongTitle">Contenido de la emisión (ES)</h5>
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                        <div class="modal-body">
                            {!! $configuracion->value_ES !!}
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal fade" id="modal-EN" tabindex="-1" role="dialog" aria-labelledby="exampleModalLongTitle2"
                aria-hidden="true">
                <div class="modal-dialog" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h5 class="modal-title" id="exampleModalLongTitle2">Contenido de la emisión (EN)</h5>
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                        <div class="modal-body">
                            {!! $configuracion->value_EN !!}
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
                        </div>
                    </div>
                </div>
            </div>


        @endif
    </div>
		<div class="card-footer card-footer-mostrar ">

      <a style="margin-right: 5px;" href="{{url('configuraciones')}}/{{$configuracion->id}}/edit" class="btn btn-lg btn-default right">Modificar</a>
      <a  href="{{url('configuraciones')}}" class="btn btn-lg btn-default left">Salir</a>
		</div>

    <!-- /.card-body -->
  </div>
@stop

@section('css')

@stop

@section('js')

    <script>


      @if(Session::has('modificado'))
					Swal.fire(
							'¡Modificado!',
						'El gasto fue modificado con éxito.',
						'success'
					)
			@endif

    </script>
@stop
