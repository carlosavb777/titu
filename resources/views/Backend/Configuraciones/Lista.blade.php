@extends('adminlte::page')

@section('title', 'Dashboard')

@section('content_header')
    <h1>Modificar Configuraciones</h1>
@stop

@section('content')
<div class="row">
	<div class="col-sm-12" >

			<div class="card">
					<div class="card-header">
						<h3 class="card-title">Selecciona una configuracón de la lista para ver su detalle. </h3>
					</div>
					<!-- /.card-header -->
					<div class="card-body">
							<table id="tabla-preguntas" class="table table-bordered table-striped dataTable dtr-inline" role="grid" aria-describedby="example1_info">
							<thead>
                                <th>Configuración</th>

								<th>Acciones</th>
							</thead>
							<tbody>
								@foreach ($configuraciones as $configuracion)
								<tr>
									<td>{{ $configuracion->key }}</td>

									<td style="float:right">
										<a href="{{url("configuraciones")}}/{{$configuracion->id}}" ><i class="iconos-lista fas fa-eye"></i></a>
										<a href="{{url("configuraciones")}}/{{$configuracion->id}}/edit" ><i class="iconos-lista fas fa-edit"></i></a>
										  </td>
								</tr>

								@endforeach

							</tfoot>
						</table>
					</div>
					<!-- /.card-body -->
			</div>
	</div>
</div>

@stop

@section('css')

@stop
@section('plugins.Datatables', true)
@section('js')
    <script>
			$(document).ready( function () {
					$('#tabla-preguntas').DataTable({
						language: {
									url: 'https://cdn.datatables.net/plug-ins/1.10.22/i18n/Spanish.json'
							},
							responsive: true
					});



			});

	</script>
@stop
