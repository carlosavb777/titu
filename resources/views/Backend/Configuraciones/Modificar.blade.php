@extends('adminlte::page')

@section('title', 'Dashboard')

@section('content_header')
    <h1>Modificar Configuración General</h1>
    <link href="https://cdn.jsdelivr.net/npm/summernote@0.8.18/dist/summernote-bs4.min.css" rel="stylesheet">
@stop

@section('content')
<div class="card card-primary">
    <div class="card-header">
      <h3 class="card-title">Edite los campos.</h3>
    </div>
    <!-- /.card-header -->
    <div class="card-body">
      <form action="{{url('configuraciones')}}" method="POST" enctype="multipart/form-data">
				@csrf
        <input type="hidden" name="id" value="{{$configuracion->id}}" >
        <div class="row">
            <div class="col-sm-12">
                <div class="form-group">
                  <label>Configuración</label>
                  <input type="text" name="" value="{{$configuracion->key}}"  disabled readonly class="form-control" >
                </div>
            </div>
        </div>
        @if($configuracion->type == 'TEXT')


            <div class="row" >
                <div class="col-6">
                    <div class="form-group">
                        <label>Valor (ES)</label><br>
                        <input type="text" name="value_ES" required value="{{$configuracion->value_ES}}"  class="form-control"  >
                    </div>
                </div>
                <div class="col-6">
                    <div class="form-group">
                        <label>Valor (EN)</label><br>
                        <input type="text" name="value_EN" required value="{{$configuracion->value_EN}}"  class="form-control"  >
                    </div>
                </div>
            </div>

        @elseif($configuracion->type == 'TEXTAREA' || $configuracion->type == 'EDITOR')

        <div class="row" >
            <div class="col-12">
                <div class="form-group">
                    <label>Valor (ES)</label><br>
                    <textarea name="value_ES" id=""  class="form-control    @if($configuracion->type == 'EDITOR') summernote @endif" >{{$configuracion->value_ES}}</textarea>
                </div>
            </div>
        </div>
        <div class="row" >

            <div class="col-12">
                <div class="form-group">
                    <label>Valor (EN)</label><br>
                    <textarea name="value_EN" id="" class="form-control @if($configuracion->type == 'EDITOR') summernote @endif" >{{$configuracion->value_EN}}</textarea>
                </div>
            </div>
        </div>

        @elseif($configuracion->type == 'FILE')

        <div class="row" >
            <div class="col-6">
                <div class="form-group">
                    <label>Adjuntar documento (ES)</label><a href="{{asset($configuracion->value_ES)}}" target="_blank" > CLCIK AQUÍ PARA VER EL DOCUMENTO ACTUAL</a>
                    <div class="input-group">
                        <div class="custom-file">
                            <input type="file" name="archivo_ES" class="custom-file-input" id="exampleInputFile">
                            <label class="custom-file-label" for="exampleInputFile"></label>
                        </div>
                        <div class="input-group-append">
                            <span class="input-group-text">Subir</span>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-6">
                <div class="form-group">
                    <label>Adjuntar documento (EN)</label><a href="{{asset($configuracion->value_EN)}}" target="_blank" > CLCIK AQUÍ PARA VER EL DOCUMENTO ACTUAL</a>

                    <div class="input-group">
                        <div class="custom-file">
                            <input type="file" name="archivo_EN" class="custom-file-input" id="exampleInputFile2">
                            <label class="custom-file-label" for="exampleInputFile2"></label>
                        </div>
                        <div class="input-group-append">
                            <span class="input-group-text">Subir</span>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        @endif

    </div>
		<div class="card-footer">
			<button type="submit" class="btn btn-lg btn-primary right">Guardar</button>
      <a  href="{{url('configuraciones')}}" class="btn btn-lg btn-default left">Salir</a>
		</div>
	</form>
    <!-- /.card-body -->
  </div>
@stop

@section('css')

@stop

@section('js')
<script src="{{ asset('js/input-file-custom.js') }}"></script>
<script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js" integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" crossorigin="anonymous"></script>
<script src="https://cdn.jsdelivr.net/npm/summernote@0.8.18/dist/summernote-bs4.min.js"></script>
<script>
    $(function() {
        bsCustomFileInput.init();
        $('.summernote').summernote({
                    toolbar: [
                            ['style', ['style']],
                            ['font', ['bold', 'underline', 'clear']],
                            ['fontname', ['fontname']],
                            ['color', ['color']],
                            ['para', ['ul', 'ol', 'paragraph']],
                            ['table', ['table']],
                            ['insert', ['link', 'picture']],
                            ['view', ['fullscreen', 'help']],
                            ],
                    height: 300
                });
    });
</script>
@stop
