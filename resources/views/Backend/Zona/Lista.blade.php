@extends('adminlte::page')

@section('title', 'Dashboard')

@section('content_header')
    <h1>Zonas</h1>
@stop

@section('content')

<div class="row"> 
	<div class="col-sm-12" >

			<div class="card">
				<div class="card-header">
					<h3 class="card-title">Filtros</h3>
				</div>
				<div class="card-body">
					<div class="row">
						<div class="col-sm-6">
							<div class="form-group">
								<label for="departamentos">Departamentos</label>
								<select required id="departamentos" name="departamento" class="form-control selector">
									<option value="0"  selected >Todos</option>
									@foreach ($departamentos as $departamento)
									<option  @if($departamento->id == $departamento_id) selected @endif value="{{$departamento->id}}" class="departamento" >{{ $departamento->nombre}}</option>
									@endforeach
								</select>
							</div>
						</div>
						<div class="col-sm-6">
							<div class="form-group">
								<label for="municipios">Municipios</label>
								<select required id="municipios" name="municipio" class="form-control selector">
									<option  selected >Todos</option>
									@foreach ($municipios as $municipio)
										<option @if($municipio->id == $municipio_id) selected @endif  value="{{$municipio->id}}" class="municipio" >{{ $municipio->nombre}}</option>
									@endforeach
								</select>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
</div>


<div class="row"> 
	<div class="col-sm-12" >

			<div class="card">
					<div class="card-header">
						<h3 class="card-title">Selecciona una zona de la lista para ver su detalle. </h3>
						<a href="{{url("zonas/create")}}" class="btn btn-block btn-primary btn-sm btn-nuevo">Nueva zona</a>
					</div>
					<!-- /.card-header -->
					<div class="card-body">
							<table id="tabla-zonas" class="table table-bordered table-striped dataTable dtr-inline" role="grid" aria-describedby="example1_info">
							<thead>
								<th>Nombre</th>
								<th>Departamento</th>
								<th>Municipio</th>								
								<th>Acciones</th>
							</thead>
							<tbody>
								@foreach ($zonas as $zona)
								<tr>
									<td>{{ $zona->nombre }}</td>
									<td>{{ $zona->departamento}}</td>
									<td>{{ $zona->municipio }}</td>
									<td style="float:right">
										<a href="{{url("zonas")}}/{{$zona->id}}" ><i class="iconos-lista fas fa-eye"></i></a> 
										<a href="{{url("zonas")}}/{{$zona->id}}/edit" ><i class="iconos-lista fas fa-edit"></i></a>
										<a href="#" class="btn-eliminar" data-id="{{$zona->id}}" ><i class="iconos-lista fas fa-trash"></i></a>  </td>
								</tr>

								@endforeach

							</tfoot>
						</table>
					</div>
					<!-- /.card-body -->
			</div>
	</div>
</div>
    
@stop

@section('css')
    
@stop
@section('plugins.Datatables', true)
@section('js')
    <script>
			$(document).ready( function () {
					$('#tabla-zonas').DataTable({
						language: {
									url: 'https://cdn.datatables.net/plug-ins/1.10.22/i18n/Spanish.json'
							},
							responsive: true
					});

					$( ".btn-eliminar" ).on( "click", function() {
						var id = $(this).data("id");
        		alerta_eliminar("eliminar_zona/"+id, titulo = "Eliminar zona", texto="¿Realmente quieres eliminar esta zona?");

					});

					$( "#departamentos" ).on( "change", function() {
						var id = $(this).val();
        		window.location.href = APP_URL+"/filtros/"+id;

					});

					$( "#municipios" ).on( "change", function() {
						var id = $(this).val();
						var departamento_id = $("#departamentos").val();
        		window.location.href = APP_URL+"/filtros/"+departamento_id+"/"+id;

					});

					@if(Session::has('eliminado'))
					Swal.fire(
							'¡Eliminada!',
						'La zona fue eliminada.',
						'success'
					)
					@endif

			});

	</script>
@stop