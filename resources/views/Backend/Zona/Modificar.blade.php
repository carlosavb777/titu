@extends('adminlte::page')

@section('title', 'Dashboard')

@section('content_header')
    <h1>Modificar zona</h1>
@stop

@section('content')
<div class="card card-primary">
    <div class="card-header">
      <h3 class="card-title">Edite los campos.</h3>
    </div>
    <!-- /.card-header -->
    <div class="card-body">
      <form action="{{url('guardar_zona')}}" method="POST">
				@csrf
        <input type="hidden" name="id" value="{{$zona->id}}">
        <div class="row">
          <div class="col-sm-12">
            <div class="form-group">
              <label>Nombre</label>
              <input type="text" name="nombre" value="{{$zona->nombre}}" maxlength="64" required class="form-control" placeholder="Ingrese un nombre" >
            </div>
          </div>
        </div>
        <div class="row">
          <div class="col-sm-6">
            <div class="form-group">
              <label for="departamentos">Departamentos</label>
              <select required id="departamentos" name="departamento" class="form-control selector">
                <option  disabled selected>Seleccione un departamento</option>
                @foreach ($departamentos as $departamento)
                <option @if($departamento->id == $zona->departamento_id) selected @endif  value="{{$departamento->id}}" class="departamento" >{{ $departamento->nombre}}</option>
                @endforeach
              </select>
            </div>
          </div>
          <div class="col-sm-6">
            <div class="form-group">
              <label for="municipios">Municipios</label>
              <select required id="municipios" name="municipio" class="form-control selector">
                <option  disabled selected>Seleccione un municipio</option>
                @foreach ($municipios as $municipio)
                  <option @if($municipio->id == $zona->municipio_id) selected @endif  value="{{$municipio->id}}" class="municipio" >{{ $municipio->nombre}}</option>
                @endforeach
              </select>
            </div>
          </div>
        </div>
				
			
        {{-- <div class="row">
					<div class="col-sm-12">
					</div>
				</div> --}}
       
    </div>
		<div class="card-footer">
			<button type="submit" class="btn btn-lg btn-primary right">Guardar</button>
      <a  href="{{url('zonas')}}" class="btn btn-lg btn-default left">Salir</a>
		</div>
	</form>
    <!-- /.card-body -->
  </div>
@stop

@section('css')
    
@stop

@section('js')
    <script> 
    
    $( "#departamentos" ).on( "change", function() {
        var departamento_id = $(this).val();
       obtener_municipios(departamento_id);

    });
    </script>
@stop