@extends('adminlte::page')

@section('title', 'Dashboard')

@section('content_header')
    <h1>Zona</h1>
@stop

@section('content')
<div class="card card-success">
    <div class="card-header">
      <h3 class="card-title">Detalles de la zona.</h3>
    </div>
    <!-- /.card-header -->
    <div class="card-body">
      
				@csrf
        <div class="row">
          <div class="col-sm-6">
            <!-- text input -->
            <div class="form-group">
              <label>Nombre</label>
             <p>{{$zona->nombre}}</p>
            </div>
          </div>
          <div class="col-sm-6">
            <div class="form-group">
              <label>Departamento</label>
              <p>{{$zona->departamento}}</p>
            </div>
          </div>
        </div>
				<div class="row">
					<div class="col-sm-12">
						<!-- textarea -->
						<div class="form-group">
							<label>Municipio</label>
              <p>{{$zona->municipio}}</p>
						</div>
					</div>
				</div>
				{{-- <div class="row">
					<div class="col-sm-12">

					</div>
				</div> --}}
       
     
    </div>
		<div class="card-footer card-footer-mostrar ">
    
      <a  href="{{url('zonas/create')}}" class="btn btn-lg btn-primary right">Nueva zona</a>
      <a style="margin-right: 5px;" href="{{url('zonas')}}/{{$zona->id}}/edit" class="btn btn-lg btn-default right">Modificar</a>
      <button style="margin-right: 5px;" type="button" id="eliminar" data-id="{{$zona->id}}" class="btn btn-lg btn-danger right">Eliminar</button>
      <a  href="{{url('zonas')}}" class="btn btn-lg btn-default left">Salir</a>				
		</div>

    <!-- /.card-body -->
  </div>
@stop

@section('css')
    
@stop

@section('js')

    <script>

    
      $( "#eliminar" ).on( "click", function() {
       var id = $(this).data("id");
        alerta_eliminar("eliminar_zona/"+id, titulo = "Eliminar zona", texto="¿Realmente quieres eliminar este zona?");
      });
    
    
    </script>
@stop