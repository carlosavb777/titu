@extends('adminlte::page')

@section('title', 'Dashboard')

@section('content_header')
    <h1>Circulares</h1>
@stop

@section('content')
<div class="row">
	<div class="col-sm-12" >

			<div class="card">
					<div class="card-header">
						<h3 class="card-title">Selecciona un elemento de la lista para ver su detalle. </h3>
						<a href="{{url("circulares/create")}}" class="btn btn-block btn-primary btn-sm btn-nuevo">Nueva circular</a>
					</div>
					<!-- /.card-header -->
					<div class="card-body">
							<table id="tabla-circulares" class="table table-bordered table-striped dataTable dtr-inline" role="grid" aria-describedby="example1_info">
							<thead>
                                <th>Fecha</th>
								<th>Circular</th>
								<th>Categoria</th>
								<th>Acciones</th>
							</thead>
							<tbody>
								@foreach ($circulares as $circular)
								<tr>
                                    <td>{{ date_format($circular->created_at, 'd-m-Y')  }}</td>
									<td>{{ $circular->titulo_ES }}</td>
                                    <td>{{ $circular->categoria }}</td>
									<td style="float:right">
										<a href="{{url("circulares")}}/{{$circular->id}}" ><i class="iconos-lista fas fa-eye"></i></a>
										<a href="{{url("circulares")}}/{{$circular->id}}/edit" ><i class="iconos-lista fas fa-edit"></i></a>
										<a href="#" class="btn-eliminar" data-id="{{$circular->id}}" ><i class="iconos-lista fas fa-trash"></i></a>  </td>
								</tr>

								@endforeach

							</tbody>
						</table>
					</div>
					<!-- /.card-body -->
			</div>
	</div>
</div>

@stop

@section('css')

@stop
@section('plugins.Datatables', true)
@section('js')
    <script>
			$(document).ready( function () {
					$('#tabla-circulares').DataTable({
						language: {
									url: 'https://cdn.datatables.net/plug-ins/1.10.22/i18n/Spanish.json'
							},
							responsive: true
					});

					$( ".btn-eliminar" ).on( "click", function() {
						var id = $(this).data("id");
        		    alerta_eliminar("eliminar_circulares/"+id, titulo = "Eliminar circular", texto="¿Realmente quieres eliminar esta circular?");

					});

					@if(Session::has('eliminado'))
					Swal.fire(
							'¡Eliminada!',
						'La circular fue eliminada.',
						'success'
					)
					@endif

			});

	</script>
@stop
