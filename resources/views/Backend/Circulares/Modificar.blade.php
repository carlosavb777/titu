@extends('adminlte::page')

@section('title', 'Emisiones')

@section('content_header')
    <h1>Modificar la circular</h1>
    <link href="https://cdn.jsdelivr.net/npm/summernote@0.8.18/dist/summernote-bs4.min.css" rel="stylesheet">
@stop

@section('content')
    <div class="card card-primary">
        <div class="card-header">
            <h3 class="card-title">Edite los campos.</h3>
        </div>
            <!-- /.card-header -->
            <div class="card-body">
                <form action="{{ url('circulares') }}" method="POST" enctype="multipart/form-data">
                    @csrf
                    <div class="row">
                        <input type="hidden" name="id" value="{{$circular->id}}" >
                        <div class="col-sm-12">
                            <div class="form-group">
                                <label for="categorias">Cartegoría</label>
                                <select class="form-control" required name="categoria" id="categorias">
                                    @foreach ($categorias as $categoria )
                                        <option @if ($circular->categoria_id == $categoria->id) selected @endif  value="{{$categoria->id}}">{{$categoria->categoria_ES}}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-12">
                            <div class="form-group">
                                <label>Titulo (ES)</label>
                                <input type="text" value="{{$circular->titulo_ES}}" name="titulo_ES" maxlength="512" required class="form-control"
                                    placeholder="Ingrese un titulo">
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-12">
                            <label for="DC" >Descripción corta (ES)</label><br>
                            <textarea id="DC" name="descripcion_corta_ES" class="form-control" >{{$circular->descripcion_corta_ES}}</textarea>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-12">
                            <label for="categorias">Contenido de la emisión (ES)</label><br>
                            <textarea name="contenido_ES" class="summernote">{{$circular->contenido_ES}}</textarea>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-12">
                            <div class="form-group">
                                <label>Titulo (EN)</label>
                                <input type="text" name="titulo_EN" value="{{$circular->titulo_EN}}" maxlength="512" required class="form-control"
                                    placeholder="Ingrese un titulo">
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-12">
                            <label >Descripción corta (EN)</label><br>
                            <textarea name="descripcion_corta_EN" class="form-control" >{{$circular->descripcion_corta_EN}}</textarea>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-12">
                            <label for="categorias">Contenido de la emisión (EN)</label><br>
                            <textarea name="contenido_EN" class="summernote">{{$circular->contenido_EN}}</textarea>
                        </div>
                    </div>

                    <div class="card-footer">
                        <button type="submit" class="btn btn-lg btn-primary right guardando">Guardar</button>
                        <a href="{{ url('circulares') }}" class="btn btn-lg btn-default left">Salir</a>
                    </div>
                </form>
                <!-- /.card-body -->
            </div>
        </div>

@stop

@section('css')

@stop

@section('js')
<script src="{{ asset('js/input-file-custom.js') }}"></script>
<script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js" integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" crossorigin="anonymous"></script>
<script src="https://cdn.jsdelivr.net/npm/summernote@0.8.18/dist/summernote-bs4.min.js"></script>
<script>
    $(function() {
        bsCustomFileInput.init();

        $('.summernote').summernote({
                    toolbar: [
                            ['style', ['style']],
                            ['font', ['bold', 'underline', 'clear']],
                            ['fontname', ['fontname']],
                            ['color', ['color']],
                            ['para', ['ul', 'ol', 'paragraph']],
                            ['table', ['table']],
                            ['insert', ['link', 'picture']],
                            ['view', ['fullscreen', 'help']],
                            ],
                    height: 300
                });







    });
</script>
@stop
