@extends('adminlte::page')

@section('title', 'Categorias')

@section('content_header')
    <h1>Modificar la Categoría</h1>
@stop

@section('content')
<div class="card card-primary">
    <div class="card-header">
      <h3 class="card-title">Edite los campos.</h3>
    </div>
    <!-- /.card-header -->
    <div class="card-body">
      <form action="{{url('informes_financieros')}}" method="POST" enctype="multipart/form-data">
				@csrf
                <div class="row">
                    <div class="col-sm-6">
                        <div class="form-group">
                            <label for="anio">Año {{$informe->anio}} </label>
                            <select class="form-control" required name="anio" id="anio">
                                <option disabled  >Seleccione una año</option>

                                @for ($i=2020; $i<2030; $i++)
                                <option @if ($informe->anio == $i) selected="selected" @endif  value="{{$i}}">{{$i}}</option>
                                @endfor



                            </select>
                        </div>
                    </div>
                    <div class="col-sm-6">
                        <div class="form-group">
                            <label for="categoria">Categorías</label>
                            <select class="form-control" required name="categoria_id" id="categoria">
                                <option disabled selected >Seleccione una opción</option>
                                @foreach ($categorias as $categoria)
                                <option {{ $informe->informes_financieros_categoria_id == $categoria->id ? 'selected' : '' }} value="{{$categoria->id}}">{{$categoria->categoria_ES}}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-sm-6">
                        <div class="form-group">
                          <label>Nombre documento (ES)</label>
                          <input type="text" name="nombre_ES" value="{{$informe->nombre_ES}}" maxlength="512" required class="form-control" placeholder="Ingrese una nombre" >
                        </div>
                      </div>
                      <div class="col-sm-6">
                        <div class="form-group">
                            <label>Adjuntar documento (ES)   <a href="{{asset($informe->url_ES)}}" target="_blank" rel="noopener noreferrer">CLCIK AQUÍ PARA VER EL DOCUMENTO ACTUAL ES</a></label>

                            <div class="input-group">
                                <div class="custom-file">
                                    <input type="file" name="archivo_ES" class="custom-file-input" id="exampleInputFile3">
                                    <label class="custom-file-label" for="exampleInputFile3"></label>
                                </div>
                                <div class="input-group-append">
                                    <span class="input-group-text">Subir</span>
                                </div>
                            </div>
                        </div>
                    </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-6">
                            <div class="form-group">
                              <label>Nombre documento (EN)</label>
                              <input type="text" name="nombre_EN" value="{{$informe->nombre_EN}}" maxlength="512" required class="form-control" placeholder="Ingrese una nombre" >
                            </div>
                          </div>
                          <div class="col-sm-6">
                            <div class="form-group">
                                <label>Adjuntar documento (EN)   <a href="{{asset($informe->url_EN)}}" target="_blank" rel="noopener noreferrer">CLCIK AQUÍ PARA VER EL DOCUMENTO ACTUAL EN</a></label>
                                <div class="input-group">
                                    <div class="custom-file">
                                        <input type="file" name="archivo_EN" class="custom-file-input" id="exampleInputFile2">
                                        <label class="custom-file-label" for="exampleInputFile2"></label>
                                    </div>
                                    <div class="input-group-append">
                                        <span class="input-group-text">Subir</span>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
    </div>
		<div class="card-footer">
			<button type="submit" class="btn btn-lg btn-primary right">Guardar</button>
      <a  href="{{url('informes_financieros')}}" class="btn btn-lg btn-default left">Salir</a>
		</div>
	</form>
    <!-- /.card-body -->
  </div>
@stop

@section('css')

@stop

@section('js')
    <script> </script>
@stop
