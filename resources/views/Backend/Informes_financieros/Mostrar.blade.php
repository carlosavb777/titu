@extends('adminlte::page')

@section('title', 'Dashboard')

@section('content_header')
    <h1>Categoría</h1>
@stop

@section('content')
<div class="card card-success">
    <div class="card-header">
      <h3 class="card-title">Detalles de la categoría</h3>
    </div>
    <!-- /.card-header -->
    <div class="card-body">

        <div class="row">
            <div class="col-sm-6">
                <div class="form-group">
                    <label for="cartera">Año</label>
                    <p>{{$informe->anio}}</p>
                </div>
            </div>
            <div class="col-sm-6">
                <div class="form-group">
                    <label for="categoria">Categorías</label>
                    <p>{{$categoria->categoria_ES}}</p>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-sm-6">
                <div class="form-group">
                  <label>Nombre documento (ES)</label>
                  <p>{{$informe->nombre_ES}}</p>
                </div>
              </div>
              <div class="col-sm-6">
                <div class="form-group">
                    <label>Adjuntar documento (ES)</label><br>
                    <a href="{{asset($informe->url_ES)}}" target="_blank" rel="noopener noreferrer">CLCIK AQUÍ PARA VER EL DOCUMENTO ES</a>
                </div>
            </div>
            </div>
            <div class="row">
                <div class="col-sm-6">
                    <div class="form-group">
                      <label>Nombre documento (EN)</label>
                      <p>{{$informe->nombre_EN}}</p>
                    </div>
                  </div>
                  <div class="col-sm-6">
                    <div class="form-group">
                        <label>Adjuntar documento (EN)</label><br>
                        <a href="{{asset($informe->url_EN)}}" target="_blank" rel="noopener noreferrer">CLCIK AQUÍ PARA VER EL DOCUMENTO EN</a>
                    </div>
                </div>
                </div>


    </div>
		<div class="card-footer card-footer-mostrar ">

      <a  href="{{url('informes_financieros/create')}}" class="btn btn-lg btn-primary right">Nuevo informe</a>
      <a style="margin-right: 5px;" href="{{url('informes_financieros')}}/{{$informe->id}}/edit" class="btn btn-lg btn-default right">Modificar</a>
      <button style="margin-right: 5px;" type="button" id="eliminar" data-id="{{$informe->id}}" class="btn btn-lg btn-danger right">Eliminar</button>
      <a  href="{{url('informes_financieros')}}" class="btn btn-lg btn-default left">Salir</a>
		</div>

    <!-- /.card-body -->
  </div>
@stop

@section('css')

@stop

@section('js')

    <script>


      $( "#eliminar" ).on( "click", function() {
       var id = $(this).data("id");
        alerta_eliminar("eliminar_informes_financieros/"+id, titulo = "Eliminar informe", texto="¿Realmente quieres eliminar este informe?");
      });

      @if(Session::has('nuevo'))
					Swal.fire(
							'¡Creado!',
						'El informe fue creado con éxito.',
						'success'
					)
			@endif

      @if(Session::has('modificado'))
					Swal.fire(
							'¡Modificado!',
						'El informe fue modificado con éxito.',
						'success'
					)
			@endif

    </script>
@stop
