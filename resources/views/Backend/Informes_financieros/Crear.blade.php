@extends('adminlte::page')

@section('title', 'Dashboard')

@section('content_header')
    <h1>Nuev informe</h1>
@stop

@section('content')
<div class="card card-primary">
    <div class="card-header">
      <h3 class="card-title">Complete los campos.</h3>
    </div>
    <!-- /.card-header -->
    <div class="card-body">
      <form action="{{url('informes_financieros')}}" method="POST" enctype="multipart/form-data">
		@csrf
        <div class="row">
            <div class="col-sm-6">
                <div class="form-group">
                    <label for="cartera">Año</label>
                    <select class="form-control" required name="anio" id="cartera">
                        <option disabled selected >Seleccione una año</option>
                        <option value="2020">2020</option>
                        <option value="2021">2021</option>
                        <option value="2022">2022</option>
                        <option value="2023">2023</option>
                        <option value="2024">2024</option>
                        <option value="2025">2025</option>
                        <option value="2026">2026</option>
                        <option value="2027">2027</option>
                        <option value="2028">2028</option>
                    </select>
                </div>
            </div>
            <div class="col-sm-6">
                <div class="form-group">
                    <label for="categoria">Categorías</label>
                    <select class="form-control" required name="categoria_id" id="categoria">
                        <option disabled selected >Seleccione una opción</option>
                        @foreach ($categorias as $categoria)
                        <option value="{{$categoria->id}}">{{$categoria->categoria_ES}}</option>
                        @endforeach
                    </select>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-sm-6">
                <div class="form-group">
                  <label>Nombre documento (ES)</label>
                  <input type="text" name="nombre_ES" maxlength="512" required class="form-control" placeholder="Ingrese una nombre" >
                </div>
              </div>
              <div class="col-sm-6">
                <div class="form-group">
                    <label>Adjuntar documento (ES)</label>

                    <div class="input-group">
                        <div class="custom-file">
                            <input type="file" name="archivo_ES" class="custom-file-input" id="exampleInputFile2">
                            <label class="custom-file-label" for="exampleInputFile2"></label>
                        </div>
                        <div class="input-group-append">
                            <span class="input-group-text">Subir</span>
                        </div>
                    </div>
                </div>
            </div>
            </div>
            <div class="row">
                <div class="col-sm-6">
                    <div class="form-group">
                      <label>Nombre documento (EN)</label>
                      <input type="text" name="nombre_EN" maxlength="512" required class="form-control" placeholder="Ingrese una nombre" >
                    </div>
                  </div>
                  <div class="col-sm-6">
                    <div class="form-group">
                        <label>Adjuntar documento (ES)</label>
                        <div class="input-group">
                            <div class="custom-file">
                                <input type="file" name="archivo_EN" class="custom-file-input" id="exampleInputFile2">
                                <label class="custom-file-label" for="exampleInputFile2"></label>
                            </div>
                            <div class="input-group-append">
                                <span class="input-group-text">Subir</span>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

        <div class="card-footer">
          <button type="submit" class="btn btn-lg btn-primary right guardando">Guardar</button>
          <a  href="{{url('informes_financieros')}}" class="btn btn-lg btn-default left">Salir</a>
        </div>
	  </form>
    <!-- /.card-body -->
  </div>
@stop

@section('css')

@stop

@section('js')
    <script src="{{ asset('js/input-file-custom.js') }}"></script>
    <script>
        $(function() {
            bsCustomFileInput.init();
        });
    </script>
@stop
