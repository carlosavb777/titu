@extends('adminlte::page')

@section('title', 'Dashboard')

@section('content_header')
    <h1>Informes</h1>
@stop

@section('content')
<div class="row">
	<div class="col-sm-12" >

			<div class="card">
					<div class="card-header">
						<h3 class="card-title">Selecciona un informe de la lista para ver su detalle. </h3>
						<a href="{{url("informes_financieros/create")}}" class="btn btn-block btn-primary btn-sm btn-nuevo">Nuevo informe</a>
					</div>
					<!-- /.card-header -->
					<div class="card-body">
							<table id="tabla-categorias" class="table table-bordered table-striped dataTable dtr-inline" role="grid" aria-describedby="example1_info">
							<thead>
                                <th>Categoría</th>
                                <th>Nombre</th>
                                <th>Anio</th>
								<th>Acciones</th>
							</thead>
							<tbody>
								@foreach ($informes as $informe )
								<tr>
                                    <td>{{ $informe->categoria }}</td>
                                    <td>{{ $informe->nombre_ES }}</td>
                                    <td>{{ $informe->anio }}</td>
									<td style="float:right">
										<a href="{{url("informes_financieros")}}/{{$informe->id}}" ><i class="iconos-lista fas fa-eye"></i></a>
										<a href="{{url("informes_financieros")}}/{{$informe->id}}/edit" ><i class="iconos-lista fas fa-edit"></i></a>
										<a href="#" class="btn-eliminar" data-id="{{$informe->id}}" ><i class="iconos-lista fas fa-trash"></i></a>  </td>
								</tr>

								@endforeach

							</tfoot>
						</table>
					</div>
					<!-- /.card-body -->
			</div>
	</div>
</div>

@stop

@section('css')

@stop
@section('plugins.Datatables', true)
@section('js')
    <script>
			$(document).ready( function () {
					$('#tabla-categorias').DataTable({
						language: {
									url: 'https://cdn.datatables.net/plug-ins/1.10.22/i18n/Spanish.json'
							},
							responsive: true
					});

					$( ".btn-eliminar" ).on( "click", function() {
						var id = $(this).data("id");
        		    alerta_eliminar("eliminar_informes_financieros/"+id, titulo = "Eliminar informe", texto="¿Realmente quieres eliminar este informe?");

					});

					@if(Session::has('eliminado'))
					Swal.fire(
							'¡Eliminado!',
						'El informe fue eliminado.',
						'success'
					)
					@endif

			});

	</script>
@stop
