@extends('adminlte::master')

@inject('layoutHelper', 'JeroenNoten\LaravelAdminLte\Helpers\LayoutHelper')

@if($layoutHelper->isLayoutTopnavEnabled())
    @php( $def_container_class = 'container' )
@else
    @php( $def_container_class = 'container-fluid' )
@endif

@section('adminlte_css')
    @stack('css')
    @yield('css')
    <link rel="stylesheet" href="{{ asset('css/backend/backend_style.css') }}">
		<link href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css" rel="stylesheet">

@stop

@section('classes_body', $layoutHelper->makeBodyClasses())

@section('body_data', $layoutHelper->makeBodyData())

@section('body')

    <div class="wrapper">

        {{-- Top Navbar --}}
        @if($layoutHelper->isLayoutTopnavEnabled())
            @include('adminlte::partials.navbar.navbar-layout-topnav')
        @else
            @include('adminlte::partials.navbar.navbar')
        @endif

        {{-- Left Main Sidebar --}}
        @if(!$layoutHelper->isLayoutTopnavEnabled())
            @include('adminlte::partials.sidebar.left-sidebar')
        @endif

        {{-- Content Wrapper --}}
        <div class="content-wrapper {{ config('adminlte.classes_content_wrapper') ?? '' }}">


		 {{--			<div class="alert alert-warning" role="alert">
						<h4 class="alert-heading">Hola {{Auth::user()->nombres}}!</h4>
						<p>El sistema se está actualizando, es posible se presenten errores durante este proceso sin embargo la información ya ingresada esta resguardada con seguridad.
<br>
							Esto puede durar varios minutos, agradecemos tu comprensión.  </p>
						<hr>
						<p class="mb-0">Comunícate con el administrador para mayor información.</p>
					</div>



            Content Header

						<div class="alert alert-info" role="alert">
							<h4 class="alert-heading">Hola {{Auth::user()->nombres}}!</h4>
							<p>El sistema se ha actualizado, Si ocurre un error avisa de inmediato al administrador.
						<br>
								En este nueva actualización se incluye el modulo de CUENTAS POR COBRAR. Desde este momento todos los abonos parciales o totales a las deudas de los clientes deber ser agregados en ese modulo. <br>
						<br>

							</p>
							<hr>
							<p class="mb-0">Comunícate con el administrador para mayor información.</p>
						</div>--}}


            <div class="content-header">
                <div class="{{ config('adminlte.classes_content_header') ?: $def_container_class }}">
                    @yield('content_header')
                </div>
            </div>

            {{-- Main Content --}}
            <div class="content">
                <div class="{{ config('adminlte.classes_content') ?: $def_container_class }}">
                    @yield('content')
                </div>
            </div>

        </div>

        {{-- Footer --}}
        @hasSection('footer')
            @include('adminlte::partials.footer.footer')
        @endif

        {{-- Right Control Sidebar --}}
        @if(config('adminlte.right_sidebar'))
            @include('adminlte::partials.sidebar.right-sidebar')
        @endif

    </div>
@stop

@section('adminlte_js')
<script src="https://cdn.jsdelivr.net/npm/sweetalert2@10.15.5/dist/sweetalert2.all.min.js" ></script>
<script> var APP_URL = '{{ url("/") }}'; </script>

<script>


$(document).ready(function () {

    $(document).on('keyup', '.numerico', function(event) {
                    event.preventDefault();
                    $(this).val($(this).val().replace( /[^0-9]/g, "" ));
                });

    $(document).on('keyup', '.numerico-dinero', function(event) {
                    event.preventDefault();
                    $(this).val($(this).val().replace( /[^0-9.]/g, "" ));
                });



								$('.selector').select2();

});
    function alerta_eliminar(uri, titulo = "Eliminar registro", texto="¿Realmente quieres eliminar este registro?"){

        Swal.fire({
          title: titulo,
          text: texto,
          icon: 'warning',
          showCancelButton: true,
          confirmButtonColor: '#3085d6',
          cancelButtonColor: '#d33',
          confirmButtonText: 'Si, ¡Eliminar!',
          cancelButtonText: 'Cancelar'
        }).then((result) => {
          if (result.isConfirmed) {
              eliminando();
            window.location.href = APP_URL+'/'+uri;

          }
        })
    }









        function obtener_municipios(departamento_id) {

        $.ajax({
                url:  APP_URL +"/municipios",
                type: 'POST',
                data: {
                    id: departamento_id,
                    '_token': $('meta[name="csrf-token"]').attr('content')
                },
                })
            .done(function(data) {

                data = jQuery.parseJSON(data);

                options = '<option disabled selected>Seleccione un municipio</option>';
                if ( data.length !== 0) {
                $.each(data, function(index, val) {
                    // if (index !== 1) {
                        options += '<option data-nombre='+val.nombre+' value="' + val.id + '">' + val.nombre + '</option>';
                    // }
                });
                }
                $('#municipios').empty().append(options);

            });

        }


    function obtener_zonas(municipio_id) {

    $.ajax({
            url:  APP_URL +"/zonas",
            type: 'POST',
            data: {
                id: municipio_id,
                '_token': $('meta[name="csrf-token"]').attr('content')
            },
            })
        .done(function(data) {

            data = jQuery.parseJSON(data);

            options = '<option disabled selected>Seleccione una zona</option>';
            if ( data.length !== 0) {
            $.each(data, function(index, val) {
                // if (index !== 1) {
                    options += '<option data-nombre='+val.nombre+' value="' + val.id + '">' + val.nombre + '</option>';
                // }
            });
            }
            $('#zonas').empty();

        });

    }



function guardando(){

let timerInterval
					Swal.fire({
						title: 'Guardando',
						html: 'Estamos registrando los datos ingresados.',
						timer: 9999,
						timerProgressBar: true,
						didOpen: () => {
							Swal.showLoading()
							timerInterval = setInterval(() => {
								const content = Swal.getContent()
								if (content) {
									const b = content.querySelector('b')
									if (b) {
										b.textContent = Swal.getTimerLeft()
									}
								}
							}, 100)
						},
						willClose: () => {
							clearInterval(timerInterval)
						}
					}).then((result) => {
						/* Read more about handling dismissals below */
						if (result.dismiss === Swal.DismissReason.timer) {
							console.log('I was closed by the timer')
						}
					})


}

function eliminando(){

let timerInterval
					Swal.fire({
						title: 'Eliminando',
						html: 'Estamos eliminando los datos.',
						timer: 9999,
						timerProgressBar: true,
						didOpen: () => {
							Swal.showLoading()
							timerInterval = setInterval(() => {
								const content = Swal.getContent()
								if (content) {
									const b = content.querySelector('b')
									if (b) {
										b.textContent = Swal.getTimerLeft()
									}
								}
							}, 100)
						},
						willClose: () => {
							clearInterval(timerInterval)
						}
					}).then((result) => {
						/* Read more about handling dismissals below */
						if (result.dismiss === Swal.DismissReason.timer) {
							console.log('I was closed by the timer')
						}
					})


}

    $(document).on('click', '.guardando', function(event) {
        guardando();

    });

    $(document).on('click', '.eliminando', function(event) {
        eliminando();

    });

</script>


    @stack('js')
    @yield('js')
@stop
