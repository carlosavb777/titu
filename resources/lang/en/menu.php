<?php

return [
    'nuestra-titularizadora' => 'Our securitization company',
    'accionistas' => 'Shareholders',
    'gobierno-corporativo' => 'Corporate Governance',
    'junta-directiva' => 'Board of Directors',
    'organizacion' => 'Company',
    'normatividad' => 'Regulation',
    'informacion-financiera' => 'Financial information',
    'riesgos' => 'Risks',
    'relacion-con-inversionistas' => 'Investor Relations',
    'buscar' => 'Search',
    'flujos-futuros' => 'Future flows',
    'liquidiacion-emision' => 'Liquidation of issuance',
    'informacion-emision' => 'Information of issuance',
    'informacion-activo-titularizado' => 'Information on the securitized asset',
    'universalidad' => 'Universality',
    'titulos' => 'Titles',
    'cartera-libranzas' => 'Payroll portfolio',
    'cartera-consumo-comercial' => 'Consumer and commercial portfolio',
    'cartera-vehiculos' => 'Vehicle portfolio',
    'emisiones-canceladas' => 'Canceled issuances',
];
