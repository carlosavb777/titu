<?php

return [

    //nav bar
    'nav-txt1' => 'Future flows',
    'nav-txt2' => 'Payroll portfolio',
    'nav-txt3' => 'Consumer and commercial portfolio',
    'nav-txt4' => 'Vehicle portfolio',
    'nav-txt5' => 'Canceled issuances',

    //modal
    'modal-txt1' =>
    'In accordance with the provisions of articles 5.2.3.1.4, 5.2.3.1.5 and 7.2.1.1.2 of Decree 2555 of 2010, at the time of accessing this information you declare that you are a qualified investor or that you are interested in consulting the information associated with the issuance of securities for general information purposes.',
    'modal-txt2' => 'Continue'
];
