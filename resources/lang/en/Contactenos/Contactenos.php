<?php

return [

    'txt1' => 'Contact us',
    'txt2' => 'Our experts are here to serve you',
    'txt3' => 'Direction',
    'txt4' => 'E-mail',
    'txt5' => 'Telephone',
    'txt6' => 'Write us and we will contact you soon!'

];