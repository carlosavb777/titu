<?php

return [
    'nosotros' => 'ABOUT TITULARICE',
    'educacion' => 'FINANCIAL EDUCATION',
    'emisiones' => 'ISSUANCES',
    'contactenos' => 'CONTACT US',
    'a_slide1' => 'Structuring optimal solutions',
    'a_slide2' => 'Creating value',
    'a_slide3' => 'An alternative offering',
    'slide1' => 'for our Originators',
    'slide2' => 'through securitization',
    'slide3' => 'of securities to the investors',
    'btncontacto' => 'Contact Us',

    'texto1' => 'About us',
    'texto2' => 'Innovation in',
    'texto3' => 'Securitization',
    'texto4' => "Titularice is a Colombian company, whose corporate purpose is the structuring, constitution, administration and legal representation of universalities
    of securitization. <br><br>
    Titularice, thanks to a team of highly qualified professionals, has as fundamental axes of his
    action the  innovation, to contribute to the commitment to the development of the region's capital markets, as well as
    such as rigor and commitment to the client in such a complex sector
    and sophisticated as that of securitization",
    'texto5' => 'Get to know us',
    'texto6' => 'Originator',
    'texto7' => 'Natural or Legal Person that owns assets that generate mathematically predictable flows and are susceptible to securitization.',
    'texto8' => 'Professional Investor',
    'texto9' => 'Legal Entity that has the necessary experience and knowledge to properly understand, evaluate and manage the risks inherent in any investment decision.',
    'texto10' => 'Debt titles',
    'texto11' => 'They are the instruments issued in the securities market, whose backing is the securitized asset and credit enhancers of the securitization structure.',
    'texto12' => 'Assets Susceptible to Securitization',
    'texto13' => 'They are assets that have a predictable behavior of generating cash flows.',
    'texto14' => 'Titularice',
    'texto15' => 'In search of the optimal solution, which adjusts to the needs of each originator, to develop the securities market and the country.',
    'texto16' => 'Our Experts',
    'texto17' => 'Meet the Titularice team',
    'texto18' => 'He has 22 years of experience in the securities and banking markets of El Salvador and the Central American region.',
    'texto19' => 'Joel Aguilar',
    'texto20' => 'Job position',
    'texto21' => 'He has more than 16 years of experience in the area of corporate finance and investment and commercial banking.',
    'texto22' => 'Joel Aguilar',
    'texto23' => 'Job position',
    'texto24' => 'He has more than 10 years of experience in corporate finance, banking and stock markets.',
    'texto25' => 'Joel Aguilar',
    'texto26' => 'Job position',
    'texto27' => 'Preview',
    'texto28' => 'Next',
    'texto29' => 'CONTACT US',
    'texto30' => 'Do you want to know more?',
     'texto31' => 'Send',
     'texto32' => 'Name and surname',
     'texto33' => 'Email',
     'texto34' => 'Message',
];
