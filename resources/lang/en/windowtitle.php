<?php

return [

    //about
    'about' => 'About Titularice',
    'accionistas' => 'Shareholders',
    'gobierno' => 'Corporate Governance',
    'directiva' => 'Board of Directors',
    'organizacion' => 'Organizational Structure',
    'normatividad' => 'Regulation',
    'info-financiera' => 'Financial Information',
    'riesgos' => 'Risks',
    'inversionistas' => 'Investor Relations',

    //edu financiera
    'edu-financiera' => 'Financial Education',
    'emisiones' => 'Issuances',

    //contacto
    'contacto' => 'Contáct Us',

    //footer
    'enlaces' => 'Links of interest',
    'informacion' => 'Information',
    'hechos' => 'Facts Information',
    'datospersonales' => 'Personal Data Policies',
    'faq' => 'FAQ',
    'tyc' => 'Terms and Conditions',

];
