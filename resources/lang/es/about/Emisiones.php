<?php

return [

    //nav bar
    'nav-txt1' => 'Flujos futuros',
    'nav-txt2' => 'Cartera de libranza',
    'nav-txt3' => 'Cartera de consumo y comercial',
    'nav-txt4' => 'Cartera de vehículos',
    'nav-txt5' => 'Emisiones canceladas',

    //modal
    'modal-txt1' =>
    'Conforme con lo establecido en los artículos 5.2.3.1.4, 5.2.3.1.5 y 7.2.1.1.2 del Decreto 2555 de 2010, al momento de acceder a esta información usted declara que es un inversionista calificado o que está interesado en consultar la información asociada a las emisiones de títulos con fines de información general.',
    'modal-txt2' => 'Continuar'
];
