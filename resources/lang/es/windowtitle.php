<?php

return [

    //about
    'about' => 'Acerca de Titularice',
    'accionistas' => 'Accionistas',
    'gobierno' => 'Gobierno Corporativo',
    'directiva' => 'Junta Directiva',
    'organizacion' => 'Estructura Organizativa',
    'normatividad' => 'Normatividad',
    'info-financiera' => 'Información Financiera',
    'riesgos' => 'Riesgos',
    'inversionistas' => 'Relación con Inversionistas',

    //edu financiera
    'edu-financiera' => 'Educación Financiera',
    'emisiones' => 'Emisiones',

    //contacto
    'contacto' => 'Contáctanos',

    //footer
    'enlaces' => 'Enlaces de interés',
    'informacion' => 'Información',
    'hechos' => 'Información de Hechos',
    'datospersonales' => 'Políticas de Datos Personales',
    'faq' => 'Preguntas Frecuentes',
    'tyc' => 'Términos y Condiciones',

];