<?php

return [

    'txt1' => 'Contáctanos',
    'txt2' => 'Nuestros expertos están para servirte',
    'txt3' => 'Dirección',
    'txt4' => 'Correo electrónico',
    'txt5' => 'Teléfono',
    'txt6' => '¡Escríbenos y pronto nos comunicaremos contigo!'

];