<?php

return [
    'nuestra-titularizadora' => 'Nuestra titularizadora',
    'accionistas' => 'Accionistas',
    'gobierno-corporativo' => 'Gobierno corporativo',
    'junta-directiva' => 'Junta directiva',
    'organizacion' => 'Organización',
    'normatividad' => 'Normatividad',
    'informacion-financiera' => 'Información financiera',
    'riesgos' => 'Riesgos',
    'relacion-con-inversionistas' => 'Relación con inversionistas',
    'buscar' => 'Buscar',
    'flujos-futuros' => 'Flujos futuros',
    'liquidiacion-emision' => 'Liquidación de la emisión',
    'informacion-emision' => 'Información de la emisión',
    'informacion-activo-titularizado' => 'Información del activo titularizado',
    'universalidad' => 'Universalidad',
    'titulos' => 'Títulos',
    'cartera-libranzas' => 'Cartera de libranzas',
    'cartera-consumo-comercial' => 'Cartera de consumo y comercial',
    'cartera-vehiculos' => 'Cartera de vehículos',
    'emisiones-canceladas' => 'Emisiones Canceladas',



];
