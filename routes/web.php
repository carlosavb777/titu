<?php

use Illuminate\Support\Facades\Route;


use App\Http\Controllers\Backend\Auth\LoginController;
use App\Http\Controllers\Backend\Dashboard\DashboardController;
use App\Http\Controllers\Backend\Zona\ZonaController;
use App\Http\Controllers\Ajax\AjaxController;
use App\Http\Controllers\Backend\Gastos\GastosController;
use App\Http\Controllers\Backend\Roles\RolesController;
use App\Http\Controllers\Backend\Usuarios\UsuariosController;
use App\Http\Controllers\Backend\Bitacora\BitacoraController;
use App\Http\Controllers\Backend\PreguntasFrecuentes\PreguntasFrecuentesController;
use App\Http\Controllers\Backend\Configuraciones\ConfiguracionesController;
use App\Http\Controllers\Backend\Encabezados\EncabezadosController;
use App\Http\Controllers\Backend\Accionistas\AccionistasController;
use App\Http\Controllers\Backend\Gobierno_corporativo\Gobierno_corporativoController;
use App\Http\Controllers\Backend\Categorias\CategoriasController;
use App\Http\Controllers\Backend\Emisiones\EmisionesController;
use App\Http\Controllers\Backend\InformacionFinancieraCategorias\InformacionFinancieraCategoriasController;
use App\Http\Controllers\Backend\InformesFinancieros\InformesFinancierosController;
use App\Http\Controllers\Backend\Normatividad\Categorias_normatividadController;
use App\Http\Controllers\Backend\Normatividad\CircularesController;
use App\Http\Controllers\Backend\Articulos\ArticulosController;
use App\Http\Controllers\Backend\Reportes\ReportesController;
use App\Http\Controllers\Backend\Reportes_pais\Reportes_paisController;
use App\Http\Controllers\Backend\Junta_directiva\Junta_directivaController;
use App\Http\Controllers\Backend\Equipo\EquipoController;
use App\Http\Controllers\Backend\Inversionistas\InversionistasController;
use App\Http\Controllers\Backend\Blog\BlogController;

use App\Http\Controllers\Frontend\Home\HomeController;
use App\Http\Controllers\Frontend\About\AboutController;
use App\Http\Controllers\Frontend\Contactenos\ContactenosController;
use App\Http\Controllers\Frontend\GobiernoCorporativo\GobiernoCorporativoController;
use App\Http\Controllers\Frontend\Footer\FooterController;
use App\Http\Controllers\Frontend\EducacionFinanciera\EducacionFinancieraController;
use App\Http\Controllers\Frontend\Organizacion\OrganizacionController;
use App\Http\Controllers\Frontend\JuntaDirectiva\JuntaDirectivaController;
use App\Http\Controllers\Backend\Ajax\AjaxController as BackendAjaxController;
use App\Http\Controllers\Backend\FileTree\FileTreeController;
use App\Http\Controllers\Frontend\EmisionesFrontend\EmisionesFrontendController;
use App\Http\Middleware\Authenticate;
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route::get('/', function () {
//     return view('welcome');
// });




// ********* F R O N T E N D  ********* //


Route::get('/', HomeController::class);

Route::get('nuestra-titularizadora', [AboutController::class, 'NuestraTitularizadora']);
Route::get('accionistas_', [AboutController::class, 'Accionistas']);
Route::get('gobierno-corporativo', [AboutController::class, 'GobiernoCorporativo']);
Route::get('junta-directiva', [JuntaDirectivaController::class, 'MostrarJuntaDirectiva']);
Route::get('normatividad', [AboutController::class, 'Normatividad']);
Route::get('informacion-financiera/{anio?}', [AboutController::class, 'InformacionFinanciera']);
Route::get('riesgos', [AboutController::class, 'Riesgos']);
Route::get('relacion-inversionistas', [AboutController::class, 'RelacionInversionistas']);
Route::get('contactenos', [ContactenosController::class, 'Contactenos']);
Route::post('enviar-correo', [ContactenosController::class, 'EnviarCorreo']);
Route::get('educacion-financiera', [EducacionFinancieraController::class, 'EducacionFinanciera']);
Route::get('articulo/{id}', [EducacionFinancieraController::class, 'Articulo']);

Route::get('que_es', [EducacionFinancieraController::class, 'Articulo'])->name('que_es');
Route::get('ventaja', [EducacionFinancieraController::class, 'Articulo'])->name('ventaja');
Route::get('organizacion', [OrganizacionController::class, 'MostrarOrganizacion']);


//Ruta email
Route::get('email',[ContactenosController::class,'EnviarCorreo']);
Route::get('lang/{lang}', ['as' => 'lang.switch', 'uses' => 'App\Http\Controllers\LanguageController@switchLang']);

///conector filetree///
Route::any('filetree-connector', [FileTreeController::class, 'Connector']);

Route::get('enlace-interes', [FooterController::class, 'EnlaceInteres']);
Route::get('informacion', [FooterController::class, 'Informacion']);
Route::get('terminos-condiciones', [FooterController::class, 'TerminosCondiciones']);
Route::get('politicas-datos-personales', [FooterController::class, 'PoliticasDatosPersonales']);
Route::get('preguntas-frecuentes', [FooterController::class, 'PreguntasFrecuentes']);
Route::get('informacion-Hechos', [FooterController::class, 'InformacionHechosRelevantes']);




// ********* B A C K E N D  ********* //

Route::get('admin', [LoginController::class, 'LoginVista'])->name("admin");
Route::post('login', [LoginController::class, 'Login']);
Route::post('logout', [LoginController::class, 'Logout']);
Route::get('get-preguntas-frecuentes/{leng}', [BackendAjaxController::class, 'get_preguntas_frecuentes']);

Route::middleware([Authenticate::class])->group(function () {

    Route::get('emisiones', [EmisionesFrontendController::class, 'Emisiones']);
    Route::get('detalle-emision/{id}', [EmisionesFrontendController::class, 'Emision']);

        Route::get('dashboard', [DashboardController::class, 'Dashboard']);


        Route::resource('zonas', ZonaController::class);
        Route::get('eliminar_zona/{id}', [ZonaController::class, 'destroy']);
        Route::get('filtros/{idd?}/{idm?}', [ZonaController::class, 'index']);

        Route::post('guardar_zona', [ZonaController::class, 'store']);

        Route::resource('gastos', GastosController::class);
        Route::get('eliminar_gasto/{id}', [GastosController::class, 'destroy']);

        Route::resource('roles', RolesController::class);
        Route::get('eliminar_rol/{id}', [RolesController::class, 'destroy']);
        Route::get('eliminar_permiso/{id}', [RolesController::class, 'eliminar_permiso']);
        Route::post('guardar_permiso', [RolesController::class, 'guardar_permiso']);


        Route::resource('usuarios', UsuariosController::class);
        Route::get('eliminar_usuario/{id}', [UsuariosController::class, 'destroy']);

        Route::resource('preguntas_frecuentes', PreguntasFrecuentesController::class);
        Route::get('eliminar_pregunta/{id}', [PreguntasFrecuentesController::class, 'destroy']);


        Route::resource('configuraciones', ConfiguracionesController::class);

        Route::resource('encabezados', EncabezadosController::class);

        Route::resource('accionistas', AccionistasController::class);
        Route::get('eliminar_accionista/{id}', [AccionistasController::class, 'destroy']);

        Route::resource('gobierno_corporativo', Gobierno_corporativoController::class);
        Route::get('eliminar_documento/{id}', [Gobierno_corporativoController::class, 'destroy']);

        Route::resource('categorias', CategoriasController::class);
        Route::get('eliminar_categoria/{id}', [CategoriasController::class, 'destroy']);

        Route::resource('informes_financieros_categorias', InformacionFinancieraCategoriasController::class);
        Route::get('eliminar_informes_financieros_categorias/{id}', [InformacionFinancieraCategoriasController::class, 'destroy']);

        Route::resource('informes_financieros', InformesFinancierosController::class);
        Route::get('eliminar_informes_financieros/{id}', [InformesFinancierosController::class, 'destroy']);

        Route::resource('emision', EmisionesController::class);
        Route::get('eliminar_emision/{id}', [EmisionesController::class, 'destroy']);
        Route::post('adjuntar_documentos', [EmisionesController::class, 'adjuntar_documentos']);
        Route::get('eliminar_documento/{id}', [EmisionesController::class, 'eliminar_documento']);

        Route::resource('normatividad_categorias', Categorias_normatividadController::class);
        Route::get('eliminar_normatividad_categorias/{id}', [Categorias_normatividadController::class, 'destroy']);

        Route::resource('circulares', CircularesController::class);
        Route::get('eliminar_circulares/{id}', [CircularesController::class, 'destroy']);

        Route::resource('articulos', ArticulosController::class);
        Route::get('eliminar_articulos/{id}', [ArticulosController::class, 'destroy']);

        Route::resource('informes_gobierno', ReportesController::class);
        Route::resource('encuesta_pais', Reportes_paisController::class);

        Route::resource('directiva', Junta_directivaController::class);
        Route::get('eliminar_directiva/{id}', [Junta_directivaController::class, 'destroy']);

        Route::resource('enlace', EquipoController::class);
        Route::get('eliminar_enlace/{id}', [EquipoController::class, 'destroy']);

        Route::resource('inversionistasback', InversionistasController::class);
        Route::get('eliminar_inversionistasback/{id}', [InversionistasController::class, 'destroy']);

        Route::resource('blog', BlogController::class);
        Route::get('eliminar_blog/{id}', [BlogController::class, 'destroy']);


        //AJAX
        Route::post('municipios', [AjaxController::class, 'municipios']);
        Route::post('obtener_categorias', [AjaxController::class, 'obtener_categorias']);
        Route::post('verificar_nickname', [AjaxController::class, 'verificar_nickname']);
        Route::post('autorizacion', [AjaxController::class, 'autorizacion']);
        Route::post('listar_deudas_clientes', [AjaxController::class, 'listar_deudas_clientes']);


        Route::post('info_box_ruta', [AjaxController::class, 'info_box_ruta']);


        Route::get('home', [DashboardController::class, 'Dashboard']);

        Route::get('bitacora', [BitacoraController::class, 'index']);
        Route::get('bitacora/show/{id}', [BitacoraController::class, 'show']);
        Route::post('filtrar_bitacoras', [BitacoraController::class, 'index']);
        Route::get('accesos', [BitacoraController::class, 'accesos']);
        Route::post('filtrar_accesos', [BitacoraController::class, 'accesos']);


        Route::get('logs', '\Rap2hpoutre\LaravelLogViewer\LogViewerController@index');
});
